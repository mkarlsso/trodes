# Qmake defaults for trodesnetwork
#
# The purpose of this file is to be included in any `.pro` file that needs to
# interact with the trodesnetwork library.
#
# Because the transition between the legacy library and the new library is
# still underway, we have plenty of work to do.



HEADERS += \
    $$PWD/trodesnetwork/include/TrodesNetwork/Generated/ConsoleCommand.h \
    $$PWD/trodesnetwork/include/TrodesNetwork/Generated/ConsoleOutput.h \
    $$PWD/trodesnetwork/include/TrodesNetwork/Generated/ModuleStatus.h \
    $$TRODES_REPO_DIR/Trodes/src-network/AbstractModuleClient.h \
    $$TRODES_REPO_DIR/Trodes/src-network/CZHelp.h \
    $$TRODES_REPO_DIR/Trodes/src-network/highfreqclasses.h \
    $$TRODES_REPO_DIR/Trodes/src-network/networkDataTypes.h \
    $$TRODES_REPO_DIR/Trodes/src-network/networkincludes.h \
    $$TRODES_REPO_DIR/Trodes/src-network/trodesglobaltypes.h \
    $$TRODES_REPO_DIR/Trodes/src-network/trodesmsg.h

SOURCES += \
    $$TRODES_REPO_DIR/Trodes/src-network/AbstractModuleClient.cpp \
    $$TRODES_REPO_DIR/Trodes/src-network/CZHelp.cpp \
    $$TRODES_REPO_DIR/Trodes/src-network/highfreqclasses.cpp \
    $$TRODES_REPO_DIR/Trodes/src-network/networkDataTypes.cpp \
    $$TRODES_REPO_DIR/Trodes/src-network/networkincludes.cpp \
    $$TRODES_REPO_DIR/Trodes/src-network/trodesglobaltypes.cpp \
    $$TRODES_REPO_DIR/Trodes/src-network/trodesmsg.cpp

# the trodesnetwork library

HEADERS += \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/Modules/SimulatedSourceConfig.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/Modules/SimulatedSource.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/Modules/SimulateSpikes.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/Modules/SpikeGeneratorConfig.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/Modules/SpikeGenerator.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/Modules/TimestampedSample.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Connection.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/AcquisitionCommand.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/AnnotationRequest.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/DataCommand.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/ConsoleCommand.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/ConsoleOutput.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/Event.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/Handshake.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HardwareRequest.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HWClear.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HWGlobalStimulationCommand.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HWGlobalStimulationSettings.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HWStartStop.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HWStimulationCommand.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/HWTrigger.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/InfoRequest.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/InfoResponse.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/ITime.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/ITimerate.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/ServerRequestSimple.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/SourceStatus.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/SpikePacket.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesAnalogData.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesDigitalData.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesEvent.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesLFPData.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesNeuralData.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesNotification.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesRequest.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesSpikeWaveformData.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/TrodesTimestampData.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Generated/VideoPacket.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/RawServiceConsumer.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/RawServiceProvider.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/RawSinkPublisher.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/RawSinkSubscriber.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/RawSourcePublisher.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/RawSourceSubscriber.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/ServiceConsumer.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/ServiceProvider.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/SinkPublisher.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/SinkSubscriber.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/SourcePublisher.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Resources/SourceSubscriber.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Server.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/ServerState.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/TrodesNetwork/Util.h \
    $$TRODES_REPO_DIR/trodesnetwork/include/Trodes/TimestampUtil.h

SOURCES += \
    $$TRODES_REPO_DIR/trodesnetwork/src/Connection.cpp \
    $$TRODES_REPO_DIR/trodesnetwork/src/Server.cpp \
    $$TRODES_REPO_DIR/trodesnetwork/src/ServerState.cpp \
    $$TRODES_REPO_DIR/trodesnetwork/src/TimestampUtil.cpp \
    $$TRODES_REPO_DIR/trodesnetwork/src/Util.cpp

## system include paths for headers

INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/src-network

# trodesnetwork
INCLUDEPATH += \
    $$TRODES_REPO_DIR/extern/cppzmq \
    $$TRODES_REPO_DIR/extern/libzmq/include \
    $$TRODES_REPO_DIR/extern/msgpack-c/include \
    $$TRODES_REPO_DIR/trodesnetwork/include \

# legacy TrodesNetwork: cereal, czmq
INCLUDEPATH += \
    $$TRODES_REPO_DIR/Libraries/Linux/TrodesNetwork/include \
    $$TRODES_REPO_DIR/extern/zmq/include/
 
## platform specific library includes

unix:!macx: {
    # trodesnetwork
    # LIBS += -L$$TRODES_REPO_DIR/Libraries/Linux/trodesnetwork/lib -ltrodesnetwork
    message(-L$$TRODES_REPO_DIR/Libraries/Linux/trodesnetwork/lib -ltrodesnetwork)

    # legacy TrodesNetwork: libraries
    # libzmq is kept
    #LIBS += -L../../Libraries/Linux/TrodesNetwork/lib -lTrodesNetwork
    LIBS += -Wl,--whole-archive $$TRODES_REPO_DIR/extern/zmq/lib/static/libzmq.a -Wl,--no-whole-archive
    LIBS += -Wl,--whole-archive $$TRODES_REPO_DIR/extern/zmq/lib/static/libczmq.a -Wl,--no-whole-archive
    LIBS += -Wl,--whole-archive $$TRODES_REPO_DIR/extern/zmq/lib/static/libmlm.a -Wl,--no-whole-archive

    QMAKE_CXXFLAGS += -std=c++17
}

win32 {
    LIBS += $$TRODES_REPO_DIR/extern/zmq/msvc64/ZeroMQ/lib/libzmq-v140-mt-4_3_1.lib
    LIBS += $$TRODES_REPO_DIR/extern/zmq/msvc64/czmq/lib/czmq.lib
    LIBS += $$TRODES_REPO_DIR/extern/zmq/msvc64/malamute/lib/mlm.lib

    libraries.files += \
        $$TRODES_REPO_DIR/extern/zmq/msvc64/czmq.dll \
        $$TRODES_REPO_DIR/extern/zmq/msvc64/libmlm.dll \
        $$TRODES_REPO_DIR/extern/zmq/msvc64/libsodium.dll \
        $$TRODES_REPO_DIR/extern/zmq/msvc64/ZeroMQ/bin/libzmq-v140-mt-4_3_1.dll

    INSTALLS += libraries

    QMAKE_CXXFLAGS += /std:c++17
}



macx {
    #LIBS += -Wl,--whole-archive $$TRODES_REPO_DIR/extern/zmq/macos/libzmq/lib/libzmq.a -Wl,--no-whole-archive
    #LIBS += -Wl,--whole-archive $$TRODES_REPO_DIR/extern/zmq/macos/czmq/lib/libczmq.a -Wl,--no-whole-archive
    #LIBS += -Wl,--whole-archive $$TRODES_REPO_DIR/extern/zmq/macos/malamute/lib/libmlm.a -Wl,--no-whole-archive

    LIBS += -Wl,$$TRODES_REPO_DIR/extern/zmq/macos/libzmq/lib/libzmq.a -Wl
    LIBS += -Wl,$$TRODES_REPO_DIR/extern/zmq/macos/czmq/lib/libczmq.a -Wl
    LIBS += -Wl,$$TRODES_REPO_DIR/extern/zmq/macos/malamute/lib/libmlm.a -Wl

    #LIBS += -L$$TRODES_REPO_DIR/extern/zmq/macos/libzmq/lib -llibzmq
    #LIBS += -L$$TRODES_REPO_DIR/extern/zmq/macos/czmq/lib -llibczmq
    #LIBS += -L$$TRODES_REPO_DIR/extern/zmq/macos/malamute/lib -llibmlm


    QMAKE_CXXFLAGS += -std=c++17
}


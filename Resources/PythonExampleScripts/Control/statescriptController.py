
# import necessary modules
# do `pip install trodesnetwork` to get this package
import trodesnetwork
from trodesnetwork import trodes
from trodesnetwork import socket
import threading
import math
import time


#Create the statescript client
sscClient = trodesnetwork.StateScriptClient("tcp://127.0.0.1:49152")


#Start the thread that monitors incoming messages. You only need to do this if you want to monitor the messages. 
#Commands will work without it
sscClient.open()

#send a statescript command
sscClient.send('version;\n') 


#consoleClient.send('blahblah') #this command is a syntax error and will create an error message
time.sleep(.25) #Give some time for the commands to process

#Print the available console messages, separated by three channels. 
#Each of these commands copies the messages and clears the original message buffer
o = sscClient.getOutputs() #Output channel
e = sscClient.getErrors() #Error channel
d = sscClient.getDebugs() #Debug channel (verbose output)
s = sscClient.getStateScriptMessages() #StateScript output channel

#Print the messages
print("\nStateScript:\n")
for message in s:
    message.printMessage()
    print("\n")
print("\nOutputs:\n")
for message in o:
    message.printMessage()
    print("\n")
print("\nErrors:\n")
for message in e:
    message.printMessage()
    print("\n")
print("\nDebug statements:\n")
for message in d:
    message.printMessage()
    print("\n")

#this is needed to shut down the client's listener thread if you did open() 
sscClient.close() 

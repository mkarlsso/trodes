
#Example of how to control trodescore using console commands 
#and receive all console outputs, divided into threee separate channels.
#It relies on the use of trodesnetwork.ConsoleCommandClient

# import necessary modules
# do `pip install trodesnetwork` to get this package
# you will also need to pip install zmq and msgpack
import trodesnetwork
import time

#Create the console command client
consoleClient = trodesnetwork.ConsoleCommandClient('tcp://127.0.0.1:49152')

#Start the thread that monitors incoming messages. You only need to do this if you want to monitor the messages. 
#Commands will work without it
consoleClient.open()

#Send some commands
consoleClient.send('version') #this command will print version info to the output channel
consoleClient.send('blahblah') #this command is a syntax error and will create an error message
time.sleep(.25) #Give some time for the commands to process

#Print the available console messages, separated by three channels. 
#Each of these commands copies the messages and clears the original message buffer
o = consoleClient.getOutputs() #Output channel
e = consoleClient.getErrors() #Error channel
d = consoleClient.getDebugs() #Debug channel (verbose output)

#Print the messages
print("\nOutputs:\n")
for message in o:
    message.printMessage()
    print("\n")
print("\nErrors:\n")
for message in e:
    message.printMessage()
    print("\n")
print("\nDebug statements:\n")
for message in d:
    message.printMessage()
    print("\n")

#this is needed to shut down the client's listener thread if you did open() 
consoleClient.close() 




    
   
   
    

    

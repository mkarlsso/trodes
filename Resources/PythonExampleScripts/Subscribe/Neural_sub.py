# import necessary modules
# do `pip install trodesnetwork` to get this package
import trodesnetwork
from trodesnetwork import trodes
from trodesnetwork import socket
import threading
import math



# os.nice(-19) #for linux only-- increases process priority to max


hardware = trodes.TrodesHardware(server_address="tcp://127.0.0.1:49152")
dataClient = trodesnetwork.DataClient("tcp://127.0.0.1:49152")
dataClient.enable('neural') #needed to turn on neural stream


#mode = 'lfp'
#mode = 'spikes'
#mode = 'digital'
#mode = 'waveforms'
mode = 'neural'


numTrials = 5
counter = 0
pulseCounter = 0

lock = 0


def input_event_process_thread():
    
    global counter
    global pulseCounter
    global lock
    input_event = socket.SourceSubscriber('source.'+ mode, server_address="tcp://127.0.0.1:49152")
    lastTrialTime = 0
    newTime = 0
    oldTime = 0
    
    newDValue = 0

    while True:

        s = input_event.receive()
        stimestamp = s['localTimestamp']
        log = 0
                   
        if (mode == 'neural'):
            data = s['neuralData']
            #ntrode_id = s['nTrodeId']
            #newDValue = ntrode_id
            
                
        
        newTime = math.floor((stimestamp/30000)%60)
        if (newTime != oldTime):
            print("Time:",newTime,"value:",data[0])
            counter = counter+1
        oldTime = newTime
        
        



input_event_thread = threading.Thread(target=input_event_process_thread)  # Insert into a thread
print("input_event thread started")
input_event_thread.start()

input_event_thread.join()


# time.sleep(1)
# stop_diothread = True
# dio_thread.join()
# print("Thread stopped")

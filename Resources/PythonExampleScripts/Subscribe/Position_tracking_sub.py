# import necessary modules
# do `pip install trodesnetwork` to get this package
from trodesnetwork import trodes
from trodesnetwork import socket
import threading

hardware = trodes.TrodesHardware(server_address="tcp://127.0.0.1:49152")
mode = 'position'


def input_event_process_thread():
       
    input_event = socket.SourceSubscriber('source.'+ mode, server_address="tcp://127.0.0.1:49152")   
    while True:

        s = input_event.receive()
        timestamp = s['timestamp']
        xpos = s['x']
        ypos = s['y']
        x2pos = s['x2']
        y2pos = s['y2']
        pixelsPerCM = s['pixelsPerCM']
        
        
        print("Time:", timestamp, "X1:", xpos, "Y1:", ypos, "PixelsPerCM", pixelsPerCM)
        
        
        
       
        
       



input_event_thread = threading.Thread(target=input_event_process_thread)  # Insert into a thread
print("input_event thread started")
input_event_thread.start()
input_event_thread.join()






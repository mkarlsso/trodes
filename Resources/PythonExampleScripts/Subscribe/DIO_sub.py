# import necessary modules
# do `pip install trodesnetwork` to get this package
from trodesnetwork import trodes
from trodesnetwork import socket
import trodesnetwork
import threading
import numpy as np

# os.nice(-19) #for linux only-- increases process priority to max



hardware = trodes.TrodesHardware(server_address="tcp://127.0.0.1:49152")
dataClient = trodesnetwork.DataClient("tcp://127.0.0.1:49152")
dataClient.enable('digital') #needed to turn on neural stream

#mode = 'lfp'
#mode = 'spikes'
mode = 'digital'
#mode = 'waveforms'

input_eventrecord = np.array([])
pulserecord = np.array([])

mcu_byte = 1  # location of the MCU data
ecu_in_byte = 2  # start location of the ECU digital input data
ecu_out_byte = 6  # start location of the ECU digital output data

#ecu_in_byte = 4  # start location of the ECU digital input data
#ecu_out_byte = 8  # start location of the ECU digital output data

# desired_device = ecu_in_byte  # choose the device from the above list
desired_device = ecu_out_byte  # choose the device from the above list
desired_device_channel = 0  # this is 0-based, so '0' is the first channel in the device

numTrials = 101
counter = 0
pulseCounter = 0

lock = 0


def input_event_process_thread():
    global input_eventrecord
    global counter
    global pulseCounter
    global lock
    input_event = socket.SourceSubscriber('source.'+ mode, server_address="tcp://127.0.0.1:49152")
    lastTrialTime = 0
    oldDValue = 0
    newDValue = 0

    while counter < numTrials:
        
        s = input_event.receive()
        stimestamp = s['localTimestamp']
        log = 0
        if (mode == 'digital'):
            byte_data = bytearray(s['digitalData'][0])
            for x in range(ecu_in_byte,ecu_in_byte+4):
                for y in range(0,8):
                    channel_data = (byte_data[x] >> y) & 1  # Here we isolate just the desired bit
                    if (channel_data == 1):
                        print('Input channel high: Byte ' + str(x) +' bit '+ str(y) + ' Time: ' + str(stimestamp))
            for x in range(ecu_out_byte,ecu_out_byte+1):
                for y in range(0,8):
                    channel_data = (byte_data[x] >> y) & 1  # Here we isolate just the desired bit
                    if (channel_data == 1):
                        print('Output channel high: Byte '+ str(x) + ' bit '+ str(y) + ' Time: ' + str(stimestamp))           
            	



# stop_diothread = False
input_event_thread = threading.Thread(target=input_event_process_thread)  # Insert into a thread
print("input_event thread started")
input_event_thread.start()

input_event_thread.join()


# time.sleep(1)
# stop_diothread = True
# dio_thread.join()
# print("Thread stopped")

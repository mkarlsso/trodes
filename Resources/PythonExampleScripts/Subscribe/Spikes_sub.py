# import necessary modules
# do `pip install trodesnetwork` to get this package
from trodesnetwork import trodes
from trodesnetwork import socket
import threading
import math



# os.nice(-19) #for linux only-- increases process priority to max


hardware = trodes.TrodesHardware(server_address="tcp://127.0.0.1:49152")
#mode = 'lfp'
mode = 'spikes'
#mode = 'digital'
#mode = 'waveforms'


numTrials = 1000
counter = 0
pulseCounter = 0

lock = 0


def input_event_process_thread():
    
    global counter
    global pulseCounter
    global lock
    input_event = socket.SourceSubscriber('source.'+ mode, server_address="tcp://127.0.0.1:49152")
    lastTrialTime = 0
    newTime = 0
    oldTime = 0
    
    newDValue = 0

    while counter < numTrials:

        s = input_event.receive()
        stimestamp = s['localTimestamp']
        computerTimestamp = s['systemTimestamp']
        log = 0
                   
        if (mode == 'spikes' or mode == 'waveforms'):
            cluster = s['cluster']
            ntrode_id = s['nTrodeId']
            newDValue = ntrode_id
            
                
        print("NTrode: ", newDValue, "Cluster: ", cluster, "Hardware timestamp: ", stimestamp, "System time: ", computerTimestamp)
        #newTime = math.floor((stimestamp/30000)%60)
        #if (newTime != oldTime):
        #    print(newTime)
        #oldTime = newTime
        
        counter = counter+1



input_event_thread = threading.Thread(target=input_event_process_thread)  # Insert into a thread
print("input_event thread started")
input_event_thread.start()

input_event_thread.join()


# time.sleep(1)
# stop_diothread = True
# dio_thread.join()
# print("Thread stopped")

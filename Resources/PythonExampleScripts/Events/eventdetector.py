
# Example of how to detect events
# import necessary modules
# do `pip install trodesnetwork` to get this package
# you will also need to pip install zmq and msgpack

from trodesnetwork import trodes
from trodesnetwork import events
from trodesnetwork import socket
from trodesnetwork.socket import SinkSubscriber, SourcePublisher, SinkPublisher, SourceSubscriber
import time


#Create the event subscriber
ev_sub = SourceSubscriber('trodes.event', server_address="tcp://127.0.0.1:49152")

while True:
    
    #Receive all new events. 
    res = ev_sub.receive()
    
    #localTimestamp is the sample number of the data stream
    #systemTimestamp is the linux time in ns
    print(res['name'], "at hardware timestamp", res['localTimestamp'], "and at system time", res['systemTimestamp'])
    
    
        
    

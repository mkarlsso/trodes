
# example: create a specific event
# import necessary modules
# do `pip install trodesnetwork` to get this package
# you will also need to pip install zmq and msgpack

from trodesnetwork import trodes
from trodesnetwork import events
from trodesnetwork import socket
from trodesnetwork.socket import SinkSubscriber, SourcePublisher, SinkPublisher, SourceSubscriber
import time

info = trodes.TrodesInfoRequester()
ev_pub = SinkPublisher('trodes.event.inbox', server_address="tcp://127.0.0.1:49152")
            

while True:
    time.sleep(.25)
    #locatTimestamp is the sample number of the data stream
    #systemTimestamp is the linux time in ns
    utctime = time.time_ns()
    ev_pub.publish({'name': 'something happened', 'localTimestamp': info.request_time(), 'systemTimestamp': utctime})
        
    
        
    

#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    setDefaultMenuEnabled(false); //the default options are turned off
    requireRecFile = false;
    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {


    printf("\nUsed to create a Rec file from SD file recording.  Output file is saved is same directory by default.\n");
    printf("Usage:  sdtorec -sd SDINPUTFILENAME -output OUTPUTFILENAME -mergeconf FILEWORKSPACE\n");
    printf("Optional arguments: -output OUTPUTFILENAME -outputdirectory OUTPUTDIRECTORYNAME\n\n");

    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;

    QString sd_string = "";
    QString merge_workspace_string = "";
    QString numchan_string = "";

    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }  else if ((argumentList.at(optionInd).compare("-mergeconf",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            merge_workspace_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-sd",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            sd_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-numchan",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            //Ignore this input now
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }

    if (merge_workspace_string.isEmpty()) {
        qDebug() << "No merge workspace filename given.  Must include -mergeconf <filename> in arguments.";
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        mergedConfFileName = merge_workspace_string;

        QFileInfo fi(mergedConfFileName);

        if (!fi.exists()) {
            qDebug() << "File could not be found: " << mergedConfFileName;
            argumentReadOk = false;
        }

    }

    if (sd_string.isEmpty()) {
        qDebug() << "Error. Must include -sd SDINPUTFILENAME in arguments.";
        argumentReadOk = false;
        return;
    }  else {
        //Make sure the first file exists
        sdFileName = sd_string;

        QFileInfo fi(sdFileName);

        if (!fi.exists()) {
            qDebug() << "File could not be found: " << sdFileName;
            argumentReadOk = false;
            return;
        }

    }

    sdFilePacketSize = scanSDFileForPacketSize();
    if (sdFilePacketSize == -1) {
        qDebug() << "Error: could not detect packet size in file.";
        return;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "\nCreating rec file from SD file...";


    TrodesConfiguration mergeConfigData;
    QString errorString = mergeConfigData.readTrodesConfig(mergedConfFileName);
    if (!errorString.isEmpty()) {
        qDebug() << "Error: " << errorString;
        return -1;
    }
    filePacketSize = mergeConfigData.getPacketSize();
    if (sdFilePacketSize != filePacketSize) {
        printf("Error. The packet size scanned in the file (%d) does not match the packet size of the provided workspace (%d).\n",sdFilePacketSize,filePacketSize);
        return -1;
    }

    if (!openSDInputFile()) {
        return -1;
    }




    QFileInfo fi(sdFileName);
    QString fileBaseName;

    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName()+"_fromSD";
    } else {
        fileBaseName = outputFileName.endsWith(".rec") ? outputFileName.replace(".rec", "") : outputFileName;
    }

    QString saveLocation;
    if (outputDirectory.isEmpty()) {
        saveLocation = fi.absolutePath()+QString(QDir::separator());
    } else {
        saveLocation = outputDirectory;
    }



    QList<QFile*> neuroFilePtrs;



    //Create an output file for the merged data
    //*****************************************

    neuroFilePtrs.push_back(new QFile);
    neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".rec"));
    if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
    neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    QFile finalConfFile;



    finalConfFile.setFileName(mergedConfFileName);
    if (!finalConfFile.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QString configLine;
    bool foundEndOfConfig = false;
    int endPos = 0;
    int lastFCPos = 0;

    while (!finalConfFile.atEnd()) {
        configLine += finalConfFile.readLine();


        //neuroFilePtrs.last()->write()  write(configLine);
        //configContent += configLine;
        if (configLine.indexOf("</Configuration>") > -1) {
            foundEndOfConfig = true;
            endPos = finalConfFile.pos();
            //endPos = lastFCPos + configLine.indexOf("</Configuration>") + 16;
            break;
        } else {
            lastFCPos = finalConfFile.pos();
        }
        configLine = "";
    }

    if (!foundEndOfConfig) {
        qDebug() << "Error in reading mergeconf file.";
        return -1;
    }
    finalConfFile.seek(0);
    neuroFilePtrs.last()->write(finalConfFile.read(endPos+1));


    //*neuroStreamPtrs.last() << configLine;

    finalConfFile.close();

    neuroFilePtrs.last()->flush();
    //neuroFilePtrs.last()->close();


    //************************************************

    //int inputFileInd = 0;

    progressMarker = sdFilePtr->size()/100;
    currentProgressMarker = 0;
    lastProgressMod = 100000;
    if (progressMarker == 0) {
        progressMarker = 1;
    }





    //Process the data and stream results to output files
    while(!sdFilePtr->atEnd()) {

        if (sdFilePtr->size()-sdFilePtr->pos() < sdFilePacketSize) {
            //qDebug() << "Could not read a full packet from file. Closing file.";
            break;
        }
        //readNextRecPacket();
        if (!readNextSDPacket()) {
            qDebug() << "Error: could not read from file. Closing file.";
            break;
        }

        //neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data(),10); //sync byte, extra aux byte, sensor data (8 bytes)


        //neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data(),1); //sync byte
        //neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+3,1); //aux byte moved from 4th byte to second
        //neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+2,8); //sensor data (with repeat aux byte)


        //*neuroStreamPtrs.at(0) << currentSDTimeStamp; //Write the timestamp from the trodes file
        //Write the neural data from the sd card
        neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data(),sdFilePacketSize);

        if ((sdFilePtr->pos()%progressMarker) < lastProgressMod) {

            printf("\n%d%%\n\n",currentProgressMarker);
            fflush(stdout);

            /*if (currentProgressMarker > 0) {
                qDebug() << currentProgressMarker << "%";
            }*/
            currentProgressMarker = 100.0*((double)sdFilePtr->pos()/(double)sdFilePtr->size());
            //currentProgressMarker+=10;
        }
        lastProgressMod = (sdFilePtr->pos()%progressMarker);
        //printProgress();
        //pointsSinceLastLog = (pointsSinceLastLog+1)%decimation;







        //currentSDPacket++;
        //lastTimeStampInFile = currentTimeStamp;
        //lastSDTimeStamp = currentSDTimeStamp;




    }


    sdFilePtr->close();

    printf("\rClosing output file...\n");
    neuroFilePtrs.last()->flush();
    neuroFilePtrs.last()->close();



    printf("\rDone\n");

    return 0;
}

int  DATExportHandler::scanSDFileForPacketSize()
{
    //This function is used to calculate the number of neural channels in the SD recording
    //if the user does not specify it.

    QFile sdFile;
    sdFile.setFileName(sdFileName);

    //open the raw data file
    if (!sdFile.open(QIODevice::ReadOnly)) {
        printf("Error: could not open sd recording file for reading.\n");
        return -1;
    }

    char syncByteCheck;
    int byteScan = 0;
    int syncIntervals[400]; //this is the number of sync bytes we are going to scan through
    int syncIntervalIndex = 0;


    //Read one byte at a time, check if it is a sync byte (0x55). If so, assume that
    //a new packet is starting (not always true). Record the number of bytes
    //in the last packet. Do this multiple times to see if the same number comes up a lot.
    while ((!sdFile.atEnd()) && (syncIntervalIndex < 400)) {
        bool doubleFound = false;
        bool atSync = false;
        qint64 currentpos = sdFile.pos();
        sdFile.read(&syncByteCheck,1);
        if ((syncByteCheck == 0x55) && (byteScan>0)) { //78 is the smallest possible packet size with 32 channels
            //qDebug() << "Sync at" << byteScan;
            atSync = true;
            if (sdFile.seek(currentpos+byteScan)) {
                sdFile.read(&syncByteCheck,1);
                if (syncByteCheck == 0x55) {
                    //qDebug() << "Packet size double" << byteScan;
                    doubleFound = true;
                }
                sdFile.seek(currentpos+1);
            }
        }
        if (doubleFound) {
            sdFile.seek(currentpos+byteScan+1);
            //qDebug() << "Packet size" << byteScan;
            syncIntervals[syncIntervalIndex] = byteScan;
            syncIntervalIndex++;
            byteScan=1;
        } else if (atSync) {
            byteScan = 1;
        } else {
            byteScan++;
        }

    }

    sdFile.close();

    //Find all the unique packet sizes, and create a tally for each
    int uniqueSizesFound = 0;
    int packetSizes[400];
    int tallies[400];
    int maxValue = 0;
    int maxValueTally = 0;
    for (int i=0; i<400; i++) {
        packetSizes[i]=0;
        tallies[i]=0;
    }
    for (int i=0; i<400; i++) {
      int tempSize = syncIntervals[i];
      int found = -1;
      for (int j=0; j<uniqueSizesFound; j++) {
           if (packetSizes[j]==tempSize) {
                found = j;
                tallies[j]++;
                break;
           }
      }
      if (found == -1) {
            packetSizes[uniqueSizesFound] = tempSize;
            tallies[uniqueSizesFound] = 1;
            uniqueSizesFound++;
      }

      if (tempSize > maxValue) {
          maxValue = tempSize;
          maxValueTally = 1;
      } else if (tempSize == maxValue) {
          maxValueTally++;
      }
    }

    //find the packet size that has the highest tally
    int maxTally = 0;
    int maxIndex = 0;



    for (int i=0; i<400;i++) {
        //qDebug() << tallies[i];
        if ((tallies[i] > 10) && (tallies[i] > maxTally) ) {
            //We have a hit.
            maxTally = tallies[i];
            maxIndex = i;
        }

    }

    int packetSize_calc = -1;

    //We need to subtract the size of the head section that does
    //not contain neural channels.
    //int headerSectionSize = 14; //The default header size
    //If the data came from neuropixels, the header section also contains LFP data
    /*if (mergeConfigData.spikeConf.deviceType == "neuropixels1") {
        int numberOfProbes = mergeConfigData.hardwareConf.NCHAN/384;
        headerSectionSize += ((numberOfProbes*64)+2); //This is the number of bytes that the LFP section adds.
    }*/


    if (maxValueTally > 1) {
        //The largest packet size found happended more than once. We go with that
        packetSize_calc = maxValue;
        return packetSize_calc;
    } else if (maxTally > 0) {
        //The max value was not repeated, so we can't trust it. So we use whatever value happened a lot (more than 10)

        packetSize_calc = packetSizes[maxIndex];
        return packetSize_calc;
    } else {

        //If we get here, no tally reached the required threshold.
        return -1;
    }

}



bool DATExportHandler::readNextSDPacket() {


    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        //We have reached the end of the sd file
        return false;
    }


    //Find the time stamp from sd file
    //char *sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    //uint32_t *sdTimePtr = (uint32_t *)(sdBufferPtr);
    //currentSDTimeStamp = *sdTimePtr;

    return true;

}


bool DATExportHandler::openSDInputFile() {
    sdFilePtr = new QFile;
    sdFilePtr->setFileName(sdFileName);

    //open the raw data file
    if (!sdFilePtr->open(QIODevice::ReadOnly)) {
        delete sdFilePtr;
        qDebug() << "Error: could not open sd recording file for reading.";
        return false;
    }

    sdFilePacketBuffer.resize(sdFilePacketSize*2); //we make the input buffer the size of two packets
    //dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    //dataLastTimePoint.fill(0);
    //pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    /*
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }*/

//variables not used
//    char *sdBufferPtr;
//    uint32_t *sdTimePtr;


    //Read in a packet of data to make sure everything looks good
    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        delete sdFilePtr;
        qDebug() << "Error: could not read from SD card recording file";
        return false;
    }

    sdFilePtr->seek(0);

    /*
    qDebug() << "Finding sync packets in SD file...";

    //Seek back to begining of data
    sdFilePtr->seek(0);

    uint32_t currentSDPacket = 0;
    //Find all of the sync stamps


    uint32_t ltime=0;
    uint32_t thistime=0;

    while (!sdFilePtr->atEnd()) {
        //Seek to flag location of data packet
        if (!sdFilePtr->seek((currentSDPacket*sdFilePacketSize))) {
            break;
        }
        if (sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketStartDataLoc) == sdFilePacketStartDataLoc) {

            //Report dropped packets

            sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
            thistime = *sdTimePtr;
            if (thistime < ltime) {
                qDebug() << "-" << ltime-thistime << "gap in data. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }
            else if (thistime-ltime > 1) {
                qDebug() << thistime-ltime << "gap in data at time. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }
            ltime = thistime;


            sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketFlagByteLoc;

            if (*sdFilePacketBuffer.data() != 0x55) {
                qDebug() << "Bad SD file packet found.";
            }
            if (*sdBufferPtr == 1) {
                //The 0th bit is set in the flag byte, we have a sync value




                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketSyncByteLoc);
                uint32_t sVal = *sdTimePtr;
                sVal = sVal & 0xFFFFFF;
                //qDebug() << "SD sync: " << sVal;

                sdSyncValues.push_back(sVal);


                if (sdSyncValues.length() > 1) {
                    if (!(sdSyncValues.last() > sdSyncValues[sdSyncValues.length()-2])) {
                        qDebug() << "Error: sync values in SD file are not increasing.";
                        return false;
                    }
                }

                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
                sdTimeDuringSync.push_back(*sdTimePtr);
                sdPacketDuringSync.push_back(currentSDPacket);
            }

        }
        currentSDPacket++;
        //sdFilePtr->

    }
    qDebug() << sdSyncValues.length() << "sync values found on SD file.";
    //qDebug() << sdPacketDuringSync;


    currentSDTimeStamp = 0;
    lastSDTimeStamp = 0;

    //Find first time stamp
    //sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    //sdTimePtr = (uint32_t *)(sdBufferPtr);
    //currentSDTimeStamp = *sdTimePtr;
    //lastSDTimeStamp = currentSDTimeStamp-1;

    //Seek back to begining of data
    sdFilePtr->seek(0);

    */


    return true;

}


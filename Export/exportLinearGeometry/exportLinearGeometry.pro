include(../export_defaults.pri)

TARGET = exportLinearGeometry


SOURCES     +=  main.cpp \
                txtexporthandler.cpp \
		../../Modules/cameraModule/src/geometry.cpp
HEADERS     +=  txtexporthandler.h \
                ../../Modules/cameraModule/src/geometry.h
INCLUDEPATH +=  ../../Modules/cameraModule/src \
                ../exportLinearGeometry

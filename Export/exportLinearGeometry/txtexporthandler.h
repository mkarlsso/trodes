#ifndef TXTEXPORTHANDLER_H
#define TXTEXPORTHANDLER_H
#include <QStringList>

class TXTExportHandler
{

public:
    TXTExportHandler(const QStringList& arguments);
    ~TXTExportHandler();

    int processData();

    void parseArguments(const QStringList& arguments);
    void printHelpMenu();

    bool fileNameSpecified();
    bool fileNameFound();
    bool argumentsSupported();
    bool wasHelpMenuDisplayed();


private:
    QString filename;
    QString directory;
    QString basename;
    bool fileNameSpecifiedFlag;
    bool fileNameFoundFlag;
    bool argumentsSupportedFlag;
    bool wasHelpMenuDisplayedFlag;

};

#endif

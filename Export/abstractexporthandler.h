#ifndef ABSTRACTEXPORTHANDLER_H
#define ABSTRACTEXPORTHANDLER_H

#include <QObject>
#include <QtCore>
#include "configuration.h"
#include "iirFilter.h"
#include "spikeDetectorThread.h"
#include "streamprocesshandlers.h"
#include "vectorizedneuraldatahandler.h"

//Global access to the loaded configuration info and the network object
/*extern GlobalConfiguration *globalConf;
//extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;
extern HardwareConfiguration *hardwareConf;
extern BenchmarkConfig *benchConfig;*/


struct nTrodeInfo {
    bool nTrodeRefOn;
    bool filterOn;
    bool lfpFilterOn;
    bool lfpRefOn;
    bool rawRefOn;
    int nTrodeRefNtrode;
    int nTrodeRefNtrodeChannel;
    int nTrodeLowPassFilter;
    int nTrodeHighPassFilter;
    int numChannels;
    int lfpChannel;
};

class AbstractExportHandler: public QObject
{

    Q_OBJECT

public:
    AbstractExportHandler(QStringList arguments);
    bool fileNameFound();
    bool argumentsOK();
    bool argumentsSupported();
    bool fileConfigOK();
    bool wasHelpMenuDisplayed();
    void displayVersionInfo();
    virtual int processData() = 0; //To be defined in inheriting class


    ~AbstractExportHandler();

    //The exported data falls into different catagories, each with specific flags available
    enum DataCategories {
      SpikeBand         = 0x01,
      LfpBand           = 0x02,
      RawBand           = 0x04,
      ContinuousOutput  = 0x08,
      DigitalIO         = 0x10,
      AnalogIO          = 0x20,
      SpikeEvents       = 0x40
    };

protected:
    virtual void printHelpMenu();
    virtual void parseArguments();
    virtual void createFilters();


    int readConfig();
    int readNextConfig(QString configName);
    void calculateChannelInfo();
    //void calculateChannelInfo_New();
    void calculateCARDataInPacket(int16_t* startOfNeuralData);
    bool openInputFile();
    void printProgress();
    int findEndOfConfigSection(QString fileName);
    void setDefaultMenuEnabled(bool enabled);
    void writeDefaultHeaderInfo(QFile*);
    bool getNextPacket();

    uint16_t supportedDataCategories;
    bool avxsupported;

    AbstractNeuralDataHandler* neuralDataHandler;
    QList<int> nTrodeIndexList;  //list of all nTrodes indices to process
    QString configString;
    TrodesConfiguration embeddedWorkspace;
    TrodesConfiguration externalWorkspace;
    TrodesConfiguration* loadedWorkspacePtr; //pointer to the loaded workspace (embedded or external).
    TrodesConfigurationPointers conf_ptrs;

    GlobalConfiguration *globalConf;
    HardwareConfiguration *hardwareConf;
    headerDisplayConfiguration *headerConf;
    streamConfiguration *streamConf;
    SpikeConfiguration *spikeConf;
    ModuleConfiguration *moduleConf;
    BenchmarkConfig *benchConfig;

    //virtual void printCustomMenu() = 0; //To be defined in inheriting class
    //virtual void parseCustomArguments(int &argumentsProcessed) = 0; //To be defined in inheriting class


    QStringList argumentList;
    QString recFileName;
    QStringList recFileNameList;
    QString externalWorkspaceFile;
    QString outputFileName;
    QString outputDirectory;

    QList<int> sortedDeviceList;

    int filterLowPass_SpikeBand;
    int filterHighPass_SpikeBand;
    int filterLowPass_LFPBand;
    int64_t maxGapSizeInterpolation;

    bool override_Spike_Refs;
    bool override_LFPRefs;
    bool override_RAWRefs;
    bool override_useSpikeFilters;
    bool override_useLFPFilters;
    bool override_useNotchFilters;

    bool useRefs;
    bool useLFPRefs;
    bool useRAWRefs;
    bool useSpikeFilters;
    bool useLFPFilters;
    bool useNotchFilters;

    bool invertSpikes;
    bool threshOverride;
    int threshOverrideVal;
    //int threshOverrideVal_rangeConvert;


    bool defaultMenuEnabled;
    int argumentsProcessed;
    bool argumentReadOk;
    bool _argumentsSupported;
    bool _fileNameFound;
    bool _configReadOk;
    bool helpMenuPrinted;
    int dataStartLocBytes;
    int decimation;
    int outputSamplingRate;
    int paddingBytes;
    bool enableAVX;

    bool abortOnBackwardsTimestamp;


    bool requireRecFile;


    int numChannelsInFile;
    int packetHeaderSize;
    int packetTimeLocation;
    int filePacketSize;
    uint64_t startOffsetTime;

    uint64_t packetsRead;


    QList<int> nTrodeForChannels; //contains theuser-specified ID number for the nTrode
    QList<int> nTrodeIndForChannels; //contains the 0-based index of the nTrode;
    QList<int> channelInNTrode;
    QList<int> channelPacketLocations;
    QList<bool> refOn;
    QList<bool> lfpRefOn;
    QList<bool> groupRefOn;
    QList<int> refGroup;
    QList<int> refPacketLocations;

    //Buffer, variables for data reading
    //----------------------------------
    QVector<char> buffer;
    QVector<char> currentPacket;
    QVector<char> lastReadPacket;
    char* bufferPtr;
    uint32_t* tPtr;
    uint32_t currentTimeStamp;
    QVector<int16_t> CARData;
    uint32_t nextTimeStampInFile;
    uint32_t lastTimeStampInFile;
    int64_t systemTimeAtCreation;
    uint32_t timestampAtCreation;
    bool firstRecProcessed;
    int16_t* tempDataPtr;
    int16_t tempDataPoint;
    int16_t tempFilteredDataPoint;
    int16_t tempRefPoint;
    QVector<int16_t> dataLastTimePoint;
    int pointsSinceLastLog;
    QList<BesselFilter*> channelFilters;
    QVector<bool> filterOn;
    QVector<int> lowPassFilters;
    QVector<int> highPassFilters;

    QVector<bool> lfpFilterOn;
    QVector<int> lfpLowPassFilters;

    //For nTrode-based organization
    QList <ThresholdSpikeDetector *> spikeDetectors;
    QList <nTrodeInfo> nTrodeSettings;

    //Pointers to the neuro files
    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;

    qint64 progressMarker;
    int currentProgressMarker;
    int lastProgressMod;

    //Input file
    QFile* filePtr;

};

#endif // ABSTRACTEXPORTHANDLER_H

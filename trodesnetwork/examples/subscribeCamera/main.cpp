
#include <TrodesNetwork/Server.h>
#include <TrodesNetwork/Resources/SourceSubscriber.h>

#include <TrodesNetwork/Generated/VideoPacket.h>

#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

int main(int argc, char** argv) {
    if (argc) {}
    if (argv) {}

    trodes::network::Server server;

    std::thread listener([]() {
        trodes::network::SourceSubscriber<trodes::network::VideoPacket>
            timestamps{"source.position"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "P: ";
            result << recvd.timestamp << " ";
            result << recvd.lineSegment << " ";
            result << recvd.posOnSegment << " ";
            result << recvd.x << " ";
            result << recvd.y << " ";
            result << "\n";

            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    listener.join();

    return 0;
}

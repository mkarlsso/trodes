#include <TrodesNetwork/Generated/Handshake.h>
#include <TrodesNetwork/Generated/ServerRequestSimple.h>
#include <TrodesNetwork/Resources/RawServiceConsumer.h>
#include <TrodesNetwork/Resources/RawSourceSubscriber.h>
#include <TrodesNetwork/Connection.h>

#include <chrono>
#include <functional>
#include <iostream>
#include <thread>

namespace trodes {
namespace network {

    Connection::Connection(std::string address, int port):
        conn_ok(false)
    {
        auto connection_string = util::get_connection_string(address, port);


        RawSourceSubscriber<Handshake> sub(connection_string);
        if (!sub.connection_ok()) {
            conn_ok = false;
            return;
        }

        Handshake handshake = sub.receive();
        service_endpoint = handshake.replyEndpoint;
        logger_endpoint = handshake.mailEndpoint;
        conn_ok = true;

    }

    Connection::~Connection() {
    }

    bool Connection::connection_ok() {
        return conn_ok;
    }
    std::string Connection::get_service_endpoint() const {
        return service_endpoint;
    }

    std::string Connection::get_logger_endpoint() const {
        return logger_endpoint;
    }

    void Connection::add_endpoint(std::string name, std::string endpoint) {
        RawServiceConsumer<ServerRequestSimple, std::string> query(service_endpoint);

        ServerRequestSimple request = {"add", name, endpoint};
        query.request(request);
    }

    std::string Connection::get_endpoint(std::string name) {
        RawServiceConsumer<ServerRequestSimple, std::string> query(service_endpoint);

        ServerRequestSimple request = {"get", name, ""};
        auto endpoint = query.request(request);
        
        return endpoint;
    }


}
}

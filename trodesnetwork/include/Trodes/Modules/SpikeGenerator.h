#pragma once

#include <Trodes/Modules/SpikeGeneratorConfig.h>
#include <cmath>
#include <cstdint>
#include <iostream>

namespace trodes {

class SpikeGenerator {
public:
    SpikeGenerator(SpikeGeneratorConfig config)
        : config(config)
    {
    }

    ~SpikeGenerator() {
    }

    int16_t getSample(uint64_t timestamp_ns) {
        const double cyclePos = (double)timestamp_ns / (double) 10e9 * config.waveFrequency;
        const int cycleIndex = (int)((int64_t)(cyclePos * config.waveRes) % config.waveRes);
        const double wavePosition = (double) cycleIndex / (double) config.waveRes;
        const double unitAmplitude = std::sin(2 * M_PI * wavePosition);
        return (int16_t)(unitAmplitude * config.waveAmplitude);
    }

private:
    SpikeGeneratorConfig config;
};

}
#pragma once

#include <TrodesNetwork/Generated/Handshake.h>
#include <TrodesNetwork/Util.h>
#include <TrodesNetwork/Generated/Handshake.h>
#include <TrodesNetwork/Generated/ServerRequestSimple.h>
#include <TrodesNetwork/Resources/RawServiceConsumer.h>
#include <TrodesNetwork/Resources/RawSourceSubscriber.h>
#include <TrodesNetwork/Connection.h>

#include <thread>
#include <zmq.hpp>

namespace trodes {
namespace network {

class Connection {
public:
    Connection(std::string address, int port);
    ~Connection();

    std::string get_service_endpoint() const;
    std::string get_logger_endpoint() const;

    void add_endpoint(std::string name, std::string endpoint);
    std::string get_endpoint(std::string name);
    bool connection_ok();

    static bool network_exists(std::string address, int port) {
        //Used to check if another trodes network is already active on the same address
        auto connection_string = util::get_connection_string(address, port);
        std::string wildcard_address = address + ":*";

        //zmq::context_t ctx_sub(1);
        //zmq::socket_t socket_sub(ctx_sub, zmq::socket_type::sub);
        //zmq::context_t ctx_req(1);
        //zmq::socket_t socket_req(ctx_req, zmq::socket_type::req);


        zmq::context_t ctx_pub(1);
        zmq::socket_t socket_pub(ctx_pub, zmq::socket_type::pub);

        try {
            socket_pub.bind(connection_string.c_str()); //Will throw an exception if an other raw publisher already exists on this address
            socket_pub.send(zmq::message_t("Hello"), zmq::send_flags::dontwait);
            socket_pub.disconnect(connection_string.c_str());

        }  catch (...) {
            //Another network appears to be active already.
            return true;
        }

        return false;
    };

private:
    std::string service_endpoint;
    std::string logger_endpoint;
    bool conn_ok;
};


}
}

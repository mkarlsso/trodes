#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawServiceProvider.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename A, typename B>
class ServiceProvider {
public:
    ServiceProvider(std::string address, int port, std::string name)
        : name(name),
          service(util::get_wildcard_string(address))
    {
        Connection c(address, port);
        c.add_endpoint(name, service.last_endpoint());
    }

    ~ServiceProvider() {
    }

    void handle(std::function<B(A)> f) {
        return service.handle(f);
    }

private:
    std::string name;
    RawServiceProvider<A, B> service;
};


}
}
#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawSourcePublisher.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename T>
class SourcePublisher {
public:
    SourcePublisher(std::string address, int port, std::string name)
        : name(name),
          pub(util::get_wildcard_string(address)),
          conn_ok(false)
    {

        Connection c(address, port);
        if (!c.connection_ok()) {
            conn_ok = false;
            return;
        }

        c.add_endpoint(name, pub.last_endpoint());        
        conn_ok = true;

    }

    ~SourcePublisher() {
    }

    bool connection_ok() {
        return conn_ok;
    }

    void publish(const T &data) {
        return pub.publish(data);
    }

private:
    std::string name;
    RawSourcePublisher<T> pub;
    bool conn_ok;
};


}
}

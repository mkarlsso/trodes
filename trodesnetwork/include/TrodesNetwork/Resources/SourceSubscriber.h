#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawSourceSubscriber.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename T>
class SourceSubscriber {
public:
    SourceSubscriber(std::string address, int port, std::string name)
        : name(name),
        endpoint(util::get_endpoint_retry_forever(address, port, name)),
        sub(endpoint)
    {
    }

    ~SourceSubscriber() {
    }

    T receive() {
        return sub.receive();
    }

private:
    std::string name;
    std::string endpoint;
    RawSourceSubscriber<T> sub;
};

}
}
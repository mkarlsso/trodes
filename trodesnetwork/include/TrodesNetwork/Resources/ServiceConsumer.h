#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawServiceConsumer.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename A, typename B>
class ServiceConsumer {
public:
    ServiceConsumer(std::string address, int port, std::string name)
        : name(name),
        endpoint(util::get_endpoint_retry_forever(address, port, name)),
        consumer(endpoint)
    {
    }

    ~ServiceConsumer() {
    }

    B request(A request) {
        return consumer.request(request);
    }

private:
    std::string name;
    std::string endpoint;
    RawServiceConsumer<A, B> consumer;
};


}
}

#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor DataCommand
struct ConsoleCommand {
    // field impl command
    std::string command;
    MSGPACK_DEFINE_MAP(command);
};


}  // network
}  // trodes

#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor HWGlobalStimulationSettings
struct HWGlobalStimulationSettings {
    // field impl scaleValue
    std::string scaleValue;
    MSGPACK_DEFINE_MAP(scaleValue);
};


}  // network
}  // trodes
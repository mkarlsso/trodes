#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesSpikeWaveformData
struct TrodesSpikeWaveformData {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl nTrodeId
    uint32_t nTrodeId;
    // field impl samples
    std::vector< std::vector< int16_t > > samples;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, nTrodeId, samples, systemTimestamp);
};


}  // network
}  // trodes
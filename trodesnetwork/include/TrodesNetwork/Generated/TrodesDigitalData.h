#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesDigitalData
struct TrodesDigitalData {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl digitalData
    std::vector< std::vector< uint8_t > > digitalData;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, digitalData, systemTimestamp);
};


}  // network
}  // trodes
#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor Event
struct ModuleStatus {
    // field impl name
    std::string moduleName;
    std::string moduleStatus;
    int16_t moduleInstance;
    MSGPACK_DEFINE_MAP(moduleName,moduleStatus,moduleInstance);
};


}  // network
}  // trodes

#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesEvent
struct TrodesEvent {
    // field impl name
    std::string name;
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(name, localTimestamp, systemTimestamp);
};


}  // network
}  // trodes
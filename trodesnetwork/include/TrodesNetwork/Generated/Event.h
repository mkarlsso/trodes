#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor Event
struct Event {
    // field impl name
    std::string name;
    MSGPACK_DEFINE_MAP(name);
};


}  // network
}  // trodes
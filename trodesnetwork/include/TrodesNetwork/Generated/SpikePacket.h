#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor SpikePacket
struct SpikePacket {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl nTrodeId
    int32_t nTrodeId;
    // field impl cluster
    int32_t cluster;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, nTrodeId, cluster, systemTimestamp);
};


}  // network
}  // trodes
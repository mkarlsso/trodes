#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor ConsoleOutput
struct ConsoleOutput {
    // field impl message
    std::string message;
    // field impl channel
    std::string channel;
    MSGPACK_DEFINE_MAP(message, channel);
};


}  // network
}  // trodes

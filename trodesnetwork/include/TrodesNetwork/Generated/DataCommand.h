#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor DataCommand
struct DataCommand {
    // field impl command
    std::string command;
    // field impl streamname
    std::string streamname;
    MSGPACK_DEFINE_MAP(command, streamname);
};


}  // network
}  // trodes
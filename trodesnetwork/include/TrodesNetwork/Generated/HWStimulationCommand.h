#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor HWStimulationCommand
struct HWStimulationCommand {
    // field impl _group
    uint8_t _group;
    // field impl slot
    uint8_t slot;
    // field impl cathodeChannel
    uint16_t cathodeChannel;
    // field impl anodeChannel
    uint16_t anodeChannel;
    // field impl cathodeNtrodeID
    uint16_t cathodeNtrodeID;
    // field impl anodeNtrodeID
    uint16_t anodeNtrodeID;
    // field impl leadingPulseWidth_Samples
    uint16_t leadingPulseWidth_Samples;
    // field impl leadingPulseAmplitude
    uint8_t leadingPulseAmplitude;
    // field impl secondPulseWidth_Samples
    uint16_t secondPulseWidth_Samples;
    // field impl secondPulseAmplitude
    uint8_t secondPulseAmplitude;
    // field impl interPhaseDwell_Samples
    uint16_t interPhaseDwell_Samples;
    // field impl pulsePeriod_Samples
    uint16_t pulsePeriod_Samples;
    // field impl startDelay_Samples
    uint16_t startDelay_Samples;
    // field impl numPulsesInTrain
    uint16_t numPulsesInTrain;
    MSGPACK_DEFINE_MAP(_group, slot, cathodeChannel, anodeChannel, cathodeNtrodeID, anodeNtrodeID, leadingPulseWidth_Samples, leadingPulseAmplitude, secondPulseWidth_Samples, secondPulseAmplitude, interPhaseDwell_Samples, pulsePeriod_Samples, startDelay_Samples, numPulsesInTrain);
};


}  // network
}  // trodes
#-------------------------------------------------
#
# Project created by QtCreator 2017-10-12T09:59:11
#
#-------------------------------------------------
include(../module_defaults.pri)

QT       += core gui xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleModule
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
        $$REPO_LIBRARY_DIR/qtmoduleclient.cpp \
        $$REPO_LIBRARY_DIR/qtmoduledebug.cpp

HEADERS  += mainwindow.h \
#        ../../TrodesNetwork/trodesnetwork.h\
#         ../../Trodes/src-main/trodesSocketDefines.h \ #delete this when done debugging
         $$REPO_LIBRARY_DIR/qtmoduleclient.h \
         $$REPO_LIBRARY_DIR/qtmoduledebug.h

FORMS    += mainwindow.ui
INCLUDEPATH += $$PWD/../../Trodes/src-config
INCLUDEPATH += $$PWD/../../Trodes/src-network
#INCLUDEPATH += $$PWD/../../Trodes/src-main
INCLUDEPATH += $$PWD/../../Libraries/Utility
INCLUDEPATH += $$REPO_LIBRARY_DIR/
UI_DIR = $$PWD

unix:!macx{
LIBS += -L$$OUT_PWD/../../Libraries/Linux/TrodesNetwork/lib -lTrodesNetwork
INCLUDEPATH += $$PWD/../../Libraries/Linux/TrodesNetwork/include
}


win32{
LIBS += -L$$PWD/../../Libraries/Windows/TrodesNetwork/lib -lTrodesNetwork
INCLUDEPATH += $$PWD/../../Libraries/Windows/TrodesNetwork/include
DEPENDPATH += $$PWD/../../Libraries/Windows/TrodesNetwork/include
}


include(../module_defaults.pri)

QT       += opengl core gui network xml multimedia widgets openglwidgets

DEFINES += AVT_GIGE
#DEFINES += VIMBA

TARGET = cameraModule
TEMPLATE = app

# Use Qt Resource System for images
RESOURCES += \
    $$TRODES_REPO_DIR/Resources/Images/buttons.qrc

INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
INCLUDEPATH  += ../../Trodes/src-network
INCLUDEPATH  += ../../Trodes/src-display
INCLUDEPATH  += ./src
INCLUDEPATH  += $$REPO_LIBRARY_DIR
INCLUDEPATH  += $$REPO_LIBRARY_DIR/Utility

# trodesnetwork: includes, libs
include(../../trodesnetwork_defaults.pri)

#########################################
#Source files and libraries
#########################################
SOURCES += src/main.cpp\
        src/mainwindow.cpp \
        src/videoDisplay.cpp \
        src/videoDecoder.cpp \
        src/videoEncoder.cpp \
        src/dialogs.cpp \
        src/abstractCamera.cpp \
        src/webcamWrapper.cpp \
        src/CameraModuleClient.cpp \
	src/geometry.cpp \
        ../../Trodes/src-main/trodesSocket.cpp \
        ../../Trodes/src-config/configuration.cpp \
        ../../Trodes/src-main/trodesdatastructures.cpp \
        ../../Trodes/src-main/eventHandler.cpp \
        ../../Trodes/src-display/sharedtrodesstyles.cpp \
        ../../Libraries/qtmoduleclient.cpp \
        ../../Libraries/qtmoduledebug.cpp \

HEADERS  += src/mainwindow.h \
        src/videoDisplay.h \
        src/videoDecoder.h \
        src/videoEncoder.h \
        src/dialogs.h \
        src/abstractCamera.h \
        src/webcamWrapper.h \
        src/CameraModuleClient.h \
        ../../Trodes/src-main/trodesSocket.h \
        ../../Trodes/src-config/configuration.h \
        ../../Trodes/src-main/trodesdatastructures.h \
        ../../Trodes/src-main/eventHandler.h \
        ../../Trodes/src-display/sharedtrodesstyles.h \
        ../../Libraries/qtmoduleclient.h \
        ../../Libraries/qtmoduledebug.h


#RESOURCES += ./src/pyCameraModule.py


DISTFILES += \
    src/pythonPath.txt

unix:!macx: {
    # FFMPEG libraries
    INCLUDEPATH += ../cameraModule/libraries/linux/x264
    LIBS += -L"../cameraModule/libraries/linux/x264" -lavcodec -lavformat -lswscale -lavutil -lx264 -lavresample

}

win32 {
#    INCLUDEPATH +=  $$TRODES_REPO_DIR/Libraries/Windows \
#                    $$TRODES_REPO_DIR/Libraries \
#                    $$TRODES_REPO_DIR/Libraries/Utility \
#                    $$TRODES_REPO_DIR/Libraries/Windows/TrodesNetwork/include

#    LIBS += -L$$TRODES_REPO_DIR/Libraries/Windows/TrodesNetwork/lib -lTrodesNetwork

    RC_ICONS += ./src/trodesWindowsIcon_camera.ico

    win32-g++ { # MinGW
        LIBS += -L$$quote($$PWD/Libraries/Windows/FTDI/i386) -lftd2xx
        LIBS += -L../Libraries/Windows/TrodesNetwork/lib -lTrodesNetwork

        # FFMPEG libraries
        INCLUDEPATH += ../cameraModule/libraries/win32
        INCLUDEPATH += ../cameraModule/libraries/win32/libx264
        LIBS += -L$$quote($${PWD}\libraries\win32) -lavutil -lavcodec -lavformat -lswscale -lx264-142

    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
        } else { # MSVC-64

            LIBS += -L$$REPO_LIBRARY_DIR/Windows64/lib -lTrodesNetwork
            INCLUDEPATH += $$REPO_LIBRARY_DIR/Windows64/include
#            DEPENDPATH += $$PWD/../../../../newlibinstallsmsvc/include

            # FFMPEG libraries
            INCLUDEPATH += $$PWD/libraries/win64/ffmpeg/include/
            LIBS += -L$$PWD/libraries/win64/ffmpeg/lib/ -lavutil -lavcodec -lavformat -lswscale

            # x264 library
            INCLUDEPATH += $$PWD/libraries/win64/x264/include
            LIBS += -L$$PWD/libraries/win64/x264/lib/ -lx264

        }
    }
}

macx {
    ICON        = $$PWD/src/trodesMacIcon_camera.icns
    QMAKE_INFO_PLIST = $$PWD/src/Info.plist

    # FFMPEG libraries
    INCLUDEPATH += $$PWD/libraries/osx/ffmpeg/include $$PWD/libraries/osx/x264/include
    LIBS += -L$${PWD}/libraries/osx/ffmpeg/lib -lavutil.55 -lavcodec.57 -lavformat.57 -lswscale.4
    LIBS += -L$${PWD}/libraries/osx/x264/lib -lx264
    # PvAPI
    INCLUDEPATH += $$PWD/libraries/osx/GigESDK/inc-pc
    LIBS += -L"$$PWD/libraries/osx/GigESDK/bin-pc/x64" -lPvAPI

    INCLUDEPATH += ../../Libraries/MacOS/include
    LIBS += -L../../Libraries/MacOS/lib -lTrodesNetwork
    QMAKE_POST_LINK += "cp $$TRODES_REPO_DIR/Libraries/MacOS/lib/libTrodesNetwork.*.dylib $$DESTDIR/$${TARGET}.app/Contents/MacOS/"
}

contains(DEFINES, AVT_GIGE) {
    #older PvAPI library for Allied Vision cameras
    HEADERS += src/avtWrapper.h
    SOURCES += src/avtWrapper.cpp
    unix:!macx: {
        # PvAPI library
        INCLUDEPATH += "$$PWD/libraries/linux/GigESDK/inc-pc"
        LIBS += -L"$$PWD/libraries/linux/GigESDK/bin-pc/x64" -lPvAPI
    }
    win32-g++ { # MinGW
        # PvAPI library
        INCLUDEPATH += "$$PWD/libraries/win32/GigESDK/inc-pc"
        LIBS += -L"$$PWD/libraries/win32/GigESDK/lib-pc" -lPvAPI
    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
        } else { # MSVC-64

            # PvAPI library
            INCLUDEPATH += "$$PWD/libraries/win64/GigESDK/inc-pc"
            LIBS += -L$$PWD/libraries/win64/GigESDK/lib-pc/x64 -lPvAPI
        }
    }
    macx {

        # PvAPI
        INCLUDEPATH += $$PWD/libraries/osx/GigESDK/inc-pc
        LIBS += -L"$$PWD/libraries/osx/GigESDK/bin-pc/x64" -lPvAPI

    }


}

contains(DEFINES, VIMBA) {
    #newer Vimba library
    HEADERS += src/vimbawrapper.h
    SOURCES += src/vimbawrapper.cpp

    unix:!macx: {

    INCLUDEPATH += "$$PWD/libraries/linux/Vimba/VimbaCPP/Include"
    INCLUDEPATH += "$$PWD/libraries/linux/Vimba/VimbaC/Include"
    INCLUDEPATH += "$$PWD/libraries/linux/Vimba"
    LIBS += -L"$$PWD/libraries/linux/Vimba" -lVimbaC
    LIBS += -L"$$PWD/libraries/linux/Vimba" -lVimbaCPP
    LIBS += -L"$$PWD/libraries/linux/Vimba" -lVimbaImageTransform

    HEADERS  += libraries/linux/Vimba/VimbaCPP/Include/*.h

    }
    win32-g++ { # MinGW

    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
        } else { # MSVC-64

            # Vimba library
            INCLUDEPATH += "$$PWD/libraries/win64/Vimba/VimbaCPP/Include"
            INCLUDEPATH += "$$PWD/libraries/win64/Vimba/VimbaC/Include"
            INCLUDEPATH += "$$PWD/libraries/win64/Vimba"

            LIBS += -L$$PWD/libraries/win64/Vimba -lVimbaC
            LIBS += -L$$PWD/libraries/win64/Vimba -lVimbaCpp
            LIBS += -L$$PWD/libraries/win64/Vimba -lVimbaImageTransform

            HEADERS  += libraries/win64/Vimba/VimbaCPP/Include/*.h
        }
    }

}



#########################################
#Install
#########################################

unix:!macx{

    #QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/Libraries\''
    #QMAKE_LFLAGS += '-Wl,-rpath-link,\'\$$ORIGIN/Libraries\''

    # libraries.path defined in build_defaults.pri
    libraries.files += \
        ./libraries/linux/x264/libavcodec.so.56*\
        ./libraries/linux/x264/libavformat.so.56*\
        ./libraries/linux/x264/libswscale.so.3*\
        ./libraries/linux/x264/libavutil.so.54*\
        ./libraries/linux/x264/libx264.so.148*\
        ./libraries/linux/x264/libavresample.so.2*\
        ./libraries/linux/GigESDK/bin-pc/x64/libPvAPI.so\
        ./libraries/linux/Vimba/libVimbaC.so\
        ./libraries/linux/Vimba/libVimbaCPP.so\
        ./libraries/linux/Vimba/libVimbaImageTransform.so\
        $$TRODES_REPO_DIR/PythonQt3.0/Python/lib/libpython3.5m.so.1.0\
        $$TRODES_REPO_DIR/PythonQt3.0/lib/libPythonQt.so.1\
        $$TRODES_REPO_DIR/PythonQt3.0/lib/libPythonQt_QtAll.so.1
#        ../../Libraries/Linux/TrodesNetwork/lib/libTrodesNetwork.so*
    INSTALLS += libraries

    cameramoduleicon.path = $$INSTALL_DIR/Resources/Images
    cameramoduleicon.files += $$PWD/src/cameramoduleIcon.png
    INSTALLS += cameramoduleicon

    QtDeploy.commands += cp -a $$TRODES_REPO_DIR/Libraries/Linux/TrodesNetwork/lib/libTrodesNetwork.so* $$INSTALL_DIR/Libraries/

}

win32{
    # libraries.path defined in build_defaults.pri
    win32-g++ { # MinGW
        libraries.files += \
            ./libraries/win32/avcodec-56.dll \
            ./libraries/win32/avformat-56.dll \
            ./libraries/win32/avutil-54.dll \
            ./libraries/win32/swscale-3.dll \
            ./libraries/win32/swresample-1.dll\
            ./libraries/win32/libx264-142.dll \
            ./libraries/win32/GigESDK/lib-pc/PvAPI.dll
        INSTALLS += libraries
    }
    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
        } else { # MSVC-64




            libraries.files += \
                ./libraries/win64/ffmpeg/bin/avcodec-58.dll \
                ./libraries/win64/ffmpeg/bin/avdevice-58.dll \
                ./libraries/win64/ffmpeg/bin/avfilter-7.dll \
                ./libraries/win64/ffmpeg/bin/avformat-58.dll \
                ./libraries/win64/ffmpeg/bin/avutil-56.dll \
                ./libraries/win64/ffmpeg/bin/swresample-3.dll \
                ./libraries/win64/ffmpeg/bin/swscale-5.dll \
                ./libraries/win64/GigESDK/bin-pc/x64/PvAPI.dll \
                ./libraries/win64/Vimba/VimbaC.dll \
                ./libraries/win64/Vimba/VimbaCPP.dll \
                ./libraries/win64/Vimba/VimbaImageTransform.dll

#                ./libraries/win64/x264/ # accidentally built static version of library, so no need for libx264.dll right now.
            INSTALLS += libraries
        }
    }

}

macx{
    libraries.files += \
        ./libraries/osx/ffmpeg/lib/libavcodec.57.dylib \
        ./libraries/osx/ffmpeg/lib/libavdevice.57.dylib \
        ./libraries/osx/ffmpeg/lib/libavfilter.6.dylib \
        ./libraries/osx/ffmpeg/lib/libavformat.57.dylib \
        ./libraries/osx/ffmpeg/lib/libavutil.55.dylib \
#        ./libraries/osx/ffmpeg/lib/libpostproc.54.dylib \
        ./libraries/osx/ffmpeg/lib/libswresample.2.dylib \
        ./libraries/osx/ffmpeg/lib/libswscale.4.dylib \
#        ./libraries/osx/ffmpeg/lib/libidn2.0.dylib \
        ./libraries/osx/x264/lib/libx264.dylib \
        ./libraries/osx/x264/lib/libx264.148.dylib \
        ./libraries/osx/GigESDK/bin-pc/x64/libPvAPI.dylib
#        ../../Libraries/MacOS/lib/libTrodesNetwork.*.dylib
    INSTALLS += libraries

    QtDeploy.commands += "install_name_tool -change @loader_path/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libavcodec.57.dylib @executable_path/../Frameworks/libavcodec.57.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libavformat.57.dylib @executable_path/../Frameworks/libavformat.57.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libswscale.4.dylib @executable_path/../Frameworks/libswscale.4.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libx264.148.dylib @executable_path/../Frameworks/libx264.148.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
    QtDeploy.commands += "install_name_tool -change @loader_path/libPvAPI.dylib @executable_path/../Frameworks/libPvAPI.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/cameraModule)" ;
#    QtDeploy.commands += "install_name_tool -change libTrodesNetwork.0.dylib @executable_path/../Frameworks/libTrodesNetwork.0.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/$${TARGET})" ;
    QtDeploy.commands += $$shell_path($$[QT_INSTALL_BINS]/macdeployqt) $$shell_path($$INSTALL_DIR/$${TARGET}.app) -libpath=$$TRODES_REPO_DIR/Libraries/MacOS/lib/  -libpath=$${PWD}/libraries/osx/ffmpeg/lib -libpath=${PWD}/libraries/osx/x264/lib -libpath=$$PWD/libraries/osx/GigESDK/bin-pc/x64 -always-overwrite ;

}





#include "CameraModuleClient.h"

#include "trodesglobaltypes.h"

CameraModuleClient::CameraModuleClient()
    : QtModuleClient(CAMERA_MODULE_NETWORK_ID, DEFAULT_SERVER_ADDRESS, DEFAULT_SERVER_PORT) {

    IDnum = 1;

}

CameraModuleClient::CameraModuleClient(QString address, int port)
    : QtModuleClient(CAMERA_MODULE_NETWORK_ID, address.toStdString().c_str(), port){

    IDnum = 1;

}

CameraModuleClient::~CameraModuleClient() {

}

std::string CameraModuleClient::getAddress() const {
    qDebug() << "getAddress is a no-op (returning blank).";
    return "";
}

int CameraModuleClient::getPort() const {
    qDebug() << "getPort is a no-op (returning zero)";
    return 0;
}

/*int CameraModuleClient::provideEvent(const char *event) {
    qDebug() << "provideEvent is a no-op (returning zero)";
    return 0;
}

int CameraModuleClient::unprovideEvent(const char *event) {
    qDebug() << "unprovideEvent is a no-op (returning zero).";
    return 0;
}*/

/*int CameraModuleClient::getID() const {

    return IDnum;
}

void CameraModuleClient::setID(int ID) {
    IDnum = ID;
}*/

bool CameraModuleClient::isInitialized() const {
    qDebug() << "isInitialized is a no-op (returning true)";
    return true;
}

int CameraModuleClient::registerHighFreqData(HighFreqDataType dataType) {
    qDebug() << "registerHighFreqData is a no-op (returning zero)";
    return 0;
}


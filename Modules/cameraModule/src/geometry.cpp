#include "geometry.h"

QDataStream & operator<< (QDataStream& stream, const LineNodeIndex& line){
    stream << line.line << line.startNodeIndex << line.endNodeIndex << line.zone;
    return stream;
}

QDataStream & operator>> (QDataStream& stream, LineNodeIndex& line){
    stream >> line.line >> line.startNodeIndex >> line.endNodeIndex >> line.zone;
    return stream;
}

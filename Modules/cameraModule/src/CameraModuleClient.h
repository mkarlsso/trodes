#ifndef CAMERAMODULECLIENT_H
#define CAMERAMODULECLIENT_H

#include <qtmoduleclient.h>
#include <cameramodulenetworkdefines.h>
#include <QDebug>

class CameraModuleClient : public QtModuleClient {
    Q_OBJECT
public:
    CameraModuleClient();
    CameraModuleClient(QString address, int port);
    ~CameraModuleClient();

    std::string getAddress() const; // from MlmWrap
    int getPort() const; // from MlmWrap
    //int provideEvent(const char *event); // from MlmWrap
    //int unprovideEvent(const char *event); // from MlmWrap
    //int getID() const; // from MlmWrap
    //void setID(int ID);
    bool isInitialized() const; // from MlmWrap
    int registerHighFreqData(HighFreqDataType dataType); // from MlmWrap

private:
    int IDnum;

};

#endif // CAMERAMODULECLIENT_H

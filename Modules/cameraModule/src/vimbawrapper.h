#ifndef VIMBAWRAPPER_H
#define VIMBAWRAPPER_H


#include <QtWidgets>
#include <QMutex>
#include "abstractCamera.h"
#include "VimbaCPP.h"
#include "VmbTransform.h"
#include <string>
#include <queue>


#ifdef __APPLE__
#define _OSX
#define _x64
#endif

#ifdef linux
#define _LINUX
#define _x64
#endif

#ifdef WIN32
#define _WINDOWS
#endif

#ifdef _WINDOWS
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#define PVDECL           __stdcall
//#include "StdAfx.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN
//#include <Winsock2.h>
//#include <Windows.h>
#endif

#if defined(_LINUX) || defined(_QNX) || defined(_OSX)
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#endif

//#include <PvApi.h>
//#include <ImageLib.h>

#ifdef _WINDOWS
#define STDCALL __stdcall
#else
#define STDCALL
//#define TRUE     0
#endif



#define MAX_CAMERA_LIST 20
// number of frames to be used
#define FRAMESCOUNT 20

#ifdef UNICODE
typedef std::wstring    string_type;
#define MAKE_STRING_LITERAL_(s) L ## s
#else
typedef std::string      string_type;
#define MAKE_STRING_LITERAL_(s) s
#endif
#define MAKE_STRING_LITERAL(s) MAKE_STRING_LITERAL_(s)

using namespace AVT;
using namespace VmbAPI;

// camera's data
typedef struct
{
    unsigned long   UID;
    //tPvHandle       Handle;
    //tPvFrame        Frames[FRAMESCOUNT];
    //tPvUint32       Counter;
    bool            Abort;
    unsigned long   Discarded; //Count of missing frames.
    char            Filename[20];

} vimCamera;

struct vimCameraInfo
{
    std::string cameraName;
    std::string id;
    std::string interfaceID;
    std::string modelName;
    std::string serialNumber;
    VmbAccessModeType accessType;


    //QString cameraName;
    //unsigned long ipAddress;
    //unsigned long id;
    //unsigned long permittedAccess;
    //QString firmware;
};

inline string_type ErrorCodeToMessage( VmbError_t eError )
{
    switch( eError )
    {
    case VmbErrorSuccess:           return string_type( MAKE_STRING_LITERAL( "Success." ) );
    case VmbErrorInternalFault:     return string_type( MAKE_STRING_LITERAL( "Unexpected fault in VmbApi or driver." ) );
    case VmbErrorApiNotStarted:     return string_type( MAKE_STRING_LITERAL( "API not started." ) );
    case VmbErrorNotFound:          return string_type( MAKE_STRING_LITERAL( "Not found." ) );
    case VmbErrorBadHandle:         return string_type( MAKE_STRING_LITERAL( "Invalid handle " ) );
    case VmbErrorDeviceNotOpen:     return string_type( MAKE_STRING_LITERAL( "Device not open." ) );
    case VmbErrorInvalidAccess:     return string_type( MAKE_STRING_LITERAL( "Invalid access." ) );
    case VmbErrorBadParameter:      return string_type( MAKE_STRING_LITERAL( "Bad parameter." ) );
    case VmbErrorStructSize:        return string_type( MAKE_STRING_LITERAL( "Wrong DLL version." ) );
    case VmbErrorMoreData:          return string_type( MAKE_STRING_LITERAL( "More data returned than memory provided." ) );
    case VmbErrorWrongType:         return string_type( MAKE_STRING_LITERAL( "Wrong type." ) );
    case VmbErrorInvalidValue:      return string_type( MAKE_STRING_LITERAL( "Invalid value." ) );
    case VmbErrorTimeout:           return string_type( MAKE_STRING_LITERAL( "Timeout." ) );
    case VmbErrorOther:             return string_type( MAKE_STRING_LITERAL( "TL error." ) );
    case VmbErrorResources:         return string_type( MAKE_STRING_LITERAL( "Resource not available." ) );
    case VmbErrorInvalidCall:       return string_type( MAKE_STRING_LITERAL( "Invalid call." ) );
    case VmbErrorNoTL:              return string_type( MAKE_STRING_LITERAL( "TL not loaded." ) );
    case VmbErrorNotImplemented:    return string_type( MAKE_STRING_LITERAL( "Not implemented." ) );
    case VmbErrorNotSupported:      return string_type( MAKE_STRING_LITERAL( "Not supported." ) );
    default:                        return string_type( MAKE_STRING_LITERAL( "Unknown" ) );
    }
}

class CameraObserver : public QObject, public ICameraListObserver
{
    Q_OBJECT

public:
    //
    // This is our callback routine that will be executed every time a camera was plugged in or out
    //
    // Parameters:
    //  [in]    pCam            The camera that triggered the callback
    //  [in]    reason          The reason why the callback was triggered
    //
    virtual void CameraListChanged( CameraPtr pCamera, UpdateTriggerType reason );

signals:
    //
    // The camera list changed event (Qt signal) that notifies about a camera change and its reason
    //
    // Parameters:
    //  [out]    reason          The reason why this event was fired
    //
    void CameraListChangedSignal( int reason );
};

class FrameObserver : public QObject, virtual public IFrameObserver
{
    Q_OBJECT

public:
    //
    // We pass the camera that will deliver the frames to the constructor
    //
    // Parameters:
    //  [in]    pCamera         The camera the frame was queued at
    //
    FrameObserver( CameraPtr pCamera ) : IFrameObserver( pCamera ) {;}

    //
    // This is our callback routine that will be executed on every received frame.
    // Triggered by the API.
    //
    // Parameters:
    //  [in]    pFrame          The frame returned from the API
    //
    virtual void FrameReceived( const FramePtr pFrame );

    //
    // After the view has been notified about a new frame it can pick it up.
    // It is then removed from the internal queue
    //
    // Returns:
    //  A shared pointer to the latest frame
    //
    FramePtr GetFrame();

    //
    // Clears the internal (double buffering) frame queue
    //
    void ClearFrameQueue();

private:
    // Since a Qt signal cannot contain a whole frame
    // the frame observer stores all FramePtr
    std::queue<FramePtr> m_Frames;
    QMutex m_FramesMutex;

signals:
    //
    // The frame received event (Qt signal) that notifies about a new incoming frame
    //
    // Parameters:
    //  [out]   status          The frame receive status
    //
    void FrameReceivedSignal( int status );
};


class VimbaCameraController : public AbstractCamera {

    Q_OBJECT

public:
    VimbaCameraController(bool ptpenabled=false);
    ~VimbaCameraController();

    QStringList availableCameras(); //Get a list of available device names
    bool checkIfInUse(int ID);


    //
    // Starts the Vimba API and loads all transport layers
    //
    // Returns:
    //  An API status code
    //
    VmbErrorType        StartUp();

    //
    // Shuts down the API
    //
    void                ShutDown();




    //
    // Gets the width of a frame
    //
    // Returns:
    //  The width as integer
    //
    int                 GetWidth() const;

    //
    // Gets the height of a frame
    //
    // Returns:
    //  The height as integer
    //
    int                 GetHeight() const;

    //
    // Gets the pixel format of a frame
    //
    // Returns:
    //  The pixel format as enum
    //
    VmbPixelFormatType  GetPixelFormat() const;

    //
    // Gets all cameras known to Vimba
    //
    // Returns:
    //  A vector of camera shared pointers
    //
    CameraPtrVector     GetCameraList();

    //
    // Gets the oldest frame that has not been picked up yet
    //
    // Returns:
    //  A frame shared pointer
    //
    FramePtr            GetFrame();

    //
    // Queues a given frame to be filled by the API
    //
    // Parameters:
    //  [in]    pFrame          The frame to queue
    //
    // Returns:
    //  An API status code
    //
    VmbErrorType        QueueFrame( FramePtr pFrame );

    //
    // Clears all remaining frames that have not been picked up
    //
    void                ClearFrameQueue();

    //
    // Returns the camera observer as QObject pointer to connect their signals to the view's slots
    //
    QObject*            GetCameraObserver();

    //
    // Returns the frame observer as QObject pointer to connect their signals to the view's slots
    //
    QObject*            GetFrameObserver();

    //
    // Translates Vimba error codes to readable error messages
    //
    // Parameters:
    //  [in]    eErr        The error code to be converted to string
    //
    // Returns:
    //  A descriptive string representation of the error code
    //
    std::string         ErrorCodeToMessage( VmbErrorType eErr ) const;


private:

    // A reference to our Vimba singleton
    VimbaSystem&                m_system;
    // The currently streaming camera
    CameraPtr                   m_pCamera;
    // Every camera has its own frame observer
    IFrameObserverPtr           m_pFrameObserver;
    // Our camera observer
    ICameraListObserverPtr      m_pCameraObserver;
    // The current pixel format
    VmbInt64_t                  m_nPixelFormat;
    // The current width
    VmbInt64_t                  m_nWidth;
    // The current height
    VmbInt64_t                  m_nHeight;

    vimCamera cameraInfo;
    bool isCameraOpen;
    bool isCameraAcquiring;
    bool initialized;
    bool isBlackAndWhite;
    bool bayerDemosaicNeeded;
    QTimer frameTriggerTimer;
    QVector<vimCameraInfo> detectedCameras;
    QVector<QRgb> colorTable;
    int frameIndex;
    QTimer blinkTimer;
    QTimer startBlinkTimer;

    int oldFrameCount;
    int currentCameraID;

    bool hasReset;

    bool ptpChecksEnabled;
    bool ptpCapableCamera;
    bool ptpInitialized;
    QTimer ptpChecker;

    vimCameraInfo getCameraInfo( const CameraPtr &camera );
    void updateCameraList();
    VmbErrorType copyToImage( VmbUchar_t *pInBuffer, VmbPixelFormat_t ePixelFormat, QImage* pOutImage);


public slots:
    bool open(int cameraID); //opens a camera using the index of the list from availableCameras()
    void close(); //closes the camera
    bool start(); //starts acquistion
    void stop(); //stops acquisition
    void blinkAcquire();
    void resetCurrentCamera();


private slots:
    //    void getFrame();
    //void getAvailableFrames();
    void onFrameReady( int status );
    void endBlink();
    void startBlink();

    void ResetPTP();
    void checkPTPStatus();

signals:
    void PTPFullyInitialized();
    void PTPInitializing();
    void PTPNoMessage();
    void PTPCalibrating();
    void PTPNotProperlySetup();
    void PTPWrongConfiguration();
    void PTPWarningMessage(QString);
};


#endif // VIMBAWRAPPER_H

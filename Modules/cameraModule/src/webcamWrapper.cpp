#include "webcamWrapper.h"



WebcamWrapper::WebcamWrapper()
    : cameraInput(NULL) {
    //surface = new WebcamDisplaySurface(this);
    numFramesRecieved = 0;

    //connect(surface,SIGNAL(newFrame(QImage*, uint32_t, uint64_t, bool, qint64)),this,SLOT(sendFrameSignals(QImage*,uint32_t,uint64_t,bool,qint64)));
    //connect(surface,SIGNAL(activeChanged(bool)),this,SLOT(cameraStateChanged(bool)));
    connect(&watchdogTimer,SIGNAL(timeout()),this,SLOT(watchdogFunction()));
    connect(&restartTimer,SIGNAL(timeout()),this,SLOT(restart_continued()));



    isCameraOpen = false;
    isCameraAcquiring = false;
    isCameraMalfunctioning = false;

}

WebcamWrapper::~WebcamWrapper() {
}

QStringList WebcamWrapper::availableCameras() {
    QStringList out;
    //QList<QCameraInfo> availCameras = QCameraInfo::availableCameras();
    availCameras = QMediaDevices::videoInputs();
    qDebug() << "Webcams:" << availCameras;
    for (int i = 0; i < availCameras.length(); i++) {
        out.append(availCameras[i].description() + " (" + availCameras[i].id() +")");


        /*qDebug() << "Camera " << availCameras[i].description();
        QList<QCameraFormat> f = availCameras[i].videoFormats();
        for (int j=0;j<f.length();j++) {
            qDebug() << "   Supported resolution:" << f[j].resolution() << " and frame rate:" << f[j].maxFrameRate();
        }*/

    }
    return out;
}

void WebcamWrapper::cameraErrorOccurred(QCamera::Error error, const QString &errorString) {

    if (!isCameraAcquiring) {
        //The error likely occured because the camera is in use by another program
        watchdogTimer.stop();
        //cameraInput->stop();
        isCameraAcquiring = false;
        numFramesRecieved = 0;
        cameraInput->deleteLater();
        camSink->deleteLater();
        captureSession->deleteLater();
        isCameraOpen = false;

    } else {

        qDebug() << "Error with camera:" << errorString;
    }

}

bool WebcamWrapper::checkIfInUse(int cameraID) {
    if (availCameras.length() <= cameraID) {
        return true;
    }

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("USBCamerasUsed"));
    int testifopen = settings.value(QLatin1String(availCameras[cameraID].id()), 0).toInt();
    settings.endGroup();

    qDebug() << "Checking if camera is in use:" << availCameras[cameraID].description() << testifopen;
    return (bool)testifopen;

}

bool WebcamWrapper::open(int cameraID) {

    /*void Camera::setCamera(const QCameraDevice &cameraDevice)
    {
        m_camera.reset(new QCamera(cameraDevice));
        m_captureSession.setCamera(m_camera.data());

        connect(m_camera.data(), &QCamera::activeChanged, this, &Camera::updateCameraActive);
        connect(m_camera.data(), &QCamera::errorOccurred, this, &Camera::displayCameraError);

        if (!m_mediaRecorder) {
            m_mediaRecorder.reset(new QMediaRecorder);
            m_captureSession.setRecorder(m_mediaRecorder.data());
            connect(m_mediaRecorder.data(), &QMediaRecorder::recorderStateChanged, this,
                    &Camera::updateRecorderState);
            connect(m_mediaRecorder.data(), &QMediaRecorder::durationChanged, this,
                    &Camera::updateRecordTime);
            connect(m_mediaRecorder.data(), &QMediaRecorder::errorChanged, this,
                    &Camera::displayRecorderError);
        }

        if (!m_imageCapture) {
            m_imageCapture.reset(new QImageCapture);
            m_captureSession.setImageCapture(m_imageCapture.get());
            connect(m_imageCapture.get(), &QImageCapture::readyForCaptureChanged, this,
                    &Camera::readyForCapture);
            connect(m_imageCapture.get(), &QImageCapture::imageCaptured, this,
                    &Camera::processCapturedImage);
            connect(m_imageCapture.get(), &QImageCapture::imageSaved, this, &Camera::imageSaved);
            connect(m_imageCapture.get(), &QImageCapture::errorOccurred, this,
                    &Camera::displayCaptureError);
        }

        m_captureSession.setVideoOutput(ui->viewfinder);

        updateCameraActive(m_camera->isActive());
        updateRecorderState(m_mediaRecorder->recorderState());
        readyForCapture(m_imageCapture->isReadyForCapture());

        updateCaptureMode();

        m_camera->start();
    }*/


    //QList<QCameraInfo> availCameras = QCameraInfo::availableCameras();
    //if (availableCameras.length() > cameraID) {
    qDebug() << "Opening USB camera" << availCameras[cameraID].description();


    /*QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("USBCamerasUsed"));
    int testifopen = settings.value(QLatin1String(availCameras[cameraID].id()), 0).toInt();
    if (testifopen == 1) {
        qDebug() << "Error: that camera is already in use.";
        return false;
    }
    settings.setValue(QLatin1String(availCameras[cameraID].id()), 1);
    settings.endGroup();*/

    if (checkIfInUse(cameraID)) {
        qDebug() << "Error: that camera is already in use.";
        return false;
    }


    cameraInput = new QCamera(availCameras[cameraID]);

    //QCameraFormat form = cameraInput->cameraFormat();
    //qDebug() << "Camera's reported resolution:" << form.resolution();


    //cameraInput->setFocusMode(QCamera::FocusModeAuto);
    //qDebug() << "Exposure supported" << cameraInput->isExposureModeSupported(QCamera::ExposureSteadyPhoto);
    //cameraInput->setExposureMode(QCamera::ExposureSteadyPhoto);

    captureSession = new QMediaCaptureSession();
    //captureSession.setCamera(cameraInput);
    captureSession->setCamera(cameraInput);


    camSink = new QVideoSink;
    //captureSession->setVideoOutput(camSink);
    captureSession->setVideoSink(camSink);
    connect(camSink, SIGNAL(videoFrameChanged(const QVideoFrame&)),this,SLOT(videoFrameChanged(const QVideoFrame&)));
    connect(cameraInput, &QCamera::errorOccurred, this, &WebcamWrapper::cameraErrorOccurred);
    //camSink->videoFrameChanged()

    //cameraInput->setViewfinder(videoSurface());
    isCameraOpen = true;
    isCameraMalfunctioning = false;
    numFramesRecieved = 0;
    currentCameraID = cameraID;


    /*} else {
        qDebug() << "Error in opening webcam";
        return false;
    }*/

    qDebug() << "USB camera opened";

    //Flag this camera as in use
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("USBCamerasUsed"));
    settings.setValue(QLatin1String(availCameras[cameraID].id()), 1);
    settings.endGroup();

    return true;
}

bool WebcamWrapper::start() {
    if (isCameraOpen) {
         qDebug() << "Starting USB camera";
         cameraInput->start(); // to start the viewfinder
         lastFrameInterval = 0;
         frameIntervalTimer.restart();
         watchdogTimer.start(300);
         //watchdogTimer.start(10000); //for testing
         qDebug() << "USB camera started";

         return true;
    } else {
        return false;
    }

}

void WebcamWrapper::restart() {
    close();
    restartTimer.start(300); //restart the camera after a delay.

    //open(currentCameraID);
    //start();
}

void WebcamWrapper::restart_continued() {

    restartTimer.stop();
    open(currentCameraID);
    start();
}

void WebcamWrapper::watchdogFunction() {
    //qDebug() << "Watchdog.";
    bool testMode = false;

    if (isCameraAcquiring && testMode) {
        qDebug() << "Restarting camera";
        isCameraMalfunctioning = true;
        //emit cameraMalfunction(true);
        watchdogTimer.stop();
        lastFrameInterval = 0;
        frameIntervalTimer.restart();
        restart();
    } else if (isCameraAcquiring && ((frameIntervalTimer.elapsed() > 300))) {
        //The camera is not behaving right, so we will reset it.

        /*qDebug() << "Restarting camera";
        isCameraMalfunctioning = true;
        //emit cameraMalfunction(true);
        watchdogTimer.stop();
        lastFrameInterval = 0;
        frameIntervalTimer.restart();
        restart();*/

    }
}

void WebcamWrapper::videoFrameChanged(const QVideoFrame &currentFrame) {


    if (isCameraAcquiring) {
        if (frameIntervalTimer.elapsed() < 10) {
            //USB cameras with higher than 100 f/sec-- frames will be dropped.
            return;
        } /*else if (frameIntervalTimer.elapsed() > 300) {
            qDebug() << "Restarting camera";
            frameIntervalTimer.restart();
            restart();
            return;

        }*/
    }
    lastFrameInterval = frameIntervalTimer.elapsed();
    frameIntervalTimer.restart();
    numFramesRecieved++;

    if (!currentFrame.isValid()) {
        qDebug("Invalid frame recieved. Dropping");
    }


    /*QVideoFrame frame(currentFrame);  // make a copy we can call map (non-const) on
    bool mapSuccess = frame.map(QVideoFrame::ReadOnly);

    if (!mapSuccess) {
        qDebug("Error mapping frame. Dropping");
        return;
    } else {
        frame.unmap();
    }*/

   /*QImage img;
    QImage *sendImage = new QImage();

    QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(
        frame.pixelFormat());
    // BUT the frame.pixelFormat() is QVideoFrame::Format_Jpeg, and this is
    // mapped to QImage::Format_Invalid by
    // QVideoFrame::imageFormatFromPixelFormat
    if (imageFormat != QImage::Format_Invalid) {
        img = QImage(frame.bits(),
                     frame.width(),
                     frame.height(),
                     // frame.bytesPerLine(),
                     imageFormat);
    } else {
        // e.g. JPEG
        int nbytes = frame.mappedBytes();
        img = QImage::fromData(frame.bits(), nbytes);
    }
    frame.unmap();*/


    //QImage *sendImage = new QImage(frame.toImage());
    QImage *sendImage = new QImage(currentFrame.toImage());


    /*QImage *sendImage = new QImage(currentFrame.bits(),
                     currentFrame.width(),
                     currentFrame.height(),
                     currentFrame.bytesPerLine(),
                     imageFormat);*/

    if (!isCameraAcquiring) {
        isCameraAcquiring = true;
        qDebug() << "Video format is: " << sendImage->format();
        //Log the video format
        switch (sendImage->format()) {
        case QImage::Format_RGB32:
            qDebug() << "RGB32 video format";
            setFormat(AbstractCamera::Fmt_RGB32);
            break;
        case QImage::Format::Format_RGBA8888_Premultiplied:
            qDebug() << "RGB24 video format";
            setFormat(AbstractCamera::Fmt_RGB24);
            break;
        case QImage::Format_RGB888:
            qDebug() << "RGB24 video format";
            setFormat(AbstractCamera::Fmt_RGB24);
            break;
        case QImage::Format_ARGB32:
            qDebug() << "ARGB32 video format";
            setFormat(AbstractCamera::Fmt_ARGB32);
            break;
        default:
            qDebug() << "Video format not supported";
            setFormat(AbstractCamera::Fmt_Invalid);
            //isCameraAcquiring = false;
            break;

        }
    }


    //delete sendImage;
    emit newFrame(sendImage,0, 0, false, -1); //send image to image processor, do not flip frame
    emit newFrame_timerequest();
}

void WebcamWrapper::close() {
    qDebug() << "Closing webcam";
    stop();
    isCameraAcquiring = false;

    if (cameraInput != NULL) {




        //captureSession.setCamera(cameraInput);
        //captureSession.
        //camSink = new QVideoSink;


        //delete camSink;
        //delete cameraInput;
        //delete camSink;


        cameraInput->deleteLater();
        camSink->deleteLater();
        captureSession->deleteLater();

        //cameraInput = NULL;

        /* Do not delete the cameraInput pointer
         * It's a memory leak, but on linux, deleting a qcamera causes
         * a crash later down the road for some reason. Even breaks when
         * deleting later on a timer
         */
//        cameraInput->deleteLater();
//        delete cameraInput;
//        cameraInput = NULL;
    }
    isCameraOpen = false;
    qDebug() << "Camera closed";

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("USBCamerasUsed"));
    settings.setValue(QLatin1String(availCameras[currentCameraID].id()), 0);
    //qDebug() << "Camera memory: " << settings.value(QLatin1String(availCameras[currentCameraID].id()), 0).toInt();
    settings.endGroup();

}

void WebcamWrapper::stop() {

    if (isCameraAcquiring) {

        //qDebug() << "Disconnecting camsink";
        //disconnect(camSink);
        //qDebug() << "Deleting camsink";
        //delete camSink;
        //qDebug() << "Deleting capture session";
        //delete captureSession;
        qDebug() << "Stopping camera stream";

        cameraInput->stop();

        qDebug() << "Camera stream stopped";
        isCameraAcquiring = false;
        numFramesRecieved = 0;
        watchdogTimer.stop();
    }

}

/*void WebcamWrapper::cameraStateChanged(bool active) {
    if (active) {
        isCameraAcquiring = true;
        qDebug() << "Video format is: " << (QImage::Format)surface->getFormat();
        //Log the video format
        switch (surface->getFormat()) {
        case QImage::Format_RGB32:
            qDebug() << "RGB32 video format";
            setFormat(AbstractCamera::Fmt_RGB32);
            break;
        case QImage::Format_RGB888:
            qDebug() << "RGB24 video format";
            setFormat(AbstractCamera::Fmt_RGB24);
            break;
        case QImage::Format_ARGB32:
            qDebug() << "ARGB32 video format";
            setFormat(AbstractCamera::Fmt_ARGB32);
            break;
        default:
            qDebug() << "Video format not supported";
            setFormat(AbstractCamera::Fmt_Invalid);
            break;

        }
    } else {
        isCameraAcquiring = false;
    }
}*/


//----------------------------------------------------



/*
WebcamDisplaySurface::WebcamDisplaySurface(QObject *parent)
    : QAbstractVideoSurface(parent)
    , imageFormat(QImage::Format_Invalid) {
    frameTimer.start();
}

QList<QVideoFrame::PixelFormat> WebcamDisplaySurface::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const {
    if (handleType == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB32
                << QVideoFrame::Format_ARGB32
                << QVideoFrame::Format_ARGB32_Premultiplied
                << QVideoFrame::Format_RGB565
                << QVideoFrame::Format_RGB555;
    } else {
        return QList<QVideoFrame::PixelFormat>();
    }
}

QImage::Format WebcamDisplaySurface::getFormat() {
    return imageFormat;
}

bool WebcamDisplaySurface::isFormatSupported(
        const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const
{
    Q_UNUSED(similar);

    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    return imageFormat != QImage::Format_Invalid
            && !size.isEmpty()
            && format.handleType() == QAbstractVideoBuffer::NoHandle;
}

bool WebcamDisplaySurface::start(const QVideoSurfaceFormat &format)
{
    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    if (imageFormat != QImage::Format_Invalid && !size.isEmpty()) {
        this->imageFormat = imageFormat;

        imageSize = size;
        sourceRect = format.viewport();

        QAbstractVideoSurface::start(format);

        //widget->updateGeometry();
        //updateVideoRect();

        return true;
    } else {
        return false;
    }
}

void WebcamDisplaySurface::stop()
{
    currentFrame = QVideoFrame();
    targetRect = QRect();

    QAbstractVideoSurface::stop();

    //widget->update();
}

bool WebcamDisplaySurface::present(const QVideoFrame &frame) {

    if (surfaceFormat().pixelFormat() != frame.pixelFormat()
            || surfaceFormat().frameSize() != frame.size()) {
        setError(IncorrectFormatError);
        stop();

        return false;
    } else {

        if(!frame.isValid()){
            return true;
        }



        currentFrame = frame;
        if(QVideoFrame::imageFormatFromPixelFormat(currentFrame.pixelFormat()) == QImage::Format_Invalid ||
           QVideoFrame::imageFormatFromPixelFormat(currentFrame.pixelFormat()) != imageFormat){
            return true;
        }
        //widget->update();

        if (currentFrame.map(QAbstractVideoBuffer::ReadOnly)) {
#ifdef __APPLE__
            QImage *sendImage = new QImage(currentFrame.bits(),
                             currentFrame.width(),
                             currentFrame.height(),
                             currentFrame.bytesPerLine(),
                             imageFormat);
#else
            QImage *sendImage = new QImage(currentFrame.width(),
                             currentFrame.height(),
                             imageFormat);
            memcpy(sendImage->bits(), frame.bits(), frame.mappedBytes());
#endif

            //widget->newImage(*sendImage);


#ifdef WIN32
               emit newFrame(sendImage,0, 0, true, -1); //send image to image processor, flip frame
#else
               emit newFrame(sendImage,0, 0, false, -1); //send image to image processor
#endif

            //emit newFrame(); //for time request via tcp server
            currentFrame.unmap();
        }

        return true;
    }
}*/


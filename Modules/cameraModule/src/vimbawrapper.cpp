#include "vimbawrapper.h"
#include <QtGui>
#include <sstream>
#include <iostream>
#include <vector>
#include <algorithm>


enum    { NUM_FRAMES=3, };
#define NUM_COLORS 3
#define BIT_DEPTH 8

quint64 numFramesHandled;
QAtomicInteger<quint64> numFramesIn;
/*void STDCALL FrameDoneCB(tPvFrame* pFrame)
{
    numFramesIn++;
}*/

//void CameraEventCB(void* Context, tPvHandle Camera,const tPvCameraEvent* EventList,unsigned long EventListLength){
//    AvtCameraController* self = (AvtCameraController*)Context;

//    for(int i=0; i < EventListLength; ++i){
//        qDebug() << "CameraEventCB: " << EventList[i].EventId;
//        switch (EventList[i].EventId) {
//        case 40006: //EventPtpSyncLocked
//            qDebug() << "================ CameraEventCB: event sync locked";
//            emit self->PTPFullyInitialized();
//            break;
//        case 40005: //EventPtpSyncLost
//            qDebug() << "================ CameraEventCB: event sync lost";
//            emit self->PTPNotProperlySetup();
//            break;
//        default:
//            break;
//        }
//    }
//}

VimbaCameraController::VimbaCameraController(bool ptpenabled)
    // Get a reference to the Vimba singleton
    : m_system( VimbaSystem::GetInstance() )
{
    ptpChecksEnabled = ptpenabled;
    ptpCapableCamera = false;
    isCameraOpen = false;
    initialized = false;
    isCameraAcquiring = false;
    isBlackAndWhite= false;

    hasReset = false;

    for (int i=0; i<256; i++) {
        colorTable.append(qRgb(i,i,i));
    }



    blinkTimer.setSingleShot(true);
    connect(&blinkTimer,SIGNAL(timeout()),this,SLOT(endBlink()));
    startBlinkTimer.setSingleShot(true);
    connect(&startBlinkTimer,SIGNAL(timeout()),this,SLOT(startBlink()));

    connect(this, &VimbaCameraController::PTPNotProperlySetup, this, &VimbaCameraController::ResetPTP, Qt::QueuedConnection); // Queued conn b/c signal can come from Vimba
    connect(&ptpChecker, &QTimer::timeout, this, &VimbaCameraController::checkPTPStatus);


    // Start Vimba
    VmbErrorType err = StartUp();

    if( VmbErrorSuccess == err ) {
            VmbVersionInfo_t info;
            if (VmbErrorSuccess != m_system.QueryVersion( info ))
            {
                std::cout << "Error: Could not query version";
                return;
            }
            std::cout << "Vimba version " << info.major << "." << info.minor << "." << info.patch << "\n";

            updateCameraList();
            initialized = true;

    } else {
        //std::cout << "Could not start system. Error code: " << err <<"("<<ErrorCodeToMessage(err)<<")"<< "\n";

    }


}

VimbaCameraController::~VimbaCameraController() {
    // uninit the API
    if (isCameraOpen) {
        close();
    }

    if (initialized) {
        ShutDown();
    }
}

//
// Starts the Vimba API and loads all transport layers
//
// Returns:
//  An API status code
//
VmbErrorType VimbaCameraController::StartUp()
{
    VmbErrorType res;

    // Start Vimba
    res = m_system.Startup();
    if( VmbErrorSuccess == res )
    {
        // This will be wrapped in a shared_ptr so we don't delete it
        SP_SET( m_pCameraObserver , new CameraObserver() );
        // Register an observer whose callback routine gets triggered whenever a camera is plugged in or out
        res = m_system.RegisterCameraListObserver( m_pCameraObserver );
    }

    return res;
}

//
// Shuts down the API
//
void VimbaCameraController::ShutDown()
{
    // Release Vimba
    m_system.Shutdown();
    initialized = false;
}
/*** helper function to set image size to a value that is dividable by modulo 2 and a multiple of the increment.
\note this is needed because VimbaImageTransform does not support odd values for some input formats
*/
inline VmbErrorType SetValueIntMod2( const CameraPtr &camera, const std::string &featureName, VmbInt64_t &storage )
{
    VmbErrorType    res;
    FeaturePtr      pFeature;
    VmbInt64_t      minValue = 0;
    VmbInt64_t      maxValue = 0;
    VmbInt64_t      incrementValue = 0;

    res = SP_ACCESS( camera )->GetFeatureByName( featureName.c_str(), pFeature );
    if( VmbErrorSuccess != res )
    {
        return res;
    }

    res = SP_ACCESS( pFeature )->GetRange( minValue, maxValue );
    if( VmbErrorSuccess != res )
    {
        return res;
    }

    res = SP_ACCESS( pFeature )->GetIncrement( incrementValue);
    if( VmbErrorSuccess != res)
    {
        return res;
    }

    maxValue = maxValue - ( maxValue % incrementValue );
    if( maxValue % 2 != 0)
    {
        maxValue -= incrementValue;
    }

    res = SP_ACCESS( pFeature )->SetValue( maxValue );
    if( VmbErrorSuccess != res )
    {
        return res;
    }

    storage = maxValue;
    return res;
}


//
// Gets all cameras known to Vimba
//
// Returns:
//  A vector of camera shared pointers
//
CameraPtrVector VimbaCameraController::GetCameraList()
{
    CameraPtrVector cameras;
    // Get all known cameras
    if( VmbErrorSuccess == m_system.GetCameras( cameras ) )
    {
        // And return them
        return cameras;
    }
    return CameraPtrVector();
}

//
// Gets the width of a frame
//
// Returns:
//  The width as integer
//
int VimbaCameraController::GetWidth() const
{
    return static_cast<int>(m_nWidth);
}

//
// Gets the height of a frame
//
// Returns:
//  The height as integer
//
int VimbaCameraController::GetHeight() const
{
    return static_cast<int>(m_nHeight);
}

//
// Gets the pixel format of a frame
//
// Returns:
//  The pixel format as enum
//
VmbPixelFormatType VimbaCameraController::GetPixelFormat() const
{
    return static_cast<VmbPixelFormatType>(m_nPixelFormat);
}

//
// Gets the oldest frame that has not been picked up yet
//
// Returns:
//  A frame shared pointer
//
FramePtr VimbaCameraController::GetFrame()
{
    return SP_DYN_CAST( m_pFrameObserver, FrameObserver )->GetFrame();
}

//
// Clears all remaining frames that have not been picked up
//
void VimbaCameraController::ClearFrameQueue()
{
    SP_DYN_CAST( m_pFrameObserver,FrameObserver )->ClearFrameQueue();
}

//
// Queues a given frame to be filled by the API
//
// Parameters:
//  [in]    pFrame          The frame to queue
//
// Returns:
//  An API status code
//
VmbErrorType VimbaCameraController::QueueFrame( FramePtr pFrame )
{
    return SP_ACCESS( m_pCamera )->QueueFrame( pFrame );
}

//
// Returns the camera observer as QObject pointer to connect their signals to the view's slots
//
QObject* VimbaCameraController::GetCameraObserver()
{
    return SP_DYN_CAST( m_pCameraObserver, CameraObserver ).get();
}

//
// Returns the frame observer as QObject pointer to connect their signals to the view's slots
//
QObject* VimbaCameraController::GetFrameObserver()
{
    return SP_DYN_CAST( m_pFrameObserver, FrameObserver ).get();
}

//
// Gets the version of the Vimba API
//
// Returns:
//  The version as string
//



bool VimbaCameraController::checkIfInUse(int ID) {
    if (detectedCameras.length() <= ID) {
        return true;
    }

    if (detectedCameras[ID].accessType & VmbAccessModeFull) {
        return false;
    } else {
        return true;
    }


}

QStringList VimbaCameraController::availableCameras() {

    QStringList cameraNames;



    for (int i=0; i<detectedCameras.length();i++) {
        cameraNames << (QString("Allied Vision ") + QString().fromStdString(detectedCameras[i].modelName) + " " + QString().fromStdString(detectedCameras[i].serialNumber));
    }

    return cameraNames;
}

void VimbaCameraController::updateCameraList()
{
    detectedCameras.clear();
    VmbErrorType err;
    CameraPtrVector cameras;
    err = m_system.GetCameras( cameras );            // Fetch all cameras known to Vimba
    if( VmbErrorSuccess == err ) {

        std::cout << "Vimba cameras found: " << cameras.size() <<"\n\n";
        for (int i=0; i<cameras.size();i++) {
            detectedCameras.push_back(getCameraInfo(cameras[i]));
        }
    }
}

vimCameraInfo VimbaCameraController::getCameraInfo( const CameraPtr &camera )
{
    std::string strID;
    std::string strName;
    std::string strModelName;
    std::string strSerialNumber;
    std::string strInterfaceID;
    VmbAccessModeType accessType;
    vimCameraInfo out;

    std::ostringstream ErrorStream;

    camera->GetPermittedAccess(accessType);

    VmbErrorType err = camera->GetID( strID );
    if( VmbErrorSuccess != err )
    {
        //ErrorStream << "[Could not get camera ID. Error code: " << err << "("<<ErrorCodeToMessage(err)<<")"<< "]";
        strID =  ErrorStream.str();
    }

    err = camera->GetName( strName );
    if( VmbErrorSuccess != err )
    {
        //ErrorStream << "[Could not get camera name. Error code: " << err << "("<<ErrorCodeToMessage(err)<<")"<< "]";
        strName = ErrorStream.str() ;
    }

    err = camera->GetModel( strModelName );
    if( VmbErrorSuccess != err )
    {
        //ErrorStream << "[Could not get camera mode name. Error code: " << err << "("<<ErrorCodeToMessage(err)<<")"<< "]";
        strModelName = ErrorStream.str();
    }

    err = camera->GetSerialNumber( strSerialNumber );
    if( VmbErrorSuccess != err )
    {
        //ErrorStream << "[Could not get camera serial number. Error code: " << err << "("<<ErrorCodeToMessage(err)<<")"<< "]";
        strSerialNumber = ErrorStream.str();
    }

    err = camera->GetInterfaceID( strInterfaceID );
    if( VmbErrorSuccess != err )
    {
        //ErrorStream << "[Could not get interface ID. Error code: " << err << "("<<ErrorCodeToMessage(err)<<")"<< "]";
        strInterfaceID = ErrorStream.str() ;
    }

    out.cameraName = strName;
    out.id = strID;
    out.interfaceID = strInterfaceID;
    out.modelName = strModelName;
    out.serialNumber = strSerialNumber;
    out.accessType = accessType;

    /*std::cout   << "/// Camera Name    : " << strName           << "\n"
              << "/// Model Name     : " << strModelName      <<  "\n"
              << "/// Camera ID      : " << strID             <<  "\n"
              << "/// Serial Number  : " << strSerialNumber   <<  "\n"
              << "/// @ Interface ID : " << strInterfaceID    << "\n\n";*/


    return out;
}


bool VimbaCameraController::open(int cameraID) {


    if (isCameraOpen) {
        return false;
    }

    if (checkIfInUse(cameraID)) {
        qDebug() << "Error: that camera is already in use.";
        return false;
    }

    VmbErrorType err;

    if(detectedCameras.length() > cameraID) {

        // Open the desired camera by its ID
        VmbErrorType res = m_system.OpenCameraByID(detectedCameras[cameraID].id.c_str(), VmbAccessModeFull, m_pCamera );
        if( VmbErrorSuccess != res ) {
            if (res == VmbErrorInvalidAccess) {
                qDebug() << "Camera access denied.";
            } else if (res == VmbErrorNotFound){
                qDebug() << "Camera not found";
            } else {
                qDebug() << "Camera opening error";
            }
            return false;
        }

        if( VmbErrorSuccess == res )
        {
            // Set the GeV packet size to the highest possible value
            // (In this example we do not test whether this cam actually is a GigE cam)
            FeaturePtr pCommandFeature;
            if( VmbErrorSuccess == SP_ACCESS( m_pCamera )->GetFeatureByName( "GVSPAdjustPacketSize", pCommandFeature ) )
            {
                if( VmbErrorSuccess == SP_ACCESS( pCommandFeature )->RunCommand() )
                {
                    bool bIsCommandDone = false;
                    do
                    {
                        if( VmbErrorSuccess != SP_ACCESS( pCommandFeature )->IsCommandDone( bIsCommandDone ) )
                        {
                            break;
                        }
                    } while( false == bIsCommandDone );
                }
            }
            res = SetValueIntMod2( m_pCamera,"Width", m_nWidth );
            if( VmbErrorSuccess == res ) {

                res = SetValueIntMod2( m_pCamera, "Height", m_nHeight );
                if( VmbErrorSuccess == res ) {

                    // Store currently selected image format
                    FeaturePtr pFormatFeature;
                    res = SP_ACCESS( m_pCamera )->GetFeatureByName( "PixelFormat", pFormatFeature );


                    if( VmbErrorSuccess == res ) {

                        res = SP_ACCESS( pFormatFeature )->GetValue( m_nPixelFormat );
                        if ( VmbErrorSuccess == res ) {

                            VmbPixelFormatType pf = GetPixelFormat();

                            if (pf == VmbPixelFormatRgb8) {
                                isBlackAndWhite = false;
                                bayerDemosaicNeeded = false;
                                setFormat(AbstractCamera::Fmt_RGB24);
                            } else if (pf == VmbPixelFormatMono8){
                                isBlackAndWhite = true;
                                bayerDemosaicNeeded = false;
                                setFormat(AbstractCamera::Fmt_RGB24);
                            } else {
                                isBlackAndWhite = false;
                                bayerDemosaicNeeded = false;
                                setFormat(AbstractCamera::Fmt_RGB24);
                            }


                            isCameraOpen = true;
                            currentCameraID = cameraID;
                          }
                    }
                }
            }
            if ( VmbErrorSuccess != res ) {

                // If anything fails after opening the camera we close it
                SP_ACCESS( m_pCamera )->Close();
                isCameraOpen = false;
                return false;
            }

        }

        //Turn on PTP system
        if(ptpChecksEnabled){
            FeaturePtr ptpStatus;
            FeaturePtr ptpMode;
            res = SP_ACCESS( m_pCamera )->GetFeatureByName( "PtpStatus", ptpStatus );
            if ( VmbErrorSuccess == res ) {
                //PTP is available
                ptpCapableCamera = true;
                ptpInitialized = false;
                res = SP_ACCESS( m_pCamera )->GetFeatureByName( "PtpMode", ptpMode );
                if ( VmbErrorSuccess == res ) {
                    std::string modeValue;
                    res = ptpMode->GetValue( modeValue );
                    QString ptpmode = QString().fromStdString(modeValue);

                    if(ptpmode != "Slave"){
                          qDebug() << "Allied Vision camera needs PTP mode turned to Slave to work here.";
                          emit PTPWrongConfiguration();
                    } else {
                          std::string statusValue;
                          res = ptpStatus->GetValue( statusValue );
                          QString ptpstatus = QString().fromStdString(statusValue);
                          if(ptpstatus == "Initializing" || ptpstatus == "Listening"){
                            emit PTPWarningMessage("PTPStillSyncing");
                          }

                          ptpChecker.start(5000);
                    }



                }
            } else {
                emit PTPWarningMessage("NotPTPCapable");
            }
        }


        return true;
    } else {
        return false;
    }
}

void VimbaCameraController::close() {
    VmbErrorType    res;
    if (isCameraAcquiring) {
        stop();
    }
    if (isCameraOpen) {
        ptpChecker.stop();
        emit PTPNoMessage();
        // Close camera
        res = m_pCamera->Close();
        if ( VmbErrorSuccess == res ) {
            qDebug() << "Camera closed.";
            isCameraOpen = false;
        } else {
            qDebug() << "Error closing camera";
        }

    }
}

bool VimbaCameraController::start() {
    //Starts acquisition

    if (!isCameraOpen) {
        return false;
    }
    VmbErrorType    res;

    // Create a frame observer for this camera (This will be wrapped in a shared_ptr so we don't delete it)
    SP_SET( m_pFrameObserver , new FrameObserver( m_pCamera ) );
    // Start streaming
    res = SP_ACCESS( m_pCamera )->StartContinuousImageAcquisition( NUM_FRAMES,  m_pFrameObserver );
    if ( VmbErrorSuccess == res ) {

        QObject::connect( GetFrameObserver(), SIGNAL( FrameReceivedSignal(int) ), this, SLOT( onFrameReady(int) ) );
        isCameraAcquiring = true;
    } else {

        stop();
        isCameraAcquiring = false;

        return false;

    }

    return true;

}


void VimbaCameraController::onFrameReady( int status )
{
    if( isCameraAcquiring) {
        // Pick up frame
        FramePtr pFrame = GetFrame();
        if( SP_ISNULL( pFrame ) ) {

            qDebug() << "Frame pointer is NULL, late frame ready message";
            return;
        }
        // See if it is not corrupt
        if( VmbFrameStatusComplete == status )
        {
            VmbUchar_t *pBuffer;
            VmbErrorType err = SP_ACCESS( pFrame )->GetImage( pBuffer );
            if( VmbErrorSuccess == err )
            {
                VmbUint32_t nSize;
                err = SP_ACCESS( pFrame )->GetImageSize( nSize );
                if( VmbErrorSuccess == err )
                {
                    VmbPixelFormatType ePixelFormat = GetPixelFormat();
                    QImage* sendImagePtr = new QImage(GetWidth(),GetHeight(), QImage::Format_RGB888);

                    copyToImage( pBuffer,ePixelFormat, sendImagePtr);

                    VmbUint64_t fTimestamp;
                    VmbUint64_t fID;
                    err = SP_ACCESS( pFrame )->GetTimestamp( fTimestamp );
                    err = SP_ACCESS( pFrame )->GetFrameID( fID );

                    emit newFrame(sendImagePtr, fID, fTimestamp, false, -1);
                    emit newFrame_timerequest();


                }
            }
        } else {

            // If we receive an incomplete image we do nothing but logging
            qDebug() << "Failure in receiving image";
        }

        // And queue it to continue streaming
        QueueFrame( pFrame );
    }
}

VmbErrorType VimbaCameraController::copyToImage(VmbUchar_t *pInBuffer, VmbPixelFormat_t ePixelFormat, QImage *pOutImage)
{
    const int           nHeight = GetHeight();
    const int           nWidth  = GetWidth();

    VmbImage            SourceImage,DestImage;
    VmbError_t          Result;
    SourceImage.Size    = sizeof( SourceImage );
    DestImage.Size      = sizeof( DestImage );

    Result = VmbSetImageInfoFromPixelFormat( ePixelFormat, nWidth, nHeight, & SourceImage );
    if( VmbErrorSuccess != Result ) {

        qDebug() << "Could not set source image info";
        return static_cast<VmbErrorType>( Result );
    }
    QString             OutputFormat;
    OutputFormat = "RGB24";

    Result = VmbSetImageInfoFromString( OutputFormat.toStdString().c_str(), OutputFormat.length(),nWidth,nHeight, &DestImage );
    if( VmbErrorSuccess != Result ) {
        qDebug() << "Could not set output image info";
        return static_cast<VmbErrorType>( Result );
    }
    SourceImage.Data    = pInBuffer;
    DestImage.Data      = pOutImage->bits();

    Result = VmbImageTransform( &SourceImage, &DestImage,NULL,0 );

    if( VmbErrorSuccess != Result ) {
        qDebug() << "Could not transform image";
        return static_cast<VmbErrorType>( Result );
    }
    return static_cast<VmbErrorType>( Result );
}


void VimbaCameraController::stop() {


    // Stop streaming
    if (isCameraAcquiring) {
        SP_ACCESS( m_pCamera )->StopContinuousImageAcquisition();
    }

    // Clear all frames that we have not picked up so far
    ClearFrameQueue();
    isCameraAcquiring = false;
}

void VimbaCameraController::blinkAcquire() {

    startBlinkTimer.start(250); //The landmark pause starts 1/4 second after record start to make sure a few frames come in.
    /*
    if (isCameraAcquiring) {
        qDebug() << "Creating brief pause in video acquisition";
        stop();
        blinkTimer.start(500);
    }*/
}

void VimbaCameraController::resetCurrentCamera() {
    int camID = currentCameraID;

    close();

    if (!open(camID)) {
        qDebug() << "Error during camera reset.  Open procedure failed.";
    }
    if (!start()) {
        qDebug() << "Error during camera reset.  Start procedure failed.";
    }
    QThread::msleep(500);

}

void VimbaCameraController::startBlink() {

    if (isCameraAcquiring) {
        qDebug() << "Creating brief pause in video acquisition";
        stop();
        blinkTimer.start(500); //If this value is changed,
        //Change the blinkLength var in exporthwtimes processData(), first line.
    }
}

void VimbaCameraController::endBlink() {

    if (isCameraOpen) {
        start();
    }
}



void VimbaCameraController::ResetPTP(){
    //tPvErr err = PvAttrEnumSet(cameraInfo.Handle, "PtpMode", "Off");//turn off ptp
    //tPvErr err2 = PvAttrEnumSet(cameraInfo.Handle, "PtpMode", "Slave");//turn ptp back on
    //qDebug() << "Reset PTP" << err << err2;
    ptpInitialized = false;
}

void VimbaCameraController::checkPTPStatus(){
    VmbErrorType res;
    FeaturePtr ptpStatus;
    FeaturePtr ptpMode;

    QString ptpmode;
    QString ptpstatus;

    res = SP_ACCESS( m_pCamera )->GetFeatureByName( "PtpStatus", ptpStatus );
    if ( VmbErrorSuccess == res ) {
        //PTP is available
        res = SP_ACCESS( m_pCamera )->GetFeatureByName( "PtpMode", ptpMode );
        if ( VmbErrorSuccess == res ) {
            std::string modeValue;
            res = ptpMode->GetValue( modeValue );
            ptpmode = QString().fromStdString(modeValue);
            std::string statusValue;
            res = ptpStatus->GetValue( statusValue );
            ptpstatus = QString().fromStdString(statusValue);

        }
    }


    if(ptpstatus == "Slave" && ptpmode == "Slave")
    {
        //PTP checks enabled, go through more sanity checks.
        /*if(ptpChecksEnabled && !ptpInitialized){
            //If camera ptpstatus and mode are both slave, all is good.
            tPvErr err = PvCommandRun(cameraInfo.Handle, "GevTimestampControlLatch");
            if(err != ePvErrSuccess){
                qDebug() << "Error calling GevTimestampControlLatch." << err;
                emit PTPWarningMessage("BadTimestampCheck");
                return;
            }
            else{
                //                qDebug() << "Is attr available:" << PvAttrExists(cameraInfo.Handle, "GevTimestampValue");
                //When porting to Vimba in the future, make sure to change the following to use GevTimestampValue.
                //PvAPI for some reason does not recognize it. Or, Vimba does the following conversions manually.
                tPvUint32 timestampvalhi, timestampvallo;
                err = PvAttrUint32Get(cameraInfo.Handle, "TimeStampValueHi", &timestampvalhi);
                if(err != ePvErrSuccess){
                    qDebug() << "Error getting TimeStampValueHi." << err << timestampvalhi;
                    emit PTPWarningMessage("BadTimestampCheck");
                    return;
                }
                err = PvAttrUint32Get(cameraInfo.Handle, "TimeStampValueLo", &timestampvallo);
                if(err != ePvErrSuccess){
                    qDebug() << "Error getting TimeStampValueLo." << err << timestampvallo;
                    emit PTPWarningMessage("BadTimestampCheck");
                    return;
                }
                qint64 timestampval =  ((qint64)timestampvalhi << 32)|((qint64)timestampvallo);
                if(qAbs(QDateTime::currentMSecsSinceEpoch()-timestampval/1000000) > 100){
                    //Sanity check for timestamp values.
                    emit PTPWarningMessage("TimestampNotCorrect");
                    return;
                }
            }
        }*/
        //if nothing went wrong, emit fully initialized
        emit PTPFullyInitialized();
        ptpInitialized = true;
    }
    else if(ptpstatus == "Uncalibrated" || ptpstatus == "Syncing"){
        //Camera ptp status is currently trying to sync.
        emit PTPCalibrating();
    }
    else if(ptpstatus == "Initializing" || ptpstatus == "Listening"){
        //Do nothing, this state should go away soon and move to uncalibrated/syncing
        emit PTPInitializing();
    }
    else if(ptpstatus == "Disabled" || ptpstatus == "Error"){
        //Mode = slave but status = disabled?? Not properly set up.
        //Reset PTP if ptpchecks enabled
        if(ptpChecksEnabled) emit PTPNotProperlySetup(); //connected to ResetPTP()
    }
    else{
        emit PTPWarningMessage("UnknownError");
    }

}

//-------------------------------------------------------------------------------


// This is our callback routine that will be executed every time a camera was plugged in or out
//
// Parameters:
//  [in]    pCam            The camera that triggered the callback
//  [in]    reason          The reason why the callback was triggered
//
void CameraObserver::CameraListChanged( CameraPtr pCam, UpdateTriggerType reason )
{
    if (    UpdateTriggerPluggedIn == reason
        || UpdateTriggerPluggedOut == reason )
    {
        // Emit the new camera signal
        emit CameraListChangedSignal( reason );
    }
}



//----------------------------------------------------------------------------------

//
// This is our callback routine that will be executed on every received frame.
// Triggered by the API.
//
// Parameters:
//  [in]    pFrame          The frame returned from the API
//
void FrameObserver::FrameReceived( const FramePtr pFrame )
{
    bool bQueueDirectly = true;
    VmbFrameStatusType eReceiveStatus;

    if(     0 != receivers(SIGNAL(FrameReceivedSignal(int)) )
        &&  VmbErrorSuccess == pFrame->GetReceiveStatus( eReceiveStatus ) )
    {
        // Lock the frame queue
        m_FramesMutex.lock();
        // Add frame to queue
        m_Frames.push( pFrame );
        // Unlock frame queue
        m_FramesMutex.unlock();
        // Emit the frame received signal
        emit FrameReceivedSignal( eReceiveStatus );
        bQueueDirectly = false;
    }

    // If any error occurred we queue the frame without notification
    if( true == bQueueDirectly )
    {
        m_pCamera->QueueFrame( pFrame );
    }
}

//
// After the view has been notified about a new frame it can pick it up.
// It is then removed from the internal queue
//
// Returns:
//  A shared pointer to the latest frame
//
FramePtr FrameObserver::GetFrame()
{
    // Lock the frame queue
    m_FramesMutex.lock();
    // Pop frame from queue
    FramePtr res;
    if( !m_Frames.empty() )
    {
        res = m_Frames.front();
        m_Frames.pop();
    }
    // Unlock frame queue
    m_FramesMutex.unlock();
    return res;
}

//
// Clears the internal (double buffering) frame queue
//
void FrameObserver::ClearFrameQueue()
{
    // Lock the frame queue
    m_FramesMutex.lock();
    // Clear the frame queue and release the memory
    std::queue<FramePtr> empty;
    std::swap( m_Frames, empty );
    // Unlock the frame queue
    m_FramesMutex.unlock();
}

/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
//#include <QWidget>
//#include <stdio.h>
//#include <stdlib.h>
//#include <fstream>

#include "mainWindow.h"



int main(int argc, char *argv[])
{

//    std::ofstream outfile;
//    outfile.open("photometryDebugLog.txt", std::ios_base::trunc);
//    outfile.close();
    qInstallMessageHandler(moduleMessageOutput);
    setModuleName("photometryModule");

    QApplication a(argc, argv);
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
    a.setStyle(new Style_tweaks);

    MainWindow w(a.arguments());
    w.show();
    return a.exec();
}


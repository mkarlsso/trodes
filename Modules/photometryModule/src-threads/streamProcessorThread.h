/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STREAMPROCESSOR_H
#define STREAMPROCESSOR_H

#include <QtGui>
#include <QGLWidget>
#include "configuration.h"
#include "iirFilter.h"
#include "globalObjects.h"
#include "../../Trodes/src-main/sharedVariables.h"
#include "../../Trodes/src-main/trodesSocket.h"

#define NUMCHANNELSPERSTREAMPROCESSOR 128 //this defines the max number of channels per processor thread
#define NEW_SPIKE_DETECTOR
#define NUMDIGSTATECHANGESTOKEEP 10000

typedef struct {
  int ch, ref;
} chan;

class StreamProcessorManager;

class StreamProcessor: public QObject {

  Q_OBJECT

public:
  StreamProcessor(QObject *parent, int groupNumber, QList<int> nTList, StreamProcessorManager* managerPtr);

  ~StreamProcessor();

  int           groupNum;
  int           quitNow;
  bool          isLooping;
  QAtomicInt    updateDataLengthFlag; // triggers run loop to update channels
  double        newDataLength;
  int           dataIdx;
  QList<int>    nFiberList;

private:
  int           nChan; // number of eeg channels to display
  double        dataLength;
  int           numLoopsWithNoData;
  bool          isSetup;

  StreamProcessorManager*   streamManager;
  QList<bool>               filtersOn;

  int           rawDataAvailableIdx;
  int           rawIdx;
  QVector <int> raw_increment;


public:
  void updateDataLength(void);

public slots:
  void runLoop();
  void setUp();

private slots:

signals:
   void bufferOverrun();
   void sourceFail();

};


class StreamProcessorManager : public QObject {

    Q_OBJECT

public:
    StreamProcessorManager(QWidget *parent);
    ~StreamProcessorManager();

    vertex2d                     **neuralDataMinMax;
    QList<StreamProcessor*>      streamProcessors; //threads to process the continous streams of data

private:
    QList<QThread*>               processorThreads;
    bool                          gotSourceFailSignal;
    void                          createNewProcessorThread(QList<int> nFiberList);

public slots:
    void startAcquisition();
    void stopAcquisition();
    void removeAllProcessors();
    void updateDataLength(double tlength);
    void sourceFail();

private slots:

signals:
    void startAllProcessorLoops();
    void bufferOverrun();
    void sourceFail_Sig();

};

#endif // STREAMPROCESSOR_H

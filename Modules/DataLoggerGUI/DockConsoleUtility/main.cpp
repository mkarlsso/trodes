#include <QCoreApplication>
#include "commandengine.h"

int main(int argc, char *argv[])
{

    CommandEngineConsoleWrapper a(argc, argv);
    return a.exec();

}

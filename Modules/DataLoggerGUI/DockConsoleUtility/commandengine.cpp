#include "commandengine.h"

/*FT_STATUS ftStatus;
FT_HANDLE ftHandle;
DWORD BytesWritten, BytesReceived;
uint8_t TxBuffer[1024], RxBuffer[131072];*/


CommandEngine::CommandEngine()
{
    currentDeviceName = "";
    extractFileName = "";
    continueExtraction = false;
    FTDIConnected = false;
    errorMessagesOn = true;
    currentOpenMode = CommandEngine::by_FTDI_description;
}

int CommandEngine::timeval_subtract (struct timeval *result,struct timeval *x,struct timeval *y)

{
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }

    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

uint8_t CommandEngine::packet_read (
            uint8_t *buff,			/* Pointer to the data buffer to store read data */
            DWORD start,    /* First data sector */
            ULONGLONG packet,
            DWORD psize,
            WORD count,
            uint8_t readcmd) {

    DWORD bytes;
    DWORD startSector, sectors;
    int offset;
    uint8_t res;

    offset = (packet * psize)%512;
    sectors = (((count * psize) + offset)/512)+1;
    startSector = ((packet * psize)/512) + start;
    bytes = sectors * 512;

    if (bytes > 131072) {
        // too many sectors
        res = 4;
    } else {
        TxBuffer[0] = readcmd;
        TxBuffer[1] = startSector & 0xff;
        TxBuffer[2] = (startSector>>8) & 0xff;
        TxBuffer[3] = (startSector>>16) & 0xff;
        TxBuffer[4] = (startSector>>24) & 0xff;
        TxBuffer[5] = sectors & 0xff;
        TxBuffer[6] = (sectors>>8) & 0xff;
        TxBuffer[7] = (sectors>>16) & 0xff;
        TxBuffer[8] = (sectors>>24) & 0xff;

        res = FTDISend(9);
        if (res==0) {

            res = FTDIReceive(bytes);
            if (res != 0) {
                return res;
            }

            memcpy(buff, RxBuffer+offset, psize*count);
        }


    }
    return res;
}

int CommandEngine::strendswith(const char *str, const char *suffix)
{
    if (!str || !suffix)
        return 0;
    size_t lenstr = strlen(str);
    size_t lensuffix = strlen(suffix);
    if (lensuffix >  lenstr)
        return 0;
    return strncmp(str + lenstr - lensuffix, suffix, lensuffix) == 0;
}

void CommandEngine::setCurrentDeviceName(QString dev) {
    currentDeviceName = dev;
}

unsigned int CommandEngine::FTDIReadBytesAvailable()
{
    if (!FTDIConnected) {
        return 0;
    }
    DWORD remaining;
    FT_GetQueueStatus(ftHandle, &remaining);
    return remaining;

}

int CommandEngine::FTDIGetMountingStatus(int *sd_detected, int *hs_detected)
{
    int res;
    if (!FTDIConnected) {
        return 1;
    }
    //Is the SD card mounted?
    TxBuffer[0] = SD_GET_STATUS_CMD;
    res = FTDISend(1);
    if (res != 0) {
        return res;
    }
    res = FTDIReceive(4); // expect 4 bytes back
    if (res != 0) {
        return res;
    }


    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
        *sd_detected = 1;        
    }

    //Is the headstage connected?
    TxBuffer[0] = HS_GET_STATUS_CMD;
    res = FTDISend(1);
    if (res != 0) {
        return res;
    }
    res = FTDIReceive(4); // expect 4 bytes back
    if (res != 0) {
        return res;
    }

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
        *hs_detected = 1;        
    }

    return 0;
}

int CommandEngine::FTDIReceive(int expectedBytes)
{
    if (!FTDIConnected) {
        return 1;
    }
    ftStatus = FT_Read(ftHandle, RxBuffer, expectedBytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        sendErrorMessage("USB - Read (FT_Read) failed.");
        //FTDIClose();
        return 1;
    }
    if (BytesReceived != 0 && BytesReceived != expectedBytes) {
        // did not get all bytes bytes -> timeout!
        sendErrorMessage(QString::asprintf("Timeout. Bytes received: %d \n", (int)BytesReceived));
        //FTDIClose();
        return 2;
    }
    return 0;
}

int CommandEngine::FTDISend(int numBytes)
{
    if (!FTDIConnected) {
        return 1;
    }

    ftStatus = FT_Write(ftHandle, TxBuffer, numBytes, &BytesWritten);
    if (ftStatus != FT_OK) {
        sendErrorMessage("USB - Write (FT_Write) failed!\n");
        //FTDIClose();
        return 1;
    }

    return 0;
}

int CommandEngine::FTDIConnect(const CommandEngine::DS_Settings &devSettings, CommandEngine::OpenMode o)
{
    if (FTDIConnected) {
        sendErrorMessage("FT2232H device already connected.");
        return 1;
    }
    UCHAR Mask = 0xff;
    UCHAR Mode;

    if (devSettings.devName.isEmpty()) {
        return 1;
    }
    if (o == CommandEngine::by_FTDI_description) {
        ftStatus = FT_OpenEx((void*)devSettings.devName.toLocal8Bit().data(),FT_OPEN_BY_DESCRIPTION,&ftHandle);
    } else if (o == CommandEngine::by_FTDI_Serial) {
        ftStatus = FT_OpenEx((void*)devSettings.FTDI_Device_SerialNumber.toLocal8Bit().data(),FT_OPEN_BY_SERIAL_NUMBER,&ftHandle);
    }

    if (ftStatus != FT_OK) {
        sendErrorMessage("Can't open FT2232H device.");
        return 2;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        sendErrorMessage("USB - Can't set sync mode.");
        FTDIClose();
        return 3;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 64000, 64000);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 2);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        sendErrorMessage("USB - Can't set parameters.");
        FTDIClose();
        return 4;
    }

    FTDIConnected = true;
    return 0;
}

int CommandEngine::FTDIConnect()
{

    if (FTDIConnected) {
        sendErrorMessage("FT2232H device already connected.");
        return 1;
    }
    UCHAR Mask = 0xff;
    UCHAR Mode;

    if (currentDeviceName.isEmpty()) {
        return 1;
    }
    if (currentOpenMode == CommandEngine::by_FTDI_description) {
        ftStatus = FT_OpenEx((void*)currentDeviceName.toLocal8Bit().data(),FT_OPEN_BY_DESCRIPTION,&ftHandle);
    } else if (currentOpenMode == CommandEngine::by_FTDI_Serial) {
        ftStatus = FT_OpenEx((void*)&currentFTDIDeviceSerial,FT_OPEN_BY_SERIAL_NUMBER,&ftHandle);
    }

    if (ftStatus != FT_OK) {
        sendErrorMessage("Can't open FT2232H device.");
        return 2;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        sendErrorMessage("USB - Can't set sync mode.");
        FTDIClose();
        return 3;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 64000, 64000);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 2);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        sendErrorMessage("USB - Can't set parameters.");
        FTDIClose();
        return 4;
    }

    FTDIConnected = true;
    return 0;
}
void CommandEngine::FTDIClose()
{
    UCHAR Mask = 0xff;
    UCHAR Mode = 0x00;
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    FTDIConnected = false;
}

QString CommandEngine::getCurrentDeviceName()
{
    return currentDeviceName;
}

QList<CommandEngine::DS_Settings> CommandEngine::getDetectedFTDIDevices()
{
    QList<CommandEngine::DS_Settings> l;
    if (FTDIConnected) {
        sendErrorMessage("You must disconnect from the existing FTDI device first.");
        return l;
    }

    QStringList detectedSerialNumbers;


    FT_HANDLE ftHandleTemp;
    DWORD numDevs;
    DWORD Flags;
    DWORD ID;
    DWORD Type;
    DWORD LocId;
    char SerialNumber[16];
    char Description[64];
    // create the device information list
    ftStatus = FT_CreateDeviceInfoList(&numDevs);
    if (ftStatus != FT_OK) {
        emit newErrorMessage("Could not get a list of connected FTDI devices.");
        return l;
    }
    QStringList supportedDevList;
    supportedDevList << "DockingStation A" << "Spikegadgets MCU A";
    if (numDevs > 0) {
           // get information for device 0
           for (int i=0; i<numDevs;i++) {
               DS_Settings temp_s;

               ftStatus = FT_GetDeviceInfoDetail(i, &Flags, &Type, &ID, &LocId, SerialNumber,Description,&ftHandleTemp);
                if (ftStatus == FT_OK) {

                    //emit newConsoleOutputMessage(QString::asprintf("Device ID: %d  Device Location ID: %d SerialNum: %s Description: %s\n", ID, LocId, SerialNumber, Description));


                    temp_s.FTDI_Device_SerialNumber = QString(SerialNumber);
                    if (!detectedSerialNumbers.contains(temp_s.FTDI_Device_SerialNumber)) {
                        temp_s.FTDI_Device_SerialNumber = QString(SerialNumber);
                        detectedSerialNumbers.append(temp_s.FTDI_Device_SerialNumber);
                    } else {
                        temp_s.FTDI_Device_SerialNumber = "";
                    }

                    bool devSupported = false;
                    for (int devInd=0;devInd<supportedDevList.length();devInd++) {

                        if (supportedDevList.at(devInd).compare(QString(Description)) == 0) {
                            devSupported = true;
                            break;
                        }

                    }
                    if (devSupported) {
                        //CommandEngine::RecordingSessionInfo d;
                        //d.deviceName = Description;
                        //int ret = readDockingstation(&d, 1, 1, 0, 0, 0, 0);
                        currentDeviceName = QString(Description);
                        temp_s.devName = QString(Description);
                        //currentOpenMode = CommandEngine::by_FTDI_ID;
                        //currentOpenMode = CommandEngine::by_FTDI_description;

                        int ret = getDockSettings(&temp_s);
                        if (ret != 0) {
                            continue;
                        }



                        emit newConsoleOutputMessage("User description: " + QString(temp_s.description) + " | Device name: " + temp_s.devName + " | USB Serial: " + temp_s.FTDI_Device_SerialNumber);

                        l.append(temp_s);
                    }


                }
           }
    }
    return l;
}

void CommandEngine::abortExtraction() {
    continueExtraction = false;
}

// int main ( int argc, char *argv[] ) {
int CommandEngine::readDockingstation(RecordingSessionInfo *d, int detect, int readconfig, int pcheck, int extract, int trodesparse){
    //readDockingStation does things in order: detect, readconfig, pcheck, extract
    //if a later step is toggled, all steps before should also be toggled.
    //if trodesparse, they are all toggled.
    detect = detect || readconfig || pcheck || extract || trodesparse;
    readconfig = readconfig || pcheck || extract || trodesparse;
    pcheck = pcheck || extract || trodesparse;
    char* tmpDevice = d->deviceName;
    memset(d, 0, sizeof(*d));
    d->deviceName = tmpDevice;
    continueExtraction = true;
    uint8_t Mask = 0xff;
    uint8_t Mode;
    DWORD bytes;
    uint64_t blocks;
    uint8_t res;
    uint16_t w;
    uint64_t dw;
    uint64_t start;
    uint32_t psize;
    uint32_t auxsize;
    // uint32_t sample_size, sample_rate;
    uint32_t channels;
    uint64_t maxPackets, lastPacket, packets_read, total_packets;
    uint64_t droppedPackets;
    uint32_t progress, lastProgress;
    int i, j;
    uint32_t shift;
    //FILE *fp;
    QFile outfile;
    uint8_t Buff[131072];
    int p1, p2;
    int bytesWritten;
    //struct timeval t_start, t_end, t_diff;
    uint32_t total_sectors, packets_to_read;
    uint64_t total_bytes;
    uint32_t excess_bytes, bytes_to_write;
    unsigned char id;

    setvbuf(stdout, 0, _IONBF, 0);

    start = 1;
    psize = 0;
    // sample_size = 0;
    // sample_rate = 30;
    auxsize = 0;
    id = 0;
    d->dock_USB_communication_ok = 0;
    d->sd_card_enabled_for_record = -1;


    currentDeviceName = QString(d->deviceName);
    //emit newDebugMessage(QString::asprintf("Attempting to open %s\n", d->deviceName));
    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    if(!detect){
        FTDIClose();
        return 0;
    }



    res = FTDIGetMountingStatus(&d->sd_detected, &d->hs_detected);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    emit newDebugMessage(QString::asprintf("Detect: %s, SD: %d, Headstage: %d\n",d->deviceName, d->sd_detected, d->hs_detected));


    uint8_t getsizecmd = 0, readsectorscmd = 0;
    if(d->hs_detected){
        getsizecmd = HS_GET_SIZE_CMD;
        readsectorscmd = HS_READ_SECTORS_CMD;
    }
    else if(d->sd_detected){
        getsizecmd = SD_GET_SIZE_CMD;
        readsectorscmd = SD_READ_SECTORS_CMD;
    }

    else{

        //nothing detected
        FTDIClose();

        if (extract) {
            sendErrorMessage("Error: No SD card or headstage detected on the device.");
            return 1;
        } else {
            return 0;
        }
    }

    // get disk size

    TxBuffer[0] = getsizecmd;
    res = FTDISend(1);
    if (res != 0) {
        printf("No size.");
        FTDIClose();
        return res+10;
    }
    res = FTDIReceive(4); //expect 4 bytes back
    if (res != 0) {
        FTDIClose();
        return res+10;
    }

    blocks = (RxBuffer[3] << 24) | (RxBuffer[2] << 16) | (RxBuffer[1] << 8) | RxBuffer[0];
    d->disk_size_MB = (blocks*512)/1000;


    if(!readconfig){
        FTDIClose();
        return 0;
    }


    // read first 5 sectors to get config info and first packet
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x05; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;

    res = FTDISend(9);
    if (res != 0) {
        FTDIClose();
        return res+20;
    }
    res = FTDIReceive(2560); //expect 5 * 512 bytes back
    if (res != 0) {
        FTDIClose();
        return res+20;
    }



    // Process the data in the config sector
    channels = 0;
    for (i = 0; i < 32; i++) {
        if (RxBuffer[i]) {
            for (j = 0; j < 8; j++) {
                if ((RxBuffer[i] >> j) & 0x01) {
                channels++;
                }
            }
        }
    }
    d->channels = channels;

    if (RxBuffer[32] & 0x01)
        d->waitforstart_override = 1;
    else
        d->waitforstart_override = 0;

    if (RxBuffer[32] & 0x02)
        d->cardenablecheck_override = 1;
    else
        d->cardenablecheck_override = 0;

    if (RxBuffer[32] & 0x04)
        d->sample_rate_khz = 20;
    else
        d->sample_rate_khz = 30;

    if (RxBuffer[32] & 0x08)
        d->mag_enabled = 0;
    else
        d->mag_enabled = 1;

    if (RxBuffer[32] & 0x10)
        d->acc_enabled = 0;
    else
        d->acc_enabled = 1;

    if (RxBuffer[32] & 0x20)
        d->gyr_enabled = 0;
    else
        d->gyr_enabled = 1;

    if (RxBuffer[32] & 0x40)
        d->smartref_enabled = 1;
    else
        d->smartref_enabled = 0;

    if (RxBuffer[32] & 0x80)
        d->sample_size_bits = 12;
    else
        d->sample_size_bits = 16;

    if (RxBuffer[33] > 0) {
        d->rf_channel = (int)RxBuffer[33];
    }
    else
        d->rf_channel = 2;

    if (RxBuffer[34] > 0 || RxBuffer[35] > 0) {   
        d->autofs_threshold =  (int)(RxBuffer[35]<<8 | RxBuffer[34]);
        d->autofs_threshold_mV = (float)((int)((RxBuffer[35]<<8 | RxBuffer[34])))/5128;
    }
    else{
        d->autofs_threshold = -1;
        d->autofs_threshold_mV = -1;
    }

    if (RxBuffer[36] > 0 || RxBuffer[37] > 0) {
    //   printf("Autofs channels set to %d\n", (int)(RxBuffer[37]<<8 | RxBuffer[36]));
        d->autofs_channels = (int)(RxBuffer[37]<<8 | RxBuffer[36]);
    }
    else{
        d->autofs_channels = -1;
    }
    if (RxBuffer[38] > 0 || RxBuffer[39] > 0) {
    //   printf("Autofs timeout set to %d samples\n", (int)(RxBuffer[39]<<8 | RxBuffer[38]));
        d->autofs_timeout_samples = (int)(RxBuffer[39]<<8 | RxBuffer[38]);
    }
    else{
        d->autofs_timeout_samples = -1;
    }


    // printf("  %d channels enabled, sample size = %d bits, sample rate = %d kHz\n", channels, sample_size, sample_rate);

    if ((RxBuffer[512] == 0xaa) &&
            (RxBuffer[513] == 0x55) &&
            (RxBuffer[514] == 0xc3) &&
            (RxBuffer[515] == 0x3c)) {
        //printf("Card is enabled for recording.\n");
        d->sd_card_enabled_for_record = 1;

    } else {
        //printf("Card is NOT enabled for recording.\n");
        d->sd_card_enabled_for_record = 0;
    }



    // See if we have a hw info sector
    if (RxBuffer[512] == 0xc3) {

        // hwinfo
        time_t epoch;
        struct tm ts;
        char tbuf[80];
        unsigned int hwinfo_version;
        unsigned int serial;
        unsigned int model;
        unsigned int hwminor;
        unsigned int hwmajor;
        unsigned int hwpatch;
        uint16_t batterysize;
        uint64_t chipid;
        uint8_t sensorversion;
        uint32_t serialsensor;
        uint16_t intanchannels;

        // printf("Reading headstage info:\n");
        epoch = RxBuffer[513]<<24 | RxBuffer[514]<<16 | RxBuffer[515]<<8 | RxBuffer[516];
        serial = RxBuffer[518]<<8 | RxBuffer[517];
        model = RxBuffer[520]<<8 | RxBuffer[519];
        batterysize = RxBuffer[522]<<8 | RxBuffer[521];
        hwminor = RxBuffer[523];
        hwmajor = RxBuffer[524];
        chipid =  (uint64_t)RxBuffer[532]<<56 | (uint64_t)RxBuffer[531]<<48 | (uint64_t)RxBuffer[530]<<40 | (uint64_t)RxBuffer[529]<<32 |
                (uint64_t)RxBuffer[528]<<24 | (uint64_t)RxBuffer[527]<<16 | (uint64_t)RxBuffer[526]<<8 | (uint64_t)RxBuffer[525];
        sensorversion = RxBuffer[533];
        serialsensor = RxBuffer[537]<<24 | RxBuffer[536]<<16 | RxBuffer[535]<<8 | RxBuffer[534];
        intanchannels = RxBuffer[539]<<8 | RxBuffer[538];
        hwinfo_version = RxBuffer[540];

        if (RxBuffer[547] & 0x01)
            d->acc_enabled = 1;
        else
            d->acc_enabled = 0;

        if (RxBuffer[547] & 0x02)
            d->gyr_enabled = 1;
        else
            d->gyr_enabled = 0;

        if (RxBuffer[547] & 0x04)
            d->mag_enabled = 1;
        else
            d->mag_enabled = 0;

        if (RxBuffer[548] > 0)
            d->rf_channel = (int)RxBuffer[548];
        else
            d->rf_channel = 2;

        if (hwinfo_version == 0) {          
            start = 2;
            hwpatch = 0;
            // printf("  Headstage serial number: %u\n", serial);
            // printf("  Headstage model number: %u\n", model);
            // printf("  Headstage hw version: %u.%u\n", hwmajor, hwminor);
        } else if (hwinfo_version == 1) {
            hwpatch = RxBuffer[541];
            start = RxBuffer[545]<<24 | RxBuffer[544]<<16 | RxBuffer[543]<<8 | RxBuffer[542];
            auxsize = RxBuffer[546];
            id = RxBuffer[549];


            /*if (RxBuffer[548] > 0)
                d->rf_channel = (int)RxBuffer[548];
            else
                d->rf_channel = 2;*/
            if (RxBuffer[551] == 0) {
                // neuropixels probe
                auxsize = (((auxsize - 10) * 10) / 16) + 10;
                d->sample_rate_khz = 30;
                d->sample_size_bits = 10;
                d->smartref_enabled = 0;
            } else if (RxBuffer[551] == 1) {
                // intan probe
                if (RxBuffer[550] & 0x01)
                    d->sample_size_bits = 12;
                else
                    d->sample_size_bits = 16;

                if (RxBuffer[550] & 0x02)
                    d->sample_rate_khz = 20;
                else
                    d->sample_rate_khz = 30;
                if (RxBuffer[550] & 0x04)
                    d->smartref_enabled = 1;
                else
                    d->smartref_enabled = 0;
            }
            // printf("  Headstage serial number: %u\n", serial);
            // printf("  Headstage model number: %u\n", model);
            // printf("  Headstage hw version: %u.%u.%u\n", hwmajor, hwminor, hwpatch);
        } else if (hwinfo_version == 2) {
            // hwinfo_version 2
            hwpatch = RxBuffer[541];
            //printf("  Headstage hw version: %u.%u.%u\n", hwmajor, hwminor, hwpatch);
            start = RxBuffer[545]<<24 | RxBuffer[544]<<16 | RxBuffer[543]<<8 | RxBuffer[542];
            auxsize = RxBuffer[546];
            id = RxBuffer[549];
            if (RxBuffer[551] == 0) {
                // neuropixels probe
                printf("  NP probe\n");
                //auxsize = (((auxsize - 10) * 10) / 16) + 10;
                d->sample_rate_khz = 30;
                d->smartref_enabled = 0;

                if (RxBuffer[550] & 0x01) {
                    d->sample_size_bits = 10;
                    printf("  10 bits\n");
                } else {
                    d->sample_size_bits = 16;
                    printf("  16 bits\n");
                }
            } else if (RxBuffer[551] == 1) {
                // intan probe
                if (RxBuffer[550] & 0x01)
                    d->sample_size_bits = 12;
                else
                    d->sample_size_bits = 16;
                printf("  Buffer 550 value: %d\n",RxBuffer[550]);
                if (RxBuffer[550] & 0x10) {
                    d->sample_rate_khz = 2;

                } else if (RxBuffer[550] & 0x02) {
                    d->sample_rate_khz = 20;

                } else {
                    d->sample_rate_khz = 30;

                }

                if (RxBuffer[550] & 0x04)
                    d->smartref_enabled = 1;
                else
                    d->smartref_enabled = 0;

                //if ((RxBuffer[550] & 0x20) && (d->sample_size_bits == 16))
                    //compressed = 1;
                //else
                    //compressed = 0;
            }
        }

        d->hs_serial_number = serial;
        d->hs_model_number = model;
        d->batterysize_mAh = batterysize;
        d->hs_major_version = hwmajor;
        d->hs_minor_version = hwminor;
        d->hs_patch_version = hwpatch;
        d->chipid = chipid;
        d->sensor_version = sensorversion;
        d->serial_sensor = serialsensor;
        d->intan_channels = intanchannels;
        if (epoch > 0) {
            ts = *localtime(&epoch);
            strftime(tbuf, sizeof(tbuf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
            // printf("Recording date: %s\n", tbuf);
            strcpy(d->recording_datetime, tbuf);
        }
    } else {
        sendErrorMessage("Warning: No system info detected on the drive.");
        start = 1;
        d->hs_serial_number = -1;
        d->hs_model_number = -1;
        d->hs_major_version = 0;
        d->hs_minor_version = 0;
        d->hs_patch_version = 0;
    }

    d->configvalid = 1;
    d->start = start;

    if(!pcheck){
        FTDIClose();

        return 0;
    }


    //printf("Running pcheck\n");
    // read first 8 sectors
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = start & 0xff;
    TxBuffer[2] = (start>>8) & 0xff;
    TxBuffer[3] = (start>>16) & 0xff;
    TxBuffer[4] = (start>>24) & 0xff;
    TxBuffer[5] = 0x08; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;

    res = FTDISend(9);
    if (res != 0) {
        FTDIClose();
        return res+30;
    }
    res = FTDIReceive(4096); //expect 8 * 512 bytes back
    if (res != 0) {
        FTDIClose();
        return res+30;
    }


    // Look for packet start
    if (RxBuffer[0] != 0x55) {

        FTDIClose();
        if (extract) {
            sendErrorMessage("No data found on the SD card or headstage. Extraction aborted.");
            return 1+40;
        } else {
            return 0;
        }
    }


    // Look for packet start
    if (auxsize > 100000) {
        sendErrorMessage("Aux size calculation too large.");
        FTDIClose();
        return 2 + 40;
    }

    if (id == 0)
        id = RxBuffer[1];
    // Figure out the packet size
    // search for the next packet header
    // first assume no sensor data

    i =  1;

    if (auxsize > 0) {

        while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+auxsize+2] != 0x01) ||
                (RxBuffer[i+auxsize+3] != 0x00) || (RxBuffer[i+auxsize+4] != 0x00) ||
                (RxBuffer[i+auxsize+5] != 0x00)) && (i++ < 4095));


        if (i == 4096) {
            sendErrorMessage("Recording - Can't find the second packet start.");
            FTDIClose();
            return 2;
        }
    } else {

        while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+2] != 0x01) ||
                (RxBuffer[i+3] != 0x00) || (RxBuffer[i+4] != 0x00) ||
                (RxBuffer[i+5] != 0x00)) && (i++ < 4095));
        if (i == 4096) {
            // next try with 8 byte sensor data
            i = 1;
            auxsize = 8;
            while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+10] != 0x01) ||
                    (RxBuffer[i+11] != 0x00) || (RxBuffer[i+12] != 0x00) ||
                    (RxBuffer[i+13] != 0x00)) && (i++ < 4095));
            if (i == 4096) {
                sendErrorMessage("Recording - Can't find the second packet start.");
                FTDIClose();
                return 2;
            }
        }
    }
    psize = i;
    emit newDebugMessage(QString::asprintf("Packet size: %u bytes/packet", psize));

    //printf("\nPacket size: %u bytes/packet\n", psize);
    d->packet_size_bytes = psize;

    // figure out the recorded channel count based on the packet size
    channels = ((psize - auxsize - 6) * 8) / d->sample_size_bits;
    d->channels = channels;
    d->aux_size_bytes = auxsize;
    d->trodes_packet_size = 6 + auxsize + channels*2;

    // verify the packet size
    if ((RxBuffer[psize] != 0x55) || (RxBuffer[psize+auxsize+2] != 0x01) ||
        (RxBuffer[psize+auxsize+3] != 0x00) || (RxBuffer[psize+auxsize+4] != 0x00) ||
        (RxBuffer[psize+auxsize+5] != 0x00)) {
        sendErrorMessage("Recording - Packet size mismatch.");
        FTDIClose();
        return 2;
    }

    w = 512;
    dw = blocks * 1024;
    maxPackets = ((((ULONGLONG)dw - start) * w)/psize);
    // printf("Maximum packets on the disk = %lu\n", maxPackets);
    d->max_packets = maxPackets;

    lastPacket = 1;
    shift = 0;
    while (lastPacket < maxPackets) {
        shift++;
        lastPacket = lastPacket<<1;
    }
    lastPacket = 0;

    //Starting from a value where the most significant bit resulted in a hit, scan towards the end for sync bytes to determine the exact location of the last packet.
    for (i=shift; i>=0; i--) {
        lastPacket |= (1<<i);
        if (lastPacket >= maxPackets) {
            lastPacket &= ~(1<<i);
            continue;
        }
        res = packet_read(Buff, start, lastPacket, psize, 1, readsectorscmd);
        if (res) {
            sendErrorMessage("SD-card packet_read failed.");
            break;
        } else if ((Buff[0] != 0x55) || (Buff[1] != id)) {
            //We are likely past the end of the recording. But there is a chance that this was a failed write command, which can result in up to 1024 sectors of 0's. We test this by reading a packet a 1024 sectors later.
            if (lastPacket < (maxPackets-((1024*512)/psize)-1)) {
                res = packet_read(Buff, start, lastPacket+((1024*512)/psize)+1, psize, 1, readsectorscmd);
                //If no sync byte is found there either, then we can be fairly confident that we are past the end
                if ((Buff[0] != 0x55) || (Buff[1] != id)) {
                    //Flip the bit back to zero
                    lastPacket &= ~(1<<i);
                }
            } else {
                //Flip the bit back to zero
                lastPacket &= ~(1<<i);
            }
        }
    }
    if (lastPacket < (maxPackets-1))
        // Disk not full, don't use the last recorded packet since it might not be complete
        lastPacket--;

    // printf("Packets recorded on the disk = %lu\n", lastPacket+1);
    emit newDebugMessage(QString::asprintf("Packets recorded on the disk = %lu", lastPacket+1));
    d->recorded_packets = lastPacket+1;

    res = packet_read(Buff, start, lastPacket, psize, 1, readsectorscmd);
    if (res) {
        sendErrorMessage("SD-card packet_read failed");
        FTDIClose();
        return 2;
    }
    droppedPackets = (Buff[auxsize+5]<<24 | Buff[auxsize+4]<<16 | Buff[auxsize+3]<<8 | Buff[auxsize+2]) - lastPacket;
    d->dropped_packets = droppedPackets;
    d->packetsvalid = 1;

    //If we are not extracting any data, stop here.
    if (extract == 0) {
        FTDIClose();
        return 0;
    }

    //============================================================================================
    //This part of the function deals with extracting all of the data saved on the SD card.
    //============================================================================================

    //emit newConsoleOutputMessage(QString::asprintf("Creating file %s...\n", filename));
    //extractFileName
    bool fileOpenSuccess = false;
    if (!extractFileName.isEmpty()) {
        //timeFilePtrs[fileDataType].push_back(new QFile);
        //timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
        //if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        outfile.setFileName(extractFileName);
        fileOpenSuccess = outfile.open(QIODevice::WriteOnly);

    } /*else {
        outfile.setFileName("data.dat");
        fileOpenSuccess = outfile.open(QIODevice::WriteOnly);

    }*/
    if (!fileOpenSuccess) {
        //sendErrorMessage(QString::asprintf("Can't open file %s for writing\n", filename));
        sendErrorMessage(QString("Can't open file for writing:")+extractFileName);
        FTDIClose();
        return 2;
    }


    /*if (strlen(filename)) {
        if(strendswith(filename, ".rec")){
            //append data after trodes config (TODO: verify validity of trodesconf)
            fp = fopen(filename, "ab");
        }
        else{
            fp = fopen(filename, "wb");
        }
    } else {
        fp = fopen("data.dat", "wb");
    }
    if (!fp) {
        sendErrorMessage(QString::asprintf("Can't open file %s for writing\n", filename));
        FTDIClose();
        return 2;
    }*/


    emit newConsoleOutputMessage("Extracting data...");
    emit extractionProgress(0);

    packets_read = 0;
    progress = 0;
    lastProgress = 0;
    total_packets = lastPacket+1;

    // calculate the total number of sectors to read
    total_sectors = (psize * total_packets)/512 + 1;
    total_bytes = total_sectors * 512;

    // calculate number of excess bytes on the last sector
    excess_bytes = total_bytes - (psize * total_packets);

    //------------------------------
    uint64_t numberOfSectorsToReadPerCommand = 1000; //This is the number of sectors we will ask for per transfer cycle.
    uint64_t currentSector = start;
    uint64_t sectors_read = 0;
    //uint64_t cleared_sector_marker = start;
    uint64_t total_bytes_cleared = 0;
    int bytesToFF = 0;

    int bytesLeftToProcess = 0;
    //int bytesLeftFromLastCycle = 0;
    bool recoveryNeeded = false;
    bool badDataSectionEncountered = false;
    int numConsecRecoveriesTried = 0;


    //Main transfer cycle loop
    while (continueExtraction && (sectors_read < total_sectors)) {

        if (numberOfSectorsToReadPerCommand > (total_sectors-sectors_read)) {
            numberOfSectorsToReadPerCommand = total_sectors-sectors_read;
        }

        // Send a command to the dock (via USB) instructing it to stream the requested
        //sectors via USB
        TxBuffer[0] = readsectorscmd;
        TxBuffer[1] = currentSector & 0xff;
        TxBuffer[2] = (currentSector>>8) & 0xff;
        TxBuffer[3] = (currentSector>>16) & 0xff;
        TxBuffer[4] = (currentSector>>24) & 0xff;
        TxBuffer[5] = numberOfSectorsToReadPerCommand & 0xff;
        TxBuffer[6] = (numberOfSectorsToReadPerCommand>>8) & 0xff;
        TxBuffer[7] = (numberOfSectorsToReadPerCommand>>16) & 0xff;
        TxBuffer[8] = (numberOfSectorsToReadPerCommand>>24) & 0xff;

        res = FTDISend(9);
        if (res != 0) {
            FTDIClose();
            return res;
        }

        int packets_cleared_in_cycle = 0;
        int leftOverByteFromLastTransfer = bytesLeftToProcess;

        if (recoveryNeeded) {
            if (numConsecRecoveriesTried > 5) {
                //We have tried to recover too many times, so we need to give up and scan past this section
                badDataSectionEncountered = true;
                emit newDebugMessage("Warning: a corrupted section of the recording was encountered. Trying to skip past it.");
            }

            //A recovery from a transfer error occured, so we need to fast-foreward to the current byte in the sector
            res = FTDIReceive(bytesToFF);
            if (res != 0) {
                FTDIClose();
                outfile.close();
                return res;
            } else {
                //Recovery process completed.              
                bytesLeftToProcess = (numberOfSectorsToReadPerCommand*512)-bytesToFF;
                recoveryNeeded = false;
                numConsecRecoveriesTried++;
            }



        } else {
            //No recovery needed, so we add all of the trasfered sectors to the pile
            bytesLeftToProcess += numberOfSectorsToReadPerCommand*512;
            numConsecRecoveriesTried = 0;
        }

        int packets_left_in_buffer = bytesLeftToProcess/psize;


        // Calculate the maximum number of packets to read in each FT_Read()
        // FT_Read() max request is 65536 bytes
        if (packets_left_in_buffer > (64000/psize)) {
            packets_to_read = (64000/psize);
        } else {
            packets_to_read = packets_left_in_buffer;
        }

        while ((!recoveryNeeded) && (packets_to_read > 0)) {
            //ftStatus = FT_Read(ftHandle, RxBuffer, packets_to_read * psize, &BytesReceived);
            res = FTDIReceive(packets_to_read * psize);
            //Did the read operation work?
            if (res != 0) {
                recoveryNeeded = true;
            }

            //Is the data ok?
            bool foundGoodSync = false;
            for (i = 0; i < packets_to_read; i++) {
                if ((RxBuffer[i*psize] != 0x55) && (!badDataSectionEncountered)) {
                    //printf("\nError: Malformed data! Missing packet header\n");

                    recoveryNeeded = true;
                    break;
                }
                if (RxBuffer[i*psize] == 0x55) {
                    foundGoodSync = true;
                }
            }
            if ((foundGoodSync) && (badDataSectionEncountered)) {
                //We have scanned past a bad section of data
                badDataSectionEncountered = false;
                emit newDebugMessage("Bad data section skipped.");
            }


            if (recoveryNeeded) {
                printf("Recovery Needed\n");
                //Something went wrong in this last transfer cycle. We need to try it again.

                //Reset the dock
                TxBuffer[0] = SD_RESET_CMD;
                res = FTDISend(1);
                if (res != 0) {
                    FTDIClose();
                    outfile.close();
                    return res;
                }

                //Completely reset the USB connection
                FTDIClose();
                res = FTDIConnect();
                if (res != 0) {
                    outfile.close();
                    return res;
                }

                int bytesLeftFromLastTransfer = bytesLeftToProcess - ((int)numberOfSectorsToReadPerCommand*512);
                int bytesProcessedFromThisTransfer;


                if (bytesLeftFromLastTransfer < 0) {
                    //The issue occured after whatever was left from the previous transfer cycle,
                    //so we don't need to rewind to that.
                    bytesLeftFromLastTransfer = 0;
                    currentSector = (total_bytes_cleared/512);
                    bytesToFF = total_bytes_cleared-(currentSector*512);
                    bytesProcessedFromThisTransfer =  ((int)numberOfSectorsToReadPerCommand*512) - bytesLeftToProcess;

                } else {
                    bytesProcessedFromThisTransfer = 0;
                    currentSector = (total_bytes_cleared/512);
                    bytesToFF = total_bytes_cleared-(currentSector*512);

                }

                if (numConsecRecoveriesTried > 3) {
                    //We have tried multiple times to recover, so now we start printing out the details before
                    //we give up completely.
                    emit newDebugMessage("Attempting recovery.");
                    emit newDebugMessage(QString::asprintf("Sectors tranferred before current transfer: %d \n"
                           "Bytes that initially remained from that transfer %d \n"
                           "Bytes left to process from that transfer %d \n"
                           "Bytes processed successfully from the current transfer %d \n"
                           "Packet size: %d \n"
                           "Recovering at sector %d \n"
                           "Fast-forward num bytes: %d \n", (int)sectors_read, leftOverByteFromLastTransfer, bytesLeftFromLastTransfer,bytesProcessedFromThisTransfer,psize,(int)currentSector,(int)bytesToFF));
                }
                bytesLeftToProcess = 0;
                numberOfSectorsToReadPerCommand = 100; //Reduce read size just for the recovery
                //sectors_read += (bytesProcessedFromThisTransfer/512); //we will skip the sectors that were succesfully processed
                sectors_read = currentSector;
                currentSector += start;

            } else {

                //Everything checks out, so now we process the data and write to file.

                if (d->sample_size_bits == 10) {
                    //10-bit recording. We need to convert to 16-bit.

                    p1 = 0;
                    p2 = 0;
                    for (j = 0; j < packets_to_read; j++) {
                        // copy the first part of the packet header
                        memcpy(&Buff[p2], &RxBuffer[p1], 12);
                        p1 += 12;
                        p2 += 12;
                        // unpack the 10-bit lfp data to Buff2
                        for (i = 0; i < (auxsize-10)*8/10; i+=4) {
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                            Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                            p2 += 8;
                            p1 += 5;
                        }
                        // copy the timestamp
                        memcpy(&Buff[p2], &RxBuffer[p1], 4);
                        p1 += 4;
                        p2 += 4;
                        // unpack the 10-bit ap data to Buff
                        for (i = 0; i < channels-3; i+=4) {
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                            Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                            p2 += 8;
                            p1 += 5;
                        }
                        // must take care of runt data in case channels is not a multiple of 4
                        switch (channels % 4) {
                        case 3:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            p2 += 6;
                            p1 += 4;
                            break;
                        case 2:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            p2 += 4;
                            p1 += 3;
                            break;
                        case 1:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            p2 += 2;
                            p1 += 2;
                            break;
                        }
                    }
                    bytes_to_write = p2;
                    bytesWritten = outfile.write((char*)Buff,bytes_to_write);
                    //bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
                } else if (d->sample_size_bits == 12) {
                    // 12-bit data: expand packet data from 12 to 16 bits
                    p1 = 0;
                    p2 = 0;
                    for (j = 0; j < packets_to_read; j++) {
                        // copy the packet header
                        memcpy(&Buff[p2], &RxBuffer[p1], auxsize + 6);
                        p1 += auxsize + 6;
                        p2 += auxsize + 6;
                        // unpack the 12-bit packet data from RxBuffer to Buff
                        for (i = 0; i < channels; i+=2) {
                            Buff[p2]   = RxBuffer[p1] << 4;
                            Buff[p2+1] = (RxBuffer[p1] >> 4) | (RxBuffer[p1+1] << 4);
                            Buff[p2+2] = RxBuffer[p1+1] & 0xf0;
                            Buff[p2+3] = RxBuffer[p1+2];
                            p2 += 4;
                            p1 += 3;
                        }
                    }
                    bytes_to_write = p2;
                    bytesWritten = outfile.write((char*)Buff,bytes_to_write);
                    //bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
                } else {
                    // 16-bit data: no data processing needed
                    bytes_to_write = packets_to_read * psize;

                    //bytesWritten = outfile.write((char*)Buff,bytes_to_write);
                    //bytesWritten = fwrite(RxBuffer, 1, bytes_to_write, fp);
                    bytesWritten = outfile.write((char*)RxBuffer,bytes_to_write);
                }

                if (bytesWritten != bytes_to_write) {
                    printf("Disk write failed.\n");
                    sendErrorMessage("Disk write failed. Make sure there is enough free disk space.");
                    FTDIClose();
                    outfile.close();
                    return 3;
                }



                //Update byte markers to prepare for next buffer read cycle

                total_bytes_cleared += packets_to_read*psize;
                packets_read += packets_to_read;
                packets_left_in_buffer -= packets_to_read;
                packets_cleared_in_cycle += packets_to_read;
                bytesLeftToProcess -= packets_to_read*psize;
                // Check if packets_to_read needs to be updated
                // This would be the case for the last FT_Read()
                if (packets_to_read > packets_left_in_buffer) {
                    packets_to_read = packets_left_in_buffer;
                }

            }

        }

        if (!recoveryNeeded) {
            //Update sector markers to prepare for next USB transfer
            sectors_read += numberOfSectorsToReadPerCommand;
            currentSector += numberOfSectorsToReadPerCommand;

            // Update progress
            progress = (packets_read*100)/total_packets;
            if (progress > lastProgress) {
                printf("\r%u%%",(unsigned)progress);
                emit extractionProgress((unsigned)progress);
                lastProgress = progress;
            }

            numberOfSectorsToReadPerCommand = 1000; //It seems we may need to keep this to be below the USB incoming buffer size

        }

    }
    //-----------------------

    outfile.close();
    //fclose(fp);

    // flush out the remaining bytes left from the last sector
    if (excess_bytes) {
        res = FTDIReceive(excess_bytes);
        //ftStatus = FT_Read(ftHandle, RxBuffer, excess_bytes, &BytesReceived);
    }

    FTDIClose();

    if (!continueExtraction) {
        emit newConsoleOutputMessage("Extraction aborted.");
        return -1;
    }

    printf("\r100%%\nDone\n");

    //printf("Recorded time: %d min %d sec\n",
    //    (int)((lastPacket+1)/d->sample_rate_khz/1000/60),
    //    ((int)((lastPacket+1)/d->sample_rate_khz/1000)%60));
    //printf("Download time: %ld min %ld sec\n", t_diff.tv_sec/60, t_diff.tv_sec%60);
    //printf("Transfer rate: %f MBytes/sec\n", rate);


    return 0;
}

void CommandEngine::setExtractFileName(QString filename) {
    extractFileName = filename;
}

void CommandEngine::startDockExtract() {


    int ret = dockExtract(extractFileName);

    if (ret > 0) {
        emit extractFailed();
    } else if (ret < 0) {
        emit extractAborted();
    } else {
        emit extractSucceeded();
    }

    emit extractFinished();
}

int CommandEngine::dockExtract(QString filename) {

    //setAutoPrintOn();
    RecordingSessionInfo d;
    QByteArray devArray = currentDeviceName.toLocal8Bit();
    d.deviceName = devArray.data();
    char* fname = filename.toLocal8Bit().data();
    int ret = readDockingstation(&d, 1, 1, 1, 1, 0);

    return ret;
}


int CommandEngine::dockCardEnable(){

    unsigned char id, hwinfo_version;
    int hs_detected = 0, sd_detected = 0;
    int res;

    setvbuf(stdout, 0, _IONBF, 0);

    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    res = FTDIGetMountingStatus(&sd_detected, &hs_detected);
    if (res != 0) {
        FTDIClose();
        return res;
    }

    uint8_t readsectorscmd = 0, writesectorscmd = 0; //, erasecardcmd = 0;
    if(hs_detected){
        readsectorscmd = HS_READ_SECTORS_CMD;
        writesectorscmd = HS_WRITE_SECTORS_CMD;
    }
    else if(sd_detected){
        readsectorscmd = SD_READ_SECTORS_CMD;
        writesectorscmd = SD_WRITE_SECTORS_CMD;
    }

    else{
        //nothing detected
        sendErrorMessage("There is no SD card or headstage connected to enable for recording.");
        FTDIClose();
        return -2;
    }


    // read first 2 sectors to get config info and hw info
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x02; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;

    res = FTDISend(9);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    res = FTDIReceive(1024); // expect 2 * 512 bytes back
    if (res != 0) {
        FTDIClose();
        return res;
    }

    id = 0;

    // See if we have a hw info sector
    if (RxBuffer[512] == 0xc3) {
      hwinfo_version = RxBuffer[540];
      if (hwinfo_version > 0) {
        id = RxBuffer[549] + 1;
      }
    }
    //Card enable
    emit newDebugMessage("Writing over recorded sectors ...");
    TxBuffer[0] = writesectorscmd;
    TxBuffer[1] = 0x01; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x01; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    TxBuffer[9] = 0xaa;
    TxBuffer[10] = 0x55;
    TxBuffer[11] = 0xc3;
    TxBuffer[12] = 0x3c;
    for (int i = 4; i < 512; i++) {
      TxBuffer[i+9] = id;
    }

    res = FTDISend(521);
    if (res != 0) {
        FTDIClose();
        return res;
    }


    // uint16_t dockstatus;
    int writefinished=0;
    int writecounter=0;
    errorMessagesOn = false;
    do{
        Sleep(100);

        RxBuffer[0] = 0; RxBuffer[1] = 0; RxBuffer[2] = 0; RxBuffer[3] = 0;
        res = FTDIReceive(1);
        if(res == 0){
            writefinished = 1;
        }
        else if (res == 2){
            //Still waiting for response
            writecounter++;
        } else {
            errorMessagesOn = true;
            sendErrorMessage("Can't establish connection to eneable card.");
        }

        //printf(".");
    } while(writefinished==0 && writecounter < 30);
    //printf("\n");

    errorMessagesOn = true;

    if (writefinished == 0) {
        sendErrorMessage("No reponse recieved from hardware.");
        FTDIClose();
        return -2;
    }

    DWORD remaining = FTDIReadBytesAvailable();
    res = FTDIReceive(remaining);
    if (res != 0) {
        FTDIClose();
        return res;
    }

    FTDIClose();
    return 0;
}

int CommandEngine::dockWriteConfig(const char *cfgfile){

    char answer[100];
    int match;

    int res;
    int hs_detected = 0, sd_detected = 0;

    setvbuf(stdout, 0, _IONBF, 0);
    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    res = FTDIGetMountingStatus(&sd_detected, &hs_detected);
    if (res != 0) {
        FTDIClose();
        return res;
    }

    uint8_t readsectorscmd = 0, writesectorscmd = 0;
    if(hs_detected){
      readsectorscmd = HS_READ_SECTORS_CMD;
      writesectorscmd = HS_WRITE_SECTORS_CMD;
    }
    else if(sd_detected){
      readsectorscmd = SD_READ_SECTORS_CMD;
      writesectorscmd = SD_WRITE_SECTORS_CMD;
    }
    else{
        //nothing detected
        sendErrorMessage("There is no SD card or headstage connected to config to.");
        FTDIClose();
        return -2;
    }


    // read first 4 sectors to get config info and first packet
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x01; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;

    res = FTDISend(9);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    res = FTDIReceive(512); // expect 1 * 512 bytes back
    if (res != 0) {
        FTDIClose();
        return res;
    }


    // Is the disk used for spike recordings?
    // see if there is a partition table
    for (int i = 0x1be; i < 0x1ff; i++) {
        if (RxBuffer[i] != 0) {
            // looks like there is!
            printf("\n************************************************************\n");
            printf("WARNING: This does not look like a disk used for recordings.\n");
            printf("Are you sure this is the correct disk?\n");
            printf("************************************************************\n\n");
            printf("Continue? (yes or no) ");
            match = scanf("%s", answer);
            if (match == 0 || strcmp(answer,"yes")) {
                printf("Aborting...\n");
                FTDIClose();
                return 0;
            }
            break;
        }
    }

    //Write Config
    unsigned char buff[512];
    char str[100];
    uint8_t mask;
    float autofs_threshold;
    int autofs_channels, autofs_timeout;
    int ch;
    FILE* fpConfigFile = fopen(cfgfile, "r");
    if ( NULL == fpConfigFile ) {
        printf("Error: Could not open config file %s: %s\n", cfgfile, strerror(errno) );
        return -7;
    }
    for (int i = 0; i < 512; i++) {
        buff[i] = 0;
    }
    for (int i = 0; i < NUM_CHANNELS_PER_MODULE; i++) {
        match = fscanf (fpConfigFile, "%s", str);
        mask = 0;
        if (strlen(str) == NUM_MODULES) {
            for (int j = 0; j < NUM_MODULES; j++) {
            mask = mask << 1;
            if (str[j] == '1')
                mask |= 0x01;
            }
        }
        buff[i] = mask;
    }

    match = fscanf (fpConfigFile, "%s", str);
    mask = 0;
    if (strlen(str) == 8) {
        for (int j = 0; j < 8; j++) {
            mask = mask << 1;
            if (str[j] == '1')
                mask |= 0x01;
        }
    }
    buff[32] = mask;

    // get the rf channel
    match = fscanf(fpConfigFile, "%d", &ch);
    if (match > 0) {
        if (ch < 128)
            buff[33] = ch;
        else {
            printf("\nWarning: RF channel out of range (must be less than 128 but is set to %d\n. Setting to default", ch);
            buff[33] = 0;
        }
    }
    // get the auto-fastsettle parameters
    match = fscanf(fpConfigFile, "%f", &autofs_threshold);
    if (match > 0) {
        if ((autofs_threshold * 5128) < 32768) {
            int threshold = autofs_threshold *5128;
            buff[34] = threshold & 0xff;
            buff[35] = threshold >> 8;
        } else {
            printf("\nWarning: Autofs threshold out of range (must be less than 6.39 mV but is set to %.2f)\n", autofs_threshold);
            buff[34] = 0xff;
            buff[35] = 0x7f;
        }
    }
    match = fscanf(fpConfigFile, "%d", &autofs_channels);
    if (match > 0) {
        if (autofs_channels < 257) {
            buff[36] = autofs_channels & 0xff;
            buff[37] = autofs_channels >> 8;
        } else {
            printf("\nWarning: Autofs channels out of range (must be 256 or less but is set to %d)\n", autofs_channels);
            buff[36] = 0x00;
            buff[37] = 0x01;
        }
    }
    match = fscanf(fpConfigFile, "%d", &autofs_timeout);
    if (match > 0) {
        if (autofs_timeout < 65536) {
            buff[38] = autofs_timeout & 0xff;
            buff[39] = autofs_timeout >> 8;
        } else {
            printf("\nWarning: Autofs timeout out of range (must be less than 65536 but is set to %d)\n", autofs_timeout);
            buff[38] = 0xff;
            buff[39] = 0xff;
        }
    }
  // for(int i = 0; i < 512; ++i) printf("%02X ", buff[i]); printf("\n");
    TxBuffer[0] = writesectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x01; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    for (int i = 0; i < 512; i++) {
      TxBuffer[i+9] = buff[i];
    }

    res = FTDISend(521);
    if (res != 0) {
        FTDIClose();
        return res;
    }

    int writefinished=0;
    int writecounter=0;
    do{
        Sleep(100);       
        RxBuffer[0] = 0; RxBuffer[1] = 0; RxBuffer[2] = 0; RxBuffer[3] = 0;
        res = FTDIReceive(1);
        if (res == 0) {
            writefinished = 1;
        } else if (res == 2) {
            //Still waiting for repsonse
            writecounter++;
        } else {
            //Something went wrong
            FTDIClose();
            return res;
        }

    } while(writefinished==0 && writecounter < 30);

    FTDIClose();

    if (writefinished == 0) {
        sendErrorMessage("No reponse recieved from hardware.");
        return -2;
    } else {
      return 0;
    }

}

int CommandEngine::dockReadSerial(uint16_t* model, uint16_t* serial){

    int res;

    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    TxBuffer[0] = DOCK_GETSERIAL_CMD;
    res = FTDISend(1);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    res = FTDIReceive(4); // expect 4 bytes back
    if (res != 0) {
        FTDIClose();
        return res;
    }

    *model = RxBuffer[1]<<8 | RxBuffer[0];
    *serial = RxBuffer[3]<<8 | RxBuffer[2];

    FTDIClose();
    return 0;
}

/*int CommandEngine::dockLastPL(){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    TxBuffer[0] = DOCK_LATEST_PAYLOAD;//DOCK_LATEST_PL

    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    bytes = 1; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    // if (BytesReceived != bytes) {
    //     // did not get all bytes bytes -> timeout!
    //     printf("  Timeout! bytes received: %lu \n", BytesReceived);
    //     Mode = 0x00; //reset mode
    //     ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    //     ftStatus = FT_Close(ftHandle);
    //     return 3;
    // }
    printf("----received %lu bytes: ", BytesReceived);
    for(DWORD i =0; i < BytesReceived; ++i) printf("%02x ", RxBuffer[i]);
    printf("\n");

    bytes = RxBuffer[0]; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }
    printf("----received %lu bytes: ", BytesReceived);
    for(DWORD i =0; i < BytesReceived; ++i) printf("%02x ", RxBuffer[i]);
    printf("\n");

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}*/

int CommandEngine::dockSetRFChan(uint8_t chan){

    int res;

    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    TxBuffer[0] = DOCK_SET_RF_CHANNEL;
    TxBuffer[1] = chan;
    res = FTDISend(2);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    res = FTDIReceive(1); // expect 1 bytes back
    if (res != 0) {
        FTDIClose();
        return res;
    }

    /*for(DWORD i =0; i < BytesReceived; ++i)
      printf("%02x ", RxBuffer[i]);
    printf("\n");*/

    FTDIClose();
    return 0;
}

int CommandEngine::dockGetRFChan(uint8_t *chan){
    int res;

    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    TxBuffer[0] = DOCK_GET_RF_CHANNEL;

    res = FTDISend(1);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    res = FTDIReceive(1); // expect 1 bytes back
    if (res != 0) {
        FTDIClose();
        return res;
    }

    *chan = RxBuffer[0];
    FTDIClose();

    return 0;
}

int CommandEngine::dockEraseSD(){
    int res;

    res = FTDIConnect();
    if (res != 0) {
        return res;
    }

    TxBuffer[0] = SD_CARD_ERASE_CMD;

    res = FTDISend(1);
    if (res != 0) {
        FTDIClose();
        return res;
    }
    res = FTDIReceive(1); // expect 1 bytes back
    if (res != 0) {
        FTDIClose();
        return res;
    }

    FTDIClose();
    return 0;
}


int CommandEngine::getDockSettings(DS_Settings *s){


    if (s->devName.isEmpty()) {
        s->devName = currentDeviceName;
    }
    int ret;

    if (!s->FTDI_Device_SerialNumber.isEmpty()) {
        ret = FTDIConnect(*s,CommandEngine::by_FTDI_Serial);
    } else {
        ret = FTDIConnect(*s,CommandEngine::by_FTDI_description);
    }
    if (ret != 0) {
        return ret;
    }

    //First we get the firmware version
    TxBuffer[0] = DOCK_FIRMWARE_VERSION;
    ret = FTDISend(1);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }
    ret = FTDIReceive(3); //3 bytes expected back
    if (ret != 0) {
        FTDIClose();
        return ret;
    }

    s->firmwareMajor = RxBuffer[0];
    s->firmwareMinor = RxBuffer[1];
    s->firmwarePatch = RxBuffer[2];

    if (s->firmwareMajor == 0) {
        FTDIClose();
        return 10;
    }

    //Next we get the expected length of the settings data
    TxBuffer[0] = DOCK_USERDATA_LEN;
    ret = FTDISend(1);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }
    ret = FTDIReceive(4); //4 bytes expected back
    if (ret != 0) {
        FTDIClose();
        return ret;
    }
    int len = (RxBuffer[3] << 24) | (RxBuffer[2] << 16) | (RxBuffer[1] << 8) | RxBuffer[0];

    TxBuffer[0] = DOCK_GET_SETTINGS;
    ret = FTDISend(1);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }
    ret = FTDIReceive(len); //len bytes expected back
    if (ret != 0) {
        FTDIClose();
        return ret;
    }

    if (len < 1024) {
        memcpy(deviceSettingsBuffer,RxBuffer,len);
    } else {
        emit newErrorMessage("Device settings return message too large");
        FTDIClose();
        return 3;
    }

    QString outp;
    if (strncmp(currentDeviceName.toLocal8Bit().data(),"Spikegadgets MCU",16) == 0) {
        //We are connected to an MCU
        s->rfchan = RxBuffer[2];
        s->samplingrate = (RxBuffer[3] == 20) ? 20 : 30;
        s->description[0] = '\0';
        s->RFMode = ((RxBuffer[4] >> 2) & 0xf)+1;
        s->devName = currentDeviceName;

    } else if (strncmp(currentDeviceName.toLocal8Bit().data(),"DockingStation",14) == 0){
        //We are connected to a docking station.
        s->rfchan = RxBuffer[0];
        memcpy(s->description, RxBuffer+1, 63);
        s->description[63] = '\0';
        s->samplingrate = (RxBuffer[64] == 20) ? 20 : 30;
        s->RFMode = RxBuffer[65]+1;
        s->devName = currentDeviceName;
    } else {
        emit newErrorMessage("Not a supported device.");
        FTDIClose();
        return 4;
    }

    FTDIClose();
    return 0;
}

int CommandEngine::setDockSettings(DS_Settings s){

    int ret;
    /*DS_Settings old_s;

    ret = getDockSettings(&old_s);
    if (ret != 0) {
        emit newErrorMessage(QString::asprintf("Could not retrieve settings before writing new ones."));
        return ret;
    }*/

    ret = FTDIConnect();
    if (ret != 0) {
        return ret;
    }

    //First we get the expected length of the settings data
    TxBuffer[0] = DOCK_USERDATA_LEN;
    ret = FTDISend(1);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }
    ret = FTDIReceive(4); //4 bytes expected back
    if (ret != 0) {
        FTDIClose();
        return ret;
    }

    int len = (RxBuffer[3] << 24) | (RxBuffer[2] << 16) | (RxBuffer[1] << 8) | RxBuffer[0];

    TxBuffer[0] = DOCK_SET_SETTINGS;
    ret = FTDISend(1);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }

    memset(TxBuffer, 0, len);
    if (strncmp(currentDeviceName.toLocal8Bit().data(),"Spikegadgets MCU",16) == 0) {
        //We are connecting to an MCU, and we need to write back the whole settings structure.
        //We modify the parts we give access to, and keep the rest unchanged.
        memcpy(TxBuffer,deviceSettingsBuffer,len);      
        TxBuffer[2] = s.rfchan;
        TxBuffer[3] = s.samplingrate;
        if (s.RFMode) {
            TxBuffer[4] |= (1 << 2);
        } else {
            TxBuffer[4] &= ~(1 << 2);
        }


    } else if (strncmp(currentDeviceName.toLocal8Bit().data(),"DockingStation",14) == 0){
        //we are connecting to a logger dock
        //rf chan (2-124)

        TxBuffer[0] = s.rfchan;
        //description (63 chars)
        int slen = (63 < strlen(s.description)) ? 63 : strlen(s.description);
        memcpy(TxBuffer+1, s.description, slen);
        TxBuffer[64] = s.samplingrate;
        TxBuffer[65] = s.RFMode;

    } else {
        emit newErrorMessage("Not a supported device.");
        FTDIClose();
        return 4;
    }

    ret = FTDISend(len);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }

    FTDIClose();
    return 0;
}

int CommandEngine::getDockFirmwareVersion(uint8_t *major, uint8_t *minor, uint8_t *patch){

    int ret;

    ret = FTDIConnect();
    if (ret != 0) {
        return ret;
    }

    TxBuffer[0] = DOCK_FIRMWARE_VERSION;
    ret = FTDISend(1);
    if (ret != 0) {
        FTDIClose();
        return ret;
    }
    ret = FTDIReceive(3); //3 bytes expected back
    if (ret != 0) {
        FTDIClose();
        return ret;
    }

    *major = RxBuffer[0];
    *minor = RxBuffer[1];
    *patch = RxBuffer[2];

    FTDIClose();
    return 0;
}


void CommandEngine::printconfig(RecordingSessionInfo d){

    QString outp;
    outp.append(QString::asprintf(
                "HS_serial:         %d\n"
                "HS_model#:         %d\n"
                "HS version:        %d.%d\n"
                "# channels:        %d\n"
                "Sample size:       %d bits\n"
                "Sample rate:       %d khz\n"
                "Disk size:         %d MB\n"
                "Mag enabled:       %s\n"
                "Acc enabled:       %s\n"
                "Gyr enabled:       %s\n"
                "Smart ref:         %s\n"
                "RF channel:        %d\n"
                "Battery size:      %d mAh\n"
                /*"Chip id:           %lu\n"
                "Sensor version     %u\n"
                "Sensor serial      %u\n"
                "Intan channels     %u\n"*/,
                d.hs_serial_number,
                d.hs_model_number,
                d.hs_major_version, d.hs_minor_version,
                d.channels,
                d.sample_size_bits,
                d.sample_rate_khz,
                d.disk_size_MB,
                (d.mag_enabled ? "yes":"no"),
                (d.acc_enabled ? "yes":"no"),
                (d.gyr_enabled ? "yes":"no"),
                (d.smartref_enabled ? "yes":"no"),
                d.rf_channel,
                d.batterysize_mAh /*,
                d.chipid,
                d.sensor_version,
                d.serial_sensor,
                d.intan_channels*/));
    if(d.waitforstart_override){
        outp.append(QString::asprintf( "Wait for start:    override\n"));
    }
    if(d.cardenablecheck_override){
        outp.append(QString::asprintf( "Card enable check: override\n"));
    }
    outp.append(QString::asprintf("\n"));
    emit newConsoleOutputMessage(outp);
}

void CommandEngine::printconfigfortrodes(RecordingSessionInfo d){
    //!!!!!!!Don't change %llu for now, windows needs it to be llu for some reason ...
    QString outp = QString::asprintf( "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d "
#if defined(_WIN32) || defined(__APPLE_CC__)
            "%llu %llu %llu "
#else
            "%lu %lu %lu "
#endif
            "%s\n",
                d.hs_serial_number,
                d.hs_model_number,
                d.hs_major_version,
                d.hs_minor_version,
                d.channels,
                d.mag_enabled,
                d.acc_enabled,
                d.gyr_enabled,
                d.smartref_enabled,
                d.rf_channel,
                d.aux_size_bytes,
                d.sample_size_bits,
                d.sample_rate_khz,
                d.trodes_packet_size,
                d.waitforstart_override,
                d.cardenablecheck_override,
                d.max_packets,
                d.recorded_packets,
                d.dropped_packets,
                strlen(d.recording_datetime) ? d.recording_datetime : "");

    emit newConsoleOutputMessage(outp);
}

void CommandEngine::sendErrorMessage(QString m)
{

    if (errorMessagesOn) {
        emit newErrorMessage(m);
    }
}

void CommandEngine::printOutputMessage(QString m)
{
    printf("\33[2K\r%s\n",m.toLocal8Bit().data());
    fflush(stdout);
}

void CommandEngine::setAutoPrintOn()
{
    connect(this,&CommandEngine::newConsoleOutputMessage,this,&CommandEngine::printOutputMessage);
    connect(this,&CommandEngine::newErrorMessage,this,&CommandEngine::printOutputMessage);
    connect(this,&CommandEngine::newDebugMessage,this,&CommandEngine::printOutputMessage);
}


//------------------------------------------------------------------

CommandEngineConsoleWrapper::CommandEngineConsoleWrapper(int & argc_in, char **argv_in )
    : QCoreApplication(argc_in, argv_in),
      argc(argc_in),
      argv(argv_in)
{

    connect(&engine,&CommandEngine::newConsoleOutputMessage,this,&CommandEngineConsoleWrapper::printTextOutput);
    connect(&engine,&CommandEngine::newErrorMessage,this,&CommandEngineConsoleWrapper::printErrorOutput);
    connect(&engine,&CommandEngine::newDebugMessage,this,&CommandEngineConsoleWrapper::printVerboseOutput);
    QTimer::singleShot(0, this, SLOT(processArgumentsAndQuit())); //will run once the event loop starts with an exec call

}

void CommandEngineConsoleWrapper::processArgumentsAndQuit()
{
    exit(run()); //quits the application with the return code
}

int CommandEngineConsoleWrapper::run()
{
    bool deviceNameGiven = false;
    //char desc[] = "DockingStation A";


    int c;
    int detect=0, readconfig=0, extract=0, cardenable=0, writeconfig=0, pcheck=0, trodesparse=0,
        readserial=0, setrfchan = 0, lastpl = 0, getrfchan=0,
        erasesd=0, getdocksettings=0, list = 0, setdocksettings=0; //setsamplerate=0;
    uint16_t model=0, serial=0;
    uint8_t rfchan = 0;
    char *extractfile=NULL;
    char *configfile=NULL;
    char *devicestring=NULL;
    int setSettingsStartArg = 0;
    while (1){
        static struct option long_options[] = {
            //command line options
            //must also add the letter to the string in the function getopt_long() below
            {"cardenable",    no_argument,        0, 'c'},
            {"detect",        no_argument,        0, 'd'},
            {"extract",       required_argument,  0, 'e'},
            {"setrfchan",     required_argument,  0, 'f'},
            {"getserial",     no_argument,        0, 'g'},
            {"help",          no_argument,        0, 'h'},
            {"getrfchan",     no_argument,        0, 'i'},
            {"erasesd",       no_argument,        0, 'j'},
            // {"setsamplerate", required_argument,  0, 'k'},
            {"list",        no_argument,        0, 'l'},
            {"getdocksettings",no_argument,       0, 'm'},
            {"setdocksettings",no_argument,       0, 'n'},
            {"packetcheck",   no_argument,        0, 'p'},
            {"readconfig",    no_argument,        0, 'r'},
            {"trodesparse",   no_argument,        0, 't'},
            {"version",       no_argument,        0, 'v'},
            {"writeconfig",   required_argument,  0, 'w'},
            {"devicename",    required_argument,  0, 'x'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        //just the letter if no_argument, letter + ':' for required arguments
        c = getopt_long (argc, argv, "hdrpctglijmnvk:e:w:f:x:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c){
            case 'h':
                printHelpMenu();
                //engine.printHelpmenu(argv[0]);
                exit(0);
                break;
            case 'l':
                list = 1;
                //QList<CommandEngine::DS_Settings> l;
                engine.getDetectedFTDIDevices();
                return 0;
            case 'f':
                setrfchan=1;
                rfchan = strtoul(optarg,0,0);
                break;
            case 'i':
                getrfchan=1;
                break;
            case 'j':
                erasesd=1;
                break;
            // case 'k':
            //     setsamplerate=1;
            //     samplerate = strtoul(optarg, 0, 0);
            //     break;
            case 'm':
                getdocksettings=1;
                break;
            case 'n':
                setdocksettings=1;
                setSettingsStartArg = option_index;
                break;
            case 'd':
                detect = 1;
                break;
            case 'r':
                readconfig = 1;
                break;
            case 'p':
                pcheck = 1;
                break;
            case 'e':
                extract = 1;
                extractfile = (char*)malloc(strlen(optarg)+1);
                strcpy(extractfile, optarg);
                break;
            case 'x':
                devicestring = (char*)malloc(strlen(optarg)+1);
                strcpy(devicestring, optarg);
                engine.setCurrentDeviceName(QString(optarg));
                deviceNameGiven = true;
                break;
            case 'c':
                cardenable = 1;
                break;
            case 'w':
                writeconfig = 1;
                configfile = (char*)malloc(strlen(optarg)+1);
                strcpy(configfile, optarg);
                break;
            case 't':
                trodesparse = 1;
                break;
            case 'g':
                readserial = 1;
                // dockReadSerial();
                // return 0;
                break;
            case 'v':{
                uint8_t major, minor, patch;
                int ret = engine.getDockFirmwareVersion(&major, &minor, &patch);
                printf("Firmware version: %u.%u.%u\n", major, minor, patch);
                return ret;
            }
            default:
                printf("Problem with provided arguments. \n");
                exit(2);
        }
    }

    if (!deviceNameGiven) {
        printf("A device name must be provided with the -x option. \n");
        exit(2);
    }

    if(detect || readconfig || pcheck || extract || trodesparse){
        RecordingSessionInfo d;
        d.deviceName = devicestring;
        if (extract == 1) {
            engine.setExtractFileName(QString().fromLocal8Bit(extractfile));
        } else {
            engine.setExtractFileName("raw_data.dat");
        }

        printf("Attempting connection to %s.\n",d.deviceName);
        int ret = engine.readDockingstation(&d, detect, readconfig, pcheck, extract, trodesparse);


        if(detect){
            if(ret==0){
                printf("Docking station detected.\n");
                if(d.hs_detected)
                    printf("Device: Headstage\n");
                else if(d.sd_detected)
                    printf("Device: SD Card\n");
                else
                    printf("Device: None mounted\n");
                printf("Size: %d MB\n\n", d.disk_size_MB);
            }
            else{
                printf("Docking station not detected. Please check your USB connection\n");
                return ret;
            }
        }

        if (ret != 0) {
            //something went wrong
            //TODO: Error output here
            return ret;

        }

        if(!d.hs_detected && !d.sd_detected){
            printf("No device detected on the docking station! \nExiting\n");
            exit(0);
        }
        else if(trodesparse){
            engine.printconfigfortrodes(d);
            return 0;
        }

        if(readconfig){
            if(d.sd_detected || d.hs_detected)
                engine.printconfig(d);
            else
                printf("No device detected! \n");
        }

        if(pcheck){
            if(d.packetsvalid){
                printf( "Recording information: \n"
                    "Packet size:           %d bytes\n"
                    "Aux size:              %d bytes\n"
                    //!!!!!!!Don't change %llu for now, windows needs it to be llu for some reason ...
#if defined(_WIN32) || defined(__APPLE_CC__)
                    "Max packets:           %llu\n"
                    "Recorded packets:      %llu\n"
                    "Dropped packets:       %llu\n"
#else
                    "Max packets:           %lu\n"
                    "Recorded packets:      %lu\n"
                    "Dropped packets:       %lu\n"
#endif
                    "%s%s\n",
                    d.packet_size_bytes, d.aux_size_bytes, d.max_packets, d.recorded_packets, d.dropped_packets,
                    (strlen(d.recording_datetime) ? "Recording datetime:    " : ""),
                    (strlen(d.recording_datetime) ? d.recording_datetime : "")
                );
            }
            else{
                printf( "No recording or packet information available!\n");
                exit(1);
            }
        }
    }
    else if(cardenable){
        RecordingSessionInfo d;
        d.deviceName = devicestring;
        int ret = engine.readDockingstation(&d, 1, 0, 0, 0, 0);
        ret = engine.dockCardEnable();
        if(ret == -2){
            printf("Error running card-enable: No device mounted on the docking station!\n");
            exit(2);
        }
// #ifndef _WIN32
// #ifndef __APPLE_CC__
//         if(d.sd_detected){
//             Sleep(1000);
//             ret = dockCardEnable();
//         }
// #endif
// #endif
        if(ret){
            printf("Error running card-enable: Communication error with USB. %d \n", ret);
            exit(1);
        }
        else{
            printf("Card-enable successful. You may now use this device for recordings.\n");
        }

        return 0;
    }
    else if(writeconfig){
        int ret = engine.dockWriteConfig(configfile);
        if(ret == -2){
            printf("Error running write-config: No device mounted on the docking station!\n");
            exit(2);
        }
        else if(ret){
            printf("Error running write-config: Communication error with USB. \n");
            exit(1);
        }
        else{
            printf("Write-config successful.\n");
        }

        return 0;
    }

    else if(readserial){
        uint16_t model, serial;
        int ret = engine.dockReadSerial(&model, &serial);
        if(ret == 0){
            printf("Model:  %d\n", model);
            printf("Serial: %d\n", serial);
        }
        else{
            printf("Error reading model/serial\n");
        }
        return ret;
    }

    else if(lastpl){
        //return engine.dockLastPL();
    }
    else if(setrfchan){
        return engine.dockSetRFChan(rfchan);
    }
    else if(getrfchan){
        uint8_t retChan;
        int retCode;
        retCode = engine.dockGetRFChan(&retChan);
        if (retCode == 0) {
            printf("RF Channel: %d\n", retChan);
        }
        return retCode;
    }
    else if(erasesd){
        return engine.dockEraseSD();
    }

    else if(getdocksettings){
        /*uint8_t major, minor, patch;
        if (engine.getDockFirmwareVersion(&major, &minor, &patch)) {
            printf("Error getting firmware version.");
            return 0;
        }
        if(major==0){
            printf("Firmware version %d.%d.%d not supported\n",major,minor,patch);
            return 0;
        }*/
        CommandEngine::DS_Settings s;
        int setRet = engine.getDockSettings(&s);
        if (setRet == 0) {
            QString outp;
            outp.append(QString::asprintf("Settings\n"));
            //When printing settings, make sure to separate field title and value with a ':'
            //Necessary for the GUI to parse properly. The GUI will be able to handle as many settings as available
            //Also, print these in the order they are represented in flash memory!
            outp.append(QString::asprintf("RF Channel(2-124):                %d\n", s.rfchan));
            outp.append(QString::asprintf("Description(63 chars):            %s\n", s.description));
            outp.append(QString::asprintf("Sampling Rate kHz (20 or 30):     %d\n", s.samplingrate));
            outp.append(QString::asprintf("RF mode (1=legacy, 2=session ID): %d\n", s.RFMode));

            printf("%s",outp.toLocal8Bit().data());

        }
        return setRet;

    }
    else if(setdocksettings){
        /*uint8_t major, minor, patch;
        if (engine.getDockFirmwareVersion(&major, &minor, &patch)) {
            printf("Error getting firmware version.");
            return 0;
        } else {
            if(major==0){
                printf("Firmware version %d.%d.%d not supported\n",major,minor,patch);
                return 0;
            }
        }*/
        //return engine.setDockSettings(argc-2, argv+2);
        return setDockSettings(argc-setSettingsStartArg-1, argv+setSettingsStartArg+1);
    }
    else{
        printHelpMenu();
        //engine.printHelpmenu(argv[0]);
        return 0;
    }

    return 0;

}

int CommandEngineConsoleWrapper::setDockSettings(int argc_in, char **argv_in)
{
    CommandEngine::DS_Settings s;
    if (strncmp(engine.getCurrentDeviceName().toLocal8Bit().data(),"Spikegadgets MCU",16) == 0) {

        s.rfchan = (atoi(argv_in[0]) <= 124 && atoi(argv_in[0])>= 2) ? atoi(argv_in[0]) : 2;
        s.samplingrate = (atoi(argv_in[1]) == 20) ? 20 : 30;
        s.RFMode = (atoi(argv_in[2]) < 2) ? 0 : 1;

    } else if (strncmp(engine.getCurrentDeviceName().toLocal8Bit().data(),"DockingStation",14) == 0){
        s.rfchan = (atoi(argv_in[0]) <= 124 && atoi(argv_in[0])>= 2) ? atoi(argv_in[0]) : 2;
        int slen = (63 < strlen(argv_in[1])) ? 63 : strlen(argv_in[1]);
        memcpy(s.description, argv_in[1], slen);
        s.samplingrate = (atoi(argv_in[2]) == 20) ? 20 : 30;
        if (argc_in > 2) {
            s.RFMode = (atoi(argv_in[3]) < 2) ? 0 : 1;
        }
    } else {
        printf("Not a supported device.");
        return 4;
    }
    return engine.setDockSettings(s);
}

void CommandEngineConsoleWrapper::printHelpMenu()
{


    printf( "Usage: %s [options] [args]\n"
            "\n"
            "Options:\n"
            "  -h or --help                       Display this \n"
            "  -l or --list                       List all detected docking stations \n"
            "  -d or --detect                     Detect the status of the chosen docking station and whether a device is mounted\n"
            "  -r or --readconfig                 Read and display the configuration of mounted device\n"
            "  -p or --packetcheck                Find and display recording and packet information. \n"
            "  -c or --cardenable                 Enable the mounted device for recording, wipes out existing recording data.\n"
            "  -e or --extract=<data.dat>         Extract the existing recording data to a file passed in by the user.\n"
            "                <data.rec>            - If a file with extension .rec was passed, data will be appended to it. Proper Trodes config is NOT CHECKED.\n"
            "  -w or --writeconfig=<config.cfg>   Overwrite the configuration of mounted device with the .cfg file provided\n"
            "  -t or --trodesparse                Used by Trodes. Displays relevant config numbers and exits.\n"
            "  -g or --getserial                  Print out the model and serial number of the docking station itself.\n"
            "  -m or --getdocksettings            Print DockingStation settings like rf_channel, description, or samplingrate\n"
            "  -n or --setdocksettings            Set DockingStation settings. Pass settings as arguments in the same order that 'getdocksettings' prints them\n"
            "                                      - Example: ./dockingstation -n 3 \"My Description Here\" 20 2\n"
            "  -v or --version                    Get the firmware version number.\n"
            "\n"
            "For use with the SpikeGadgets Docking Station and wireless logging headstages. Visit http://spikegadgets.com for more info\n"
            , argv[0]);
}



void CommandEngineConsoleWrapper::printTextOutput(QString outputString)
{
    printf("\33[2K\r%s\n",outputString.toLocal8Bit().data());
    fflush(stdout);
}
void CommandEngineConsoleWrapper::printErrorOutput(QString outputString)
{
    printf("\33[2K\rError: %s\n",outputString.toLocal8Bit().data());
    fflush(stdout);
}
void CommandEngineConsoleWrapper::printVerboseOutput(QString outputString)
{
    printf("\33[2K\r%s\n",outputString.toLocal8Bit().data());
    fflush(stdout);
}

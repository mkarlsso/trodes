#ifndef MERGEPROCESS_H
#define MERGEPROCESS_H

#include "abstractprocess.h"

class MergeProcess : public AbstractProcess
{
    Q_OBJECT
public:
    explicit MergeProcess(QString datfile, QString recfile,  QString trodesconf, QString outfile, QStringList extraFlags, QWidget *parent = 0);
    void start();
signals:
public slots:
private:
    QString datFile;
    QString recFile;
    QString trodesConf;
    QString mergedOutputFile;
    QStringList extraFlags;


//    QProcess *mergeprocess;
//    QTextEdit *console;
//    void readOutput();
//    void customReadOutput(const QString &line);
    void start_merge();
//    void read_line(QString line);
    void moveFile(int code);
};

#endif // MERGEPROCESS_H

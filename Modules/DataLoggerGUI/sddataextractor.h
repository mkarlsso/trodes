#ifndef SDDATAEXTRACTOR_H
#define SDDATAEXTRACTOR_H
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <time.h>
#include <QObject>
#include <QtCore>
#include "hardwaresettings.h"
#include "DockConsoleUtility/commandengine.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "xplatform_diskio.h"
#ifdef _WIN32
#include <windows.h>
#else
#include "WinTypes.h"
#include <stdlib.h>
#endif

class SDTransferBlockRingBuffer
{
public:

    enum blockStatus {
        readyToWrite,
        readyToCheck,
        readyToSave,
        readyToCheck_Last,
        readyToSave_Last,
        check_rejected
    };

    SDTransferBlockRingBuffer(uint64_t maxTransferSize = 0, uint64_t RingSize = 0);
    ~SDTransferBlockRingBuffer();
    void setBufferSize(uint64_t maxTransferSize, uint64_t RingSize);
    uchar* getNextWritePtr(bool *ok);
    void finishBlockWrite(uint64_t size, uint64_t startSector, bool lastPacket);
    void finishBlockCheck(uint64_t size);
    void finishBlockSave();
    void rejectBlockCheck();
    uint64_t getRejectedBlockStartSector();

    uchar* getNextReadPtr(bool *ok, uint64_t *transferSize, uint64_t *startSector, bool *last);
    uchar* getNextNextReadPtr(bool *ok, uint64_t *transferSize, uint64_t *startSector, bool *last);
    uchar* getNextSavePtr(bool *ok, uint64_t *transferSize, bool *last);
    void addPacketStartLocation(int locationbyte);
    QVector<int> getSavePacketLocations();


    int bytesAvailable();
    void clear();


private:


    QList<QVector<int>> packetStartLocations;
    QList<QVector<uchar> > buffers;
    QVector<uint64_t> transferSizes;
    QVector<uint64_t> transferStartSector;
    QVector<blockStatus> transferStatus;
    uint64_t currentWriteBlock;
    uint64_t currentReadBlock;
    uint64_t currentSaveBlock;
    uint64_t currentReadBlockByte;

    uint64_t totalBytesWritten;
    uint64_t totalBytesRead;

    uint64_t totalBlocksWritten;
    uint64_t totalBlocksRead;


};

class SDDataExtractor : public QObject
{
    Q_OBJECT
public:



    enum devType {
        type_removable,
        type_dock
    };

    struct DetectedDisk {

        DriveDeviceInfo dInfo;
        devType interfaceType;
        RecordingSessionInfo sessionInfo;
        uint64_t disk_size_MB;
        QString deviceName;
        bool enabledForRecording;
    };

    SDDataExtractor();

    QList<DetectedDisk> getDetectedDisks();

    RecordingSessionInfo getCombinedSessionInfo(DetectedDisk dd); //Combine the two below info one output
    RecordingSessionInfo getSessionHeaderInfo(DetectedDisk dd); //Just the info in the header before data begins
    RecordingSessionInfo getSessionPacketInfo(DetectedDisk dd, uint32_t startSector, uint32_t auxSize, uint32_t bitsPerSample, uchar sessionID, uint64_t blocksOnDisk); //Analyze the actual data structure on the disk
    //bool transferDataFromDisk(DetectedDisk *dd, RecordingSessionInfo info, const char *filename);
    void parseSessionInfo(RecordingSessionInfo *ri);
    bool openDisk(DetectedDisk d);
    bool closeDisk();
    disk_ret  cardEnable(DetectedDisk d, bool abortIfFormatted = true);
    disk_ret readDiskSectors(uint8_t *buffer,        // buffer to hold data to be read
                         uint32_t startSector,   // sector # to start reading
                         uint32_t numSectors);
    //write data to device starting at a sector, for a number of sectors, from a buffer
    bool writeDiskSectors(uint8_t *buffer,        // buffer with data to be written
                          uint32_t startSector,   // sector # to start writing
                          uint32_t numSectors);   // number of sectors to write

    //read a number of packets from the device onto buffer
    bool readDiskPackets(    uint8_t *buffer,        // buffer to hold data to be read
                         uint32_t start,         // sector where data packets start
                         uint64_t startPacket,   // packet number to start reading from
                         uint32_t packetSize,    // size of each packet
                         uint64_t numPackets);   // number of packets to read

    bool startExtract(DetectedDisk dd, QString fileName);



    //QString getCurrentDeviceName();

    //void setCurrentDeviceName(QString dev);

    //void setExtractFileName(QString filename);
    //void startExtract(); //For multi-threaded operation
    //int setCardEnable();

    //int getRecordingSessionInfo(RecordingSessionInfo *s);


    bool continueExtraction;

private:



    //bool FTDIConnected;
    bool errorMessagesOn;
    bool extractFailureOccured;
    bool extractAbortOccured;
    bool diskOpen;
    bool transferThreadOngoing;
    bool checkThreadOngoing;
    bool saveThreadOngoing;
    bool dockThreadOngoing;
    DetectedDisk currentDisk;
    SDTransferBlockRingBuffer transferBuffer;

    void sendErrorMessage(QString m);
    void printOutputMessage(QString m);



    //char const *desc = "DockingStation A";
    //char const *desc = "Spikegadgets MCU A";

    //OpenMode currentOpenMode;
    QString currentDeviceName;
    //char currentFTDIDeviceSerial[16];

    //FT_STATUS ftStatus;
    //FT_HANDLE ftHandle;
    //DWORD BytesWritten, BytesReceived;
    uint8_t TxBuffer[1024], RxBuffer[131072];
    uint8_t deviceSettingsBuffer[1024];

    QString extractFileName;


    int strendswith(const char *str, const char *suffix);
    int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);
   /* uint8_t packet_read (
        uint8_t *buff,			// Pointer to the data buffer to store read data
        DWORD start,    // First data sector
        ULONGLONG packet,
        DWORD psize,
        WORD count,
        uint8_t readcmd);*/

public slots:

    void                        abortExtraction();
private slots:
    void                        transferThreadFinished();
    void                        checkThreadFinished();
    void                        saveThreadFinished();
    void                        dockThreadFinished();
    void                        allThreadFinished();
    void                        saveError();
    void                        transferError();
    void                        checkError();
    void                        redoRequested();
    void                        lastTransferWasChecked();
    void                        dockTransferError();

signals:

    void                        newDebugMessage(QString msg);
    void                        newConsoleOutputMessage(QString msg);
    void                        newErrorMessage(QString msg);

    void                        extractFailed();
    void                        extractAborted();
    void                        abortSaver();
    void                        abortChecker();
    void                        abortTransfer();
    void                        initiateRedoOfLastTransfer();
    void                        allowTransferToFinish();
    void                        abortDockThread();
    void                        extractSucceeded();
    void                        extractEnded();
    void                        extractionProgress(int p);
};



class SDReadThread : public QObject
{
    Q_OBJECT
public:
    SDReadThread(SDDataExtractor::DetectedDisk dd, SDTransferBlockRingBuffer *transferBuffer, SDDataExtractor* dExtractor);

public slots:
    void startTransfer();
    void abortTransfer();
    void redoLastTransfer();
    void setLastTransferChecked();

signals:
    void transferError();
    void transferComplete();
    void transferAborted();
    void finished();
    void transferProgress(int p);
    void consoleOutputMessage(QString msg);

private:
    SDDataExtractor::DetectedDisk disk;
    SDTransferBlockRingBuffer *tBuffer;
    SDDataExtractor* diskExtractor;
    bool abortTransferFlag;
    bool redoLastTransferFlag;
    bool lastTransferChecked;
    bool atEndFlag;

};

class FileWriteThread : public QObject
{
    Q_OBJECT
public:
    FileWriteThread(SDDataExtractor::DetectedDisk dd, SDTransferBlockRingBuffer *transferBuffer, QString fileName);

public slots:
    void startSave();
    void abortSave();

signals:
    void error();
    void aborted();
    void finished();
    void consoleOutputMessage(QString msg);


private:
    QFile *saveFile;
    void savePacket(uint8_t* data, int size_bytes);
    QString fileName;
    SDTransferBlockRingBuffer *tBuffer;
    SDDataExtractor::DetectedDisk disk;
    uint8_t ouputPacketBuffer[100000];

    bool abortFlag;
    bool errorFlag;
    bool atEndFlag;

};


class DataCheckThread : public QObject
{
    Q_OBJECT
public:
    DataCheckThread(SDDataExtractor::DetectedDisk dd,SDTransferBlockRingBuffer *transferBuffer);

public slots:
    void startCheck();
    void abortCheck();

signals:
    void error();
    void aborted();
    void finished();
    void requestRedo();
    void lastPacketChecked();
    void consoleOutputMessage(QString msg);


private:

    SDTransferBlockRingBuffer *tBuffer;

    bool abortFlag;
    bool errorFlag;
    bool redoFlag;
    bool atEndFlag;

    SDDataExtractor::DetectedDisk disk;

};

#endif // SDDATAEXTRACTOR_H

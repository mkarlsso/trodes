//cross platform disk io wrapper

/*
    cross platform disk io wrapper for reading mounted sd cards and other devices
*/

#ifndef XPLATFORM_DISKIO_H
#define XPLATFORM_DISKIO_H

#include <stdint.h>
#ifdef _WIN32
    #include <windows.h>
#else
    #include "WinTypes.h"
#endif

#include "errorcodes.h"
#include <list>


#define MAX_DEVICE_FNAME_LENGTH 1024
typedef struct {
    uint64_t sectorCount; //number of sectors on device
    uint64_t sectorSize; //size of each sector in bytes
    char fname[MAX_DEVICE_FNAME_LENGTH]; //device name
    char handlename[MAX_DEVICE_FNAME_LENGTH]; //handle name
    void *impl; //(DO NOT MODIFY). pointer to platform-specific implementation (like FILE* for linux, may useful for others later)
} DriveDeviceInfo;

#ifdef _WIN32
typedef struct DEVICE_NUMBER {
    DEVICE_TYPE  DeviceType;
    ULONG  DeviceNumber;
    ULONG  PartitionNumber;
} DEVICE_NUMBER, *PDEVICE_NUMBER;
#endif

//Return a list of available devices
std::list<DriveDeviceInfo> detectDevices();

//check device string, check device access/permission, get device information (sector count/size), initialize routines (FILE*, etc)
disk_ret initDevice(const char* devicename,     // name or path of device ("/dev/sda" on linux, or "1" or "2" on windows)
                    DriveDeviceInfo *device          // struct to hold device information
                    );


//close device routines
disk_ret closeDevice(DriveDeviceInfo *device);


//read data from device starting at a sector, for a number of sectors, into a buffer
disk_ret readSectors(   DriveDeviceInfo *device,     // struct with opened device info
                        uint8_t *buffer,        // buffer to hold data to be read
                        uint32_t startSector,   // sector # to start reading
                        uint32_t numSectors);   // number of sectors to read


//write data to device starting at a sector, for a number of sectors, from a buffer
disk_ret writeSectors(  DriveDeviceInfo *device,     // struct with opened device info
                        uint8_t *buffer,        // buffer with data to be written
                        uint32_t startSector,   // sector # to start writing
                        uint32_t numSectors);   // number of sectors to write


//read a number of packets from the device onto buffer
disk_ret readPackets(   DriveDeviceInfo *device,     // struct with opened device info
                        uint8_t *buffer,        // buffer to hold data to be read
                        uint32_t start,         // sector where data packets start
                        uint64_t startPacket,   // packet number to start reading from
                        uint32_t packetSize,    // size of each packet
                        uint64_t numPackets);   // number of packets to read


#endif //XPLATFORM_DISKIO_H

#include "firmwareupdater.h"
#include "jtagprocess.h"

FirmwareUpdater::FirmwareUpdater(HeadstageSettings settings, QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);

    deviceDropdown = new QComboBox;
    layout->addWidget(deviceDropdown);

    //TODO: fix for qt6
    //connect(deviceDropdown, QOverload<const QString &>::of(&QComboBox::currentIndexChanged),this, &FirmwareUpdater::dropdownItemChanged);


    loggerfirmwareText = "Headstage firmware version:\t None detected";
    dockfirmwareText = "DockingStation firmwareversion:\t Not available";
    firmwareversion = new QLabel(dockfirmwareText);
    firmwareversion->setFixedHeight(50);
    layout->addWidget(firmwareversion, 0);

    QLabel *browseFirmwareLabel = new QLabel("Select firmware file");
    layout->addWidget(browseFirmwareLabel);

    QHBoxLayout *browseLayout = new QHBoxLayout;
    QPushButton *browseButton = new QPushButton("Browse...");
    browseText = new QLineEdit;
    browseLayout->addWidget(browseButton, 0, Qt::AlignLeft);
    browseLayout->addWidget(browseText);
    layout->addLayout(browseLayout);

    updateFirmwareBtn = new QPushButton("Upload Firmware to Headstage");
    updateFirmwareBtn->setStyleSheet("font-weight: bold;"
                                   "margin-bottom: 15px;"
                                   "margin-top: 15px;"
                                   "padding: 10px;");
    layout->addWidget(updateFirmwareBtn, 0, Qt::AlignCenter);

    layout->addWidget(new QWidget, 1);

    connect(browseButton, &QPushButton::pressed, this, &FirmwareUpdater::browseFirmware);
    connect(updateFirmwareBtn, &QPushButton::pressed, this, [this](){
        uploadFirmware(browseText->text());
    });
}

void FirmwareUpdater::browseFirmware()
{
    QString firmware = QFileDialog::getOpenFileName(this, "Select firmware file", QDir::homePath(), firmwaretype);
    if(!firmware.isEmpty()){
        browseText->setText(firmware);
    }
}

void FirmwareUpdater::uploadFirmware(QString file)
{
    qDebug() << "Selected firmware file " << file;
    qDebug() << "Uploading firmware ... ";
    emit startFirmwareUpload(deviceDropdown->currentText(), file);
    updateFirmwareBtn->setEnabled(false);
}

void FirmwareUpdater::firmwareUploadFinished()
{
    updateFirmwareBtn->setEnabled(true);
}

void FirmwareUpdater::dropdownItemChanged(const QString &text){
    if(text=="Headstage/Logger"){
        firmwareversion->setText(loggerfirmwareText);
        firmwaretype = "*.svf";
    }
    else{
        firmwareversion->setText(dockfirmwareText);
        firmwaretype = "*.bit";
    }
}

void FirmwareUpdater::deviceSelected(LoggerRecording device){
    deviceDropdown->clear();
    if(device.setup == LoggerRecording::HS_DOCK){
        deviceDropdown->addItem("Headstage/Logger");
        loggerfirmwareText = "Headstage firmware version:\t"+QString::number(device.settings.majorVersion)+"."+QString::number(device.settings.minorVersion);
        firmwareversion->setText(loggerfirmwareText);
    }
    deviceDropdown->addItem("DockingStation");
}

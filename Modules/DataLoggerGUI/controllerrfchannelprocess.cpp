#include "controllerrfchannelprocess.h"

/*ControllerRFChannelProcess::ControllerRFChannelProcess(QWidget *parent)
    : AbstractProcess ("ControllerRFChannel", parent),
      mcu_success(false),
      dock_success(false),
      settingrfchan(false),
      input_rfchan(2)
{

}*/

ControllerRFChannelProcess::ControllerRFChannelProcess(QString deviceName, uint8_t rfchan, QWidget *parent)
    : AbstractProcess ("ControllerRFChannel", parent),
      mcu_success(false),
      dock_success(false),
      settingrfchan(true),
      input_rfchan(rfchan),
      device(deviceName)
{

}

void ControllerRFChannelProcess::start()
{
    //attempt both, and report which ones were successful
    dock_start_rfchan();
    waitForFinished(2000);
    if(process->exitCode()==0){
        dock_success = true;
    }
//    process->terminate();
//    process->waitForFinished();
    mcu_start_rfchan();
    waitForFinished(2000);
    if(process->exitCode()==0){
        mcu_success = true;
    }
}

void ControllerRFChannelProcess::mcu_start_rfchan()
{
    QStringList args;
    if(settingrfchan){
        args << QString::number(input_rfchan);
    }

    process->start(MCUUTILPATH + "/set_rfchannel", args);
}

void ControllerRFChannelProcess::dock_start_rfchan()
{
    QStringList args;
    if(settingrfchan){
        args << "-f" << QString::number(input_rfchan) << "-x" << device;
    }
    else{
        args << "-i";
    }

    process->start(DOCKINGPATH, args);
}


void ControllerRFChannelProcess::customReadOutput(const QString &line)
{

}

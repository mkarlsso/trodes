#ifndef SD_ERROR_CODES_H
#define SD_ERROR_CODES_H
#include <stdio.h>
//Error codes
typedef enum {
    RET_OK,                 //no error
    RET_DEVICE_NOT_FOUND,   //device string not found or valid
    RET_FNAME_LEN_TOO_LONG, //device filepath length too long
    RET_BAD_PERMISSIONS,    //program not run with elevated permissions
    RET_DEVICE_INFO_ERR,    //error fetching device info
    RET_OPEN_FD_ERR,        //error opening file descriptor for r/w
    RET_CLOSE_FD_ERR,       //error closing file descriptor
    RET_BAD_DEVICE_SEEK,    //seek location on device not valid (bad sector start)
    RET_READ_INCOMPLETE,    //read bytes less than requested
    RET_WRITE_INCOMPLETE,   //written bytes less than requested
    RET_OPEN_FILE_ERROR,    //could not open file to write data to
    RET_PACKETS_ERROR,      //did not find packets on the disk
    RET_MALFORMED_PACKETS,  //extracted packets are malformed
    RET_DISKWRITE_ERROR,    //could not write data to disk
    RET_DISKFORMATTED,      //the disk has a partition table
    RET_ERROR_OTHER,        //undocumented error
} disk_ret;

static const char* const errorstrings[] = {
    "no error",
    "device not found or valid",
    "device filepath length too long",
    "program not run with elevated permissions",
    "error fetching device info",
    "error opening device for r/w",
    "error closing device",
    "seek location on device not valid (bad sector start)",
    "read bytes less than requested",
    "written bytes less than requested",
    "could not open file to write data to",
    "did not find packets on the disk",
    "extracted packets are malformed",
    "could not write data to disk",
    "undocumented error",
};

#define printerror(e) \
    fprintf(stderr, "Error %d: %s\n", e, errorstrings[e]);


#endif //SD_ERROR_CODES_H

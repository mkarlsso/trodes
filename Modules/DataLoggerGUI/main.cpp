#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{

    //qputenv("QT_QPA_PLATFORM","windows:darkmode=0");

    QApplication a(argc, argv);
    a.setStyle("windowsvista");


    MainWindow w;
    w.show();
    if (a.arguments().length() > 1) {
        w.setEnvironmentalRecordingPath(a.arguments().at(1));
    }

    return a.exec();
}

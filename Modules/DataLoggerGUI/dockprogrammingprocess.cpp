#include "dockprogrammingprocess.h"

DockProgrammingProcess::DockProgrammingProcess(QString firmware)
    : AbstractProcess ("DockProgramProcess", nullptr), firmwarefile(firmware)
{
}

void DockProgrammingProcess::start(){
#if defined(Q_OS_WIN)
    win_start_program();
#elif defined(Q_OS_MAC)
    mac_start_program();
#elif defined(Q_OS_LINUX)
    lin_start_program();
#else
#error "OS not supported!"
#endif
}

void DockProgrammingProcess::win_start_program(){
    process->start(".\\windows_sd_util\\program.exe", {firmwarefile});
}

void DockProgrammingProcess::lin_start_program(){
    process->start("./linux_sd_util/program", {firmwarefile});
}

void DockProgrammingProcess::mac_start_program()
{
    process->start("./macos_sd_util/program", {firmwarefile});
}

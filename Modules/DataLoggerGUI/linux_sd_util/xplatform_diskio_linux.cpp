#include "WinTypes.h"
// #include "diskio_linux.h"
#include "xplatform_diskio.h"
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <array>
#include <memory>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#ifdef __APPLE_CC__
#include <sys/disk.h>
#else
#include <linux/fs.h>
#endif
#include <fcntl.h>    // O_RDONLY, O_NONBLOCK
#include <sys/ioctl.h>
#include <sys/types.h>
#define _FILE_OFFSET_BITS 64
//#define _LARGEFILE64_SOURCE


disk_ret getDeviceInfo(const char* filename, uint64_t *sectorCount, uint64_t *sectorSize){
    int fdDevice;
    fdDevice = open(filename, O_RDONLY|O_NONBLOCK);
    if ( -1 == fdDevice ) {
        return RET_OPEN_FD_ERR;
    }

#ifdef __APPLE_CC__
    if ( -1 == ioctl(fdDevice, DKIOCGETBLOCKSIZE, sectorSize) ) {
        return RET_DEVICE_INFO_ERR;
    }
    else if ( -1 == ioctl(fdDevice, DKIOCGETBLOCKCOUNT, sectorCount) ) {
        return RET_DEVICE_INFO_ERR;
    }
#else
    uint64_t deviceSize;
    if ( -1 == ioctl(fdDevice, BLKSSZGET, sectorSize) ) {
        return RET_DEVICE_INFO_ERR;
    }
    else if ( -1 == ioctl(fdDevice, BLKGETSIZE64, &deviceSize) ) {
        return RET_DEVICE_INFO_ERR;
    }
    // no errors getting device info
    else {
        *sectorCount = deviceSize / (*sectorSize);
    }
#endif
    // done getting device info, time to close
    if ( close(fdDevice) ) {
        // don't really care if there's an error closing device because it can
        // still be considered effectively closed, but it's good practice to
        // check for errors anyway
        return RET_CLOSE_FD_ERR;
    }

    return RET_OK;
}

std::list<DriveDeviceInfo> detectDevices() {
    std::list<DriveDeviceInfo> ret;

    const char* cmd = "lsblk -p -d -n -oNAME,HOTPLUG | grep '1$' | awk {'print $1'}";
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }



    std::istringstream stream(result);
    std::string singleDev;

    while (std::getline(stream,singleDev)) {

        uint64_t sectorCount;
        uint64_t sectorSize;

            DriveDeviceInfo d;
            memset(d.fname, 0, MAX_DEVICE_FNAME_LENGTH);
            strncpy(d.fname, singleDev.c_str(), MAX_DEVICE_FNAME_LENGTH);
            memset(d.handlename, 0, MAX_DEVICE_FNAME_LENGTH);
            strncpy(d.handlename, singleDev.c_str(), MAX_DEVICE_FNAME_LENGTH);
            disk_ret r = initDevice(singleDev.c_str(),&d);
            if(r != RET_OK ){
                std::cout << "Error: can't init device.\n";
                continue;
            }



            //memset(d.fname, 0, MAX_DEVICE_FNAME_LENGTH);
            //strncpy(d.fname, singleDev.c_str(), MAX_DEVICE_FNAME_LENGTH);

            //fclose((FILE*)d.impl);
            ret.push_back(d);
            closeDevice(&d);

    }


    return ret;
}



// check device string, check device access/permission, get device information
disk_ret initDevice(const char* devicename, DriveDeviceInfo *device){
    device->sectorCount = 0;
    device->sectorSize = 0;
    device->impl = NULL;
    /*memset(device->fname, 0, MAX_DEVICE_FNAME_LENGTH);
    strncpy(device->fname, devicename, MAX_DEVICE_FNAME_LENGTH);
    memset(device->handlename, 0, MAX_DEVICE_FNAME_LENGTH);
    strncpy(device->handlename, devicename, MAX_DEVICE_FNAME_LENGTH);*/


    if ( '\0' != device->fname[MAX_DEVICE_FNAME_LENGTH-1] ) {
      return RET_FNAME_LEN_TOO_LONG;
    }

    // check that file actually exists
    if ( -1 == access(devicename, F_OK) ) {

        return RET_DEVICE_NOT_FOUND;
    }

    // check requested permission
    if ( -1 == access(devicename, R_OK) ) {
        return RET_BAD_PERMISSIONS;
    }
    if ( -1 == access(devicename, W_OK) ) {
        return RET_BAD_PERMISSIONS;
    }

    // get device info
    disk_ret ret = getDeviceInfo(devicename, &device->sectorCount, &device->sectorSize);
    if( ret != RET_OK ){
        std::cout << "Error: could not get device info for" << devicename << "\n";
        return ret;
    }

    // open device for reading and/or writing
    device->impl = fopen64(device->fname, "r+");
    if(device->impl == NULL){
        return RET_OPEN_FD_ERR;
    }

    return RET_OK;
}


//close device routines
disk_ret closeDevice(DriveDeviceInfo *device){
    //memset(device->fname, 0, MAX_DEVICE_FNAME_LENGTH);
    fclose((FILE*)device->impl);
    return RET_OK;
}


disk_ret readSectors(DriveDeviceInfo *device, uint8_t *buffer, uint32_t startSector, uint32_t numSectors){
    FILE *fpDevice = (FILE*)device->impl;
    uint64_t bytesExpected, bytesRead;
    uint64_t offset = (uint64_t)startSector * device->sectorSize;



    if ( fseeko64(fpDevice, offset, SEEK_SET) ) {
        return RET_BAD_DEVICE_SEEK;
    }

    bytesExpected = numSectors * device->sectorSize;
    bytesRead = (uint64_t)fread(buffer, sizeof(uint8_t), bytesExpected, fpDevice);
    
    if ( bytesExpected != bytesRead ) {
        return RET_READ_INCOMPLETE;
    }

    return RET_OK;
}


//write data to device starting at a sector, for a number of sectors, from a buffer
disk_ret writeSectors(DriveDeviceInfo *device, uint8_t *buffer, uint32_t startSector, uint32_t numSectors){
    FILE *fpDevice = (FILE*)device->impl;
    uint64_t bytesExpected, bytesWritten;
    unsigned int offset = startSector * device->sectorSize;

    if ( fseek(fpDevice, offset, SEEK_SET) ) {
        return RET_BAD_DEVICE_SEEK;
    }

    bytesExpected = numSectors * device->sectorSize;
    bytesWritten = (uint64_t)fwrite(buffer, sizeof(uint8_t), bytesExpected, fpDevice);

    if ( bytesExpected != bytesWritten ) {
        return RET_WRITE_INCOMPLETE;
    }

    return RET_OK;
}


//read a number of packets from the device onto buffer
disk_ret readPackets(DriveDeviceInfo *device, uint8_t *buffer, uint32_t start, uint64_t startPacketIndex, uint32_t packetSize, uint64_t numPackets){
    FILE *fpDevice = (FILE*)device->impl;
    long int startSector;//, sectors, nc, rnc;
    long int offset;
    uint64_t numPacketsRead;

    // Adding "start" because we start reading packets after the first sector
    // (the configuration sector), or two (the hwinfo sector)
    startSector = (((long long)startPacketIndex * packetSize) / device->sectorSize) + start;
    offset = ((long long)startPacketIndex * packetSize) % device->sectorSize;
    // sectors = (((numPackets * packetSize) + offset)/device->sectorSize)+1;


    // position file pointer to correct sector, then move the pointer to 
    // any additional offset from there. Faster than doing 
    // fseek(fpDevice, deviceInfoObj->sectorSize + (startPacketIndex * packetSize), SEEK_SET);
    if ( fseek(fpDevice, startSector*device->sectorSize, SEEK_SET) ) {
        return RET_BAD_DEVICE_SEEK;
    }
    if ( fseek(fpDevice, offset, SEEK_CUR) ) {
        return RET_BAD_DEVICE_SEEK;
    }
    
    numPacketsRead = (uint64_t)fread(buffer, sizeof(uint8_t) * packetSize, numPackets, fpDevice);

    if ( numPacketsRead != numPackets ) {
        return RET_READ_INCOMPLETE;
    }

    return RET_OK;
}

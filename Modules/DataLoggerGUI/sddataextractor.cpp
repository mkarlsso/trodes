#include "sddataextractor.h"
#include <QMessageBox>

SDDataExtractor::SDDataExtractor()
{
    diskOpen = false;
    transferThreadOngoing = false;
    checkThreadOngoing = false;
    saveThreadOngoing = false;
    dockThreadOngoing = false;
    errorMessagesOn = true;
}

QList<SDDataExtractor::DetectedDisk> SDDataExtractor::getDetectedDisks()
{
    std::list<DriveDeviceInfo> devList = detectDevices();

    QList<SDDataExtractor::DetectedDisk> ret;

    std::list<DriveDeviceInfo>::iterator it;
    for (it = devList.begin(); it != devList.end(); ++it){

        DetectedDisk dd;
        dd.dInfo = *it;
        dd.disk_size_MB = (it->sectorSize * it->sectorCount)/1000000;
        dd.interfaceType = type_removable; //For drives directly connected to the computer
        //dd.sessionInfo = getSessionHeaderInfo(&dd);
        dd.sessionInfo = getCombinedSessionInfo(dd);
        dd.deviceName = QString().fromLocal8Bit(it->fname);
        ret.push_back(dd);

    }

    QStringList supportedDevList;
    supportedDevList << "DockingStation A" << "Spikegadgets MCU A";

    for (int devInd=0;devInd<supportedDevList.length();devInd++) {
        CommandEngine dockCommandEngine;
        dockCommandEngine.setAutoPrintOn();
        RecordingSessionInfo d;

        QByteArray devArray = supportedDevList.at(devInd).toLocal8Bit();
        d.deviceName = devArray.data();
        int dockread = dockCommandEngine.readDockingstation(&d, 1, 1, 1, 0, 0);
        if ((dockread == 0) && (d.hs_detected || d.sd_detected)){
            DetectedDisk dd;

            dd.disk_size_MB = d.disk_size_MB;
            dd.interfaceType = type_dock; //For drives mounted in the dock or MCU
            //dd.sessionInfo = getSessionHeaderInfo(&dd);
            dd.sessionInfo = d;
            dd.deviceName = QString().fromLocal8Bit(d.deviceName);
            ret.push_back(dd);

        } else if ((dockread == 0) && (devInd==0)){
            //The docking station should be visible even if no SD or headstage is mounted
            DetectedDisk dd;

            dd.disk_size_MB = d.disk_size_MB;
            dd.interfaceType = type_dock; //For drives mounted in the dock or MCU
            //dd.sessionInfo = getSessionHeaderInfo(&dd);
            dd.sessionInfo = d;
            dd.deviceName = QString().fromLocal8Bit(d.deviceName);
            ret.push_back(dd);

        }



    }

    return ret;
}

disk_ret  SDDataExtractor::cardEnable(DetectedDisk dd, bool abortIfFormatted)
{

    disk_ret returnCode;

    if (dd.interfaceType == type_removable) {
        //Card inserted directly in computer


        if (!openDisk(dd)) {
            //Open disk failed
            return disk_ret::RET_OPEN_FD_ERR;
        }

        disk_ret readCode = readDiskSectors(RxBuffer, 0, 2);

        if (readCode != RET_OK) {
            closeDisk();
            return readCode;
        }


        //See if there is a partition table. If so, we may not want to continue
        for (int i = 446; i < 512; i++) {
            if (RxBuffer[i] != 0) {
                // looks like there is!

                if (abortIfFormatted) {
                    closeDisk();
                    return disk_ret::RET_DISKFORMATTED;
                }

                for (int j = 446; j < 513; j++) {
                    // zap data including the first byte in next sector
                    RxBuffer[j] = 0;
                }
                if (!writeDiskSectors(RxBuffer, 0, 1)) {
                    closeDisk();
                    return disk_ret::RET_DISKWRITE_ERROR;
                }

                break;
            }
        }

        unsigned char id;
        unsigned int hwinfo_version = RxBuffer[540];
        // See if we have a hw info sector with version > 0.
        if (hwinfo_version > 0) {
            //If so, then the hardware will only erase the entire card only when a flag is set to 0.
            //This is to reduce the amount of wear put on the card
            //If the number is greater than 0, we add 1 every time we do an enable. After 255 rounds, this will wrap back to 0.
            id = RxBuffer[549] + 1;
        }

        //Change the 'enable' flag
        RxBuffer[0] = 0xaa;
        RxBuffer[1] = 0x55;
        RxBuffer[2] = 0xc3;
        RxBuffer[3] = 0x3c;
        for (int k = 4; k < 512; k++) {
            RxBuffer[k] = id;
        }

        //Write the data to the second sector of the disk
        if (!writeDiskSectors(RxBuffer, 1, 1)) {
            closeDisk();
            return disk_ret::RET_DISKWRITE_ERROR;
        }
        closeDisk();
        returnCode = disk_ret::RET_OK;


    } else {
        //We are going through the dock or MCU
        //We first need to extract data from the SD card
        //qDebug() << "Extracting data from" << deviceslist->getSelectedDevicePath();
        QString device = dd.deviceName;

        CommandEngine dockCommandEngine;
        dockCommandEngine.setCurrentDeviceName(device);
        int ret = dockCommandEngine.dockCardEnable();
        if(ret == -2){
            //No device mounted on the docking station
            returnCode = disk_ret::RET_OPEN_FD_ERR;
        } else if (ret > 0){
            returnCode = disk_ret::RET_DISKWRITE_ERROR;
        } else{
            returnCode = disk_ret::RET_OK;
        }


    }

    return returnCode;
}

bool SDDataExtractor::openDisk(DetectedDisk dd)
{

    if (diskOpen) {
        qDebug() << "Error: disk already open.";
        return false;
    }

    if (dd.interfaceType == type_removable) {

        disk_ret r = initDevice(dd.dInfo.handlename,&dd.dInfo);
        if(r == RET_OK){
            diskOpen = true;
            currentDisk = dd;
            return true;
        } else {
            qDebug() << "Can't init device. Error code: " << r;
            return false;
        }
    }


    return false;
}

bool SDDataExtractor::closeDisk()
{
    if (!diskOpen) {
        return true;
    }

    if (currentDisk.interfaceType == type_removable) {
        closeDevice(&currentDisk.dInfo);
        diskOpen = false;
        return true;
    } else {
        return false;
    }

}

disk_ret SDDataExtractor::readDiskSectors(uint8_t *buffer, uint32_t startSector, uint32_t numSectors)
{
    if (!diskOpen) {
        //return disk_ret::RET_ERROR_OTHER;
        return disk_ret::RET_OPEN_FD_ERR;
    }
    if (currentDisk.interfaceType == type_removable) {
        disk_ret Ret = readSectors(&currentDisk.dInfo, buffer, startSector, numSectors);
        if (Ret != RET_OK) {
            return Ret;
        } else {
            return Ret;
        }
    } else {
        //return disk_ret::RET_ERROR_OTHER;
        return disk_ret::RET_OPEN_FD_ERR;
    }
}

bool SDDataExtractor::writeDiskSectors(uint8_t *buffer, uint32_t startSector, uint32_t numSectors)
{
    if (!diskOpen) {
        return false;
    }
    if (currentDisk.interfaceType == type_removable) {
        disk_ret Ret = writeSectors(&currentDisk.dInfo, buffer, startSector, numSectors);
        if (Ret != RET_OK) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

bool SDDataExtractor::readDiskPackets(  uint8_t *buffer,        // buffer to hold data to be read
                                        uint32_t start,         // sector where data packets start
                                        uint64_t startPacket,   // packet number to start reading from
                                        uint32_t packetSize,    // size of each packet
                                        uint64_t numPackets)    // number of packets to read
{
    if (!diskOpen) {
        return false;
    }
    if (currentDisk.interfaceType == type_removable) {
        disk_ret Ret = readPackets(&currentDisk.dInfo, buffer, start, startPacket,packetSize,numPackets);

        if (Ret != RET_OK) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

RecordingSessionInfo SDDataExtractor::getCombinedSessionInfo(DetectedDisk dd)
{


    RecordingSessionInfo d = getSessionHeaderInfo(dd);
    if (!d.configvalid) {
        QMessageBox::information(NULL,"Error","Could not read header section of drive");
        return d;
    }

    RecordingSessionInfo d2 = getSessionPacketInfo(dd,d.start,d.aux_size_bytes,d.sample_size_bits,d.sessionID,dd.disk_size_MB*2);
    if (!d2.packetsvalid) {
        //QMessageBox::information(NULL,"Error","Could not read packet structure on drive");
        qDebug() << d2.max_packets << d2.recorded_packets << d2.packet_size_bytes;
        d.packetsvalid = 0;
        return d;
    }

    d.packetsvalid = 1;
    d.max_packets = d2.max_packets;
    d.packet_size_bytes = d2.packet_size_bytes;
    d.dropped_packets = d2.dropped_packets;
    d.recorded_packets = d2.recorded_packets;
    d.channels = d2.channels;

    return d;


}

RecordingSessionInfo SDDataExtractor::getSessionHeaderInfo(DetectedDisk dd)
{
    RecordingSessionInfo d;
    d.configvalid = false;
    if (dd.interfaceType == type_removable) {
        d.sd_detected = 1;
        d.hs_detected = 0;
    } else if (dd.interfaceType == type_dock) {
        //Check if headstage is directly connected to dock
        d.sd_detected = 1;
        d.hs_detected = 0;
    }

    if (!openDisk(dd)) {
        qDebug() << "Error opening disk";
        return d;
    }
    if (readDiskSectors(RxBuffer, 0x00, 0x05) != RET_OK) {
        closeDisk();
        return d;
    }
    closeDisk();

    parseSessionInfo(&d);

    return d;
}

RecordingSessionInfo SDDataExtractor::getSessionPacketInfo(DetectedDisk dd, uint32_t startSector, uint32_t auxsize, uint32_t bitsPerSample, uchar id, uint64_t blocksOnDisk)
{
    RecordingSessionInfo d;
    d.packetsvalid = false;


    if (auxsize > 100000) {
        return d;
    }

    if (!openDisk(dd)) {
         return d;
    }

    // read first 8 sectors of data
    if (readDiskSectors(RxBuffer, startSector, 0x08) != RET_OK) {
        closeDisk();
        return d;
    }


    // Look for packet start
    if (RxBuffer[0] != 0x55) {
        closeDisk();
        return d;
    }




    if (id == 0)
        id = RxBuffer[1];
    // Figure out the packet size
    // search for the next packet header
    // first assume no sensor data

    int i =  1;

    if (auxsize > 0) {

        while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+auxsize+2] != 0x01) ||
                (RxBuffer[i+auxsize+3] != 0x00) || (RxBuffer[i+auxsize+4] != 0x00) ||
                (RxBuffer[i+auxsize+5] != 0x00)) && (i++ < 4095));


        if (i == 4096) {
            closeDisk();
            return d;
        }
    } else {

        while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+2] != 0x01) ||
                (RxBuffer[i+3] != 0x00) || (RxBuffer[i+4] != 0x00) ||
                (RxBuffer[i+5] != 0x00)) && (i++ < 4095));
        if (i == 4096) {
            // next try with 8 byte sensor data
            i = 1;
            auxsize = 8;
            while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+10] != 0x01) ||
                    (RxBuffer[i+11] != 0x00) || (RxBuffer[i+12] != 0x00) ||
                    (RxBuffer[i+13] != 0x00)) && (i++ < 4095));
            if (i == 4096) {
                closeDisk();
                return d;
            }
        }
    }
    int psize = i;


    d.packet_size_bytes = psize;
    d.sample_size_bits = bitsPerSample;

    // figure out the recorded channel count based on the packet size
    int channels = ((psize - auxsize - 6) * 8) / d.sample_size_bits;
    d.channels = channels;
    d.aux_size_bytes = auxsize;
    d.trodes_packet_size = 6 + auxsize + channels*2;

    // verify the packet size
    if ((RxBuffer[psize] != 0x55) || (RxBuffer[psize+auxsize+2] != 0x01) ||
        (RxBuffer[psize+auxsize+3] != 0x00) || (RxBuffer[psize+auxsize+4] != 0x00) ||
        (RxBuffer[psize+auxsize+5] != 0x00)) {
        closeDisk();
        return d;
    }

    uint16_t w;
    uint64_t dw, maxPackets;

    w = 512;
    dw = blocksOnDisk * 1024;
    maxPackets = ((((ULONGLONG)dw - startSector) * w)/psize);
    // printf("Maximum packets on the disk = %lu\n", maxPackets);
    d.max_packets = maxPackets;

    uint64_t lastPacket = 1;
    uint32_t shift = 0;
    while (lastPacket < maxPackets) {
        shift++;
        lastPacket = lastPacket<<1;
    }
    lastPacket = 0;

    //Starting from a value where the most significant bit resulted in a hit, scan towards the end for sync bytes to determine the exact location of the last packet.
    for (i=shift; i>=0; i--) {
        lastPacket |= (1<<i);

        if (lastPacket >= maxPackets) {
            lastPacket &= ~(1<<i);
            continue;
        }
        if (!readDiskPackets(RxBuffer, startSector, lastPacket, psize, 1)) {
            lastPacket &= ~(1<<i);
            continue;

        } else if ((RxBuffer[0] != 0x55) || (RxBuffer[1] != id)) {
            //We are likely past the end of the recording. But there is a chance that this was a failed write command, which can result in up to 1024 sectors of 0's. We test this by reading a packet a 1024 sectors later.
            if (lastPacket < (maxPackets-((1024*512)/psize)-1)) {
                readDiskPackets(RxBuffer, startSector, lastPacket+((1024*512)/psize)+1, psize, 1);

                //If no sync byte is found there either, then we can be fairly confident that we are past the end
                if ((RxBuffer[0] != 0x55) || (RxBuffer[1] != id)) {
                    //Flip the bit back to zero
                    lastPacket &= ~(1<<i);
                }
            } else {
                //Flip the bit back to zero
                lastPacket &= ~(1<<i);
            }
        }
    }
    if (lastPacket < (maxPackets-1)) {
        // Disk not full, don't use the last recorded packet since it might not be complete
        lastPacket--;
    }

    d.recorded_packets = lastPacket+1;

    if (!readDiskPackets(RxBuffer, startSector, lastPacket, psize, 1)) {
        closeDisk();
        return d;
    }


    d.dropped_packets = (RxBuffer[auxsize+5]<<24 | RxBuffer[auxsize+4]<<16 | RxBuffer[auxsize+3]<<8 | RxBuffer[auxsize+2]) - lastPacket;
    d.packetsvalid = 1;

    closeDisk();


    return d;

}

void SDDataExtractor::parseSessionInfo(RecordingSessionInfo *d)
{

    // Process the data in the config sector
    int channels = 0;
    uint64_t start;
    uint32_t auxsize;
    uchar id = 0;
    for (int i = 0; i < 32; i++) {
        if (RxBuffer[i]) {
            for (int j = 0; j < 8; j++) {
                if ((RxBuffer[i] >> j) & 0x01) {
                    channels++;
                }
            }
        }
    }
    d->channels = channels;

    if (RxBuffer[32] & 0x01)
        d->waitforstart_override = 1;
    else
        d->waitforstart_override = 0;

    if (RxBuffer[32] & 0x02)
        d->cardenablecheck_override = 1;
    else
        d->cardenablecheck_override = 0;

    if (RxBuffer[32] & 0x04)
        d->sample_rate_khz = 20;
    else
        d->sample_rate_khz = 30;

    if (RxBuffer[32] & 0x08)
        d->mag_enabled = 0;
    else
        d->mag_enabled = 1;

    if (RxBuffer[32] & 0x10)
        d->acc_enabled = 0;
    else
        d->acc_enabled = 1;

    if (RxBuffer[32] & 0x20)
        d->gyr_enabled = 0;
    else
        d->gyr_enabled = 1;

    if (RxBuffer[32] & 0x40)
        d->smartref_enabled = 1;
    else
        d->smartref_enabled = 0;

    if (RxBuffer[32] & 0x80)
        d->sample_size_bits = 12;
    else
        d->sample_size_bits = 16;

    if (RxBuffer[33] > 0) {
        d->rf_channel = (int)RxBuffer[33];
    }
    else
        d->rf_channel = 2;

    if (RxBuffer[34] > 0 || RxBuffer[35] > 0) {
        d->autofs_threshold =  (int)(RxBuffer[35]<<8 | RxBuffer[34]);
        d->autofs_threshold_mV = (float)((int)((RxBuffer[35]<<8 | RxBuffer[34])))/5128;
    }
    else{
        d->autofs_threshold = -1;
        d->autofs_threshold_mV = -1;
    }

    if (RxBuffer[36] > 0 || RxBuffer[37] > 0) {
        //   printf("Autofs channels set to %d\n", (int)(RxBuffer[37]<<8 | RxBuffer[36]));
        d->autofs_channels = (int)(RxBuffer[37]<<8 | RxBuffer[36]);
    }
    else{
        d->autofs_channels = -1;
    }
    if (RxBuffer[38] > 0 || RxBuffer[39] > 0) {
        //   printf("Autofs timeout set to %d samples\n", (int)(RxBuffer[39]<<8 | RxBuffer[38]));
        d->autofs_timeout_samples = (int)(RxBuffer[39]<<8 | RxBuffer[38]);
    }
    else{
        d->autofs_timeout_samples = -1;
    }


    // printf("  %d channels enabled, sample size = %d bits, sample rate = %d kHz\n", channels, sample_size, sample_rate);

    if ((RxBuffer[512] == 0xaa) &&
        (RxBuffer[513] == 0x55) &&
        (RxBuffer[514] == 0xc3) &&
        (RxBuffer[515] == 0x3c)) {
        //printf("Card is enabled for recording.\n");
        d->sd_card_enabled_for_record = 1;

    } else {
        //printf("Card is NOT enabled for recording.\n");
        d->sd_card_enabled_for_record = 0;
    }



    // See if we have a hw info sector
    if (RxBuffer[512] == 0xc3) {

        // hwinfo
        time_t epoch;
        struct tm ts;
        char tbuf[80];
        unsigned int hwinfo_version;
        unsigned int serial;
        unsigned int model;
        unsigned int hwminor;
        unsigned int hwmajor;
        unsigned int hwpatch;
        uint16_t batterysize;
        uint64_t chipid;
        uint8_t sensorversion;
        uint32_t serialsensor;
        uint16_t intanchannels;


        // printf("Reading headstage info:\n");
        epoch = RxBuffer[513]<<24 | RxBuffer[514]<<16 | RxBuffer[515]<<8 | RxBuffer[516];
        serial = RxBuffer[518]<<8 | RxBuffer[517];
        model = RxBuffer[520]<<8 | RxBuffer[519];
        batterysize = RxBuffer[522]<<8 | RxBuffer[521];
        hwminor = RxBuffer[523];
        hwmajor = RxBuffer[524];
        chipid =  (uint64_t)RxBuffer[532]<<56 | (uint64_t)RxBuffer[531]<<48 | (uint64_t)RxBuffer[530]<<40 | (uint64_t)RxBuffer[529]<<32 |
                 (uint64_t)RxBuffer[528]<<24 | (uint64_t)RxBuffer[527]<<16 | (uint64_t)RxBuffer[526]<<8 | (uint64_t)RxBuffer[525];
        sensorversion = RxBuffer[533];
        serialsensor = RxBuffer[537]<<24 | RxBuffer[536]<<16 | RxBuffer[535]<<8 | RxBuffer[534];
        intanchannels = RxBuffer[539]<<8 | RxBuffer[538];
        hwinfo_version = RxBuffer[540];

        //printf("Hardware info version : %d\n",hwinfo_version);
        //fflush(stdout);

        if (RxBuffer[547] & 0x01)
            d->acc_enabled = 1;
        else
            d->acc_enabled = 0;

        if (RxBuffer[547] & 0x02)
            d->gyr_enabled = 1;
        else
            d->gyr_enabled = 0;

        if (RxBuffer[547] & 0x04)
            d->mag_enabled = 1;
        else
            d->mag_enabled = 0;

        if (RxBuffer[548] > 0)
            d->rf_channel = (int)RxBuffer[548];
        else
            d->rf_channel = 2;

        if (hwinfo_version == 0) {
            start = 2;
            hwpatch = 0;
            // printf("  Headstage serial number: %u\n", serial);
            // printf("  Headstage model number: %u\n", model);
            // printf("  Headstage hw version: %u.%u\n", hwmajor, hwminor);
        } else if (hwinfo_version == 1) {
            hwpatch = RxBuffer[541];
            start = RxBuffer[545]<<24 | RxBuffer[544]<<16 | RxBuffer[543]<<8 | RxBuffer[542];
            auxsize = RxBuffer[546];
            //printf("Auxsize : %d\n",auxsize);
            //fflush(stdout);
            id = RxBuffer[549];
            /*if (RxBuffer[547] & 0x01)
                d->acc_enabled = 1;
            else
                d->acc_enabled = 0;

            if (RxBuffer[547] & 0x02)
                d->gyr_enabled = 1;
            else
                d->gyr_enabled = 0;

            if (RxBuffer[547] & 0x04)
                d->mag_enabled = 1;
            else
                d->mag_enabled = 0;

            if (RxBuffer[548] > 0)
                d->rf_channel = (int)RxBuffer[548];
            else
                d->rf_channel = 2;*/
            if (RxBuffer[551] == 0) {
                // neuropixels probe
                auxsize = (((auxsize - 10) * 10) / 16) + 10;
                //printf("New auxsize : %d\n",auxsize);
                //fflush(stdout);
                d->sample_rate_khz = 30;
                d->sample_size_bits = 10;
                d->smartref_enabled = 0;
            } else if (RxBuffer[551] == 1) {
                // intan probe
                if (RxBuffer[550] & 0x01)
                    d->sample_size_bits = 12;
                else
                    d->sample_size_bits = 16;

                if (RxBuffer[550] & 0x02)
                    d->sample_rate_khz = 20;
                else
                    d->sample_rate_khz = 30;
                if (RxBuffer[550] & 0x04)
                    d->smartref_enabled = 1;
                else
                    d->smartref_enabled = 0;
            }
            // printf("  Headstage serial number: %u\n", serial);
            // printf("  Headstage model number: %u\n", model);
            // printf("  Headstage hw version: %u.%u.%u\n", hwmajor, hwminor, hwpatch);
        } else if (hwinfo_version == 2) {
            // hwinfo_version 2
            hwpatch = RxBuffer[541];
            //printf("  Headstage hw version: %u.%u.%u\n", hwmajor, hwminor, hwpatch);
            start = RxBuffer[545]<<24 | RxBuffer[544]<<16 | RxBuffer[543]<<8 | RxBuffer[542];
            auxsize = RxBuffer[546];
            id = RxBuffer[549];

            if (RxBuffer[551] == 0) {
                // neuropixels probe
                printf("  NP probe\n");
                auxsize = (((auxsize - 10) * 10) / 16) + 10;
                d->sample_rate_khz = 30;
                d->smartref_enabled = 0;

                if (RxBuffer[550] & 0x01) {
                    d->sample_size_bits = 10;
                    printf("  10 bits\n");
                } else {
                    d->sample_size_bits = 16;
                    printf("  16 bits\n");
                }
            } else if (RxBuffer[551] == 1) {
                // intan probe
                if (RxBuffer[550] & 0x01)
                    d->sample_size_bits = 12;
                else
                    d->sample_size_bits = 16;
                if (RxBuffer[550] & 0x10) {
                    d->sample_rate_khz = 2;
                } else if (RxBuffer[550] & 0x02) {
                    d->sample_rate_khz = 20;
                } else {
                    d->sample_rate_khz = 30;
                }
                if (RxBuffer[550] & 0x04)
                    d->smartref_enabled = 1;
                else
                    d->smartref_enabled = 0;

                //if ((RxBuffer[550] & 0x20) && (d->sample_size_bits == 16))
                //compressed = 1;
                //else
                //compressed = 0;
            }
        }

        d->hs_serial_number = serial;
        d->hs_model_number = model;
        d->batterysize_mAh = batterysize;
        d->hs_major_version = hwmajor;
        d->hs_minor_version = hwminor;
        d->hs_patch_version = hwpatch;
        d->chipid = chipid;
        d->sensor_version = sensorversion;
        d->serial_sensor = serialsensor;
        d->intan_channels = intanchannels;
        d->aux_size_bytes = auxsize;
        d->sessionID = id;

        if (epoch > 0) {
            ts = *localtime(&epoch);
            strftime(tbuf, sizeof(tbuf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
            // printf("Recording date: %s\n", tbuf);
            strcpy(d->recording_datetime, tbuf);
        }
    } else {

        start = 1;
        d->hs_serial_number = -1;
        d->hs_model_number = -1;
        d->hs_major_version = 0;
        d->hs_minor_version = 0;
        d->hs_patch_version = 0;
    }

    d->configvalid = 1;
    d->start = start;
}

bool SDDataExtractor::startExtract(DetectedDisk dd, QString fileName)
{


    extractAbortOccured = false;
    extractFailureOccured = false;


    if (dd.interfaceType == type_removable) {
        //Extraction is carried out by three threads:
        //Thread 1: tranfer data from SD card to buffer
        //Thread 2: check the data for sync bytes
        //Thread 3: save the data to hard drive


        if (!openDisk(dd)) {
            return false;
        }

        if (transferThreadOngoing || checkThreadOngoing || saveThreadOngoing) {
            return false;
        }

        transferBuffer.clear();
        transferBuffer.setBufferSize(1000*512,6);

        QThread* workerThread1 = new QThread();
        SDReadThread *reader = new SDReadThread(dd,&transferBuffer,this);
        reader->moveToThread(workerThread1);
        connect(workerThread1,&QThread::started,reader,&SDReadThread::startTransfer);
        connect(workerThread1,&QThread::finished,workerThread1,&QThread::deleteLater);
        connect(reader, &SDReadThread::finished, workerThread1, &QThread::quit);
        connect(reader, &SDReadThread::finished, reader, &SDReadThread::deleteLater);
        connect(reader, &SDReadThread::finished, this, &SDDataExtractor::transferThreadFinished);
        connect(reader, &SDReadThread::transferError, this, &SDDataExtractor::transferError);
        connect(this, &SDDataExtractor::abortTransfer,reader,&SDReadThread::abortTransfer,Qt::DirectConnection);
        connect(this, &SDDataExtractor::initiateRedoOfLastTransfer,reader,&SDReadThread::redoLastTransfer,Qt::DirectConnection);
        connect(this, &SDDataExtractor::allowTransferToFinish, reader, &SDReadThread::setLastTransferChecked, Qt::DirectConnection);
        connect(reader,SIGNAL(transferProgress(int)),this,SIGNAL(extractionProgress(int)));
        connect(reader,SIGNAL(consoleOutputMessage(QString)),this,SIGNAL(newConsoleOutputMessage(QString)));
        workerThread1->setObjectName("SDReader");

        QThread* workerThread2 = new QThread();
        DataCheckThread *checker = new DataCheckThread(dd,&transferBuffer);
        checker->moveToThread(workerThread2);
        connect(workerThread2,&QThread::started,checker,&DataCheckThread::startCheck);
        connect(workerThread2,&QThread::finished,workerThread2,&QThread::deleteLater);
        connect(checker, &DataCheckThread::finished, workerThread2, &QThread::quit);
        connect(checker, &DataCheckThread::finished, checker, &DataCheckThread::deleteLater);
        connect(checker, &DataCheckThread::finished, this, &SDDataExtractor::checkThreadFinished);
        connect(checker, &DataCheckThread::error, this, &SDDataExtractor::checkError);
        connect(checker, &DataCheckThread::requestRedo, this, &SDDataExtractor::redoRequested);
        connect(checker, &DataCheckThread::lastPacketChecked, this, &SDDataExtractor::lastTransferWasChecked);
        connect(this, &SDDataExtractor::abortChecker,checker,&DataCheckThread::abortCheck,Qt::DirectConnection);
        connect(checker,SIGNAL(consoleOutputMessage(QString)),this,SIGNAL(newConsoleOutputMessage(QString)));
        workerThread2->setObjectName("DataChecker");

        QThread* workerThread3 = new QThread();
        FileWriteThread *saver = new FileWriteThread(dd,&transferBuffer, fileName);
        saver->moveToThread(workerThread3);
        connect(workerThread3,&QThread::started,saver,&FileWriteThread::startSave);
        connect(workerThread3,&QThread::finished,workerThread3,&QThread::deleteLater);
        connect(saver, &FileWriteThread::finished, workerThread3, &QThread::quit);
        connect(saver, &FileWriteThread::finished, saver, &FileWriteThread::deleteLater);
        connect(saver, &FileWriteThread::finished, this, &SDDataExtractor::saveThreadFinished);
        connect(saver, &FileWriteThread::error, this, &SDDataExtractor::saveError);
        connect(this, &SDDataExtractor::abortSaver,saver,&FileWriteThread::abortSave,Qt::DirectConnection);
        connect(saver,SIGNAL(consoleOutputMessage(QString)),this,SIGNAL(newConsoleOutputMessage(QString)));
        workerThread3->setObjectName("DataSaver");


        transferThreadOngoing = true;
        checkThreadOngoing = true;
        saveThreadOngoing = true;
        workerThread1->start();
        workerThread2->start();
        workerThread3->start();

        emit newConsoleOutputMessage("Extracting data from card...");
        return true;

    } else {

        if (dockThreadOngoing) {
            return false;
        }
        //We first need to extract data from the SD card
        //qDebug() << "Extracting data from" << deviceslist->getSelectedDevicePath();
        QString device = dd.deviceName;

        CommandEngine *dockCommandEngine = new CommandEngine();

        dockCommandEngine->setCurrentDeviceName(device);
        dockCommandEngine->setExtractFileName(fileName);


        QThread *workerThread = new QThread();
        dockCommandEngine->moveToThread(workerThread);

        connect(dockCommandEngine, SIGNAL(extractFinished()), workerThread, SLOT(quit()));
        connect(dockCommandEngine,SIGNAL(extractFinished()), this, SLOT(dockThreadFinished()));
        connect(dockCommandEngine,SIGNAL(extractFinished()), dockCommandEngine, SLOT(deleteLater()));
        connect(dockCommandEngine,SIGNAL(extractionProgress(int)),this,SIGNAL(extractionProgress(int)));
        connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
        connect(workerThread, &QThread::started, dockCommandEngine, &CommandEngine::startDockExtract);
        connect(dockCommandEngine, SIGNAL(newDebugMessage(QString)), this, SIGNAL(newConsoleOutputMessage(QString)));
        connect(dockCommandEngine, SIGNAL(newErrorMessage(QString)), this,SIGNAL(newConsoleOutputMessage(QString)));
        connect(dockCommandEngine, SIGNAL(newConsoleOutputMessage(QString)), this,SIGNAL(newConsoleOutputMessage(QString)));
        connect(dockCommandEngine, SIGNAL(extractFailed()), this, SLOT(dockTransferError()));
        connect(this, &SDDataExtractor::abortDockThread,dockCommandEngine,&CommandEngine::abortExtraction, Qt::DirectConnection);
        //connect(dockCommandEngine, SIGNAL(extractAborted()), this, SLOT(resetExtractandMergeControls()));
        //connect(CancelProcessButton, &QPushButton::pressed, dockCommandEngine, &CommandEngine::abortExtraction, Qt::DirectConnection);



        workerThread->setObjectName("DataExtractionThread");
        dockThreadOngoing = true;
        workerThread->start();

        return true;


    }
}

void SDDataExtractor::transferThreadFinished()
{
    closeDisk();
    transferThreadOngoing = false;

    if (!transferThreadOngoing && !checkThreadOngoing && !saveThreadOngoing) {
        allThreadFinished();
    }
}

void SDDataExtractor::checkThreadFinished()
{

    checkThreadOngoing = false;
    if (!transferThreadOngoing && !checkThreadOngoing && !saveThreadOngoing) {
        allThreadFinished();
    }
}

void SDDataExtractor::saveThreadFinished()
{

    saveThreadOngoing = false;
    if (!transferThreadOngoing && !checkThreadOngoing && !saveThreadOngoing) {
        allThreadFinished();
    }


}

void SDDataExtractor::allThreadFinished()
{


    if (!extractFailureOccured && !extractAbortOccured) {
        printOutputMessage("Data transfer completed successfully.");
        emit extractSucceeded();
    } else if (extractFailureOccured) {
        printOutputMessage("Data transfer failed.");
        emit extractFailed();
    } else if (extractAbortOccured) {
        printOutputMessage("Data transfer aborted.");
    }

    //closeDisk();

    emit extractEnded();
}

void SDDataExtractor::dockThreadFinished()
{

    dockThreadOngoing = false;
    if (!extractFailureOccured && !extractAbortOccured) {
        printOutputMessage("Data transfer completed successfully.");
        emit extractSucceeded();
    } else if (extractFailureOccured) {
        printOutputMessage("Data transfer failed.");
        emit extractFailed();
    } else if (extractAbortOccured) {
        printOutputMessage("Data transfer aborted.");
    }

    //closeDisk();

    emit extractEnded();
}


void SDDataExtractor::abortExtraction() {
    printOutputMessage("Extract aborted.");
    extractAbortOccured = true;

    if (transferThreadOngoing) {
        emit abortTransfer();
    }
    if (saveThreadOngoing) {
        emit abortSaver();
    }
    if (checkThreadOngoing) {
        emit abortChecker();
    }
    if (dockThreadOngoing) {
        emit abortDockThread();
    }


    emit extractAborted();
}

void SDDataExtractor::dockTransferError()
{
    sendErrorMessage("Error in extracting data.");
    dockThreadOngoing = false;
    extractFailureOccured = true;
    abortExtraction();
}
void SDDataExtractor::saveError()
{
    sendErrorMessage("Error in opening file or saving data to the file.");
    saveThreadOngoing = false;
    extractFailureOccured = true;
    abortExtraction();
}
void SDDataExtractor::transferError()
{
    sendErrorMessage("Error in transfer from SD card");
    transferThreadOngoing = false;
    extractFailureOccured = true;
    abortExtraction();
}

void SDDataExtractor::checkError()
{
    sendErrorMessage("Error in transfer from SD card");
    checkThreadOngoing = false;
    extractFailureOccured = true;
    abortExtraction();
}

void SDDataExtractor::redoRequested()
{
    sendErrorMessage("Error in transfer, requesting a redo.");
    emit initiateRedoOfLastTransfer();

}

void SDDataExtractor::lastTransferWasChecked()
{
    emit allowTransferToFinish();
}

void SDDataExtractor::sendErrorMessage(QString m)
{

    if (errorMessagesOn) {
        emit newErrorMessage(m);
    }
}

void SDDataExtractor::printOutputMessage(QString m)
{

    emit newConsoleOutputMessage(m);
    //printf("\33[2K\r%s\n",m.toLocal8Bit().data());
    //fflush(stdout);
}




//--------------------------------------------------------------------------------


//Simple ring buffer with individual write blocks of equal size. Designed to be thread safe if
//one thread is write-only and another is read-only.
SDTransferBlockRingBuffer::SDTransferBlockRingBuffer(uint64_t maxTransferSize, uint64_t ringSize)
    :currentWriteBlock(0),
    currentReadBlock(0),
    currentReadBlockByte(0),
    totalBytesWritten(0),
    totalBytesRead(0),
    totalBlocksWritten(0),
    totalBlocksRead(0)
{
    for (int i = 0;i < ringSize; i++) {
        buffers.push_back(QVector<uchar>(maxTransferSize));
        packetStartLocations.push_back(QVector<int>(0));
        transferSizes.push_back(0);
        transferStartSector.push_back(0);
        transferStatus.push_back(readyToWrite);
    }
}
SDTransferBlockRingBuffer::~SDTransferBlockRingBuffer()
{
    clear();
}
void SDTransferBlockRingBuffer::setBufferSize(uint64_t maxTransferSize, uint64_t ringSize)
{
    clear();
    for (int i = 0;i < ringSize; i++) {
        buffers.push_back(QVector<uchar>(maxTransferSize));
        packetStartLocations.push_back(QVector<int>(0));
        transferSizes.push_back(0);
        transferStartSector.push_back(0);
        transferStatus.push_back(readyToWrite);
    }
}

uchar *SDTransferBlockRingBuffer::getNextWritePtr(bool *ok)
{
    //Returns a pointer to the beginning of the current block for writing

    if ((transferStatus.at(currentWriteBlock) == readyToWrite) || (transferStatus.at(currentWriteBlock) == check_rejected)) {
        uchar* rVal =  buffers[currentWriteBlock].data();
        *ok = true;
        return rVal;

    } else {
        //We can't advance the write marker because we would overwrite unprocessed data.
        //This is the main check for buffer overrun
        *ok = false;
        return nullptr;
    }

    //return buffers[currentWriteBlock].data();
}

uchar* SDTransferBlockRingBuffer::getNextReadPtr(bool *ok, uint64_t *transferSize, uint64_t *startSector, bool *last)
{
    //Returns a pointer to the beginning of the current block for reading

    if (transferStatus.at(currentReadBlock) == readyToCheck || transferStatus.at(currentReadBlock) == readyToCheck_Last) {
        uchar* rVal =  buffers[currentReadBlock].data();
        *ok = true;
        *transferSize = transferSizes[currentReadBlock];
        *startSector = transferStartSector[currentReadBlock];
        packetStartLocations[currentReadBlock].clear();
        if (transferStatus.at(currentReadBlock) == readyToCheck_Last) {
            *last = true;
        } else {
            *last = false;
        }
        return rVal;

    } else {
        //We can't advance yet

        *ok = false;
        return nullptr;
    }
}

uchar* SDTransferBlockRingBuffer::getNextNextReadPtr(bool *ok, uint64_t *transferSize, uint64_t *startSector, bool *last)
{
    //Returns a pointer to the beginning of the current block + 1 for reading
    uint64_t nextReadBlock = (currentReadBlock+1) % buffers.size();

    if (transferStatus.at(nextReadBlock) == readyToCheck || transferStatus.at(nextReadBlock) == readyToCheck_Last) {
        uchar* rVal =  buffers[nextReadBlock].data();
        *ok = true;
        *transferSize = transferSizes[nextReadBlock];
        *startSector = transferStartSector[nextReadBlock];
        *last = (transferStatus.at(nextReadBlock) == readyToCheck_Last);
        return rVal;

    } else {
        //We can't advance yet

        *ok = false;
        return nullptr;
    }
}

uint64_t SDTransferBlockRingBuffer::getRejectedBlockStartSector()
{
    return transferStartSector[currentReadBlock];
}

void SDTransferBlockRingBuffer::addPacketStartLocation(int locationbyte)
{
    packetStartLocations[currentReadBlock].push_back(locationbyte);
}

QVector<int> SDTransferBlockRingBuffer::getSavePacketLocations()
{
    return packetStartLocations[currentSaveBlock];
}


uchar* SDTransferBlockRingBuffer::getNextSavePtr(bool *ok, uint64_t *transferSize, bool *last)
{
    //Returns a pointer to the beginning of the current block for saving

    if (transferStatus.at(currentSaveBlock) == readyToSave || transferStatus.at(currentSaveBlock) == readyToSave_Last) {
        uchar* rVal =  buffers[currentSaveBlock].data();
        *ok = true;
        *transferSize = transferSizes[currentSaveBlock];
        if (transferStatus.at(currentSaveBlock) == readyToSave_Last) {
            *last = true;
        } else {
            *last = false;
        }

        return rVal;

    } else {
        //We can't advance yet

        *ok = false;
        return nullptr;
    }
}

void SDTransferBlockRingBuffer::finishBlockWrite(uint64_t size, uint64_t startSector, bool lastPacket)
{
    //Data block was read from the card and written to a buffer for processing.


    //Check if we are in redo mode (where a block needed to be re-read
    if (transferStatus[currentWriteBlock] == check_rejected) {
        if (startSector != transferStartSector[currentWriteBlock]) {
            //If the reported start sector does not match the one that needed to be re-done, dump it.
            return;
        }
    }

    //Once a block transfer is complete, we need to remember how large it was (it may not have reached the full block size).
    transferSizes[currentWriteBlock] = size;
    transferStartSector[currentWriteBlock] = startSector;
    if (lastPacket) {
        transferStatus[currentWriteBlock] = readyToCheck_Last;
    } else {
        transferStatus[currentWriteBlock] = readyToCheck;
    }
    totalBytesWritten += transferSizes[currentWriteBlock];
    currentWriteBlock = (currentWriteBlock+1) % buffers.size();
    totalBlocksWritten++;


}

void SDTransferBlockRingBuffer::finishBlockCheck(uint64_t size)
{

    if (transferStatus[currentReadBlock] == readyToCheck_Last) {
        transferStatus[currentReadBlock] = readyToSave_Last;
    } else {
        transferStatus[currentReadBlock] = readyToSave;
    }
    transferSizes[currentReadBlock] = size; //This is where we modify the transfer size for the last transfer.
    currentReadBlock = (currentReadBlock+1) % buffers.size();
}

void SDTransferBlockRingBuffer::rejectBlockCheck()
{
    transferStatus[currentReadBlock] = check_rejected;
    currentWriteBlock = currentReadBlock;

}

void SDTransferBlockRingBuffer::finishBlockSave()
{
    transferStatus[currentSaveBlock] = readyToWrite;
    currentSaveBlock = (currentSaveBlock+1) % buffers.size();
}

int SDTransferBlockRingBuffer::bytesAvailable()
{
    return totalBytesWritten-totalBytesRead;
}

void SDTransferBlockRingBuffer::clear()
{
    for (int i=0;i<buffers.size();i++) {
        buffers[i].clear();
    }
    for (int i=0;i<packetStartLocations.size();i++) {
        packetStartLocations[i].clear();
    }
    packetStartLocations.clear();
    transferSizes.clear();
    transferStartSector.clear();
    transferStatus.clear();
    currentReadBlock = 0;
    currentSaveBlock = 0;
    currentReadBlockByte = 0;
    currentWriteBlock = 0;
    totalBytesRead = 0;
    totalBytesWritten = 0;
    totalBlocksRead = 0;
    totalBlocksWritten = 0;
}
//----------------------------------------

SDReadThread::SDReadThread(SDDataExtractor::DetectedDisk dd, SDTransferBlockRingBuffer *transferBuffer, SDDataExtractor* dExtractor)
    :disk(dd),
    tBuffer(transferBuffer),
    diskExtractor(dExtractor)

{

}


void SDReadThread::startTransfer()
{

    //emit consoleOutputMessage("SD reader thread running.");

    abortTransferFlag = false;
    atEndFlag = false;
    redoLastTransferFlag = false;
    lastTransferChecked = false;

    /*if (!diskExtractor->openDisk(disk)) {
        emit transferError();
        return;
    }*/

    //uint64_t packets_read = 0;
    uint32_t progress = 0;
    uint32_t lastProgress = 0;
    uint64_t total_packets = disk.sessionInfo.recorded_packets+1;

    // calculate the total number of sectors to read
    uint64_t total_sectors = (disk.sessionInfo.packet_size_bytes * total_packets)/512 + 1;
    uint64_t total_bytes = total_sectors * 512;

    // calculate number of excess bytes on the last sector
    uint64_t excess_bytes = total_bytes - (disk.sessionInfo.packet_size_bytes * total_packets);

    //------------------------------
    uint64_t numberOfSectorsToReadPerCommand = 1000; //This is the number of sectors we will ask for per transfer cycle.
    uint64_t currentSector = disk.sessionInfo.start;
    uint64_t lastSector = disk.sessionInfo.start+total_sectors;
    //uint64_t sectors_read = 0;
    //uint64_t cleared_sector_marker = start;
    uint64_t total_bytes_cleared = 0;
    //int bytesToFF = 0;

    //int bytesLeftToProcess = 0;
    //int bytesLeftFromLastCycle = 0;
    bool recoveryNeeded = false;
    //bool badDataSectionEncountered = false;
    //int numConsecRecoveriesTried = 0;

    //tBuffer->setBufferSize(numberOfSectorsToReadPerCommand*512,4);
    uchar* writePtr;


    //Main transfer cycle loop
    while (!abortTransferFlag) {

        if (redoLastTransferFlag) {
            //If the checker thread identified a corrupted transfer, this flag will be set, indicating that this thread needs
            //to rewind to that section of the SD card and redo the transfer.

            numberOfSectorsToReadPerCommand = 1000;
            currentSector = tBuffer->getRejectedBlockStartSector();

        }


        if (currentSector >= lastSector) {
            //Wait here until the other threads are done too
            if (lastTransferChecked) {
                break;
            } else {
                QThread::msleep(100);
                continue;
            }
        }

        if (numberOfSectorsToReadPerCommand > (lastSector-currentSector)) {
            numberOfSectorsToReadPerCommand = lastSector-currentSector;
        }

        bool ok = false;
        while (!abortTransferFlag && !ok) {
            writePtr = tBuffer->getNextWritePtr(&ok);
        }
        if (abortTransferFlag) {
            break;
        }
        disk_ret rs = diskExtractor->readDiskSectors(writePtr,currentSector,numberOfSectorsToReadPerCommand);
        if (rs != RET_OK) {
            abortTransferFlag = true;
            emit consoleOutputMessage(QString("Error: cannot read from SD card. Error code %1. Current sector: %2, Sectors to read: %3").arg(rs).arg(currentSector).arg(numberOfSectorsToReadPerCommand));
            emit transferError();
            break;
        }


        bool lastPacket = false;
        if ((currentSector+numberOfSectorsToReadPerCommand) >= lastSector) {
            lastPacket = true;
            //emit consoleOutputMessage("Read final packet from SD card.");

        }


        tBuffer->finishBlockWrite(numberOfSectorsToReadPerCommand*512, currentSector, lastPacket);


        //Update sector markers to prepare for next USB transfer
        //sectors_read += numberOfSectorsToReadPerCommand;
        currentSector += numberOfSectorsToReadPerCommand;

        // Update progress
        progress = ((currentSector-disk.sessionInfo.start)*100)/total_sectors;
        //progress = (sectors_read*100)/total_sectors;
        if (progress > lastProgress) {
            //printf("\r%u%%",(unsigned)progress);
            emit transferProgress((unsigned)progress);
            lastProgress = progress;
        }

    }

    if (abortTransferFlag) {
        emit transferAborted();
    } else {
        emit transferComplete();
    }

    //emit consoleOutputMessage("Transfer thread ended.");
    emit finished();

}

void SDReadThread::abortTransfer()
{
    abortTransferFlag = true;
}

void SDReadThread::redoLastTransfer()
{
    redoLastTransferFlag = true;
}

void SDReadThread::setLastTransferChecked()
{
    lastTransferChecked = true;
}

//------------------------------------------------------

FileWriteThread::FileWriteThread(SDDataExtractor::DetectedDisk dd, SDTransferBlockRingBuffer *transferBuffer, QString file) :
    fileName(file),
    tBuffer(transferBuffer),
    disk(dd),
    abortFlag(false),
    errorFlag(false),
    atEndFlag(false)

{

}

void FileWriteThread::savePacket(uint8_t* data, int size_bytes) {

    RecordingSessionInfo* d =  &disk.sessionInfo;
    uint8_t* Buff = ouputPacketBuffer;
    uint8_t* RxBuffer = data;
    int bytesToWrite;
    int bytesWritten;


    if (d->sample_size_bits == 10) {
        //10-bit recording. We need to convert to 16-bit.

        int p1 = 0;
        int p2 = 0;

            // copy the first part of the packet header
            memcpy(&Buff[p2], &RxBuffer[p1], 12);
            p1 += 12;
            p2 += 12;
            // unpack the 10-bit lfp data to Buff2
            for (int i = 0; i < (d->aux_size_bytes-10)*8/10; i+=4) {
                Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                p2 += 8;
                p1 += 5;
            }
            // copy the timestamp
            memcpy(&Buff[p2], &RxBuffer[p1], 4);
            p1 += 4;
            p2 += 4;
            // unpack the 10-bit ap data to Buff
            for (int i = 0; i < d->channels-3; i+=4) {
                Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                p2 += 8;
                p1 += 5;
            }
            // must take care of runt data in case channels is not a multiple of 4
            switch (d->channels % 4) {
            case 3:
                Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                p2 += 6;
                p1 += 4;
                break;
            case 2:
                Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                p2 += 4;
                p1 += 3;
                break;
            case 1:
                Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                p2 += 2;
                p1 += 2;
                break;
            }

        bytesToWrite = p2;
        //printf("BytesToWrite %d %d",bytesToWrite, d->aux_size_bytes);
        //fflush(stdout);
        bytesWritten = saveFile->write((char*)Buff, bytesToWrite);

    } else if (d->sample_size_bits == 12) {
        // 12-bit data: expand packet data from 12 to 16 bits
        int p1 = 0;
        int p2 = 0;

            // copy the packet header
            memcpy(&Buff[p2], &RxBuffer[p1], d->aux_size_bytes + 6);
            p1 += d->aux_size_bytes + 6;
            p2 += d->aux_size_bytes + 6;
            // unpack the 12-bit packet data from RxBuffer to Buff
            for (int i = 0; i < d->channels; i+=2) {
                Buff[p2]   = RxBuffer[p1] << 4;
                Buff[p2+1] = (RxBuffer[p1] >> 4) | (RxBuffer[p1+1] << 4);
                Buff[p2+2] = RxBuffer[p1+1] & 0xf0;
                Buff[p2+3] = RxBuffer[p1+2];
                p2 += 4;
                p1 += 3;
            }

        bytesToWrite = p2;
        bytesWritten = saveFile->write((char*)Buff, bytesToWrite);

    } else {
        // 16-bit data: no data processing needed
        bytesToWrite = size_bytes;
        bytesWritten = saveFile->write((char*)data, bytesToWrite);

    }


    if (bytesWritten != bytesToWrite) {
        emit consoleOutputMessage(QString("Error writing to disk. Aborting."));
        errorFlag = true;
    }
}

void FileWriteThread::startSave()
{

    bool ok;
    bool lastBlock = false;
    uchar* dataPtr;
    uint64_t transferSize;
    uint8_t inputBuff[131072];
    int inputBuffLocation = 0;
    uint8_t outputBuff[131072];
    int inputPacketSize = disk.sessionInfo.packet_size_bytes;

    //emit consoleOutputMessage("Save thread running.");

    saveFile = new QFile(fileName);
    if (!saveFile->open(QIODevice::WriteOnly)) {
        emit consoleOutputMessage("Error: cannot open file to save data to.");
        errorFlag = true;

    }


    while (!abortFlag && !errorFlag && !atEndFlag) {

        ok = false;
        while (!abortFlag && !ok) {
            dataPtr = tBuffer->getNextSavePtr(&ok, &transferSize, &lastBlock);
        }

        if (!abortFlag) {

            RecordingSessionInfo* d = &disk.sessionInfo;
            QVector<int> packetLocs = tBuffer->getSavePacketLocations();

            for (int packetIndex = 0; packetIndex < packetLocs.length(); packetIndex++) {
                if (packetIndex == 0 && packetLocs[packetIndex] > 0 && packetLocs[packetIndex] < inputPacketSize) {
                    //Save the packet that started in the previous block
                    memcpy(inputBuff+inputBuffLocation,dataPtr,inputPacketSize-inputBuffLocation);
                    savePacket(inputBuff,inputPacketSize);
                }

                inputBuffLocation = 0;
                if (packetIndex < (packetLocs.length()-1)) {
                    if (packetLocs[packetIndex+1] - packetLocs[packetIndex] == inputPacketSize) {
                        //There are more packets after this one, so this one fits entirely in the block and can be saved now
                        memcpy(inputBuff,dataPtr+packetLocs[packetIndex],inputPacketSize);
                        savePacket(inputBuff,inputPacketSize);
                    } else {
                        //The number of bytes between the two sync packets does not match the packet size. This can happen if a write error happened during logging.
                        //We don't save the data.
                    }
                } else {

                    int partialSize = transferSize-packetLocs[packetIndex];
                    if (partialSize == inputPacketSize) {
                        //This last packet fits perfectly to the end of the block. Save it now.
                        memcpy(inputBuff,dataPtr+packetLocs[packetIndex],partialSize);
                        savePacket(inputBuff,inputPacketSize);
                    } else if (partialSize < inputPacketSize) {
                        //This is the start of a packet that continues in the next block. We will save it later.
                        memcpy(inputBuff,dataPtr+packetLocs[packetIndex],partialSize);
                        inputBuffLocation = partialSize;
                    }

                }
            }

            tBuffer->finishBlockSave();
            if (lastBlock) {
                //emit consoleOutputMessage("Save thread: at end of data.");
                atEndFlag = true;
            }
        }



        /*
        ok = false;
        while (!abortFlag && !ok) {
            dataPtr = tBuffer->getNextSavePtr(&ok, &transferSize, &lastBlock);
        }


        if (!abortFlag) {

            if (d->sample_size_bits == 10) {
                //10-bit recording. We need to convert to 16-bit.

                int p1 = 0;
                int p2 = 0;
                for (int j = 0; j < packets_to_read; j++) {
                    // copy the first part of the packet header
                    memcpy(&Buff[p2], &RxBuffer[p1], 12);
                    p1 += 12;
                    p2 += 12;
                    // unpack the 10-bit lfp data to Buff2
                    for (i = 0; i < (auxsize-10)*8/10; i+=4) {
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                        Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                        Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                        Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                        Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                        Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                        p2 += 8;
                        p1 += 5;
                    }
                    // copy the timestamp
                    memcpy(&Buff[p2], &RxBuffer[p1], 4);
                    p1 += 4;
                    p2 += 4;
                    // unpack the 10-bit ap data to Buff
                    for (i = 0; i < channels-3; i+=4) {
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                        Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                        Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                        Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                        Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                        Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                        p2 += 8;
                        p1 += 5;
                    }
                    // must take care of runt data in case channels is not a multiple of 4
                    switch (channels % 4) {
                    case 3:
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                        Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                        Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                        Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                        p2 += 6;
                        p1 += 4;
                        break;
                    case 2:
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                        Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                        p2 += 4;
                        p1 += 3;
                        break;
                    case 1:
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        p2 += 2;
                        p1 += 2;
                        break;
                    }
                }
                bytes_to_write = p2;
                bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
            } else if (d->sample_size_bits == 12) {
                // 12-bit data: expand packet data from 12 to 16 bits
                p1 = 0;
                p2 = 0;
                for (j = 0; j < packets_to_read; j++) {
                    // copy the packet header
                    memcpy(&Buff[p2], &RxBuffer[p1], auxsize + 6);
                    p1 += auxsize + 6;
                    p2 += auxsize + 6;
                    // unpack the 12-bit packet data from RxBuffer to Buff
                    for (i = 0; i < channels; i+=2) {
                        Buff[p2]   = RxBuffer[p1] << 4;
                        Buff[p2+1] = (RxBuffer[p1] >> 4) | (RxBuffer[p1+1] << 4);
                        Buff[p2+2] = RxBuffer[p1+1] & 0xf0;
                        Buff[p2+3] = RxBuffer[p1+2];
                        p2 += 4;
                        p1 += 3;
                    }
                }
                bytes_to_write = p2;
                bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
            } else {
                // 16-bit data: no data processing needed
                bytes_to_write = packets_to_read * psize;
                bytesWritten = fwrite(RxBuffer, 1, bytes_to_write, fp);
            }

            if (bytesWritten != bytes_to_write) {
                sendErrorMessage("Disk write failed. Make sure there is enough free disk space.");
                FTDIClose();
                return 3;
            }*/



            /*uint64_t tSize = saveFile->write((char*)dataPtr, transferSize);

            if (tSize != transferSize) {
                emit consoleOutputMessage(QString("Error writing to disk: transfer size %1, tSize %2").arg(transferSize).arg(tSize));
                errorFlag = true;
            }
            tBuffer->finishBlockSave();
            if (lastBlock) {
                //emit consoleOutputMessage("Save thread: at end of data.");
                atEndFlag = true;
            }
        }*/
    }

    if (saveFile->isOpen()) {
        saveFile->flush();
        saveFile->close();
    }

    saveFile->deleteLater();

    if (abortFlag) {
        emit aborted();
    } else if (errorFlag) {
        emit error();
    }

    //emit consoleOutputMessage("Savethread thread ended.");
    emit finished();

}
void FileWriteThread::abortSave()
{
    abortFlag = true;
}


//------------------------------------------------


DataCheckThread::DataCheckThread(SDDataExtractor::DetectedDisk dd,SDTransferBlockRingBuffer *transferBuffer) :
    tBuffer(transferBuffer),
    abortFlag(false),
    errorFlag(false),
    redoFlag(false),
    atEndFlag(false),
    disk(dd)

{

}

void DataCheckThread::startCheck()
{


    //emit consoleOutputMessage("Checker thread running.");

    bool ok;
    bool lastBlock = false;
    int numConsecRedos = 0;
    uchar* dataPtr;
    uchar* scanPtr;
    uint64_t transferSize;
    uint64_t transferStartSector;

    uint64_t numTransfersCleared = 0;

    uint64_t pSize = disk.sessionInfo.packet_size_bytes; //packet size
    uint64_t pLocation = 0; //location within the packet structure (which does not align to the transfer segment start/stop)
    uint64_t pLocationRescue = 0; //location within the packet structure (in case a bad transfer happened, and a rewind was needed)
    uint64_t scanLocation = 0; //location within the transfer section
    uint64_t numbytesprocesssed = 0;
    uint64_t numbytescorrupted = 0;
    QVector<int> packetStartLocations;

    while (!abortFlag && !errorFlag && !atEndFlag) {

        if (redoFlag) {
            //An error occured during the last transfer, and we need to redo it.
            if (numConsecRedos > 0) {
                errorFlag = true;
            } else {

                pLocation = pLocationRescue;
                numbytescorrupted = 0;
                tBuffer->rejectBlockCheck();
                emit requestRedo();
                redoFlag = false;
                numConsecRedos++;
            }
        }


        packetStartLocations.clear();
        pLocationRescue = pLocation; //We store the pLocation value before scanning the transfer, incase we need to reqiend and re-do the transfer
        ok = false;
        while (!abortFlag && !ok) {
            dataPtr = tBuffer->getNextReadPtr(&ok, &transferSize, &transferStartSector, &lastBlock);
        }

        //emit consoleOutputMessage(QString("transfer: %1").arg(transferSize));
        numbytesprocesssed = 0;

        if (!abortFlag && !errorFlag) {

            scanPtr = dataPtr;
            uint64_t nextSync = (pSize-pLocation)%pSize;
            int nextSync_absolute = nextSync;
            uint64_t bytesLeftToScan = transferSize;

            /*for (int i=0; i < bytesLeftToScan; i++) {
                if (scanPtr[i] == 0x55) {
                    emit consoleOutputMessage(QString("Value: %1, byte %2").arg(scanPtr[i]).arg(numbytesprocesssed));
                }
                numbytesprocesssed++;
            }*/

            //printf("%d,%d,%d\n",nextSync_absolute,bytesLeftToScan, pLocation);
            //fflush(stdout);

            if (nextSync <= bytesLeftToScan) {
                scanPtr += nextSync;
                numbytesprocesssed += nextSync;
                bytesLeftToScan -= nextSync;
                pLocation = 0;

                //Is the data ok?

                while (bytesLeftToScan > 0) {
                    if ((scanPtr[0] != 0x55)&&(!lastBlock)) {
                        //error
                        if (bytesLeftToScan >= pSize) {
                            numbytescorrupted += pSize;
                        } else {
                            numbytescorrupted += bytesLeftToScan;
                        }
                        emit consoleOutputMessage(QString("Error:  Data transfer corrupted. PSize: %1, Byte in transfer %2, Number of consecutive bytes corrupted: %3").arg(pSize).arg(numbytesprocesssed).arg(numbytescorrupted));



                        if ((numbytescorrupted > 1024)) {
                            //Most write errors leave an entire sector (512 bytes) unwritten. But if more than two sectors have gone by, there is something more serious happening. Abort.
                            redoFlag = true;
                            emit consoleOutputMessage(QString("Error:  Data transfer corrupted. Start sector %1").arg(transferStartSector));


                            /*if (numConsecRedos > 0) {
                                emit consoleOutputMessage(QString("Content of single packet:"));
                                for (int i=0; i < pSize; i++) {
                                    emit consoleOutputMessage(QString("Value: %1").arg(scanPtr[i]));

                                }
                            }*/

                            break;
                        }


                    } else if ((scanPtr[0] != 0x55)&&(lastBlock)) {
                        //this is the last transfer
                        break;
                    } else {
                        //Sync byte found, all good.
                        numbytescorrupted = 0;
                        packetStartLocations.push_back(nextSync_absolute);

                        //tBuffer->addPacketStartLocation(nextSync_absolute);

                    }

                    if (bytesLeftToScan > pSize) {
                        scanPtr += pSize;
                        numbytesprocesssed += pSize;
                        bytesLeftToScan -= pSize;
                        nextSync_absolute += pSize;
                    } else {
                        pLocation += bytesLeftToScan;
                        bytesLeftToScan = 0;
                        numbytesprocesssed = transferSize;
                    }
                }
            } else {
                //The transfer size was less than one packet, which is currently an unhandled error case
                pLocation += bytesLeftToScan;
                bytesLeftToScan = 0;

                //Error
                emit consoleOutputMessage(QString("Transfer error: Bytes in transfer: %1, Next sync %2").arg(bytesLeftToScan).arg(nextSync));
                errorFlag = true;
                break;
            }


        }
        if (!abortFlag && !errorFlag && !redoFlag) {

            for (int i=0; i<packetStartLocations.length(); i++) {
                tBuffer->addPacketStartLocation(packetStartLocations.at(i));
            }

            tBuffer->finishBlockCheck(numbytesprocesssed);
            numTransfersCleared++;
            numConsecRedos = 0;

            if (lastBlock) {
                atEndFlag = true;
                emit lastPacketChecked();
            }
        }
    }



    if (abortFlag) {
        emit aborted();
    } else if (errorFlag) {
        emit error();
    }

    //emit consoleOutputMessage("Checker thread ended.");
    emit finished();



    /*int packets_cleared_in_cycle = 0;
        int leftOverByteFromLastTransfer = bytesLeftToProcess;

        if (recoveryNeeded) {
            if (numConsecRecoveriesTried > 5) {
                //We have tried to recover too many times, so we need to give up and scan past this section
                badDataSectionEncountered = true;
                emit newDebugMessage("Warning: a corrupted section of the recording was encountered. Trying to skip past it.");
            }

            //A recovery from a transfer error occured, so we need to fast-foreward to the current byte in the sector
            res = FTDIReceive(bytesToFF);
            if (res != 0) {
                FTDIClose();
                return res;
            } else {
                //Recovery process completed.
                bytesLeftToProcess = (numberOfSectorsToReadPerCommand*512)-bytesToFF;
                recoveryNeeded = false;
                numConsecRecoveriesTried++;
            }



        } else {
            //No recovery needed, so we add all of the trasfered sectors to the pile
            bytesLeftToProcess += numberOfSectorsToReadPerCommand*512;
            numConsecRecoveriesTried = 0;
        }

        int packets_left_in_buffer = bytesLeftToProcess/psize;


        // Calculate the maximum number of packets to read in each FT_Read()
        // FT_Read() max request is 65536 bytes
        if (packets_left_in_buffer > (64000/psize)) {
            packets_to_read = (64000/psize);
        } else {
            packets_to_read = packets_left_in_buffer;
        }

        while ((!recoveryNeeded) && (packets_to_read > 0)) {
            //ftStatus = FT_Read(ftHandle, RxBuffer, packets_to_read * psize, &BytesReceived);
            res = FTDIReceive(packets_to_read * psize);
            //Did the read operation work?
            if (res != 0) {
                recoveryNeeded = true;
            }

            //Is the data ok?
            bool foundGoodSync = false;
            for (i = 0; i < packets_to_read; i++) {
                if ((RxBuffer[i*psize] != 0x55) && (!badDataSectionEncountered)) {
                    //printf("\nError: Malformed data! Missing packet header\n");

                    recoveryNeeded = true;
                    break;
                }
                if (RxBuffer[i*psize] == 0x55) {
                    foundGoodSync = true;
                }
            }
            if ((foundGoodSync) && (badDataSectionEncountered)) {
                //We have scanned past a bad section of data
                badDataSectionEncountered = false;
                emit newDebugMessage("Bad data section skipped.");
            }


            if (recoveryNeeded) {
                //Something went wrong in this last transfer cycle. We need to try it again.

                //Reset the dock
                TxBuffer[0] = SD_RESET_CMD;
                res = FTDISend(1);
                if (res != 0) {
                    FTDIClose();
                    return res;
                }

                //Completely reset the USB connection
                FTDIClose();
                res = FTDIConnect();
                if (res != 0) {
                    return res;
                }

                int bytesLeftFromLastTransfer = bytesLeftToProcess - ((int)numberOfSectorsToReadPerCommand*512);
                int bytesProcessedFromThisTransfer;


                if (bytesLeftFromLastTransfer < 0) {
                    //The issue occured after whatever was left from the previous transfer cycle,
                    //so we don't need to rewind to that.
                    bytesLeftFromLastTransfer = 0;
                    currentSector = (total_bytes_cleared/512);
                    bytesToFF = total_bytes_cleared-(currentSector*512);
                    bytesProcessedFromThisTransfer =  ((int)numberOfSectorsToReadPerCommand*512) - bytesLeftToProcess;

                } else {
                    bytesProcessedFromThisTransfer = 0;
                    currentSector = (total_bytes_cleared/512);
                    bytesToFF = total_bytes_cleared-(currentSector*512);

                }

                if (numConsecRecoveriesTried > 3) {
                    //We have tried multiple times to recover, so now we start printing out the details before
                    //we give up completely.
                    emit newDebugMessage("Attempting recovery.");
                    emit newDebugMessage(QString::asprintf("Sectors tranferred before current transfer: %d \n"
                                                           "Bytes that initially remained from that transfer %d \n"
                                                           "Bytes left to process from that transfer %d \n"
                                                           "Bytes processed successfully from the current transfer %d \n"
                                                           "Packet size: %d \n"
                                                           "Recovering at sector %d \n"
                                                           "Fast-forward num bytes: %d \n", (int)sectors_read, leftOverByteFromLastTransfer, bytesLeftFromLastTransfer,bytesProcessedFromThisTransfer,psize,(int)currentSector,(int)bytesToFF));
                }
                bytesLeftToProcess = 0;
                numberOfSectorsToReadPerCommand = 100; //Reduce read size just for the recovery
                //sectors_read += (bytesProcessedFromThisTransfer/512); //we will skip the sectors that were succesfully processed
                sectors_read = currentSector;
                currentSector += start;

            } else {

                //Everything checks out, so now we process the data and write to file.

                if (d->sample_size_bits == 10) {
                    //10-bit recording. We need to convert to 16-bit.

                    p1 = 0;
                    p2 = 0;
                    for (j = 0; j < packets_to_read; j++) {
                        // copy the first part of the packet header
                        memcpy(&Buff[p2], &RxBuffer[p1], 12);
                        p1 += 12;
                        p2 += 12;
                        // unpack the 10-bit lfp data to Buff2
                        for (i = 0; i < (auxsize-10)*8/10; i+=4) {
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                            Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                            p2 += 8;
                            p1 += 5;
                        }
                        // copy the timestamp
                        memcpy(&Buff[p2], &RxBuffer[p1], 4);
                        p1 += 4;
                        p2 += 4;
                        // unpack the 10-bit ap data to Buff
                        for (i = 0; i < channels-3; i+=4) {
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                            Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                            p2 += 8;
                            p1 += 5;
                        }
                        // must take care of runt data in case channels is not a multiple of 4
                        switch (channels % 4) {
                        case 3:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            p2 += 6;
                            p1 += 4;
                            break;
                        case 2:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            p2 += 4;
                            p1 += 3;
                            break;
                        case 1:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            p2 += 2;
                            p1 += 2;
                            break;
                        }
                    }
                    bytes_to_write = p2;
                    bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
                } else if (d->sample_size_bits == 12) {
                    // 12-bit data: expand packet data from 12 to 16 bits
                    p1 = 0;
                    p2 = 0;
                    for (j = 0; j < packets_to_read; j++) {
                        // copy the packet header
                        memcpy(&Buff[p2], &RxBuffer[p1], auxsize + 6);
                        p1 += auxsize + 6;
                        p2 += auxsize + 6;
                        // unpack the 12-bit packet data from RxBuffer to Buff
                        for (i = 0; i < channels; i+=2) {
                            Buff[p2]   = RxBuffer[p1] << 4;
                            Buff[p2+1] = (RxBuffer[p1] >> 4) | (RxBuffer[p1+1] << 4);
                            Buff[p2+2] = RxBuffer[p1+1] & 0xf0;
                            Buff[p2+3] = RxBuffer[p1+2];
                            p2 += 4;
                            p1 += 3;
                        }
                    }
                    bytes_to_write = p2;
                    bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
                } else {
                    // 16-bit data: no data processing needed
                    bytes_to_write = packets_to_read * psize;
                    bytesWritten = fwrite(RxBuffer, 1, bytes_to_write, fp);
                }

                if (bytesWritten != bytes_to_write) {
                    sendErrorMessage("Disk write failed. Make sure there is enough free disk space.");
                    FTDIClose();
                    return 3;
                }



                //Update byte markers to prepare for next buffer read cycle

                total_bytes_cleared += packets_to_read*psize;
                packets_read += packets_to_read;
                packets_left_in_buffer -= packets_to_read;
                packets_cleared_in_cycle += packets_to_read;
                bytesLeftToProcess -= packets_to_read*psize;
                // Check if packets_to_read needs to be updated
                // This would be the case for the last FT_Read()
                if (packets_to_read > packets_left_in_buffer) {
                    packets_to_read = packets_left_in_buffer;
                }

            }

        }*/
}
void DataCheckThread::abortCheck()
{
    abortFlag = true;
}


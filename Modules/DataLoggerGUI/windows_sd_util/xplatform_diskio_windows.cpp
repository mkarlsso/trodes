#include <windows.h>
#include "diskio.h"
#include "xplatform_diskio.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

std::list<DriveDeviceInfo> detectDevices() {
    std::list<DriveDeviceInfo> ret;

    unsigned long availabledrives = GetLogicalDrives();
    printf("\nAvailable drives:%d\n",(int)availabledrives);
    fflush(stdout);

    int i = 0;
    //cboxDevice->clear();
    while (availabledrives != 0)
    {
        if ((availabledrives % 2) != 0)
        {

            WCHAR devicename[] = L"A:\\";
            devicename[0] += i;

            UINT DT = GetDriveType(devicename);
            wprintf(L"drive %s -type = %d\n", devicename, DT);

            if (DRIVE_REMOVABLE == DT)
            {

                HANDLE hDevice;
                DWORD bytesreturned;
                BOOL bResult;
                DEVICE_NUMBER deviceNum;

                WCHAR devicenameL[] = L"\\\\.\\A:";
                devicenameL[4] += i;
                hDevice = CreateFile(devicenameL, 0, 0, NULL, OPEN_EXISTING, 0, NULL);
                if (hDevice == INVALID_HANDLE_VALUE) {
                    printf("Invalid handle\n");
                    printf("Error: %d\n", GetLastError());
                    CloseHandle(hDevice);

                } else {

                    bResult = DeviceIoControl(hDevice, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &deviceNum, sizeof(deviceNum), &bytesreturned, NULL);
                    CloseHandle(hDevice);
                    if (bResult) {

                        char devstr[1000];

                        // sprintf() to print num to str buffer
                        sprintf(devstr, "%d", (int)deviceNum.DeviceNumber);

                        DriveDeviceInfo d;
                        disk_ret r = initDevice(devstr,&d);


                        if(r == RET_OK){

                            char dname[] = "A:/";
                            dname[0] += i;
                            memset(d.fname, 0, MAX_DEVICE_FNAME_LENGTH);
                            strncpy(d.fname, dname, MAX_DEVICE_FNAME_LENGTH);
                            memset(d.handlename, 0, MAX_DEVICE_FNAME_LENGTH);
                            strncpy(d.handlename, devstr, MAX_DEVICE_FNAME_LENGTH);

                            ret.push_back(d);
                            close_drive();
                        } else {

                            //printf("Cannot init. Code %d",r);
                        }


                    } else {
                        //Could not access device
                        //printf("Could not get device number\n");
                    }




                 }
            }

        }
        availabledrives >>= 1;
        //cboxDevice->setCurrentIndex(0);
        ++i;
    }
    return ret;
}

// check device string, check device access/permission, get device information
disk_ret initDevice(const char* device, DriveDeviceInfo *info){
    //check device exists
    char *end;
    int p = strtol(device, &end, 10);
    if(end==device && p==0){
        //device not a number
        return RET_DEVICE_NOT_FOUND;
    }

    if(!assign_drives(p)){
        return RET_DEVICE_NOT_FOUND;
    }

    //check requested permission
    //this is how the programs (extract.c, pcheck.c, etc) did it. may be a better way?
    uint8_t Buffer[1024];
    DRESULT res = disk_read(0, Buffer, 0, 1);
    if(res){
        return RET_BAD_PERMISSIONS;
    }

    //get device info
    WORD w;
    DWORD dw;
    res = disk_ioctl(0, GET_SECTOR_SIZE, &w);
    if(res){
        return RET_DEVICE_INFO_ERR;
    }
    res = disk_ioctl(0, GET_SECTOR_COUNT, &dw);
    if(res){
        return RET_DEVICE_INFO_ERR;
    }

    info->sectorSize = w;
    info->sectorCount = dw;
    return RET_OK;
}


//read data from device starting at a sector, for a number of sectors, into a buffer
disk_ret readSectors(DriveDeviceInfo *device, uint8_t *buffer, uint32_t startSector, uint32_t numSectors){
    DRESULT res = disk_read(0, buffer, startSector, numSectors);
    if(res){
        return RET_READ_INCOMPLETE;
    }

    return RET_OK;
}

//close device routines
disk_ret closeDevice(DriveDeviceInfo *device){

    close_drive();
    return RET_OK;
}


//write data to device starting at a sector, for a number of sectors, from a buffer
disk_ret writeSectors(DriveDeviceInfo *device, uint8_t *buffer, uint32_t startSector, uint32_t numSectors){
    DRESULT res = disk_write(0, buffer, startSector, numSectors);
    if(res){
        return RET_WRITE_INCOMPLETE;
    }

    return RET_OK;
}


//read a number of packets from the device onto buffer
disk_ret readPackets(DriveDeviceInfo *device, uint8_t *buffer, uint32_t startSector, uint64_t startPacketIndex, uint32_t packetSize, uint64_t numPackets){
    DRESULT res = packet_read(0, buffer, startSector,startPacketIndex, packetSize, numPackets);
    if(res){
        return RET_READ_INCOMPLETE;
    }

    return RET_OK;
}

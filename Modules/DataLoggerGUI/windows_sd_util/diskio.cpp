/*-----------------------------------------------------------------------*/
/* Low level disk control module for Win32              (C)ChaN, 2007    */
/*-----------------------------------------------------------------------*/

#include <stdio.h>
#include <windows.h>
#include "diskio.h"
#include <winioctl.h>



/*--------------------------------------------------------------------------

   Module Private Functions

---------------------------------------------------------------------------*/


#define	IHV	INVALID_HANDLE_VALUE
//#define	BUFSIZE 131072UL
#define	BUFSIZE 524288UL

typedef struct {
	DSTATUS	status;
	WORD sz_sector;
	DWORD n_sectors;
	HANDLE h_drive;
} STAT;

static
HANDLE hMutex, hTmrThread;

static volatile
STAT Stat[10];


static
DWORD TmrThreadID;

static
//BYTE *Buffer;	/* Poiter to the data transfer buffer */
void *Buffer;


/*-----------------------------------------------------------------------*/
/* Timer Functions                                                       */
/*-----------------------------------------------------------------------*/


DWORD WINAPI tmr_thread (LPVOID parms)
{
	DWORD dw;
	int drv;


	for (;;) {
		for (drv = 0; drv < 10; drv++) {
			Sleep(1);
			if (Stat[drv].h_drive == IHV || Stat[drv].status & STA_NOINIT || WaitForSingleObject(hMutex, 100) != WAIT_OBJECT_0) continue;

			if (!DeviceIoControl(Stat[drv].h_drive, IOCTL_STORAGE_CHECK_VERIFY, NULL, 0, NULL, 0, &dw, NULL))
				Stat[drv].status |= STA_NOINIT;
			ReleaseMutex(hMutex);
			Sleep(100);
		}
	}
}



BOOL get_status (volatile STAT *stat) {
	DISK_GEOMETRY parms;
	PARTITION_INFORMATION part;
	DWORD dw;
	HANDLE h = stat->h_drive;


	if (h == IHV
		|| !DeviceIoControl(h, IOCTL_STORAGE_CHECK_VERIFY, NULL, 0, NULL, 0, &dw, NULL)
		|| !DeviceIoControl(h, IOCTL_DISK_GET_PARTITION_INFO, NULL, 0, &part, sizeof(part), &dw, NULL)
		|| !DeviceIoControl(h, IOCTL_DISK_GET_DRIVE_GEOMETRY, NULL, 0, &parms, sizeof(parms), &dw, NULL)) {
		stat->status = STA_NOINIT;
		return FALSE;
	}
	stat->sz_sector = (WORD)parms.BytesPerSector;
	stat->n_sectors = (DWORD)(part.PartitionLength.QuadPart / parms.BytesPerSector);
	stat->status = DeviceIoControl(h, IOCTL_DISK_IS_WRITABLE, NULL, 0, NULL, 0, &dw, NULL) ? 0 : STA_PROTECT;
	return TRUE;
}




/*--------------------------------------------------------------------------

   Public Functions

---------------------------------------------------------------------------*/


BOOL assign_drives (int n)
{
	char str[30];
	HANDLE h;
  DWORD dwResult;
  DISK_GEOMETRY driveInfo;
  BOOL removable = FALSE;

	Buffer = VirtualAlloc(NULL, BUFSIZE, MEM_COMMIT, PAGE_READWRITE);
	if (!Buffer) return FALSE;

	hMutex = CreateMutex(0, FALSE, NULL);
	if (hMutex == IHV) return FALSE;

  if (!n) return FALSE;
  sprintf(str, "\\\\.\\PhysicalDrive%u", n);
  printf("Disk %d:",n);
  h = CreateFileA(str, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING, NULL);
  if (DeviceIoControl (h, IOCTL_DISK_GET_DRIVE_GEOMETRY, NULL, 0,
		&driveInfo, sizeof (driveInfo), &dwResult, NULL))
		removable = driveInfo.MediaType == RemovableMedia;
  Stat[0].h_drive = h;
  if (h == IHV) {
    Stat[0].status = STA_NOINIT;
    printf(" (Not Available)\n");
  } else if (!removable) {
    Stat[0].status = STA_NOINIT;
    printf(" (Not Removable Media)\n");
  } else {
    if (get_status(&Stat[0])) {
      printf(" (%lu Sectors, %u Bytes/Sector)\n", Stat[0].n_sectors, Stat[0].sz_sector);
    } else {
      printf(" (Not Ready)\n");
    }
  }

	hTmrThread = CreateThread(NULL, 0, tmr_thread, 0, 0, &TmrThreadID);
	if (hTmrThread == IHV) return FALSE;

	return TRUE;
}


void close_drive()
{

    CloseHandle(Stat[0].h_drive);
    Stat[0].status = STA_NOINIT;
    Stat[0].n_sectors = 0;
    Stat[0].sz_sector = 0;

}


/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE drv		/* Physical drive nmuber */
)
{
	DSTATUS sta;


	if (WaitForSingleObject(hMutex, 5000) != WAIT_OBJECT_0) return STA_NOINIT;

	if (drv >= 1) {
		sta = STA_NOINIT;
	} else {
		get_status(&Stat[drv]);
		sta = Stat[drv].status;
	}

	ReleaseMutex(hMutex);
	return sta;
}



/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE drv		/* Physical drive nmuber (0) */
)
{
	DSTATUS sta;


	if (drv >= 1) {
		sta = STA_NOINIT;
	} else {
		sta = Stat[drv].status;
	}

	return sta;
}


/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (BYTE drv,			/* Physical drive nmuber (0) */
    BYTE *buff,			/* Pointer to the data buffer to store read data */
    DWORD sector,		/* Start sector number (LBA) */
    DWORD count			/* Sector count (1..255) */
)
{
	DWORD nc, rnc;
	LARGE_INTEGER ofs;
    //DSTATUS res;
    DRESULT res;


	if (drv >= 1 || Stat[drv].status & STA_NOINIT || WaitForSingleObject(hMutex, 3000) != WAIT_OBJECT_0) return RES_NOTRDY;

    //printf("[R:%d:%d]", sector, count);
    nc = (DWORD)count * Stat[drv].sz_sector;
    res = RES_OK;

    if (nc > BUFSIZE) {
		res = RES_PARERR;
	} else {
		ofs.QuadPart = (LONGLONG)sector * Stat[drv].sz_sector;
		if (SetFilePointer(Stat[drv].h_drive, ofs.LowPart, &ofs.HighPart, FILE_BEGIN) != ofs.LowPart) {
			res = RES_ERROR;
        } else {
            if (!ReadFile(Stat[drv].h_drive, buff, nc, &rnc, NULL) || nc != rnc) {
                res = RES_ERROR;
            } else {
                //memcpy(buff, Buffer, nc);
                res = RES_OK;
            }
            /*if (!ReadFile(Stat[drv].h_drive, Buffer, nc, &rnc, NULL) || nc != rnc) {
				res = RES_ERROR;
			} else {
				memcpy(buff, Buffer, nc);
				res = RES_OK;
            }*/
		}
    }

	ReleaseMutex(hMutex);
	return res;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _READONLY == 0
DRESULT disk_write (
	BYTE drv,			/* Physical drive nmuber (0) */
	const BYTE *buff,	/* Pointer to the data to be written */
	DWORD sector,		/* Start sector number (LBA) */
	BYTE count			/* Sector count (1..255) */
)
{
	DWORD nc, rnc;
	LARGE_INTEGER ofs;
	DRESULT res;


	if (drv >= 1 || Stat[drv].status & STA_NOINIT || WaitForSingleObject(hMutex, 3000) != WAIT_OBJECT_0) return RES_NOTRDY;

//	printf("[W:%d:%d]", sector, count);
	res = RES_OK;
	if (Stat[drv].status & STA_PROTECT) {
		res = RES_WRPRT;
	} else {
		nc = (DWORD)count * Stat[drv].sz_sector;
		if (nc > BUFSIZE)
			res = RES_PARERR;
	}

	if (res == RES_OK) {
		ofs.QuadPart = (LONGLONG)sector * Stat[drv].sz_sector;
		if (SetFilePointer(Stat[drv].h_drive, ofs.LowPart, &ofs.HighPart, FILE_BEGIN) != ofs.LowPart) {
			res = RES_ERROR;
		} else {
			memcpy(Buffer, buff, nc);
			if (!WriteFile(Stat[drv].h_drive, Buffer, nc, &rnc, NULL) || nc != rnc)
				res = RES_ERROR;
		}
	}

	ReleaseMutex(hMutex);
	return res;
}
#endif /* _READONLY */

/*-----------------------------------------------------------------------*/
/* Read Packet(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT packet_read (
	BYTE drv,			/* Physical drive number (0) */
	BYTE *buff,			/* Pointer to the data buffer to store read data */
  DWORD start,    /* First data sector */
  LONGLONG packet,
	DWORD psize,
  WORD count
)
{
	DWORD nc, rnc;
  DWORD startSector, sectors;
  WORD offset;
	LARGE_INTEGER ofs;
    //DSTATUS res;
    DRESULT res;


	if (drv >= 1 || Stat[drv].status & STA_NOINIT || WaitForSingleObject(hMutex, 3000) != WAIT_OBJECT_0)
    return RES_NOTRDY;
  if (psize > 2062)
    return RES_PARERR;

  offset = (packet * psize)%Stat[drv].sz_sector;
  sectors = (((count * psize) + offset)/Stat[drv].sz_sector)+1;
  startSector = ((packet * psize)/Stat[drv].sz_sector) + start;
  nc = sectors * Stat[drv].sz_sector;
	if (nc > BUFSIZE) {
		res = RES_PARERR;
  } else {
    ofs.QuadPart = (LONGLONG)startSector * Stat[drv].sz_sector;
    if (SetFilePointer(Stat[drv].h_drive, ofs.LowPart, &ofs.HighPart, FILE_BEGIN) != ofs.LowPart) {
      res = RES_ERROR;
    } else {
      if (!ReadFile(Stat[drv].h_drive, Buffer, nc, &rnc, NULL) || nc != rnc) {
        res = RES_ERROR;
      } else {
        memcpy(buff, (char*)Buffer+offset, psize*count);
        res = RES_OK;
      }
    }
  }
	ReleaseMutex(hMutex);
	return res;
}
/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE drv,		/* Physical drive nmuber (0) */
	BYTE ctrl,		/* Control code */
	void *buff		/* Buffer to send/receive data block */
)
{
	DRESULT res = RES_PARERR;


	if (drv >= 1 || (Stat[drv].status & STA_NOINIT)) return RES_NOTRDY;

	switch (ctrl) {
	case CTRL_SYNC:
		res = RES_OK;
		break;

	case GET_SECTOR_COUNT:
		*(DWORD*)buff = Stat[drv].n_sectors;
		res = RES_OK;
		break;

	case GET_SECTOR_SIZE:
		*(WORD*)buff = Stat[drv].sz_sector;
		res = RES_OK;
		break;

	case GET_BLOCK_SIZE:
		*(DWORD*)buff = 128;
		res = RES_OK;
		break;
	}

	return res;
}





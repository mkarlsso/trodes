#ifndef LOGGERCONFIGWIDGET_H
#define LOGGERCONFIGWIDGET_H

#include <QtWidgets>
#include "disklistwidget.h"

class LoggerConfigWidget : public QDialog
{
    Q_OBJECT
public:
    explicit LoggerConfigWidget(LoggerRecording rec, QString device, QWidget *parent = nullptr);

signals:

public slots:

private:
    QString device;
    void donepressed();
    void loadfile();

    QSpinBox *numchanBox;
    QSpinBox *rfchanBox;
    QCheckBox *waitforstartoverride;
    QCheckBox *cardenableoverride;
    QCheckBox *samplingrate20khz;
    QCheckBox *magnetometerdisable;
    QCheckBox *accelerometerdisable;
    QCheckBox *gyroscopedisable;
    QCheckBox *smartrefenable;
    QCheckBox *samplesize12bits;

    uint8_t rfChan;
    bool    sample20khzOn;
};

#endif // LOGGERCONFIGWIDGET_H

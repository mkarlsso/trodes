/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>

#include <QHash>
#include <QTextCharFormat>
#include <QTextEdit>
#include <QRegularExpression>


class QTextDocument;

class Highlighter : public QSyntaxHighlighter {

    Q_OBJECT

public:
    Highlighter(QTextDocument *parent = 0);
    void asHtml(QTextEdit *editor, QString& html);

protected:
    void highlightBlock(const QString &text);

private:
    struct HighlightingRule {

        QRegularExpression pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    //QRegExp commentStartExpression;
    //QRegExp commentEndExpression;

    QTextCharFormat keywordFormat;
    QTextCharFormat systemFormat;
    QTextCharFormat commandFormat;
    QTextCharFormat ioFormat;
    QTextCharFormat singleLineCommentFormat;
    //QTextCharFormat multiLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat functionFormat;
};


#endif // HIGHLIGHTER_H

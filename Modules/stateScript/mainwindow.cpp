/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "mainwindow.h"
#include "../cameraModule/src/cameramodulenetworkdefines.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <TrodesNetwork/Resources/SourcePublisher.h>

FT_STATUS res;
FT_HANDLE ftdi;
char const *desc = "Spikegadgets ECU A";

int sourceSamplingRate;


StateScriptClient::StateScriptClient(QString serveraddress, int port)
    : QtModuleClient("StateScript", serveraddress.toStdString().c_str(), port),
      outputPub(serveraddress.toStdString().c_str(), port, "statescript.output")
{
    std::shared_ptr<StateScriptClient> client(this);
    auto script_subscriber_thread = std::thread([ address = serveraddress.toStdString(), port = port, client ]() {
        trodes::network::ServiceProvider<NetworkStatescriptCommand, std::string> service(address, port, "statescript.service");
        while(true) {

            service.handle([ client ](NetworkStatescriptCommand input) -> std::string {
                if (input.command == "send") {
                    auto scriptsend = QString::fromStdString(input.string);
                    emit client->sig_statescriptCommand(scriptsend);
                    return "success";
                } else if (input.command == "connect") {
                    //TODO: code to connect to hardware
                    return "success";
                } else if (input.command == "disconnect") {
                    //TODO: code to disconnect from hardware
                    return "success";
                } else {
                    return "error";
                }
            });
        }
    });
    script_subscriber_thread.detach();
}

void StateScriptClient::sendErrorMessage(QString m) {
    int64_t msg_timestamp = trodes::network::util::get_timestamp();

    NetworkStateScriptOutput cOutput = {m.toStdString(), "error",latestTrodesTimestamp(),msg_timestamp};
    outputPub.publish(cOutput);
}
void StateScriptClient::sendDebugMessage(QString m) {
    int64_t msg_timestamp = trodes::network::util::get_timestamp();

    NetworkStateScriptOutput cOutput = {m.toStdString(), "debug",latestTrodesTimestamp(),msg_timestamp};
    outputPub.publish(cOutput);
}
void StateScriptClient::sendOutputMessage(QString m) {
    int64_t msg_timestamp = trodes::network::util::get_timestamp();

    NetworkStateScriptOutput cOutput = {m.toStdString(), "output",latestTrodesTimestamp(),msg_timestamp};
    outputPub.publish(cOutput);
}
void StateScriptClient::sendStateScriptMessage(QString m) {
    int64_t msg_timestamp = trodes::network::util::get_timestamp();

    NetworkStateScriptOutput cOutput = {m.toStdString(), "statescript",latestTrodesTimestamp(),msg_timestamp};
    outputPub.publish(cOutput);
}



MainWindow::MainWindow(QStringList arguments, QWidget *parent)
    : QMainWindow(parent)
    , loadedWorkspace()
{

    if (objectName().isEmpty())
        setObjectName(QString("Main"));

    //GLOBAL CONFIGURATION POINTERS-- THESE ARE FILLED IF A WORKSPACE FILE IS PASSED IN THE ARGUMENTS
    //globalConf = NULL;
    //hardwareConf = NULL;
//    nTrodeTable = NULL;
    //streamConf = NULL;
    //spikeConf = NULL;
    //headerConf = NULL;
    //moduleConf = NULL;
    //networkConf = NULL;
    //benchConfig = NULL;

    networkClient = NULL;   
    //broker = NULL;
    //server = NULL;
    cameraProgram = NULL;
    //timestampspub = NULL;
    serveraddress = "";
    serverport = -1;
    resize(800, 600);
    unSavedData = false;
    scriptFileSelected = false;
    localFileSelected = false;
    //fileHasName = false;
    isTcpClientConnected = false;
    tcpClient = nullptr;
    currentOperationMode = 0;

    currentSerialPort = "";
    isSerialConnected = false;
    gotCompileSignal = false;
    logFileOpen = false;
    isRecording = false;

    FTDI_Initialized = false;
    FTDI_connected = false;

    udpSocket = NULL;
    udpReturnSocket = NULL;
    udpSocketInitialized = false;

    trodesTimeRate = 30000;
    currentTrodesTime = 0;
    fileNameFromTrodes = "";

    cameraModuleConnected = false;
    localLanguageConnected = false;
    waitingForScriptSend = false;
    waitingForAPIScriptSend = false;

    currentTimeStamp = 0;
    sourceSamplingRate = 1000;




    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("paths"));
    QString tempfolderName = settings.value(QLatin1String("stateScriptFolder")).toString();
    if (!tempfolderName.isNull()) {
        stateScriptFolder = tempfolderName;
    }
    tempfolderName = settings.value(QLatin1String("localScriptFolder")).toString();
    if (!tempfolderName.isNull()) {
        localScriptFolder = tempfolderName;
    }
    tempfolderName = settings.value(QLatin1String("logFileFolder")).toString();
    if (!tempfolderName.isNull()) {
        logFileFolder = tempfolderName;
    }

    localCallbackLanguage = "";
    localCallbackLanguageLocation = "";
    QStringList availableLanguages;
    availableLanguages << "Matlab" << "Python";
    QStringList tempPathList = settings.value(QLatin1String("localScriptLanguages")).toStringList();

    QString preferredLanguage = settings.value(QLatin1String("preferredLanguage")).toString();
    if (!preferredLanguage.isEmpty()) {
        localCallbackLanguage = preferredLanguage;

        int index = availableLanguages.indexOf(preferredLanguage);
        if ((index > -1)&&(index < tempPathList.length())) {
            localCallbackLanguageLocation = tempPathList[index];
        }
    }
    settings.endGroup();

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QByteArray geo = settings.value(QLatin1String("geometry")).toByteArray();
    if (!geo.isEmpty()) {
        restoreGeometry(geo);
    }

    /*QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        //Make sure the screen exists
        QScreen *screencheck = QGuiApplication::screenAt(tempPosition.topLeft());
        if (screencheck != nullptr) {
            setGeometry(tempPosition);
        }
        //setGeometry(tempPosition);
    }*/
    settings.endGroup();

    //Create a NULL networkConf variable (it's a global variable)
    //networkConf = NULL;

    //Parse the command line options

    QString trodesConfigValue;
    QString serverAddress;
    QString serverPortValue;
    QString directConnectionValue;
    QString autoConnectValue;


    int optionInd = 1;
    while (optionInd < arguments.length()) {
        if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            trodesConfigValue = arguments.at(optionInd+1);
            //nsParseTrodesConfig(trodesConfigValue);
            QString errorString = loadedWorkspace.readTrodesConfig(trodesConfigValue);
            if (!errorString.isEmpty()) {
                createNewErrorMessage(errorString);
            }
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverAddress = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverPortValue= arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-directConnection",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            directConnectionValue= arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-autoConnect",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            autoConnectValue= arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-suppressUpdates",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            //User can specify if updages should be suppressed for specific ports.
            //More than one of these arguments can be made with different port numbers
            bool ok;
            int tempPort = arguments.at(optionInd+1).toInt(&ok);
            if (ok) {
                //qDebug() << "stateScript: suppressing updates for port " << tempPort;
                ignoreUpdatePorts.append(tempPort);
            } else {
                //TODO: Display error window
            }
            optionInd++;
        }
        optionInd++;
    }


    bool connectionSuccess = false;
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        //------------------------------------------
        /*
        Module connection protocol:
         1) Trodes (or other master GUI) launches module X, or module X is started directly by the user
        2) Module X defines the provided and needed datatypes
        3) Module X creates a client and connects to Trodes (or the master GUI)
        4) When trodes responds with a module ID, a module server is automatically started if the module has any data available
        5) Module X gives Trodes it’s DataTypesAvailable structure, and it’s data server address
        6) Trodes fills its DataAvailable table entries with the given data, and checks that there are no repeats with other modules
        7) Trodes sends the current DataAvailable list to all currently connected modules.
        ...............
        8) Each module starts one client per needed data type and connects to the proper server
        */


        // Create the  moduleNet structure. This is required for all modules
        moduleNet = new TrodesModuleNetwork();

        // set the moduleName
        moduleNet->moduleName = "stateScript";


        // Define the data type that this module provides
        // LF - This shouldn't be necessary
        moduleNet->dataProvided.dataType = TRODESDATATYPE_MESSAGING; //Need a new data type here?

        //Find server address of Trodes (or the master GUI)

        if ((!serverAddress.isEmpty()) &&  (!serverPortValue.isEmpty())) {
            //The address of the server was specified in the command line,
            //so we use that
            connectionSuccess = moduleNet->trodesClientConnect(serverAddress,serverPortValue.toUInt(),false);

        } else {
            //Try to find an existing Trodes server or look in config file.  This will eventually be done
            //with Zeroconf.

            connectionSuccess = moduleNet->trodesClientConnect(false);
        }
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        if ((!serverAddress.isEmpty()) &&  (!serverPortValue.isEmpty())) {
            //The address of the server was specified in the command line,
            //so we use that

            connectionSuccess = startTrodesClient(serverAddress, serverPortValue.toInt());
        } else {

        }
    }

    if (!connectionSuccess) {
        createNewDebugMessage("No server found-- starting in standalone mode");
        currentOperationMode = STATESCRIPTMODULE_STANDALONEMODE | STATESCRIPTMODULE_DIRECTCONNECTION;
        this->setWindowTitle("StateScript  STANDALONE MODE");
        //Reset which USB cameras are in use
        QSettings camsettings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        camsettings.beginGroup(QLatin1String("USBCamerasUsed"));
        camsettings.remove("");
        camsettings.endGroup();

        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
            createNewDebugMessage("Starting ZMQ network server.");
            if (startTrodesNetwork()) {
                if (startTrodesClient(serveraddress, serverport)) {
                    connect(networkClient, &StateScriptClient::sig_cmd_openFile, this, &MainWindow::setFileNameFromTrodes);
                    //connect(networkClient, &StateScriptClient::sig_cmd_time, this, &MainWindow::setTimeFromTrodes);
                    //connect(networkClient, &StateScriptClient::sig_cmd_timerate, this, &MainWindow::setTimeRate);
                    connect(networkClient, &StateScriptClient::sig_cmd_acquisition, this, &MainWindow::getAcquisitionCmd);
                    connect(networkClient, &StateScriptClient::sig_statescriptCommand, this, &MainWindow::sendScriptCmd);
                    connect(networkClient, &StateScriptClient::sig_cmd_closeFile, this, &MainWindow::closeLogFile);
                    connect(networkClient, &StateScriptClient::sig_cmd_quit, this, &MainWindow::close);
                    connect(this, &MainWindow::newDebugMessage, networkClient, &StateScriptClient::sendDebugMessage);
                    connect(this, &MainWindow::newOutputMessage, networkClient, &StateScriptClient::sendOutputMessage);
                    connect(this, &MainWindow::newErrorMessage, networkClient, &StateScriptClient::sendErrorMessage);
                    connect(networkClient, &StateScriptClient::sig_statescriptCommand, this, &MainWindow::sendScriptCmd);
                    connect(this, &MainWindow::newStateScriptMessage, networkClient, &StateScriptClient::sendStateScriptMessage);
                } else {
                    createNewDebugMessage("Error: Could not start statescript server.");
                }
            } else {
                createNewDebugMessage("Error: Could not start ZMQ network server.");
            }
        }

    } else {
        createNewDebugMessage("Connected to server");
        serveraddress = serverAddress;
        serverport = serverPortValue.toInt();
        currentOperationMode = STATESCRIPTMODULE_SLAVEMODE;
        connect(&getCurrentTime_timer,SIGNAL(timeout()),this,SLOT(askTimeFromTrodes()));
        this->setWindowTitle("StateScript  SLAVE");
        if (!directConnectionValue.isEmpty() && directConnectionValue.toInt() == 1) {
            createNewDebugMessage("Direct USB connection mode");
            currentOperationMode |= STATESCRIPTMODULE_DIRECTCONNECTION;
            this->setWindowTitle("StateScript  SLAVE DIRECTCONNECTION");
        } else if (!directConnectionValue.isEmpty() && directConnectionValue.toInt() == 0) {
            //This can be used to pipe communuication through the MCU
            createNewDebugMessage("Connect through USB");
            currentOperationMode |= STATESCRIPTMODULE_TRODESCONNECTION;
//            connect(moduleNet->trodesClient,SIGNAL(sourceConnectEventRecieved(QString)),this,SLOT(autoSourceConnect(QString)));
            connect(networkClient, &StateScriptClient::sig_cmd_source, this, &MainWindow::autoSourceConnect);
            this->setWindowTitle("StateScript  SLAVE TRODESCONNECTION");
        } else {
           createNewDebugMessage("Default: direct USB connection mode");
           currentOperationMode |= STATESCRIPTMODULE_DIRECTCONNECTION;

           //This can be used to pipe communuication through the MCU
           //currentOperationMode |= STATESCRIPTMODULE_TRODESCONNECTION;
           //connect(moduleNet->trodesClient,SIGNAL(sourceConnectEventRecieved(QString)),this,SLOT(autoSourceConnect(QString)));
           //this->setWindowTitle("stateScript  SLAVE TRODESCONNECTION");
        }

        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {

            connect(moduleNet->trodesClient,SIGNAL(openFileEventReceived(QString)),this,SLOT(setFileNameFromTrodes(QString)));
            connect(moduleNet->trodesClient,SIGNAL(currentTimeReceived(quint32)),this,SLOT(setTimeFromTrodes(quint32)));
            connect(moduleNet->trodesClient,SIGNAL(timeRateReceived(quint32)),this,SLOT(setTimeRate(quint32)));
            connect(moduleNet->trodesClient,SIGNAL(startAquisitionEventReceived()),this,SLOT(turnRecordingOn()));
            connect(moduleNet->trodesClient,SIGNAL(stopAquisitionEventReceived()),this,SLOT(turnRecordingOff()));
            connect(moduleNet->trodesClient,SIGNAL(stateScriptCommandReceived(QString)),this,SLOT(sendScript(QString)));
            connect(moduleNet->trodesClient,SIGNAL(closeFileEventReceived()),this,SLOT(closeLogFile()));
            connect(moduleNet->trodesClient,SIGNAL(quitCommandReceived()),this,SLOT(close()));
            moduleNet->trodesClient->sendTimeRateRequest();
        } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {

            connect(networkClient, &StateScriptClient::sig_cmd_openFile, this, &MainWindow::setFileNameFromTrodes);
            connect(networkClient, &StateScriptClient::sig_cmd_time, this, &MainWindow::setTimeFromTrodes);
            connect(networkClient, &StateScriptClient::sig_cmd_timerate, this, &MainWindow::setTimeRate);
            connect(networkClient, &StateScriptClient::sig_cmd_acquisition, this, &MainWindow::getAcquisitionCmd);
            connect(networkClient, &StateScriptClient::sig_statescriptCommand, this, &MainWindow::sendScriptCmd);
            connect(networkClient, &StateScriptClient::sig_cmd_closeFile, this, &MainWindow::closeLogFile);
            connect(networkClient, &StateScriptClient::sig_cmd_quit, this, &MainWindow::close);
            connect(this, &MainWindow::newDebugMessage, networkClient, &StateScriptClient::sendDebugMessage);
            connect(this, &MainWindow::newOutputMessage, networkClient, &StateScriptClient::sendOutputMessage);
            connect(this, &MainWindow::newErrorMessage, networkClient, &StateScriptClient::sendErrorMessage);
            connect(this, &MainWindow::newStateScriptMessage, networkClient, &StateScriptClient::sendStateScriptMessage);
            networkClient->sendTimeRateRequest();
        }

        getCurrentTime_timer.start(100);

    }




    //menu setup
    //-------------------------------------------
    menuFile = new QMenu;
    menuBar()->addAction(menuFile->menuAction());

    menuLogFiles = new QMenu;
    menuFile->addAction(menuLogFiles->menuAction());
    actionCreateLogFile = new QAction(this);
    actionCreateLogFile->setEnabled(false);
    menuLogFiles->addAction(actionCreateLogFile);
    actionCloseLogFile = new QAction(this);
    actionCloseLogFile->setEnabled(false);
    menuLogFiles->addAction(actionCloseLogFile);

    menuFileFolders = new QMenu;
    menuFile->addAction(menuFileFolders->menuAction());
    actionChangeStateScriptFolder = new QAction(this);
    menuFileFolders->addAction(actionChangeStateScriptFolder);
    actionChangeLocalScriptFolder = new QAction(this);
    menuFileFolders->addAction(actionChangeLocalScriptFolder);

    connect(actionCreateLogFile, SIGNAL(triggered()), this, SLOT(createLogFile()));
    connect(actionCloseLogFile, SIGNAL(triggered()), this, SLOT(closeLogFile()));
    connect(actionChangeStateScriptFolder,SIGNAL(triggered()),this,SLOT(openStateScriptFolderSelector()));
    connect(actionChangeLocalScriptFolder,SIGNAL(triggered()),this,SLOT(openLocalCallbackScriptFolderSelector()));

    menuEdit = new QMenu;
    menuBar()->addAction(menuEdit->menuAction());
    actionUndo = new QAction(this);
    menuEdit->addAction(actionUndo);
    actionRedo = new QAction(this);
    menuEdit->addAction(actionRedo);
    menuEdit->addSeparator();
    actionCut = new QAction(this);
    menuEdit->addAction(actionCut);
    actionCopy = new QAction(this);
    menuEdit->addAction(actionCopy);
    actionPaste = new QAction(this);
    menuEdit->addAction(actionPaste);

    actionUndo->setEnabled(false);
    actionRedo->setEnabled(false);
    actionCut->setEnabled(false);
    actionCopy->setEnabled(false);
    actionPaste->setEnabled(false);

    actionSelectLocalLanguage = new QAction(this);
    //actionSelectLocalLanguage->setEnabled(false);
    menuEdit->addAction(actionSelectLocalLanguage);
    actionMacros = new QAction(this);
    menuEdit->addAction(actionMacros);
    connect(actionSelectLocalLanguage,SIGNAL(triggered()),this,SLOT(openLanguageSelector()));
    connect(actionMacros,SIGNAL(triggered()),this,SLOT(openMacroDialog()));

    /* Help Menu */
    menuHelp = new QMenu;
    menuHelp->setTitle("Help");
    actionAbout = new QAction(this);
    actionAbout->setText("About");
    actionAbout->setMenuRole(QAction::AboutRole);
    menuHelp->addAction(actionAbout);
    menuBar()->addAction(menuHelp->menuAction());
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));



    //-------------------------------------------
    TrodesFont dispFont;

    //Statusbar setup---------------------------
    statusbar = new QStatusBar(this);
    statusbar->setFont(dispFont);
    setStatusBar(statusbar);
    statusbar->showMessage("");
    //------------------------------------------

    //Layouts and tabs-----------------------------------------
    mainLayout =  new QGridLayout();
    panelSplitter = new QSplitter();
    panelSplitter->setOrientation(Qt::Vertical);

    //mainLayout->setMargin(0);
    mainLayout->setContentsMargins(QMargins(5,0,5,10));
    mainLayout->setVerticalSpacing(2);
    //mainLayout->setVerticalSpacing(0);
    scriptTabs = new QTabWidget(this);
    scriptTabs->setStyleSheet("QTabWidget::pane {margin: 0px}") ;
    scriptTabs->setTabPosition(QTabWidget::North);
    scriptTabs->setFont(dispFont);

    //editorWindow = new scriptEditor(this);
    //scriptTabs->addTab(editorWindow, QString("Embedded Script"));

    fileSelector = new FileSelectorWindow();
    fileSelector->setFont(dispFont);
    fileSelector->setFocusPolicy(Qt::NoFocus);
    fileSelector->changeStateScriptFolder(stateScriptFolder);
    fileSelector->changeLocalScriptFolder(localScriptFolder);
    scriptTabs->addTab(fileSelector, QString("File Selection"));


    connect(this,SIGNAL(newStateScriptFolder(QString)),fileSelector,SLOT(changeStateScriptFolder(QString)));
    connect(this,SIGNAL(newLocalCallbackFolder(QString)),fileSelector,SLOT(changeLocalScriptFolder(QString)));
    connect(fileSelector,SIGNAL(localScriptSelectorClicked()),this,SLOT(enableLanguageMenu()));


    localCallbackController = new localCallbackWidget(this);
    localCallbackController->setFont(dispFont);
    localCallbackController->setFocusPolicy(Qt::NoFocus);

    connect(networkClient, &StateScriptClient::sig_cblanguageCommand, localCallbackController, &localCallbackWidget::executeCode);

    scriptTabs->addTab(localCallbackController, QString("Observer"));
    panelSplitter->addWidget(scriptTabs);
    //connect(editorWindow,SIGNAL(textChanged()),this,SLOT(setUnsavedFile()));
    //mainLayout->addWidget(scriptTabs,1,0);
    //---------------------------------------------------------

    //Top control panel setup----------------------------------
    QGridLayout* headerLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
    headerLayout->setContentsMargins(QMargins(0,5,0,0));
    headerLayout->setHorizontalSpacing(3);
    int buttonLocation = 4;

    startLocalLanguageButton = new TrodesButton();

    if (!localCallbackLanguage.isEmpty()) {
        startLocalLanguageButton->setText(localCallbackLanguage);
        scriptTabs->setTabText(1,localCallbackLanguage);
    } else {
        startLocalLanguageButton->setText("Local");
    }

    startLocalLanguageButton->setCheckable(false);
    connect(startLocalLanguageButton,SIGNAL(pressed()),this,SLOT(languageButtonPressed()));
    //startLocalLanguageButton->setFixedSize(70,20);
    startLocalLanguageButton->setEnabled(false);
    headerLayout->addWidget(startLocalLanguageButton,0,buttonLocation++);

    if (currentOperationMode & STATESCRIPTMODULE_STANDALONEMODE) {
        cameraButton = new TrodesButton();
        cameraButton->setText(" Camera ");
        cameraButton->setCheckable(false);
        connect(cameraButton,SIGNAL(pressed()),this,SLOT(cameraButtonPressed()));
        //cameraButton->setFixedSize(70,20);
        headerLayout->addWidget(cameraButton,0,buttonLocation++);
    }

    /*
    tcpPortButton = new QPushButton();
    tcpPortButton->setText("Trodes");
    tcpPortButton->setCheckable(false);
    connect(tcpPortButton,SIGNAL(pressed()),this,SLOT(tcpPortButtonPressed()));
    tcpPortButton->setFixedSize(70,20);
    headerLayout->addWidget(tcpPortButton,0,3);
    */


    if (currentOperationMode & STATESCRIPTMODULE_DIRECTCONNECTION) {
        controllerButton = new TrodesButton();
        controllerButton->setText("Controller");
        controllerButton->setCheckable(false);
        connect(controllerButton,SIGNAL(pressed()),this,SLOT(controllerButtonPressed()));
        //controllerButton->setFixedSize(70,20);
        headerLayout->addWidget(controllerButton,0,buttonLocation++);
    }


    clearAllButton = new TrodesButton();
    clearAllButton->setFocusPolicy(Qt::NoFocus);

    clearAllButton->setText("Clear/stop");

    clearAllButton->setEnabled(false);
    //sendScriptButton->setFixedSize(70,20);
    connect(clearAllButton,SIGNAL(pressed()),this,SLOT(sendClearAll()));
    headerLayout->addWidget(clearAllButton,0,1);


    sendScriptButton = new TrodesButton();
    sendScriptButton->setFocusPolicy(Qt::NoFocus);
    if (currentOperationMode & STATESCRIPTMODULE_STANDALONEMODE) {
        sendScriptButton->setText("   Start   ");
    } else {
        sendScriptButton->setText("Send script");
    }
    sendScriptButton->setEnabled(false);
    //sendScriptButton->setFixedSize(70,20);
    connect(sendScriptButton,SIGNAL(pressed()),this,SLOT(sendScript()));
    connect(fileSelector,SIGNAL(stateScriptSelectorClicked()),this,SLOT(stateScriptFileSelected()));
    connect(fileSelector,SIGNAL(localScriptSelectorClicked()),this,SLOT(localScriptFileSelected()));
    headerLayout->addWidget(sendScriptButton,0,0);


    /*
    recordButton = new QPushButton();
    recordButton->setEnabled(false);
    connect(recordButton,SIGNAL(pressed()),this,SLOT(turnRecordingOn()));
    headerLayout->addWidget(recordButton,0,1);

    pauseButton = new QPushButton();
    pauseButton->setEnabled(false);
    connect(pauseButton,SIGNAL(pressed()),this,SLOT(turnRecordingOff()));
    headerLayout->addWidget(pauseButton,0,2);

    QPixmap pausePixmap(":/buttons/pauseImage.png");
    QPixmap recordPixmap(":/buttons/recordImage.png");
    QIcon recordButtonIcon(recordPixmap);
    QIcon pauseButtonIcon(pausePixmap);
    recordButton->setIcon(recordButtonIcon);
    pauseButton->setIcon(pauseButtonIcon);

    recordButton->setIconSize(QSize(15,15));
    pauseButton->setIconSize(QSize(10,10));

    recordButton->setFixedSize(50,20);
    pauseButton->setFixedSize(50,20);

    recordButton->setToolTip(tr("Record"));
    pauseButton->setToolTip(tr("Pause"));
    */


    //Time display

    QTime mainClock(0,0,0,0);
    QFont labelFont;
    labelFont.setPixelSize(20);
    labelFont.setFamily("Arial");
    timeLabel  = new QLabel;

    timeLabel->setText(mainClock.toString("hh:mm:ss.z"));
    timeLabel->setFont(labelFont);
    timeLabel->setFocusPolicy(Qt::NoFocus);
    //timeLabel->setMinimumWidth(100);
    //timeLabel->setMaximumWidth(100);
    timeLabel->setAlignment(Qt::AlignLeft);

    clockUpdateTimer = new QTimer(this);
    connect(clockUpdateTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    clockUpdateTimer->start(1); //update timer every 100 ms
    headerLayout->addWidget(timeLabel,0,buttonLocation++);

    headerLayout->setColumnStretch(3,1);
    mainLayout->addLayout(headerLayout,0,0);


    fileLabel = new QLabel();
    fileLabel->setFont(dispFont);
    fileLabel->setFocusPolicy(Qt::NoFocus);
    QString FileLabelColor("gray");
    QString FileLabelText("No recording file open");
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
    mainLayout->addWidget(fileLabel,1,0);

   //-----------------------------------------------------

    serialConsole = new Console(this);
    serialConsole->setFocusPolicy(Qt::NoFocus);
    serialConsole->setEnabled(false);
    serialConsole->setReadOnly(true);
    panelSplitter->addWidget(serialConsole);

    serial = new QSerialPort(this);
    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(serialConsole, SIGNAL(getData(QByteArray)), this, SLOT(writeData(QByteArray)));
    connect(serialConsole,SIGNAL(sendFinished()),this,SLOT(scriptSendFinished()));
    //connect(localCallbackController,SIGNAL(stateScriptCommandReceived(QString)),serialConsole,SLOT(sendString(QString)));
    connect(localCallbackController,SIGNAL(stateScriptCommandReceived(QString)),this,SLOT(writeData(QString)));

    connect(serialConsole,SIGNAL(newLineReceived(QString)),localCallbackController,SLOT(sendText(QString)));

    if (loadedWorkspace.networkConf.networkType==NetworkConfiguration::qsocket_based) {
        tcpClient = new TrodesClient(this);
        connect(tcpClient,SIGNAL(connected()),this,SLOT(setTcpClientConnected()));
        connect(tcpClient,SIGNAL(socketDisconnected()),this,SLOT(setTcpClientDisconnected()));
        connect(tcpClient,SIGNAL(socketErrorHappened(QString)),this,SLOT(displaySocketError(QString)));
        connect(tcpClient,SIGNAL(stateScriptCommandReceived(QString)),this,SLOT(receiveMessageFromTcpServer(QString)));
    }

    //Command input setup
    commandBox = new QGroupBox();
        commandLayout = new QGridLayout();
            commandInput = new CommandInput();     //text box for typing and sending single commands
            connect(commandInput,SIGNAL(returnPressed()),this,SLOT(sendCommand()));
        commandLayout->addWidget(commandInput,0,0,1,NUM_MACROS);
            macroLabel = new QLabel("Macros");
        commandLayout->addWidget(macroLabel,1,0,1,NUM_MACROS,Qt::AlignCenter);
            macroDlog = new macroDialog(this);  //dialog window for configuring macro buttons
            connect(macroDlog,SIGNAL(rejected()),this,SLOT(updateMacros()));
            for (int i = 0; i<NUM_MACROS; i++){ //add macro buttons below the command input
                macroButton[i] = new TrodesButton;
                macroButton[i]->setText(macroDlog->descriptionEdits[1]->text().replace(QString("\\n"),"\n"));
                macroButton[i]->setEnabled(true);
                commandLayout->addWidget(macroButton[i],2,i);
                connect(macroButton[i],SIGNAL(pressed()),SLOT(executeMacro()));
            }
        commandLayout->setContentsMargins(0,5,0,0);
    commandBox->setLayout(commandLayout);
    commandBox->setEnabled(false);
    commandBox->setStyleSheet("QGroupBox {border: 0px}" );
    mainLayout->addWidget(commandBox,3,0);
    getSavedMacros();

    //FTDIInit();
    connect(&FTDIReadTimer,SIGNAL(timeout()),this,SLOT(FTDIRead()));

    //----------------------------------------------------------

    connect(&compileCheckTimer,SIGNAL(timeout()),this,SLOT(sendCompileCommand()));
    mainLayout->addWidget(panelSplitter,2,0);
    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);


    QMetaObject::connectSlotsByName(this);
    retranslateUi();

    //Automatically connect to the hardware if it was given in the command line
    if (!autoConnectValue.isEmpty() && (currentOperationMode & STATESCRIPTMODULE_DIRECTCONNECTION)) {
        openSerialPort(autoConnectValue);
    }

}

MainWindow::~MainWindow() {

    if(trodesModule) {
        trodesModule->sendQuit();
    }

    if(cameraProgram){
        createNewDebugMessage("Waiting for cameraModule to end before closing...");
        cameraProgram->waitForFinished(5000);
    }
    endTrodesClient();
}


void MainWindow::setLocalCallbackLanguage(QString language, QString path) {
    bool valid = true;

    createNewDebugMessage(QString("Language: %1").arg(language));
    createNewDebugMessage(QString("Path: %1").arg(path));

    QProcess lang;
    lang.start(path, {"-h"});
    //lang.start(QStringLiteral("\"") + language + QStringLiteral("\""), {"-h"});
    if(!lang.waitForStarted(1000)){
        //process couldn't start
        createNewErrorMessage("Proccess did not start.");
        valid = false;
    }
    if(!lang.waitForFinished(1000)){
        //process couldn't finish
        createNewErrorMessage("Proccess did not finish.");
        valid = false;
    }
    if(lang.readAllStandardOutput().length()==0){
        //no help dialog outputted from process
        createNewErrorMessage("Proccess did not output a help dialog.");

        valid = false;
    }
    /*if(lang.exitCode() != 0){
        qDebug() << "Proccess did not have an exit code of 0. Exit code: " << lang.exitCode();
        valid = false;
    }*/

    if(!valid){
        QMessageBox::warning(this, "Warning: Bad observer path",
                             QString("The observer path may be incorrect. The command \"%1 -h\" did not execute properly. Please double check the executable path.")
                             .arg(path));
    }

    localCallbackLanguage = language;
    localCallbackLanguageLocation = path;

    startLocalLanguageButton->setText(localCallbackLanguage);
    scriptTabs->setTabText(1,localCallbackLanguage);

}

void MainWindow::updateTime() {
    //QTime currentTime;

    QString currentTimeString("");
    if (currentOperationMode & STATESCRIPTMODULE_STANDALONEMODE) {
//        int sourceSamplingRate = 1000;
        currentTimeStamp = serialConsole->currentTime();
        /*if(timestampspub != NULL){
            timestampspub->publishData(&currentTimeStamp, sizeof(currentTimeStamp));
        }*/
    } else if (currentOperationMode & STATESCRIPTMODULE_SLAVEMODE) {
        sourceSamplingRate = trodesTimeRate;
        currentTimeStamp = currentTrodesTime;
    }
    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp/(sourceSamplingRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*sourceSamplingRate);
    int minutesPassed = floor(tmpTimeStamp/(sourceSamplingRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*sourceSamplingRate);
    int secondsPassed = floor(tmpTimeStamp/(sourceSamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*sourceSamplingRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

    currentTimeString = QString::asprintf("%02d:%02d:%02d", hoursPassed, minutesPassed, secondsPassed);
//    if (hoursPassed < 10)
//        currentTimeString.append("0");
//    currentTimeString.append(QString::number(hoursPassed));
//    currentTimeString.append(":");
//    if (minutesPassed < 10)
//        currentTimeString.append("0");
//    currentTimeString.append(QString::number(minutesPassed));
//    currentTimeString.append(":");
//    if (secondsPassed < 10)
//        currentTimeString.append("0");
//    currentTimeString.append(QString::number(secondsPassed));
    //currentTimeString.append(".");
    //currentTimeString.append(QString::number(tenthsPassed));
    timeLabel->setText(currentTimeString);

}

void MainWindow::languageButtonPressed() {

    if (!localLanguageConnected) {
        localCallbackController->startEngine(localCallbackLanguage,localCallbackLanguageLocation,fileSelector->getCurrentLocalScriptSelection());
        fileSelector->disableLocalSelector();
        localLanguageConnected = true;

    } else {
        localCallbackController->killEngine();
        fileSelector->enableLocalSelector();
        localLanguageConnected = false;

    }

    //connect(newDialog,SIGNAL(startEngine(QString,QString,QString)),localCallbackController,SLOT(startEngine(QString,QString,QString)));
    //connect(newDialog,SIGNAL(disconnect()),localCallbackController,SLOT(killEngine()));
    //connect(newDialog,SIGNAL(disconnect()),fileSelector,SLOT(enableLocalSelector()));


}

void MainWindow::cameraButtonPressed() {


    cameraButton->setDown(false);


    if (cameraModuleConnected) {
        return;
    }

    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {


        /*if(server){
            for(auto const cl : server->getClients()){
                if(cl == CAMERA_MODULE_NETWORK_ID){
                    return;
                }
            }
        }


        if(!serveraddress.isEmpty() && serverport != -1){
            broker = new CentralBroker(serveraddress.toStdString().c_str(), serverport);
        }
        else{
            broker = new CentralBroker(DEFAULT_SERVER_ADDRESS, DEFAULT_SERVER_PORT);
        }

        if(server == NULL){

            server = new TrodesCentralServer(TRODES_NETWORK_ID,
                                             broker->getAddress().c_str(), broker->getPort());

            server->start();

            connect(this, &MainWindow::logFileOpened, server, &TrodesCentralServer::sendFileOpened);
            connect(this, &MainWindow::logFileClosed, server, &TrodesCentralServer::sendFileClose);
            connect(this, &MainWindow::recordingTurnedOn, server, &TrodesCentralServer::sendRecordingStarted);
            connect(this, &MainWindow::recordingTurnedOff, server, &TrodesCentralServer::sendRecordingStopped);
            connect(server, &TrodesCentralServer::moduleClosed, this,
                    [this](QString module){if(module == CAMERA_MODULE_NETWORK_ID){cameraModuleEnded();}});
            server->setModuleTimeRate(1000);

        }

        if(timestampspub == NULL){
            timestampspub = new HighFreqPub();
            timestampspub->initialize();
            server->registerNewContData(HighFreqDataType(TIMESTAMPS_SOCK, server->getID(), "t",
                                                         timestampspub->getAddress(), sizeof(uint32_t)));
        }*/

    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        // Create the global trodes module network object
        trodesNet = new TrodesModuleNetwork();

        // Set the ID to the specified ID for the main program.
        trodesNet->moduleID = TRODES_ID;
        trodesNet->tcpServer = new TrodesServer();

        //The server is able to respond to small data requests from the modules.
        //Here is where we give the server access to the possible data types requested.
        //So far just time.
        trodesNet->tcpServer->setModuleTimePtr(&currentTimeStamp);
        trodesNet->tcpServer->setModuleTimeRate(1000);
        connect(this, SIGNAL(logFileOpened(QString)), trodesNet->tcpServer, SLOT(sendFileOpened(QString)));
        connect(this, SIGNAL(logFileClosed()), trodesNet->tcpServer, SLOT(sendFileClose()));
        connect(this, SIGNAL(recordingTurnedOn()), trodesNet->tcpServer, SLOT(sendStartRecord()));
        connect(this, SIGNAL(recordingTurnedOff()), trodesNet->tcpServer, SLOT(sendStopRecord()));
        connect(trodesNet->tcpServer,SIGNAL(nameReceived(TrodesSocketMessageHandler*,QString)),this,SLOT(cameraModuleStarted()));
        connect(trodesNet->tcpServer,SIGNAL(clientDisconnected()),this,SLOT(cameraModuleEnded()));


        // connect slots related to data exchange among modules.  These just pass signals from a message handler through
        // tcpServer and on to trodesNet
        connect(trodesNet->tcpServer, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)), trodesNet, SLOT(sendAllDataAvailableToModule(TrodesSocketMessageHandler*)));
        connect(trodesNet->tcpServer, SIGNAL(doAddDataAvailable(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));
        connect(trodesNet->tcpServer, SIGNAL(doRemoveDataAvailable(qint8)), trodesNet, SLOT(removeDataAvailable(qint8)));
        connect(trodesNet->tcpServer, SIGNAL(nameReceived(TrodesSocketMessageHandler*,QString)), trodesNet->tcpServer,
                SLOT(setNamedModuleMessageHandler(TrodesSocketMessageHandler*,QString)));

        connect(trodesNet, SIGNAL(messageForModule(TrodesSocketMessageHandler*, TrodesMessage*)),
                trodesNet->tcpServer, SLOT(sendMessageToModule(TrodesSocketMessageHandler*, TrodesMessage*)));

        //find an available address and port
        //localhost is the last option if nothing else is available
        trodesNet->tcpServer->startServer("stateScript camera server");

        //qDebug() << "trodesServer =" << trodesNet->tcpServer->getCurrentAddress() << "port =" << trodesNet->tcpServer->serverPort();
    }

    //Then we create a new process for the camera module
    cameraProgram = new QProcess(this);
    //connect(cameraProgram,SIGNAL(started()),this,SLOT(cameraModuleStarted()));
    connect(cameraProgram,SIGNAL(finished(int)), this, SLOT(cameraModuleEnded()));

    // with this line, outputs stdout and stderr are piped back to trodes
    cameraProgram->setProcessChannelMode(QProcess::ForwardedChannels);

    SingleModuleConf s;
    s.moduleName = "cameraModule";
    s.sendTrodesConfig = 0;
    s.sendNetworkInfo = 1;
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        startSingleModule(s, cameraProgram);
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        trodesNet->startSingleModule(s,cameraProgram);
    }

}

void MainWindow::cameraModuleStarted() {
    createNewDebugMessage("Camera started");
    cameraModuleConnected = true;

    //If the user has already opened a log file, then we automatically create one in the
    //camera module too.

    if (logFileOpen) {
        //qDebug() << "Sending current filename: " << logFile.fileName();
        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
            //server->sendFileOpened(logFile.fileName());
            emit logFileOpened(logFile.fileName());
        } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
            trodesNet->tcpServer->sendFileOpened(logFile.fileName());
        }

        //server->sendFileOpened(logFile.fileName());
        //If recording has already begun, we automatically start recording
        if (isRecording) {

            if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
                //server->sendRecordingStarted();
                emit recordingTurnedOn();
            } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
                trodesNet->tcpServer->sendStartRecord();
            }
        }
    }
}

void MainWindow::cameraModuleEnded() {
    createNewDebugMessage("Camera module closed");
    cameraModuleConnected = false;

    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        /*delete server;
        server = NULL;
        delete broker;
        broker = NULL;*/
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        disconnect(trodesNet->tcpServer);
        trodesNet->tcpServer->close();
        trodesNet->deleteLater();
    }

}

void MainWindow::about() {
    QMessageBox::about(this, tr("About StateScript"), tr(qPrintable(GlobalConfiguration::getVersionInfo())));
}

//void MainWindow::tcpPortButtonPressed() {
//    tcpPortButton->setDown(false);

//    SocketDialog *newDialog = new SocketDialog(tcpClient,isTcpClientConnected, this);


//    newDialog->setWindowFlags(Qt::Popup);

//    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
//    //add something for Linux here too.
//#if defined (__WIN32__)
//    newDialog->setGeometry(QRect(this->x()+tcpPortButton->x()+8-160,this->y()+tcpPortButton->y()+70,40,100));
//#else
//    newDialog->setGeometry(QRect(this->x()+tcpPortButton->x()-160,this->y()+tcpPortButton->y()+43,40,100));
//#endif

//    //connect(this, SIGNAL(closeAllWindows()),newDialog,SLOT(close()));
//    connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
//    //connect(newDialog,SIGNAL(windowClosed()), SLOT(deleteLater()));
//    connect(newDialog,SIGNAL(tcpConnect(QString,quint16)),this,SLOT(tcpConnect(QString,quint16)));
//    connect(tcpClient,SIGNAL(connected()),newDialog,SLOT(connectionSuccessfull()));
//    connect(newDialog,SIGNAL(disconnect()),this,SLOT(tcpDisconnect()));

//    newDialog->show();

//}

void MainWindow::createNewDebugMessage(QString msg) {

    emit newDebugMessage(msg);
    qDebug() << msg;
}
void MainWindow::createNewOutputMessage(QString msg) {
    emit newOutputMessage(msg);
}
void MainWindow::createNewErrorMessage(QString msg) {

    emit newErrorMessage(msg);
    qDebug() << msg;
}

//When the controller button is pressed, a window pops up below the button
//with a port selecter and connect buttons.  This is used to connect to the
//stateScipt hardware
void MainWindow::controllerButtonPressed() {
    controllerButton->setDown(false);

    ConnectDialog *newDialog = new ConnectDialog(currentSerialPort,isSerialConnected, this);


    newDialog->setWindowFlags(Qt::Popup);

    newDialog->setGeometry(QRect(this->geometry().x()+controllerButton->x(),this->geometry().y()+controllerButton->y()+controllerButton->height()+this->menuBar()->height(),40,100));
    newDialog->setFixedWidth(200);

    //connect(this, SIGNAL(closeAllWindows()),newDialog,SLOT(close()));
    connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
    //connect(newDialog,SIGNAL(windowClosed()), SLOT(deleteLater()));
    connect(newDialog,SIGNAL(connectButtonPressed(QString)),this,SLOT(openSerialPort(QString)));
    connect(this,SIGNAL(serialConnected()),newDialog,SLOT(connectionSuccessfull()));
    connect(newDialog,SIGNAL(disconnect()),this,SLOT(closeSerialPort()));

    newDialog->show();

}

void MainWindow::setTimeRate(quint32 timeRate) {
    createNewDebugMessage(QString("StateScript received timerate: %1").arg(timeRate));

    trodesTimeRate = timeRate;
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        moduleNet->sendModuleIsReady(); //we send ready signal once we recieve the time base from Trodes
    }
}

void MainWindow::setTimeFromTrodes(quint32 newTime) {
    currentTrodesTime = newTime;
    updateTime();
}

void MainWindow::askTimeFromTrodes() {
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        moduleNet->trodesClient->sendTimeRequest();
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        networkClient->sendTimeRequest();
    }
}

void MainWindow::setFileNameFromTrodes(QString fn) {
    createNewDebugMessage("StateScript: got file name");
    QFileInfo fI = QFileInfo(fn);

    fileNameFromTrodes = fI.absolutePath()+"/"+fI.completeBaseName()+".stateScriptLog";
    if (gotCompileSignal) {
        createLogFile(fileNameFromTrodes);
    }
}

void MainWindow::setSSTextFromTrodes(QString text) {
    createNewDebugMessage("StateScript: got text to send to hardware");
    if (gotCompileSignal) {
        serialConsole->sendData(text.toLocal8Bit());
    }
}

void MainWindow::autoSourceConnect(QString source) {
    //auto connect to ECU hardware when trodes connects

    //qDebug() << "Got connection event: " << source;
    if (source == "Ethernet") {
        currentOperationMode |= STATESCRIPTMODULE_TRODESCONNECTION;
        openSerialPort("ECU");
    } else if (source == "USB") {
        currentOperationMode |= STATESCRIPTMODULE_DIRECTCONNECTION;
        openSerialPort("ECU");
    }

}

void MainWindow::openSerialPort(QString serialPortIn) {


    if (serialPortIn == "ECU") {
        if (currentOperationMode & STATESCRIPTMODULE_DIRECTCONNECTION) {
            //We are talking directly to the ECU using USB
            if (!FTDIInit()) {
                //display error dialog here
                return;
            }
        } else if (currentOperationMode & STATESCRIPTMODULE_TRODESCONNECTION) {
            //We are routing communication through the MCU via UDP socket
            if (!udpSocketInit()) {
                //display error dialog here
                return;
            }
        }
        enableConsole();
        currentSerialPort = "ECU";
    } else {
#ifdef __APPLE__
    // mac only
        //serialPortIn = "/dev/cu." + serialPortIn;
        serialPortIn = "/dev/" + serialPortIn;
    // end mac only

#endif

        serial->setPortName(serialPortIn);
        serial->setBaudRate(QSerialPort::Baud115200);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::HardwareControl);
        //qDebug() << "Port name: " << serialPortIn;
        if (serial->open(QIODevice::ReadWrite)) {
            enableConsole();
            currentSerialPort = serialPortIn;

            /*
            if (serial->setBaudRate(QSerialPort::Baud115200)
                    && serial->setDataBits(QSerialPort::Data8)
                    && serial->setParity(QSerialPort::NoParity)
                    && serial->setStopBits(QSerialPort::OneStop)
                    && serial->setFlowControl(QSerialPort::HardwareControl)) {

                enableConsole();
                currentSerialPort = serialPortIn;



            } else {
                serial->close();
                QMessageBox::critical(this, tr("Error"), serial->errorString());

            }*/
        } else {
            QMessageBox::critical(this, tr("Error"), serial->errorString());
        }
    }

}

void MainWindow::writeData(const QByteArray &data) {

    if (udpSocketInitialized) {
        udpSocketWrite(data);
    } else if (FTDI_Initialized){
        FTDIWrite(data);
    } else if (serial->isOpen()) {
        serial->write(data);
    }
    else {
        createNewErrorMessage("StateScript: No open device to write stateScript.");
    }
}

void MainWindow::writeData(const QString &data) {

    if (udpSocketInitialized){
        udpSocketWrite(data.toLocal8Bit());
    } else if (FTDI_Initialized) {
        FTDIWrite(data.toLocal8Bit());
    } else if (serial->isOpen()){
        serial->write(data.toLocal8Bit());
    }
    else {
        createNewErrorMessage("StateScript: No open device to write stateScript.");
    }
}

void MainWindow::readData() {
    //reads data stream from serial port and forwards to the console
    QByteArray data = serial->readAll();
    if (logFileOpen && isRecording) {
        logFile.write(data);
    }
    serialConsole->putData(data);

}

void MainWindow::closeSerialPort() {

    if (FTDI_Initialized) {
        closeFTDI();
    } else if (udpSocketInitialized) {
        closeUdpSocket();
    } else {
        serial->close();
    }

    gotCompileSignal = false;
    serialConsole->setEnabled(false);
    serialConsole->disconnected();
    QTextDocument* d = serialConsole->document();
    d->setPlainText("");
    //serialConsole->setDocument(QTextDocument(""));
    QPalette p = serialConsole->palette();
    p.setColor(QPalette::Window, Qt::gray);
    serialConsole->setPalette(p);
    currentSerialPort = "";
    isSerialConnected = false;
    if (logFileOpen) {
        if (isRecording) {
            turnRecordingOff();
        }

        closeLogFile();
        //recordButton->setEnabled(false);
        //pauseButton->setEnabled(false);
    }


    sendScriptButton->setEnabled(false);
    clearAllButton->setEnabled(false);
    commandBox->setEnabled(false);

}


bool MainWindow::udpSocketInit() {
    udpSocket = new QUdpSocket(this);
    udpReturnSocket = new QUdpSocket(this);
    QHostAddress tmpAddress;
    tmpAddress.setAddress("192.168.0.255");

    //We use the source port of the send socket to receive ack signals
    if (!udpSocket->bind(QHostAddress::Any,udpSocket->localPort() ,QUdpSocket::ShareAddress)) {
        newErrorMessage("Error binding to source port");
        return false;
    }


    //We use a completely separate port to recieve incoming data from the hardware
    if (!udpReturnSocket->bind(TRODESHARDWARE_STATESCRIPTRETURNPORT)) {
        newErrorMessage("Error binding to return port");
        return false;
    }

    connect(udpSocket,SIGNAL(readyRead()),this,SLOT(udpSocketRead()));
    connect(udpReturnSocket,SIGNAL(readyRead()),this,SLOT(udpReturnSocketRead()));
    udpSocketInitialized = true;
    return true;

}

void MainWindow::udpReturnSocketRead() {
    //Read all pending datagrams from the udp socket and forwards to the console


    QByteArray dataGram;

    while (udpReturnSocket->hasPendingDatagrams()) {

        dataGram.resize(udpReturnSocket->pendingDatagramSize());
        udpReturnSocket->readDatagram(dataGram.data(),dataGram.size());
        if (logFileOpen && isRecording) {
            logFile.write(dataGram);
        }
        serialConsole->putData(dataGram);
        //qDebug() << "Got message back: " << dataGram;

    }

}

void MainWindow::udpSocketRead() {
    //Read all pending datagrams from the udp socket and forwards to the console
    //Right now, just acknowlegde signals come through here, and nothing is done with them

    QByteArray dataGram;

    while (udpSocket->hasPendingDatagrams()) {
        dataGram.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(dataGram.data(),dataGram.size());
        //TODO:  use ack for something??
    }

}

void MainWindow::closeUdpSocket() {
    if (udpSocketInitialized) {
        udpSocket->close();
        udpSocket->deleteLater();
    }
    udpSocketInitialized = false;
}

bool MainWindow::udpSocketWrite(const QByteArray &data) {
    //qDebug() << "Write to socket";
    //QByteArray datagram;
    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
  /*#ifdef WIN32
    if (udpSocket->writeDatagram(data, data.size(), QHostAddress::Broadcast, TRODESHARDWARE_STATESCRIPTPORT) == -1) {
        return false;
    }
  #else*/
    QHostAddress tmpAddress;
    tmpAddress.setAddress("192.168.0.254");
    if (udpSocket->writeDatagram(data, data.size(),tmpAddress, TRODESHARDWARE_STATESCRIPTPORT) == -1) {
        return false;
    }
    //udpSocket->flush();

  //#endif
    return true;

}



bool MainWindow::FTDIInit() {
    // Setup FTDI FT2232H interface
    DWORD baud = 115200;


  #if defined (__linux) || defined (__APPLE__)
    // need to manually set VID and PID on linux
    res = FT_SetVIDPID(VENDOR, DEVICE);
    if (res != FT_OK) {
      newErrorMessage("SetVIDPID failed");
      return false;
    }
  #endif

    res = FT_OpenEx((void*)desc, FT_OPEN_BY_DESCRIPTION, &ftdi);

    if (res != FT_OK) {
        newErrorMessage("Open FTDI failed; (See Resources/SetupHelp/).");
        //emit stateChanged(SOURCE_STATE_CONNECTERROR);
        return false;

    }

    res = FT_ResetDevice(ftdi);

    res |= FT_SetDataCharacteristics(ftdi, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE);
    res |= FT_SetBaudRate(ftdi, baud);
    res |= FT_SetUSBParameters(ftdi, 65536, 65536);	//Set USB request transfer size
    res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
    res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
    //res |= FT_SetBitMode(ftdi, 0xff, 0x40);
    res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
    res |= FT_SetLatencyTimer(ftdi, 2);
    //res |= FT_SetTimeouts(ftdi, 1000, 0);
    res |= FT_SetTimeouts(ftdi, 10, 0);
    if (res != FT_OK) {
      newErrorMessage("Error initializing FTDI device");
      return false;
    }

    FTDI_Initialized = true;
    FTDIReadTimer.start(10);
    newDebugMessage("FTDI opened");
    return true;

}

bool MainWindow::FTDIWrite(const QByteArray &data) {
    //Write data to USB using d2xx driver

    DWORD BytesWritten;
    QByteArray writeArray;
    writeArray.append(data.constData(),data.size());

    res = FT_Write(ftdi, writeArray.data(), writeArray.size(), &BytesWritten);
    if (res != FT_OK) {
      newErrorMessage("Error writing to FTDI");
      return false;
    }

    return true;

}

void MainWindow::FTDIRead() {
    //Read all available data from USB using d2xx driver and forwards to the console

    DWORD RxBytes;
    DWORD BytesReceived;

    FT_GetQueueStatus(ftdi,&RxBytes);
    if (RxBytes > 0) {
        char RxBuffer[256];
        if (RxBytes > sizeof(RxBuffer) - 1){};
        RxBytes = sizeof(RxBuffer) - 1;
        res = FT_Read(ftdi,RxBuffer,RxBytes,&BytesReceived);
        if (res == FT_OK) {
            RxBuffer[BytesReceived] = 0;
            //QByteArray bufArray(RxBuffer,256);
            QByteArray bufArray = QByteArray::fromRawData(RxBuffer,BytesReceived);
            if (logFileOpen && isRecording) {
                logFile.write(bufArray);
            }
            serialConsole->putData(bufArray);
            emit newStateScriptMessage(QString(bufArray));

        }
    }
}


void MainWindow::closeFTDI() {
    FTDIReadTimer.stop();
    res = FT_Close(ftdi);
    if (res != FT_OK) {
      newErrorMessage("Error closing");
    }

    newDebugMessage("Closed FT2232H device.");
    FTDI_Initialized = false;

}

void MainWindow::startCameraModule(QString path, QStringList arguments) {

}

void MainWindow::stopCameraModule() {

}

void MainWindow::setTcpClientConnected() {
    isTcpClientConnected = true;
    //qDebug() << "connected";
}

void MainWindow::setTcpClientDisconnected() {
    isTcpClientConnected = false;
    //qDebug() << "disconnected";
}

void MainWindow::tcpConnect(QString addressIn, quint16 portIn) {

    tcpClient->setAddress(addressIn);
    tcpClient->setPort(portIn);
    tcpClient->connectToHost();

    currentTcpAddress = addressIn;
    currentTcpPort = portIn;
}

void MainWindow::tcpDisconnect() {
    tcpClient->disconnectFromHost();

}

void MainWindow::getAcquisitionCmd(QString cmd, uint32_t time){
    if(cmd == acq_RECORD){
        turnRecordingOn();
    }
    else if(cmd == acq_STOPRECORD){
        turnRecordingOff();
    }
}

void MainWindow::turnRecordingOn() {
    if (logFileOpen) {
        isRecording = true;
        //pauseButton->setEnabled(true);
        //recordButton->setEnabled(false);
        statusbar->showMessage("Recording on");
        actionSelectLocalLanguage->setEnabled(false);
        emit recordingTurnedOn();
    }


}

void MainWindow::turnRecordingOff() {
    if (isRecording) {
        isRecording = false;
        //pauseButton->setEnabled(false);
        //recordButton->setEnabled(true);
        statusbar->showMessage("Recording off");
        actionSelectLocalLanguage->setEnabled(true);
        emit recordingTurnedOff();
    }

}

void MainWindow::openStateScriptFolderSelector() {
    //dialog to select new folder location

    if (!QDir(stateScriptFolder).exists()) {

        #ifdef linux
        stateScriptFolder = "/Home/";
        #endif
        #ifdef WIN32
        stateScriptFolder = "C:\\";
        #endif
        #ifdef __APPLE__
        stateScriptFolder = "/Users/";
        #endif
    }

    QString dirName = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                     stateScriptFolder,
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);

    if (!(dirName == "")) {
        stateScriptFolder = dirName;

        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("stateScriptFolder"), stateScriptFolder);
        settings.endGroup();

        emit newStateScriptFolder(stateScriptFolder);
        sendScriptButton->setEnabled(false);
        scriptFileSelected = false;
    }
}

void MainWindow::enableLanguageMenu() {
    startLocalLanguageButton->setEnabled(true);
    //actionSelectLocalLanguage->setEnabled(true);
}

void MainWindow::openLanguageSelector() {

    LanguageDialog *newDialog = new LanguageDialog(fileSelector->getCurrentLocalScriptSelection(), this);


    //newDialog->setWindowFlags(Qt::Popup);
    newDialog->setWindowFlags(Qt::Dialog);

    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.

    //connect(this, SIGNAL(closeAllWindows()),newDialog,SLOT(close()));
    //connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
    //connect(newDialog,SIGNAL(windowClosed()), SLOT(deleteLater()));

    //connect(newDialog,SIGNAL(startEngine(QString,QString,QString)),localCallbackController,SLOT(startEngine(QString,QString,QString)));
    //connect(localCallbackController,SIGNAL(programStarted()),newDialog,SLOT(connectionSuccessfull()));
    //connect(localCallbackController,SIGNAL(programStarted()),fileSelector,SLOT(disableLocalSelector()));
    //connect(newDialog,SIGNAL(disconnect()),localCallbackController,SLOT(killEngine()));
    //connect(newDialog,SIGNAL(disconnect()),fileSelector,SLOT(enableLocalSelector()));

    connect(newDialog,SIGNAL(setLanguage(QString,QString)),this,SLOT(setLocalCallbackLanguage(QString,QString)));

    newDialog->show();
}

void MainWindow::openLocalCallbackScriptFolderSelector() {
    //dialog to select new folder location

    if (!QDir(localScriptFolder).exists()) {

        #ifdef linux
        localScriptFolder = "/Home/";
        #endif
        #ifdef WIN32
        localScriptFolder = "C:\\";
        #endif
        #ifdef __APPLE__
        localScriptFolder = "/Users/";
        #endif
    }

    QString dirName = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                     localScriptFolder,
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);

    if (!(dirName == "")) {
        localScriptFolder = dirName;
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("localScriptFolder"), localScriptFolder);
        settings.endGroup();
        emit newLocalCallbackFolder(localScriptFolder);
        //localFileSelected = false;
        startLocalLanguageButton->setEnabled(false);
//        actionSelectLocalLanguage->setEnabled(false);
    }
}

void MainWindow::displaySocketError(QString errMsg) {
    QMessageBox::critical(this, tr("Error"), errMsg);
}

void MainWindow::receiveMessageFromTcpServer(QString msg) {

    if (isSerialConnected) {
        serialConsole->sendData(msg.toLocal8Bit());
    }
    //qDebug() << msg;

}




void MainWindow::closeEvent(QCloseEvent* event) {
    QMessageBox::StandardButton reply;

    if (unSavedData) {
        //Warn that there is unsaved data
        QMessageBox messageBox;
        reply = messageBox.question(0,"Unsaved data","You have unsaved changes. Discard?",QMessageBox::Cancel|QMessageBox::Discard);

    } else {
        reply = QMessageBox::Discard;
    }

    if (reply == QMessageBox::Discard) {
        event->accept();
    } else {
        event->ignore();
    }

}

void MainWindow::sendClearAll() {
     serialConsole->sendData(QString("clear all;\n").toLocal8Bit());
}

void MainWindow::sendCommand(){
    QString commands = commandInput->text().replace(QString("\\n"),"\n");
    serialConsole->sendData((commands+"\n").toLocal8Bit());
    commandInput->eraseAndStore();

}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    //A keyboard key was pressed.
    //qDebug() << "Key";
    if ((event->key() == Qt::Key_Left) || (event->text() == "a")) {


    } else if ((event->key() == Qt::Key_Right) || (event->text() == "d")){

    }
    //graphicsWindow->passKeyEvent(event);
}

void MainWindow::sendScript() {
    //QDir::toNativeSeparators(scFilePath+"/"+stateScriptSelector->currentIndex().data().toString())


    if (logFileOpen && (currentOperationMode & STATESCRIPTMODULE_STANDALONEMODE)) {
        QMessageBox::StandardButton reply;
          reply = QMessageBox::question(this, "Test", "End session?",
                                        QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::Yes) {
            closeLogFile();
            sendScriptButton->setText("Start");
          }
    } else {


        QString fileName =  fileSelector->getCurrentStateScriptSelection();
        QString filePath = fileSelector->getCurrentStateScriptFolder();

        if (!fileName.isEmpty()) {
            QFile file;


            file.setFileName(QDir::toNativeSeparators(filePath+"/"+fileName));
            if( !file.open( QIODevice::ReadOnly ) ) {
                newErrorMessage(QString("File %1 not found").arg(QDir::toNativeSeparators(filePath+"/"+fileName)));
                return;
            }
            /*
        QByteArray fileData;
        while (!file.atEnd()) {
            file.readAll()
        }*/
            waitingForScriptSend= true;
            //serialConsole->sendData(QString("clear all;\n").toLocal8Bit());
            for (int i=0; i<ignoreUpdatePorts.length(); i++) {
                QString tempUpdateCommand;
                tempUpdateCommand = QString("updates off %1;\n").arg(ignoreUpdatePorts[i]);

                serialConsole->sendData(tempUpdateCommand.toLocal8Bit());
            }
            serialConsole->sendData(file.readAll());
            //serialConsole->sendData(QString("disp('Ready to begin session');\n").toLocal8Bit());
            file.close();
            serialConsole->sendData("\n");
        }
    }

}

void MainWindow::sendScriptCmd(QString script) {
    //Send a script stored in the QString.  This is called when another module sends a TRODESMESSAGE_STATESCRIPT
    //command to trodes, which then forwards the message on to stateScript.
    if (!script.isEmpty()) {
        //qDebug() << "stateScript: sending script to ECU: " << script;
        waitingForScriptSend= true;
        waitingForAPIScriptSend = true;
        //serialConsole->sendData(QString("clear all;\n").toLocal8Bit());

        for (int i=0; i<ignoreUpdatePorts.length(); i++) {
            QString tempUpdateCommand;
            //tempUpdateCommand = QString("updates off ") + ignoreUpdatePorts[i] + ";\n";
            tempUpdateCommand = QString("updates off %1;\n").arg(ignoreUpdatePorts[i]);

            serialConsole->sendData(tempUpdateCommand.toLocal8Bit());
        }
        serialConsole->sendData(script.toLocal8Bit());
        //serialConsole->sendData(QString("disp('Ready to begin session');\n").toLocal8Bit());
    }
}

void MainWindow::scriptSendFinished() {


    if (waitingForScriptSend) {
        waitingForScriptSend = false;
        if (!waitingForAPIScriptSend && (currentOperationMode & STATESCRIPTMODULE_STANDALONEMODE)) {
            createLogFile();
        }
    }
    if (waitingForAPIScriptSend) {
        waitingForAPIScriptSend = false;
    }
}

void MainWindow::stateScriptFileSelected() {
    scriptFileSelected = true;
    if (gotCompileSignal) {
        sendScriptButton->setEnabled(true);
    }

    //Some people do not use local scripts, so it should not be a requirement that one is selected
    /*if (gotCompileSignal && scriptFileSelected && localFileSelected ) {
        sendScriptButton->setEnabled(true);
    }*/
}

void MainWindow::localScriptFileSelected() {
    localFileSelected = true;

    //Some people do not use local scripts, so it should not be a requirement that one is selected
    /*if (gotCompileSignal && scriptFileSelected && localFileSelected ) {
        sendScriptButton->setEnabled(true);
    }*/
}

void MainWindow::createLogFile() {
    QString dataDir = logFileFolder;
    QString fileName;
    QString defaultFileName;
    QDateTime fileCreateTime = QDateTime::currentDateTime();


    defaultFileName = QString("log") + fileCreateTime.toString("MM.dd.yyyy").replace(".","-") +
                      tr("(") + fileCreateTime.toString("hh.mm.ss").replace(".","_") + tr(").stateScriptLog");
    dataDir = dataDir + "/" + defaultFileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Create Log File"),dataDir,tr("*.stateScriptLog"));


    if (!fileName.isEmpty()) {
        createLogFile(fileName);
    }
}


void MainWindow::createLogFile(QString fileName) {

    QFileInfo fileInfo;
    fileInfo.setFile(fileName);

    logFileFolder = fileInfo.absolutePath();
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("paths"));
    settings.setValue(QLatin1String("logFileFolder"), logFileFolder);
    settings.endGroup();

    logFile.setFileName(fileName);
    if( !logFile.open( QIODevice::WriteOnly ) ) {
        newErrorMessage(QString("File %1 could not be opened").arg(fileName));
        return;
    }

    logFileOpen = true;
    actionCreateLogFile->setEnabled(false);
    actionCloseLogFile->setEnabled(true);

    if (isSerialConnected) {
        //recordButton->setEnabled(true);
        //pauseButton->setEnabled(false);
    }
    QString FileLabelColor("red");
    //QString FileLabelText("No file open");
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, fileName));
    statusbar->showMessage("Log file opened: "+ fileName);

    if (currentOperationMode & STATESCRIPTMODULE_STANDALONEMODE) {
        turnRecordingOn();
        sendScriptButton->setText("End");
    }

    insertFileText(fileSelector->getCurrentStateScriptSelection(),fileSelector->getCurrentStateScriptFolder());
    insertFileText(fileSelector->getCurrentLocalScriptSelection(),fileSelector->getCurrentLocalScriptFolder());
    emit logFileOpened(fileName);
}

void MainWindow::insertFileText(QString filename, QString filepath){
    if (!filename.isEmpty()){
        QFile selectedFile;
        selectedFile.setFileName(QDir::toNativeSeparators(filepath+"/"+filename));
        if (!selectedFile.open(QIODevice::ReadOnly)){
            logFile.write("#Error trying to open " + filename.toLocal8Bit());
        }
        else{
            logFile.write("#<"+filename.toLocal8Bit()+">");
            QByteArray lineFromFile = selectedFile.readLine();
            while(lineFromFile.length()>0){
                logFile.write("\n#"+lineFromFile.left(lineFromFile.length()-1));
                lineFromFile = selectedFile.readLine();
            }
            selectedFile.close();
        }
        logFile.write("\n#\n");
    }
}

void MainWindow::closeLogFile() {
    if (isRecording) {
        turnRecordingOff();
    }

    logFile.close();
    logFileOpen = false;
    if (isSerialConnected) {
        actionCreateLogFile->setEnabled(true);
    }
    actionCloseLogFile->setEnabled(false);
    QString FileLabelColor("gray");
    QString FileLabelText("No recording file open");
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));

    fileNameFromTrodes = "";

    //recordButton->setEnabled(false);
    //pauseButton->setEnabled(false);

    statusbar->showMessage("Log file closed");

    emit logFileClosed();
}


void MainWindow::sendCompileCommand() {


    if (serialConsole->lastLineReceived.contains("~~~")&&(!gotCompileSignal)) {
        //We got a response from the hardware.  Now send clock command;
        //qDebug() << "Sending clock command.";
        gotCompileSignal = true;
        compileCheckTimer.stop();
        clearAllButton->setEnabled(true);
        commandBox->setEnabled(true);
        serialConsole->sendString("clock(reset);\r\n");
        if (scriptFileSelected) {
            sendScriptButton->setEnabled(true);
        }

        /*
        QUdpSocket t;
        QHostAddress tmpAddress;
        tmpAddress.setAddress("192.168.0.255");


        QByteArray out;
        uint16_t sendVal = 12;
        TrodesDataStream outStream(&out, QIODevice::WriteOnly);
        outStream.setVersion(TrodesDataStream::Qt_4_0);
        outStream << sendVal;


        if (t.writeDatagram(out,tmpAddress, TRODESHARDWARE_ECUDIRECTPORT) == -1) {
            qDebug() << "Cant write to port";
        }
        */

        /*
        if (t.writeDatagram("HEl",3,tmpAddress, TRODESHARDWARE_ECUDIRECTPORT) == -1) {
            qDebug() << "Cant write to port";
        }*/

        isSerialConnected = true;

        if (!logFileOpen) {
            actionCreateLogFile->setEnabled(true);
        }
        if (logFileOpen) {
            //recordButton->setEnabled(true);
            //pauseButton->setEnabled(false);
        }

        //If we have received a file name from Trodes, then open the file now.
        if (!fileNameFromTrodes.isEmpty()) {
            createLogFile(fileNameFromTrodes);
        }


        emit serialConnected();

    } else {
        if (numCompileChecks < 10) {

            serialConsole->sendString(";\r\n");
            numCompileChecks++;
        } else {
            compileCheckTimer.stop();
            closeSerialPort();
        }
    }

}

void MainWindow::enableConsole() {
    serialConsole->setEnabled(true);
    serialConsole->setLocalEchoEnabled(true);
    //actionDisconnect->setEnabled(true);
    //actionConnect->setEnabled(false);
    QPalette p = serialConsole->palette();
    p.setColor(QPalette::Window, Qt::white);
    serialConsole->setPalette(p);

    //Send the compile signal until we get a response
    serialConsole->lastLineReceived = "";
    //connect(&compileCheckTimer,SIGNAL(timeout()),this,SLOT(sendCompileCommand()));
    numCompileChecks = 0;
    compileCheckTimer.start(500);

}

void MainWindow::resizeEvent(QResizeEvent *) {
    emit closeDialogs();

    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("geometry"), saveGeometry());
    //settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *) {
    emit closeDialogs();

    //Remember the new position for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("geometry"), saveGeometry());
    //settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::getSavedMacros(){
    //get the QStringlists from memory
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("macros"));
        macroDescriptions = settings.value(QLatin1String("descriptions")).toStringList();
        macroSnippets = settings.value(QLatin1String("snippets")).toStringList();
    settings.endGroup();
    // populate the linedits in the macro dialog with the strings we got from memory
    for (int i = 0;i< NUM_MACROS; i++){
        macroDlog->descriptionEdits[i]->setText(macroDescriptions.value(i));
        macroDlog->codeEdits[i]->setText(macroSnippets.value(i));
        macroButton[i]->setText(macroDlog->descriptionEdits[i]->text().replace(QString("\\n"),"\n")); //Update the labels on the macro buttons
    }
}

void MainWindow::updateMacros(){
    macroDescriptions.clear();
    macroSnippets.clear();
    for (int i = 0;i< NUM_MACROS; i++){
        macroDescriptions <<macroDlog->descriptionEdits[i]->text(); //
        macroSnippets<<macroDlog->codeEdits[i]->text();
        macroButton[i]->setText(macroDlog->descriptionEdits[i]->text().replace(QString("\\n"),"\n")); //Update the labels on the macro buttons
    }

    //Update the QSettings with the macro values
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("macros"));
        settings.setValue(QLatin1String("descriptions"),macroDescriptions);
        settings.setValue(QLatin1String("snippets"),macroSnippets);
    settings.endGroup();
}

void MainWindow::openMacroDialog(){
    macroDlog->exec();
}

void MainWindow::executeMacro(){
    int macroNumber = 0;
    for (int i = 0; i<NUM_MACROS; i++){
        if (sender()==macroButton[i]){
            macroNumber = i;
            break;
        }
    }
    QString commands = macroDlog->codeEdits[macroNumber]->text().replace(QString("\\n"),"\n");
    if(!commands.isEmpty()){
        serialConsole->sendData((commands+"\n").toLocal8Bit());
    }
}

bool MainWindow::startTrodesClient(QString serveraddress, int port){
    bool successfullyStarted;
    networkClient = new StateScriptClient(serveraddress, port);
    if (networkClient->initialize("StateScriptModule") == -1) {
        successfullyStarted = false;
        delete networkClient;
        networkClient = NULL;
    }
    else {
        successfullyStarted = true;
    }

    return successfullyStarted;
}

void MainWindow::endTrodesClient(){
    if(networkClient){
        delete networkClient;
    }
    /*if(server){
        server->sendQuit();
        delete server;
        server = NULL;
    }
    if(broker){
        delete broker;
        broker = NULL;
    }*/
}

bool MainWindow::startTrodesNetwork() {

    try{

        /*if(server){
            for(auto const cl : server->getClients()){
                if(cl == CAMERA_MODULE_NETWORK_ID){
                    return;
                }
            }
        }*/


        /*if(timestampspub == NULL){
            timestampspub = new HighFreqPub();
            timestampspub->initialize();
            server->registerNewContData(HighFreqDataType(TIMESTAMPS_SOCK, server->getID(), "t",
                                                         timestampspub->getAddress(), sizeof(uint32_t)));
        }*/


        QString qs_address;
        int port;

        if(serveraddress.isEmpty()) {
            serveraddress = QString(DEFAULT_SERVER_ADDRESS);
            serverport = DEFAULT_SERVER_PORT;
        }

        qs_address = serveraddress;
        port = serverport;

        if(!qs_address.startsWith("tcp://")){
            qs_address.insert(0, "tcp://");
        }

        std::string address = qs_address.toStdString();
        // address is a std::string with the network address
        // port is an int with the network port

        trodesModule = std::move(std::unique_ptr<trodes::TrodesModule>(new trodes::TrodesModule(address, port)));

        trodesModule->setModuleTimePtr(&currentTimeStamp);
        trodesModule->setModuleTimeRate(1000);

       /* connect(server, &TrodesCentralServer::moduleClosed, this,
                [this](QString module){if(module == CAMERA_MODULE_NETWORK_ID){cameraModuleEnded();}});*/
        //server->setModuleTimeRate(1000);

        connect(trodesModule.get(), &trodes::TrodesModule::moduleClosed, this, &MainWindow::handleModuleClose, Qt::QueuedConnection); //Placeholder function. Doesn't do anything yet.
        connect(trodesModule.get(), &trodes::TrodesModule::moduleConnected, this, &MainWindow::handleModuleOpened, Qt::QueuedConnection);
        connect(this, &MainWindow::logFileOpened, trodesModule.get(), &trodes::TrodesModule::sendFileOpened, Qt::QueuedConnection);
        connect(this, &MainWindow::logFileClosed, trodesModule.get(), &trodes::TrodesModule::sendFileClose, Qt::QueuedConnection);
        //connect(this, &TrodesCoreUtil::sourceConnected, trodesModule.get(), &trodes::TrodesModule::sendSourceConnect, Qt::QueuedConnection);
        connect(this, &MainWindow::recordingTurnedOn, trodesModule.get(), &trodes::TrodesModule::sendRecordingStarted, Qt::QueuedConnection);
        connect(this, &MainWindow::recordingTurnedOff, trodesModule.get(), &trodes::TrodesModule::sendRecordingStopped, Qt::QueuedConnection);
        connect(this, &MainWindow::newErrorMessage, trodesModule.get(), &trodes::TrodesModule::sendConsoleErrorMessage, Qt::QueuedConnection);
        connect(this, &MainWindow::newDebugMessage, trodesModule.get(), &trodes::TrodesModule::sendConsoleDebugMessage, Qt::QueuedConnection);
        connect(this, &MainWindow::newOutputMessage, trodesModule.get(), &trodes::TrodesModule::sendConsoleOutputMessage, Qt::QueuedConnection);



        // keep these because they're necessary to generate the connects
        qRegisterMetaType<uint16_t>("uint16_t");

        return true;

    } catch(std::string &e){
        createNewErrorMessage(QString("Could not start the network. %1").arg(e.c_str()));
        return false;
    }
}

void MainWindow::endTrodesNetwork() {
    //shutdown the trodes network
    trodesModule.reset();
    createNewDebugMessage("ZMQ-based network shut down.");
}

void MainWindow::handleModuleClose(QString m) {
    //Do something when a module closes?

}

void MainWindow::handleModuleOpened(QString m) {
    //Do something when module is opened
    if (m.compare(CAMERA_MODULE_NETWORK_ID) == 0) {
        cameraModuleStarted();
    }
}


//*************Copied directly from trodessocket.
//TODO: put this into some kind of qt network headers/sources for qprocess, signals/slots, etc
QProcess *MainWindow::startSingleModule(SingleModuleConf s, QProcess *moduleProcess)
{
    // QProcess is optional -- this way caller can set up things like stderr/stdout forwarding, if needed

    // TODO:
    // -Try to call it from FSGui, stateScript?) Paths are relative to calling App,
    // not Trodes
    // -How to handle failure? Return same process, unstarted, or destroy the process and return NULL

    // N.B. relative paths are treated as relative to the directory containing
    // the executable (or, on a Mac, the app bundle) that launched the module
    // (this is usually Trodes). This is different than the current working
    // directory of the app.

    // If the filename component of the path is a bare module name (e.g.
    // "cameraModule"), then the behavior is platform-dependent. On a Mac, if
    // the requested file does not exist we look for an app bundle by that
    // name. On Windows, we look for an executable named moduleName.exe. On
    // Linux we just use the moduleName as-is

    QString moduleAbsName; // Absolute path to use
    QFileInfo moduleAbsFileInfo;

    // Absolute path of directory containing the module, or module app bundle
    // (potentially after relative path moves)
    QString moduleWorkingDir;

    QString moduleBaseName = QFileInfo(s.moduleName).completeBaseName();

    // Parent doesn't much matter (only used for QObject tree), process will still be killed when calling program exits, e.g.
    if (!moduleProcess)
      moduleProcess = new QProcess();

    //Get the directory containing the calling executable (usu. Trodes)
    QString callingAppDir = QCoreApplication::applicationDirPath();

#ifdef __APPLE__
    //If calling app is an app bundle, use its parent as the base directory
    if (callingAppDir.endsWith(QString(".app/Contents/MacOS")))
        callingAppDir = QDir::cleanPath(callingAppDir + "/../../../");
#endif

    // Convert moduleName to absolute path
    if (QFileInfo(s.moduleName).isRelative()){
        moduleAbsName= QDir::cleanPath(callingAppDir + "/" + s.moduleName);
        //qDebug() << "[MainWindow::startModule] 'moduleName' string (" << s.moduleName << ") converted to absolute path: " << moduleAbsName;
    } else {

        // Use absolute path provided
        moduleAbsName = s.moduleName;
        //qDebug() << "[MainWindow::startModule] 'moduleName' string (" << s.moduleName << ") is an absolute path (NOT RECOMMENDED); used as-is.)";

        /*if (!globalConf->suppressModuleAbsPathWarning){

            QMessageBox::warning(
                        0, "error", QString(
                            "Configuration file contains a hard-coded, absolute path to "
                            "a module executable: \n\nmoduleName=\""
                            + s.moduleName +
                            "\" \n\nThis is strongly discouraged since it can lead to "
                            "running old versions of modules. Instead, use a "
                            "relative path to locate modules. (Path is relative "
                            "to directory containing the launching application, in this case: '"
                            + callingAppDir  + QDir::separator() + "'). \n\n "
                            "When in doubt, use the bare moduleName in your config file: \n\n moduleName=\""
                            + moduleBaseName + "\"\n\n "
                            "(Suppress this warning with suppressModuleAbsPathWarning=\"1\" "
                            "in GlobalConfiguration)."));

        }*/
    }

    // Use *module's* parent directory as moduleWorkingDir
    moduleAbsFileInfo = QFileInfo(moduleAbsName);
    moduleWorkingDir = moduleAbsFileInfo.path();

#ifdef __APPLE__
    // ***************************
    // Handle Mac app bundle structure.
    //
    // The following moduleName strings:
    // 1: "/Path/to/moduleName" (where file doesn't exist, but /Path/to/moduleName.app does)
    // 2: "/Path/to/moduleName.app"
    // 3: "/Path/to/moduleName.app/Contents/MacOS/moduleName"
    // ... should launch-> "/Path/to/moduleName.app/Contents/MacOS/moduleName",
    //
    // While:
    // 4: "/Path/to/moduleName" (exists, not a bundle)
    // ... should launch -> "/Path/to/moduleName"
    //
    // In all these cases, moduleWorkingDir should be set to -> "/Path/to"

    // Case 1: User provides path to a module's base name, and an app bundle exists
    if (!moduleAbsFileInfo.exists() && QFileInfo::exists(moduleAbsFileInfo.filePath() + ".app"))
        moduleAbsName = moduleAbsName + ".app/Contents/MacOS/" + moduleBaseName;

    // Case 2: User provides absolute path to module app bundle
    else if (moduleAbsFileInfo.exists() && (moduleAbsFileInfo.suffix() == "app"))
        moduleAbsName = moduleAbsName + "/Contents/MacOS/" + moduleBaseName;

    // Case 3/4: Path to executable
    else if (moduleAbsFileInfo.exists() && !moduleAbsFileInfo.isDir()){
        // Is it part of a bundle?
        if (moduleWorkingDir.endsWith(QString(".app/Contents/MacOS"))) {
            // Case 3: executable part of a bundle, update working dir only
            moduleWorkingDir = QDir::cleanPath(moduleWorkingDir + "/../../../");
        } else {
            // Case 4: bare executable, no need to update moduleAbsName or moduleWorkingDir
        }
    }

    // ***************************


#endif
#ifdef WIN32

    // Set Windows executable paths (QProcess will launch without the '.exe'
    // suffix, but exists() and isExecutable() need the full file name.")
    if (!(moduleAbsName).endsWith(".exe")){
        moduleAbsName = moduleAbsName + ".exe";
    }

#endif
    // Update FileInfo
    moduleAbsFileInfo = QFileInfo(moduleAbsName);

    if (!moduleAbsFileInfo.exists()){
        QMessageBox messageBox;
        messageBox.critical(0, "Error", QString("Module \"%1\" could not be started at path: \"%2\".\n\nFile does not exist.").arg(moduleBaseName).arg(moduleAbsName));
        //qDebug() << "[MainWindow::startModule] MODULE LOADING ERROR! Config file moduleName string: " << s.moduleName \
        //         << ". Launching application directory: " << callingAppDir << ". Module search location (file doesn't exist): " << moduleAbsName;
        return moduleProcess;
    }
    if (!moduleAbsFileInfo.isExecutable()){
        QMessageBox messageBox;
        messageBox.critical(0, "Error", QString("Module \"%1\" could not be started at path: \"%2\".\n\nFile is not an executable.").arg(moduleBaseName).arg(moduleAbsName));
        //qDebug() << "[MainWindow::startModule] MODULE LOADING ERROR! Config file moduleName string: " << s.moduleName \
        //        << ". Launching application directory: " << callingAppDir << ". Module search location (not an executable file): " << moduleAbsName;
        return moduleProcess;
    }

    // launch the module with the main configuration file and the specified configuration file
    //qDebug() << "[MainWindow::startModule] Launching: " << moduleAbsName;
    //qDebug() << "[MainWindow::startModule] Working directory for module: " << moduleWorkingDir;

    moduleProcess->setWorkingDirectory(moduleWorkingDir);

    QStringList arglist;

    if (s.sendTrodesConfig == 1) {
        QStringList configInfo;
        // Need to pass absolute path to config file as module's working
        // directory may be different (and config file location may be relative)
        configInfo << "-trodesConfig" << QFileInfo(moduleConf->trodesConfigFileName).absoluteFilePath();
        arglist << configInfo;
    }

    if (s.sendNetworkInfo) {

        QStringList netInfo;
        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
            netInfo << "-serverAddress" << QString::fromStdString(trodesModule->getAddress()) << "-serverPort" << QString::number(trodesModule->getPort());
        }
        arglist << netInfo;
    }
    arglist << s.moduleArguments;
    qDebug().noquote() << "[MainWindow::startModule] arguments: " << arglist;
    moduleProcess->start(moduleAbsName, arglist);

    /*if (s.sendNetworkInfo) {
        QStringList netInfo;
        netInfo << "-serverAddress" << this->broker->getAddress().c_str() << "-serverPort" << QString("%1").arg(this->broker->getPort());
        arglist << netInfo;
    }
    arglist << s.moduleArguments;
    //qDebug().noquote() << "[MainWindow::startModule] arguments: " << arglist;
    moduleProcess->start(moduleAbsName, arglist);*/
    if (moduleProcess->waitForStarted(10000) == false) {
        QMessageBox messageBox;
        messageBox.critical(0, "Error", QString("%1 could not be started").arg(s.moduleName));
    }
    return moduleProcess;
}

//--------------------------------------------------------------------------

CommandInput::CommandInput(QWidget *parent) {
    currentCommandHistoryIndex = -1;
}

void CommandInput::keyPressEvent(QKeyEvent *event) {
    //A keyboard key was pressed.

    if ((event->key() == Qt::Key_Up)) {
        if (commandHistory.length() > (currentCommandHistoryIndex+1)) {
            currentCommandHistoryIndex++;
            setText(commandHistory.at(currentCommandHistoryIndex));
        }

    } else if ((event->key() == Qt::Key_Down)){
        if (currentCommandHistoryIndex > 0) {
            currentCommandHistoryIndex--;
            setText(commandHistory.at(currentCommandHistoryIndex));
        }
    }
    QLineEdit::keyPressEvent(event);
    //graphicsWindow->passKeyEvent(event);
}

void CommandInput::eraseAndStore() {
    commandHistory.push_front(text());
    clear();
    currentCommandHistoryIndex = -1;
}

#include "fsGUI.h"
#include <QApplication>
#include "qtmoduledebug.h"
int main(int argc, char *argv[])
{
    qInstallMessageHandler(moduleMessageOutput);
    setModuleName("FSGui");
    QApplication a(argc, argv);
    qDebug().noquote() << "FSGui/FSData Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log
    FSGui w(a.arguments());
    w.show();

    return a.exec();
}

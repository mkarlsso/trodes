#include <QCoreApplication>
#include <QtCore>

#include "trodescoreconsole.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

    //debugLock.lock();
    QByteArray localMsg = msg.toLocal8Bit();
    //lastDebugMsg = msg;

    //std::ofstream outfile;
    //QDateTime messageTime;
    //outfile <<  QDateTime::currentDateTime().toString().toLocal8Bit().constData() << " ";

    switch (type) {
    case QtDebugMsg:


        //outfile << localMsg.constData() << "\n";
        //outfile.flush();

        break;
    //case QtInfoMsg:
    //    fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
    //    break;
    case QtWarningMsg:
        //fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        //fflush(stderr);

        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);

        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        abort();
    default:
        //outfile << localMsg.constData() << "\n";
        //outfile.flush();
        break;
    }
    //debugLock.unlock();

}

int main(int argc, char *argv[])
{

    TrodesCoreConsole a(argc, argv);
    qInstallMessageHandler(myMessageOutput);
    if (!a.initOk()) {
        return 1;
    }

    qRegisterMetaType<TrodesEvent>("TrodesEvent");
    qRegisterMetaType<TrodesEventMessage>("TrodesEventMessage");
    qRegisterMetaType<QVector<TrodesEvent> >("QVector<TrodesEvent>");
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<size_t>("size_t");
    qRegisterMetaType<HighFreqDataType>("HighFreqDataType");
    qRegisterMetaType<HardwareControllerSettings>("HardwareControllerSettings");
    qRegisterMetaType<HeadstageSettings>("HeadstageSettings");
    qunsetenv("OPENSSL_CONF");

    fflush(NULL); //Fixes rare bug where path is too long or certain permutations of these long path names and Trodes crashes.
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());

    return a.exec();





    /*DATExportHandler h(a.arguments());

    if (h.wasHelpMenuDisplayed()) {
        //The user requested the help menu
        return 0;
    }

    if (!h.argumentsSupported()) {
        qDebug() << "Error: one or more argument flags is not supported. Use -h flag to print usage.";
        return -1;
    }

    if (!h.argumentsOK()) {
        qDebug() << "Error in the argument list. Use -h flag to print usage.";
        return -1;
    }

    if (!h.fileConfigOK()) {
        qDebug() << "Error reading recording configuration. Aborting.";
        return -1;
    }

    return h.processData();
    */


}

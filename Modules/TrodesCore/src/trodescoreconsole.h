#ifndef TRODESCORECONSOLE_H
#define TRODESCORECONSOLE_H

#include <QObject>
#include <QtCore>
#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#ifndef _WIN32
#include <unistd.h>
#include <QSocketNotifier>
#include <getopt.h>
#else
#include <QWinEventNotifier>
#include <windows.h>
#include <getopt.h>
#endif
#include "trodescoreutil.h"


class TextInputManager : public QObject
{
Q_OBJECT

public:
    TextInputManager();
    bool go;
public slots:
    void startLoop();
    void textInput();
private:
    QFile in;
#ifndef _WIN32
     QSocketNotifier        *notifier;
#else
     QWinEventNotifier      *notifier;
#endif
signals:
    void terminated();
    void newCommand(QString c);
};

class TrodesCoreConsole : public QCoreApplication
{
Q_OBJECT

public:
    TrodesCoreConsole(int & argc, char **argv);
    bool                    initOk();

public slots:


private:
     TrodesCoreUtil         *trodesUtil;
     bool                   argumentsOK;
     QTimer                 *watchdog;

     bool                   parseTextInput(QString inputString);
     void                   watchdogTimout();

     bool                   shuttingDown;
     bool                   initializing;
     bool                   waitingForCommandFinish;
     bool                   verboseMode;
     bool                   waitAfterQuit;
     bool                   waitingForQuit;

     bool scriptFileGiven;
     QFile scriptFile;


private slots:
     void                   textInput();
     void                   setShuttingDown();
     void                   commandFinished();
     void                   commandFailed();
     void                   completeShutdown();
     void                   printTextOutput(QString outputString);
     void                   printErrorOutput(QString outputString);
     void                   printVerboseOutput(QString outputString);

signals:
    void                    textReceived(QString message);
    void                    startTextManager();

};

#endif // TRODESCORECONSOLE_H

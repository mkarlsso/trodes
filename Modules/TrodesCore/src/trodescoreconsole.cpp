#include "trodescoreconsole.h"


/*public:
    TextInputManager();
public slots:
    void startLoop();
signals:
    void terminated();
    void newCommand(QString c);
  */

TextInputManager::TextInputManager()
{
    if(!in.open(stdin,QFile::ReadOnly|QFile::Unbuffered)) {
            qDebug() << in.errorString();
    }

}

void TextInputManager::startLoop()
{
    //The text input can block execution of the main thread, so it needs to be in
    //a separate thread.


#ifndef _WIN32
     notifier = new QSocketNotifier(STDIN_FILENO, QSocketNotifier::Read),
     connect(notifier, SIGNAL(activated(int)), this, SLOT(textInput()));
#else
    notifier = new QWinEventNotifier(GetStdHandle(STD_INPUT_HANDLE));
    connect(notifier, SIGNAL(activated(HANDLE)), this, SLOT(textInput()));
#endif


}

void TextInputManager::textInput()
{
    QTextStream qin(stdin);
    QString line = qin.readLine();

    emit newCommand(line);
}



TrodesCoreConsole::TrodesCoreConsole(int & argc, char **argv )
    : QCoreApplication(argc, argv),
      shuttingDown(false),
      initializing(true),
      waitingForCommandFinish(false),
      verboseMode(false),
      waitAfterQuit(false),
      waitingForQuit(false)

{
    setApplicationName("TrodesCoreConsole");

    //Parse the input arguments
    argumentsOK = true;
    int c;
    char *in_address = nullptr;
    bool addressSet = false;
    bool soundOn = false;
    bool soundSet = false;
    int in_port = 0;
    bool portSet = false;

    scriptFileGiven = false;
    QString scriptFileText;
    char *pathToFile = nullptr;
    QString str_pathToFile;




    QString addressToUse = DEFAULT_SERVER_ADDRESS;
    int portToUse = DEFAULT_SERVER_PORT;

    while (1){
        static struct option long_options[] = {
            //command line options
            //must also add the letter to the string in the function getopt_long() below
            {"sound",         no_argument,        0, 's'},
            {"verbose",       no_argument,        0, 'v'},
            {"waitquit",      no_argument,        0, 'w'},
            {"address",       required_argument,  0, 'a'},
            {"port",          required_argument,  0, 'p'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        //just the letter if no_argument, letter + ':' for required arguments
        c = getopt_long (argc, argv, "svwf:a:p:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c){
            case 'a':
                addressSet = true;
                in_address = (char*)malloc(strlen(optarg)+1);
                strcpy(in_address, optarg);
                addressToUse = QString::fromLatin1(in_address,-1);
                break;
            case 'f':
                scriptFileGiven = true;
                pathToFile = (char*)malloc(strlen(optarg)+1);
                strcpy(pathToFile, optarg);
                str_pathToFile = QString::fromLatin1(pathToFile,-1);
                break;
            case 'p':
                portSet = true;
                in_port = strtoul(optarg,0,0);
                portToUse = in_port;
                break;
            case 's':
                soundSet = true;
                soundOn = true;
                break;
            case 'v':
                verboseMode = true;
                break;
            case 'w':
                waitAfterQuit = true;
                break;
            default:
                printf("Problem with provided arguments. \n");
                argumentsOK = false;
        }
    }


    if (scriptFileGiven) {
        scriptFile.setFileName(str_pathToFile);
        if (!scriptFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
            printf("Error opening script file.\n");
            argumentsOK = false;
        }
    }

    if (!argumentsOK) {
        return;
    }

    trodesUtil = new TrodesCoreUtil();
    //connect(trodesUtil,&TrodesCoreUtil::shutDownComplete,this,QCoreApplication::quit);
    connect(trodesUtil,&TrodesCoreUtil::shutDownComplete,this,&TrodesCoreConsole::completeShutdown);
    connect(trodesUtil,&TrodesCoreUtil::shutDownIntitiated,this,&TrodesCoreConsole::setShuttingDown);
    connect(trodesUtil,&TrodesCoreUtil::newConsoleOutputMessage,this,&TrodesCoreConsole::printTextOutput);
    connect(trodesUtil,&TrodesCoreUtil::newErrorMessage,this,&TrodesCoreConsole::printErrorOutput);
    connect(trodesUtil,&TrodesCoreUtil::newDebugMessage,this,&TrodesCoreConsole::printVerboseOutput);
    connect(trodesUtil,&TrodesCoreUtil::controlCommandFinished,this,&TrodesCoreConsole::commandFinished);
    connect(trodesUtil,&TrodesCoreUtil::controlCommandFailed,this,&TrodesCoreConsole::commandFailed);

    printf("\n====================================\nTrodes interactive console interface\n      Type 'help' for options.\n====================================\n");


    if (!trodesUtil->init(addressToUse,portToUse)) {
        //TrodesUtil could not initiate. End program.
        printf("Shutting down\n");
        argumentsOK = false;

        return;
    }

    //Create the notifier for interactive user text input. This is done differently on Windows.
#ifndef _WIN32
     /*notifier = new QSocketNotifier(STDIN_FILENO, QSocketNotifier::Read),
     connect(notifier, SIGNAL(activated(int)), this, SLOT(textInput()));*/
#else
    /*qRegisterMetaType<HANDLE>("HANDLE");
    notifier = new QWinEventNotifier(GetStdHandle(STD_INPUT_HANDLE));
    QThread *nThread = new QThread();
    notifier->moveToThread(nThread);
    connect(notifier, SIGNAL(activated(HANDLE)), this, SLOT(textInput()),Qt::QueuedConnection);
    connect(trodesUtil,&TrodesCoreUtil::shutDownIntitiated,nThread,&QThread::quit);
    nThread->start();*/
#endif

    TextInputManager *inputManager = new TextInputManager();
    QThread *inputWorkerThread = new QThread();
    inputManager->moveToThread(inputWorkerThread);
    connect(inputWorkerThread,&QThread::started,inputManager,&TextInputManager::startLoop);
    //connect(this,&TrodesCoreConsole::startTextManager,inputManager,&TextInputManager::startLoop);
    connect(inputManager,&TextInputManager::terminated, inputWorkerThread, &QThread::quit);
    connect(inputManager,&TextInputManager::terminated, inputManager, &TextInputManager::deleteLater);
    connect(inputWorkerThread,&QThread::finished, inputWorkerThread, &QThread::deleteLater);
    connect(inputManager,&TextInputManager::newCommand,this,&TrodesCoreConsole::parseTextInput);

    inputWorkerThread->setObjectName("TextInputManager");
    inputWorkerThread->start();


    /*watchdog = new QTimer(this);
    connect(watchdog,&QTimer::timeout,this, &TrodesCoreConsole::watchdogTimout);
    watchdog->start(1000);*/
}

bool TrodesCoreConsole::initOk()
{
    return argumentsOK;
}

void TrodesCoreConsole::textInput()
{
    //The user entered a command into the console prompt
    /*QTextStream qin(stdin);
    QString line = qin.readLine();
    if (waitingForQuit) {
        QCoreApplication::quit();
        return;
    }
    parseTextInput(line);*/
}

void TrodesCoreConsole::setShuttingDown()
{
    shuttingDown = true;
}

void TrodesCoreConsole::completeShutdown()
{
    if (scriptFileGiven) {
        waitAfterQuit = true;
    }
    if (waitAfterQuit) {
        printf("\n\nPress enter to finish.\n\n");
        fflush(stdout);
        waitingForQuit = true;
    } else {
        QCoreApplication::quit();
    }
}

void TrodesCoreConsole::commandFinished()
{
    waitingForCommandFinish = false;
    if (shuttingDown) {
        return;
    }

    if (scriptFileGiven && !scriptFile.atEnd()) {
        QByteArray line = scriptFile.readLine();
        QString pl(line);
        pl = pl.trimmed(); //remove trailing whitespace
        parseTextInput(pl);
    } else if (scriptFileGiven && scriptFile.atEnd()) {
        scriptFileGiven = false;
        scriptFile.close();
        printf("\n<TRODES> ");
        fflush(stdout);
    } else {
        //No file. Print the prompt.
        printf("\n<TRODES> ");
        fflush(stdout);
    }


}

void TrodesCoreConsole::commandFailed()
{
    printf("\nCommand failed.\n");
    waitingForCommandFinish = false;
    //Reset the prompt
    if (!shuttingDown) {
        printf("\n<TRODES> ");
        fflush(stdout);
    }
}

bool TrodesCoreConsole::parseTextInput(QString inputString)
{

    if (waitingForQuit) {
        QCoreApplication::quit();
        return true;
    }

    if (!inputString.isEmpty()) {
        waitingForCommandFinish = true;
        bool ok;
        trodesUtil->parseTextControl(inputString, &ok);
        return ok;
    } else {
        //Empty line. Do nothing.
        if (!shuttingDown && !waitingForCommandFinish) {
            printf("\n<TRODES> ");
        }
        return true;
    }
}


void TrodesCoreConsole::printTextOutput(QString outputString)
{
    printf("\33[2K\r%s\n",outputString.toLocal8Bit().data());
    fflush(stdout);

    //Reset the prompt

    if (initializing) {
        initializing = false;
        commandFinished();
    }
    else if (!shuttingDown && !waitingForCommandFinish) {
        printf("\n<TRODES> ");
        fflush(stdout);
    }


}

void TrodesCoreConsole::watchdogTimout()
{
    printf(".\n");
}

void TrodesCoreConsole::printVerboseOutput(QString outputString)
{
    if (verboseMode) {
        printf("\33[2K\r%s\n",outputString.toLocal8Bit().data());
        //Reset the prompt
        if (!shuttingDown && !waitingForCommandFinish) {
            printf("\n<TRODES> ");
        }
        fflush(stdout);
    }
}

void TrodesCoreConsole::printErrorOutput(QString outputString)
{
    printf("\33[2K\rError: %s\n",outputString.toLocal8Bit().data());
    //Reset the prompt
    if (!shuttingDown && !waitingForCommandFinish) {
        printf("\n<TRODES> ");
    }
    fflush(stdout);
}

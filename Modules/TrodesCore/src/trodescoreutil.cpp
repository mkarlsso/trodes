#include "trodescoreutil.h"
//#include "trodes

bool linkChangesBool;

//----------------------------------------
//Public Slots/Functions
//-----------------------------------------

TrodesCoreUtil::TrodesCoreUtil()
{
    networkAddress = DEFAULT_SERVER_ADDRESS;
    networkPort = DEFAULT_SERVER_PORT;

    appendTimeToStatus = true;
    isAudioAvailable = true;
    isAudioOn = false;
    lastAudioGain = 0;
    readyForCommand = false;
    selectedNTrodeIndex = 0;
    selectedNTrodeChannelIndex = 0;

    hardwareSimulationModeOn = false;
    sourceState = SOURCE_STATE_NOT_CONNECTED;
    unresponsiveHeadstageFlag = false;
    closeInitiated = false;

    quitting = false;
    enableAVXFlag = true;

    recordFileOpen = false;
    currentFilePart = 1;
    dataStreaming = false;
    recording = false;
    channelsConfigured = false;
    closingWorkspace = false;

    headstageSettings.autoSettleOn = false;
    headstageSettings.percentChannelsForSettle = 0;
    headstageSettings.threshForSettle = 0;
    headstageSettings.smartRefOn = false;
    headstageSettings.accelSensorOn = false;
    headstageSettings.gyroSensorOn = false;
    headstageSettings.magSensorOn = false;

    headstageSettings.smartRefAvailable = false;
    headstageSettings.autosettleAvailable = false;
    headstageSettings.accelSensorAvailable = false;
    headstageSettings.gyroSensorAvailable = false;
    headstageSettings.magSensorAvailable = false;
    headstageSettings.rfAvailable = false;



}

bool TrodesCoreUtil::init(QString nAddress, int nPort)
{
    networkAddress = nAddress;
    networkPort = nPort;

    startSourceController();


    if (startTrodesNetwork()) {
        readyForCommand = true;
        consoleOutputMessage(QString("Trodes network open at address %1, port %2").arg(QString::fromStdString(trodesModule->getAddress())).arg(trodesModule->getPort()));
    } else {
        errorMessage(QString("Could not start Trodes network. Please make sure the address %1:%2 exists and that another instance of Trodes is not running already on that address.\n").arg(networkAddress).arg(networkPort));
        //printf("Could not start Trodes network. Please make sure the address %s:%d exists and that another instance of Trodes is not running already on that address.\n", address.c_str(),port);

        return false;
    }


    if (isAudioAvailable) {       
        startAudioThread();
    }


    return true;
}


TrodesCoreUtil::~TrodesCoreUtil()
{

}

bool TrodesCoreUtil::selectChannel(int nTrode, int channel)
{

    if (!channelsConfigured) {
        errorMessage("Can't select channel when no workspace is loaded.");
        return false;
    }
    if (activeWorkspacePointers.spikeConf->ntrodes.length() <= nTrode) {
        errorMessage("Can't select channel when the workspace has no ntrodes defined.");
        return false;
    }
    if (activeWorkspacePointers.spikeConf->ntrodes.at(nTrode)->hw_chan.length() <= channel) {
        errorMessage("The selected channel does not exist.");
        return false;
    }
    if ((nTrode < 0) || (channel < 0)) {
        errorMessage("The selected channel does not exist.");
        return false;
    }

    selectedNTrodeChannelIndex = channel;
    selectedNTrodeIndex = nTrode;
    if (isAudioAvailable && isAudioOn) {
        soundOut->setChannel(activeWorkspacePointers.spikeConf->ntrodes.at(selectedNTrodeIndex)->hw_chan.at(selectedNTrodeChannelIndex));
        soundOut->updateAudio();
    }
    return true;
}

bool TrodesCoreUtil::setAudioOn(bool on)
{
    if (!isAudioAvailable) {
        errorMessage("Audio is not available");
        return false;
    }
    isAudioOn = on;
    if (isAudioOn) {
        soundOut->setChannel(activeWorkspacePointers.spikeConf->ntrodes.at(selectedNTrodeIndex)->hw_chan.at(selectedNTrodeChannelIndex));
        soundOut->setGain(lastAudioGain);
        soundOut->updateAudio();

    } else {
        soundOut->setGain(0);
    }


    return true;
}

bool TrodesCoreUtil::setAudioGain(int g)
{
    if (!isAudioAvailable) {
        errorMessage("Audio is not available");
        return false;
    }
    if ((g < 0) || (g > 100)) {
        errorMessage("Audio gain must be between 0 and 100");
        return false;
    }
    soundOut->setGain(g);
    lastAudioGain = g;
    return true;
}

bool TrodesCoreUtil::setAudioThreshold(int t)
{
    if (!isAudioAvailable) {
        errorMessage("Audio is not available");
        return false;
    }
    if ((t < 0) || (t > 100)) {
        errorMessage("Audio threshold must be between 0 and 100");
        return false;
    }
    soundOut->setThresh(t);
    return true;
}

bool  TrodesCoreUtil::setAudioDeviceNumber(int n)
{
    if (!isAudioAvailable) {
        errorMessage("Audio is not available");
        return false;
    }
    TrodesAudioInfo info = getAudioInfo();
    if ((n < 0) || (n >= info.availableDevices.length())) {
        errorMessage("Audio device number does not exist in the list of available devices.");
        return false;
    }
    soundOut->setDevice(info.availableDevices.at(n));
    return true;
}



TrodesSourceStatus  TrodesCoreUtil::getSourceStatus()
{

    TrodesSourceStatus stat = sourceControl->getSourceStatus();
    return stat;
}

TrodesAudioInfo TrodesCoreUtil::getAudioInfo()
{
    TrodesAudioInfo info;
    if (isAudioAvailable) {
        info.on = isAudioOn;
        info.gain = soundOut->getGain();
        info.threshold = soundOut->getThresh();
        info.currentDevice = soundOut->getCurrentDevice();
        info.availableDevices = soundOut->getAvailableDevices();
    }

    return info;
}

void TrodesCoreUtil::printWorkspaceStatus()
{
    QString printOutput;
    printOutput.append("---------Workspace status---------\n");
    if (channelsConfigured && !playbackFileOpen) {
        printOutput.append(QString("Acquisition workspace loaded:\n %1\n").arg(loadedConfigFile));

    } else if (channelsConfigured && playbackFileOpen && usingExternalWorkspace) {
        printOutput.append(QString("External workspace loaded for playback:\n %1\n").arg(loadedConfigFile));
    } else if (channelsConfigured && playbackFileOpen) {
        printOutput.append(QString("Using workspace embedded in playback file\n"));
    } else if (!channelsConfigured) {
        printOutput.append(QString("No workspace loaded.\n"));
    }
    printOutput.append(QString("----------------------------------\n"));
    consoleOutputMessage(printOutput);
}

void TrodesCoreUtil::printSourceStatus()
{
    TrodesSourceStatus stat = getSourceStatus();
    QString printOutput;


    printOutput.append("---------Source status---------\n");
    printOutput.append(QString("Connected to source: %1\n").arg(stat.connected));
    printOutput.append(QString("Streaming: %1\n").arg(stat.streaming));
    printOutput.append(QString("%1 total packets dropped.\n").arg(stat.numPacketsDropped));
    if (stat.numPacketsProcessed > 0) {
        printOutput.append(QString("%1 percent packet drops.\n").arg(((double)stat.numPacketsDropped/(double)stat.numPacketsProcessed)*100.0));
    } else {
        printOutput.append(QString("0 percent packet drops.\n"));
    }
    printOutput.append(QString("Largest drop event: %1 packets.\n").arg(stat.largestPacketDrop));

    printOutput.append(QString("%1 total unresponsive headstage packets.\n").arg(stat.totalUnresponsiveHeadstagePackets));
    if (stat.numPacketsProcessed > 0) {
        printOutput.append(QString("%1 percent unresponsive headstage packets.\n").arg(((double)stat.totalUnresponsiveHeadstagePackets/(double)stat.numPacketsProcessed)*100.0));
    } else {
        printOutput.append(QString("0 percent unresponsive headstage packets.\n"));
    }
    if (channelsConfigured) {
        QString ss = QString(":%1").arg((stat.numPacketsProcessed/activeWorkspacePointers.hardwareConf->sourceSamplingRate)%60,2,10,QLatin1Char('0'));
        QString mm = QString(":%1").arg((stat.numPacketsProcessed/(activeWorkspacePointers.hardwareConf->sourceSamplingRate*60))%60,2,10,QLatin1Char('0'));
        QString hh = QString("%1").arg(stat.numPacketsProcessed/(activeWorkspacePointers.hardwareConf->sourceSamplingRate*60*60),2,10,QLatin1Char('0'));
        printOutput.append(QString("Streaming time: ")+hh+mm+ss + "\n");
    } else {
        printOutput.append(QString("Streaming time: 00:00:00\n"));
    }

    printOutput.append(QString("-------------------------------\n"));
    consoleOutputMessage(printOutput);

}

 void TrodesCoreUtil::printAudioStatus()
 {

     TrodesAudioInfo info = getAudioInfo();
     QString printOutput;
     printOutput.append("---------Audio status---------\n");
     printOutput.append(QString("Current device: %1\n").arg(info.currentDevice));
     printOutput.append(QString("Audio on: %1\n").arg(info.on));
     printOutput.append(QString("Gain: %1\n").arg(info.gain));
     printOutput.append(QString("Threshold: %1\n").arg(info.threshold));
     printOutput.append(QString("Available devices:\n"));
     if (info.availableDevices.length() == 0) {
         printOutput.append(QString("   None\n"));
     } else {
         for (int i=0;i<info.availableDevices.length();i++) {
             printOutput.append(QString("   (%1) %2\n").arg(i+1).arg(info.availableDevices.at(i)));
         }
     }
     printOutput.append(QString("------------------------------\n"));
     consoleOutputMessage(printOutput);
 }

TrodesRecordingStatus TrodesCoreUtil::getRecordingStatus()
{
    TrodesRecordingStatus stat;
    if (channelsConfigured) {
        stat = recordOut->getRecordingStatus();
    } else {
        stat.bytesWritten = 0;
        stat.fileName = "";
        stat.fileOpen = false;
        stat.packetsWritten = 0;
        stat.recording = false;
        stat.bytesFree = -1;
    }

    return stat;
}

void TrodesCoreUtil::printRecordingStatus()
{
    TrodesRecordingStatus stat = getRecordingStatus();


    if (!stat.fileOpen) {
        consoleOutputMessage(QString("---------Recording status---------\n"
                             "File: none\n"
                             "Currently writing: %1\n"
                             "Bytes written: %2\n"
                             "Packets written: %3\n"
                             "Bytes free: NA\n"
                             "Recording time: 00:00:00\n"
                             "----------------------------------").arg(stat.recording).arg(stat.bytesWritten).arg(stat.packetsWritten));
    } else {
        QString printOutput;
        printOutput.append(QString("---------Recording status---------\n"
                             "File: %1\n"
                             "Currently writing: %2\n"
                             "Bytes written: %3\n"
                             "Packets written: %4\n"
                             "Bytes free: %5\n").arg(stat.fileName).arg(stat.recording).arg(stat.bytesWritten).arg(stat.packetsWritten).arg(stat.bytesFree));

        if (channelsConfigured) {
            QString ss = QString(":%1").arg((stat.packetsWritten/activeWorkspacePointers.hardwareConf->sourceSamplingRate)%60,2,10,QLatin1Char('0'));
            QString mm = QString(":%1").arg((stat.packetsWritten/(activeWorkspacePointers.hardwareConf->sourceSamplingRate*60))%60,2,10,QLatin1Char('0'));
            QString hh = QString("%1").arg(stat.packetsWritten/(activeWorkspacePointers.hardwareConf->sourceSamplingRate*60*60),2,10,QLatin1Char('0'));
            printOutput.append(QString("Recording time: ")+hh+mm+ss + "\n");
        } else {
            printOutput.append(QString("Recording time: 00:00:00\n"));
        }
        printOutput.append("----------------------------------\n");
        consoleOutputMessage(printOutput);

    }
}

bool TrodesCoreUtil::isWorkspaceLoaded()
{
    return channelsConfigured;
}

DataSource TrodesCoreUtil::getCurrentSource()
{
    return sourceControl->currentSource;
}

int TrodesCoreUtil::getCurrentSourceState()
{
    return sourceState;
}

TrodesConfigurationPointers TrodesCoreUtil::getCurrentWorkspace()
{
    return activeWorkspacePointers;
}

void TrodesCoreUtil::setPlaybackFilename(QString f)
{
    if (sourceControl->currentSource != DataSource::SourceFile) {
        playbackFilename = f;
    }
}

//START Public commands that route to internal ones
//************************************************************
void TrodesCoreUtil::printTextControlMenu()
{
    QString outtext = QString(
           "Commands are given using single text lines ending in a carriage return. Each command\n"
           "is comprised of one or more words starting with the catagory of the command.\n\n"
           "EXAMPLE:\n"
           "'source connect usb' will trigger a connection to hardware using USB.\n\n"
           "                    Supported Commands\n"
           "                    ==================\n\n"
           "'quit' or 'q'       Quit program\n"
           "'version' or 'v'    Print version information\n"
           "'help' or 'h'       Print help menu\n"
           "\n"
           "'workspace'         Workspace commands:\n"
           "                        'open <filename>'               Open a workspace\n"
           "                        'close'                         Close a workspace \n"
           "                        'status'                        Print the current workspace details\n"
           "\n"
           "'source'           Source control commands:\n"
           "                        'connect':\n"
           "                            'usb'                       Connect using USB connection\n"
           "                            'ethernet'                  Connect using ethernet connection\n"
           "                            'playback <filename>'       Open a rec file for playback\n"
           "                            'simulation_waveform'       Connect to a simulated sine wave source \n"
           "                            'simulation_spikes'         Connect to a simulated source with spikes\n"
           "                        'disconnect'                    Disconnect from the source\n"
           "                        'start'                         Start acquiring data from the source\n"
           "                        'stop'                          Stop acquiring data from the source\n"
           "                        'pause'                         Pause playback (only used during playback)\n"
           "                        'detect':\n"
           "                            'usb'                       Print hardware specs using USB\n"
           "                            'ethernet'                  Print hardware specs using ethernet\n"
           "                        'settle'                        Settle the amplifyiers (if option available)\n"
           "                        'status'                        Print the current status us the source\n"
           "\n"
           "'record'            Recording data to file commands:\n"
           "                        'open <filename>'               Create a new recording file\n"
           "                        'close'                         Close the recording file\n"
           "                        'start'                         Start recording data to the file\n"
           "                        'stop'                          Pause recording data\n"
           "                        'status'                        Print the status of the recording process\n"
           "\n"
           "'ntrode'            Change ntrode settings:\n"
           "                        'select <ntrode> <channel>'     Change the selected channel\n"
           "\n"
           "'audio'             Change audio settings:\n"
           "                        'on'                            Turn on audio\n"
           "                        'off'                           Turn off audio\n"
           "                        'gain <value>'                  Change audio volume between 0 and 100\n"
           "                        'threshold <value>'             Change audio threshold between 0 and 100\n"
           "                        'device <value>'                Change audio device to another in the detected list\n"
           "                        'status'                        Print audio settings and status\n"
           "\n"
           "'test'              Specific testing commands:\n"
           "                        'impedance <ntrode> <channel>'  Print impedance of given 1-based NT and channel\n"
           "                        'noise <ntrode> <channel>'      Print noise amplitude of given channel with 1-sec bin\n"
           "                        'noise_all'                     Test all channels at once, 10-sec bin\n"

           );

            consoleOutputMessage(outtext);
}

void TrodesCoreUtil::parseTextControl_no_ok(QString inputString)
{
    bool ok;
    if (readyForCommand) {
        parseTextControl(inputString, &ok);
    }
}

void TrodesCoreUtil::parseTextControl(QString inputString, bool *ok)
{
    bool commandExecuted = true;
    if ( (inputString.compare("quit",Qt::CaseInsensitive)==0) || (inputString.compare("q",Qt::CaseInsensitive)==0) ) {
        //Quit the program
        shutDown();
    } else if ( (inputString.compare("version",Qt::CaseInsensitive)==0) || (inputString.compare("v",Qt::CaseInsensitive)==0) ) {
        printVersionInfo();
        commandFinished();
    } else if ( (inputString.compare("help",Qt::CaseInsensitive)==0) || (inputString.compare("h",Qt::CaseInsensitive)==0) ) {
        printTextControlMenu();
        commandFinished();
    } else if (inputString.startsWith("workspace ",Qt::CaseInsensitive)) {
        //Workspace commands
        QStringList l = inputString.split(" ",Qt::SkipEmptyParts);
        if (l.length() == 3 && (l.at(1).compare("open",Qt::CaseInsensitive)==0)) {
            if (!openWorkspaceFileForAcquisition(l.at(2))) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("close",Qt::CaseInsensitive)==0)) {
            if (!closeWorkspace()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("status",Qt::CaseInsensitive)==0)) {
            printWorkspaceStatus();
            commandFinished();
        }
        else {
            emit errorMessage("Improper use of workspace command. Type 'help' for options.\n");
            commandFinished();
            commandExecuted = false;
        }
    } else if (inputString.startsWith("source ",Qt::CaseInsensitive)) {
        //Source commands
        QStringList l = inputString.split(" ",Qt::SkipEmptyParts);
        if (l.length() > 2 && (l.at(1).compare("connect",Qt::CaseInsensitive)==0)) {
            bool result=false;
            if (l.at(2).compare("usb",Qt::CaseInsensitive)==0) {
                result=setSource(DataSource::SourceUSBDAQ);
            } else if (l.at(2).compare("ethernet",Qt::CaseInsensitive)==0) {
                result=setSource(DataSource::SourceEthernet);
            } else if (l.at(2).compare("simulation_waveform",Qt::CaseInsensitive)==0) {
                result=setSource(DataSource::SourceFake);
            } else if (l.at(2).compare("simulation_spikes",Qt::CaseInsensitive)==0) {
                result=setSource(DataSource::SourceFakeSpikes);
            } else if ((l.at(2).compare("playback",Qt::CaseInsensitive)==0) && (l.length() == 4) ) {
                result=openPlaybackFile(l.at(3));
            } else {
                emit errorMessage("Improper use of source command. Type 'help' for options.\n");
                commandExecuted = false;
            }
            if (!result) {
                commandFinished();
                commandExecuted = false;
            }

        } else if (l.length() == 2 && (l.at(1).compare("disconnect",Qt::CaseInsensitive)==0)) {
            if (!setSource(DataSource::SourceNone)) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("start",Qt::CaseInsensitive)==0)) {
            if (!connectToSource()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("stop",Qt::CaseInsensitive)==0)) {
            if (!disconnectFromSource()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("pause",Qt::CaseInsensitive)==0)) {
            if (!pausePlayback()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("status",Qt::CaseInsensitive)==0)) {
            printSourceStatus();
            commandFinished();
        } else if (l.length() == 2 && (l.at(1).compare("settle",Qt::CaseInsensitive)==0)) {
            if (!settleAmplifiers()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() > 2 && (l.at(1).compare("detect",Qt::CaseInsensitive)==0)) {
            if (l.at(2).compare("usb",Qt::CaseInsensitive)==0) {
                runSourceDiagnistic(DataSource::SourceUSBDAQ);
                commandFinished();
            } else if (l.at(2).compare("ethernet",Qt::CaseInsensitive)==0) {
                runSourceDiagnistic(DataSource::SourceEthernet);
                commandFinished();
            } else {
                emit errorMessage("Improper use of source detect command. Type 'help' for options.\n");
                commandFinished();
                commandExecuted = false;
            }
        }

        else {
            emit errorMessage("Improper use of source command. Type 'help' for options.\n");
            commandFinished();
            commandExecuted = false;
        }
    } else if (inputString.startsWith("record ",Qt::CaseInsensitive)) {
        //Record commands
        QStringList l = inputString.split(" ",Qt::SkipEmptyParts);
        if (l.length() == 3 && (l.at(1).compare("open",Qt::CaseInsensitive)==0)) {
            if (!openRecordFile(l.at(2))) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("close",Qt::CaseInsensitive)==0)) {
            if (!closeRecordFile()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("start",Qt::CaseInsensitive)==0)) {
            if (!startRecording()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("stop",Qt::CaseInsensitive)==0)) {
            if (!pauseRecording()) {
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() == 2 && (l.at(1).compare("status",Qt::CaseInsensitive)==0)) {
            printRecordingStatus();
            commandFinished();
        } else {
            emit errorMessage("Improper use of record command. Type 'help' for options.\n");
            commandFinished();
            commandExecuted = false;
        }
    } else if (inputString.startsWith("ntrode ",Qt::CaseInsensitive)) {
        //Ntrode settings commands
        QStringList l = inputString.split(" ",Qt::SkipEmptyParts);
        if (l.length() > 3 && (l.at(1).compare("select",Qt::CaseInsensitive)==0)) {

            bool ok;
            int nt = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve NT input to an integer.");
                commandFinished();
                commandExecuted = false;
                return;

            }
            int ch = l.at(3).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve channel input to an integer.");
                commandFinished();
                commandExecuted = false;
                return;
            }
            if (!selectChannel(nt-1, ch-1)) { //convert 1-based numbers to 0-based
                commandExecuted = false;
            }
            commandFinished();

        } else {
            emit errorMessage("Improper use of ntrode command. Type 'help' for options.\n");
            commandFinished();
            commandExecuted = false;
        }
    } else if (inputString.startsWith("audio ",Qt::CaseInsensitive)) {
        //Audio commands
        QStringList l = inputString.split(" ",Qt::SkipEmptyParts);
        if (l.length() == 3 && (l.at(1).compare("gain",Qt::CaseInsensitive)==0)) {
            bool ok;
            int v = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve gain value to an integer.");
                commandFinished();
                commandExecuted = false;
                return;
            }
            if (!setAudioGain(v)) {
                commandExecuted = false;
            }
            commandFinished();
        } else if (l.length() == 3 && (l.at(1).compare("threshold",Qt::CaseInsensitive)==0)) {
            bool ok;
            int v = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve threshold value to an integer.");
                commandFinished();
                commandExecuted = false;
                return;
            }
            if (!setAudioThreshold(v)) {
                commandExecuted = false;
            }
            commandFinished();
        } else if (l.length() == 2 && (l.at(1).compare("on",Qt::CaseInsensitive)==0)) {
            if (!setAudioOn(true)) {
                commandExecuted = false;
            }
            commandFinished();
        } else if (l.length() == 2 && (l.at(1).compare("off",Qt::CaseInsensitive)==0)) {
            if (!setAudioOn(false)) {
                commandExecuted = false;
            }
            commandFinished();

        } else if (l.length() == 3 && (l.at(1).compare("device",Qt::CaseInsensitive)==0)) {
            bool ok;
            int v = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve device number to an integer.");
                commandFinished();
                commandExecuted = false;
                return;
            }
            if (!setAudioDeviceNumber(v-1)) { //Change from 1-based to 0-based indexing
                commandExecuted = false;
            }
            commandFinished();
        } else if (l.length() == 2 && (l.at(1).compare("status",Qt::CaseInsensitive)==0)) {
            printAudioStatus();
            commandFinished();
        } else {
            emit errorMessage("Improper use of audio command. Type 'help' for options.\n");
            commandFinished();
            commandExecuted = false;
        }

    } else if (inputString.startsWith("test ",Qt::CaseInsensitive)) {
        //Test commands
        QStringList l = inputString.split(" ",Qt::SkipEmptyParts);
        if (l.length() > 3 && (l.at(1).compare("impedance",Qt::CaseInsensitive)==0)) {

            bool ok;
            int nt = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve NT input to an integer.");
                commandFinished();
                commandExecuted = false;

            }
            int ch = l.at(3).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve channel input to an integer.");
                commandFinished();
                commandExecuted = false;

            }
            if (!testImpedance(nt-1, ch-1, 200)) { //convert 1-based numbers to 0-based
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() > 3 && (l.at(1).compare("impedance_fast",Qt::CaseInsensitive)==0)) {
            //This is a faster test, but it is noisier (100 cycles)
            bool ok;
            int nt = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve NT input to an integer.");
                commandFinished();
                commandExecuted = false;

            }
            int ch = l.at(3).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve channel input to an integer.");
                commandFinished();
                commandExecuted = false;

            }
            if (!testImpedance(nt-1, ch-1,100)) { //convert 1-based numbers to 0-based
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() > 3 && (l.at(1).compare("noise",Qt::CaseInsensitive)==0)) {
            //Noise test, one channel at a time, 1 second bin per channel
            bool ok;
            int nt = l.at(2).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve NT input to an integer.");
                commandFinished();
                commandExecuted = false;

            }
            int ch = l.at(3).toInt(&ok);
            if (!ok) {
                emit errorMessage("Could not resolve channel input to an integer.");
                commandFinished();
                commandExecuted = false;

            }
            if (!calculateNoise(nt-1, ch-1)) { //convert 1-based numbers to 0-based
                commandFinished();
                commandExecuted = false;
            }
        } else if (l.length() > 1 && (l.at(1).compare("noise_all",Qt::CaseInsensitive)==0)) {
            //Noise test, all channels at once. 10 second bin

            if (!calculateNoise(-1, -1)) {
                commandFinished();
                commandExecuted = false;
            }
        } else {
            emit errorMessage("Improper use of test command. Type 'help' for options.\n");
            commandFinished();
            commandExecuted = false;
        }
}
    else {
        emit errorMessage("Command not understood. Type 'help' for options.\n");
        commandFinished();
        commandExecuted = false;
    }

    if (ok != nullptr) {
        *ok = commandExecuted;
    }


}

bool TrodesCoreUtil::openPlaybackFile(const QString fileName)
{
    if (!readyForCommand) {
        return false;
    }
    if (recordFileOpen) {
        errorMessage("You must close the active recording before opening a playback file.");
        return false;
    }
    if (playbackFileOpen) {
        errorMessage("You must close the current playback file before opening a new playback file.");
        return false;
    }


    connect(this,SIGNAL(sourceConnected(int)),this,SLOT(commandFinished()),Qt::SingleShotConnection);
    startCommandTimeout(2000);
    openPlaybackFile_internal(fileName);

    return true;
}


bool TrodesCoreUtil::openWorkspaceFileForAcquisition(const QString fileName)
{
    if (!readyForCommand) {
        return false;
    }
    if (channelsConfigured) {
        errorMessage("There is already a workspace open. It must be closed first.");
        return false;
    }

    connect(this,&TrodesCoreUtil::workspaceOpened,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    startCommandTimeout(2000);
    openWorkspaceFileForAcquisition_internal(fileName);
    return true;

}
bool TrodesCoreUtil::closeWorkspace()
{
    if (!readyForCommand) {
        return false;
    }
    if (!channelsConfigured) {
        errorMessage("There is no workspace open to close.");
        return false;
    }
    if (closingWorkspace) {
        return false;
    }

    connect(this,&TrodesCoreUtil::workspaceClosed,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    startCommandTimeout(2000);
    closeWorkspace_internal();
    return true;

}
bool TrodesCoreUtil::closeRecordFile()
{
    if (!readyForCommand) {
        return false;
    }
    if (!recordFileOpen) {
        errorMessage("There is no active recording file open.");
        return false;
    }

    connect(this,&TrodesCoreUtil::recordFileClosed,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    startCommandTimeout(1000);
    closeRecordFile_internal();

    return true;
}
bool TrodesCoreUtil::openRecordFile(QString filename)
{
    if (!readyForCommand) {
        return false;
    }
    if (recordFileOpen) {
        errorMessage("There is already an active recording file open.");
        return false;
    }

    connect(this,&TrodesCoreUtil::recordFileOpened,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    startCommandTimeout(1000);
    if (!openRecordFile_internal(filename)) {
        disconnect(this,&TrodesCoreUtil::recordFileOpened,this,&TrodesCoreUtil::commandFinished);
        return false;
    }
    return true;
}
bool TrodesCoreUtil::startRecording()
{
    if (!readyForCommand) {
        return false;
    }
    if (!recordFileOpen) {
        errorMessage("Can't start recording. There is currently no file open.");
        return false;
    }
    if (recording) {
        errorMessage("Can't start recording because it is already ongoing.");
        return false;
    }

    connect(this,&TrodesCoreUtil::recordingStarted,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    startCommandTimeout(1000);
    startRecording_internal();


    return true;
}
bool TrodesCoreUtil::pauseRecording()
{
    if (!readyForCommand) {
        return false;
    }
    if (!recordFileOpen) {
        errorMessage("Can't pause recording. There is currently no file open.");
        return false;
    }
    if (!recording) {
        errorMessage("Can't pause recording because it is already paused.");
        return false;
    }
    connect(this,&TrodesCoreUtil::recordingStopped,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    startCommandTimeout(1000);
    pauseRecording_internal();

    return true;
}
bool TrodesCoreUtil::pausePlayback()
{
    if (!readyForCommand) {
        return false;
    }
    if (!playbackFileOpen) {
        errorMessage("There is no playback file open to pause.");
        return false;
    }

    pausePlayback_internal();
    commandFinished();

    return true;
}

bool TrodesCoreUtil::settleAmplifiers()
{
    if (!readyForCommand) {
        return false;
    }
    /*if (getCurrentSourceState() != SOURCE_STATE_RUNNING) {
        errorMessage("Source must be streaming to settle amplifiers.\n");
        return false;
    }*/

    settleAmps_internal();
    commandFinished();

    return true;
}
bool TrodesCoreUtil::setSource(DataSource source)
{
    if (!readyForCommand) {
        return false;
    }
    if ((getCurrentSourceState() != SOURCE_STATE_NOT_CONNECTED)&&(source != DataSource::SourceNone)) {
        errorMessage("You are already connected to a source. Disconnect first.\n");
        return false;
    }


    if (getCurrentSource() == DataSource::SourceFile && source == DataSource::SourceNone) {
        connect(this,&TrodesCoreUtil::workspaceClosed,this,&TrodesCoreUtil::commandFinished,Qt::SingleShotConnection);
    } else {
        connect(this,SIGNAL(sourceConnected(int)),this,SLOT(commandFinished()),Qt::SingleShotConnection);
    }
    startCommandTimeout(2000);
    setSource_internal(source);
    return true;
}
bool TrodesCoreUtil::connectToSource()
{
    if (!readyForCommand) {
        return false;
    }
    if (getCurrentSourceState() == SOURCE_STATE_RUNNING) {
        errorMessage("You are already streaming from the source.\n");
        return false;
    }
    if (getCurrentSourceState() != SOURCE_STATE_INITIALIZED) {
        errorMessage("You must connect to a source before you can stream from it.\n");
        return false;
    }

    connect(this,SIGNAL(sourceStarted()),this,SLOT(commandFinished()),Qt::SingleShotConnection);
    startCommandTimeout(4000);
    connectToSource_internal();
    return true;

}
bool TrodesCoreUtil::disconnectFromSource()
{
    if (!readyForCommand) {
        return false;
    }
    if (getCurrentSourceState() != SOURCE_STATE_RUNNING) {
        errorMessage("Source not streaming. There is nothing to stop.\n");
        return false;
    }
    if (recordFileOpen) {
        if (recordOut->getBytesWritten() > 0) {
            //There is a record file open with data in it
            //This needs to be closed
            errorMessage("Recording in progress. You must close the recording before disconnecting from source.\n");
            return false;
        }
    }

    connect(this,SIGNAL(sourceStopped()),this,SLOT(commandFinished()),Qt::SingleShotConnection);
    startCommandTimeout(1000);
    disconnectFromSource_internal();

    return true;
}
bool TrodesCoreUtil::runSourceDiagnistic(DataSource source)
{
    if (!readyForCommand) {       
        return false;
    }
    if (getCurrentSourceState() != SOURCE_STATE_NOT_CONNECTED) {
        errorMessage("Cannot run diagnostic when you are already connect to a source. Please disconnect from source first.\n");
        return false;
    }

    SourceSettings s = runSourceDiagnistic_internal(source);
    consoleOutputMessage(s.report);

    return true;
}

bool TrodesCoreUtil::calculateNoise(int nt, int ch)
{
    if (!readyForCommand) {
        return false;
    }
    if (!channelsConfigured) {
        errorMessage("Noise testing requires a workspace to be open.");
        return false;
    }
    if (sourceState != SOURCE_STATE_RUNNING) {
        errorMessage("Noise testing requires the source to be connected and streaming data.");
        return false;
    }

    connect(this,SIGNAL(noiseMeasureReturned(float)),this,SLOT(commandFinished()),Qt::SingleShotConnection);
    connect(streamManager,&StreamProcessorManager::sendRMSValues,this,&TrodesCoreUtil::processRMSValues,Qt::SingleShotConnection);

    if (nt == -1) {
        //All channels selected
        requestedNoiseHWChannel = -1;
        streamManager->enableRMSCalculations(true);
        streamManager->setTenSecBin();
        consoleOutputMessage("10-second noise measurement starting...");
        startCommandTimeout(12000);

    } else {
        if ((activeWorkspacePointers.spikeConf->ntrodes.length() <= nt) || (nt < 0) ) {
            errorMessage("The selected NTrode for noise testing does not exist in the current workspace.");
            return false;
        }
        if ((activeWorkspacePointers.spikeConf->ntrodes.at(nt)->hw_chan.length() <= ch) || (ch < 0) ) {
            errorMessage("The selected channel in the selected NTrode for noise testing does not exist in the current workspace.");
            return false;
        }
        streamManager->enableRMSCalculations(true);
        streamManager->setOneSecBin();
        requestedNoiseHWChannel = activeWorkspacePointers.spikeConf->ntrodes.at(nt)->hw_chan.at(ch);
        startCommandTimeout(2000);
    }

    //testImpedance_internal(nt, ch, cycles); //recommend using 200 cycles for precision

    return true;
}

bool TrodesCoreUtil::testImpedance(int nt, int ch, int cycles)
{
    if (!readyForCommand) {
        return false;
    }
    if (!channelsConfigured) {
        errorMessage("Impedance testing requires a workspace to be open.");
        return false;
    }
    if (sourceState != SOURCE_STATE_INITIALIZED) {
        errorMessage("Impedance testing requires the source to be connected and not currently streaming data.");
        return false;
    }
    if ( (getCurrentSource() != DataSource::SourceUSBDAQ) && (getCurrentSource() != DataSource::SourceEthernet) ) {
        errorMessage("Impedance testing is only supported with USB or Ethernet source.");
        return false;
    }
    if ((activeWorkspacePointers.spikeConf->ntrodes.length() <= nt) || (nt < 0) ) {
        errorMessage("The selected NTrode for impedance testing does not exist in the current workspace.");
        return false;
    }
    if ((activeWorkspacePointers.spikeConf->ntrodes.at(nt)->hw_chan.length() <= ch) || (ch < 0) ) {
        errorMessage("The selected channel in the selected NTrode for impedance testing does not exist in the current workspace.");
        return false;
    }


    connect(this,SIGNAL(impedanceMeasureReturned(int)),this,SLOT(commandFinished()),Qt::SingleShotConnection);
    startCommandTimeout(2000);

    testImpedance_internal(nt, ch, cycles); //recommend using 200 cycles for precision

    return true;
}
//END Public commands that route to internal ones
//******************************************************




void TrodesCoreUtil::openPlaybackFile_internal(const QString fileName)
{
   // playbackFilename = fileName;
   // setSource(DataSource::SourceFile);
    if (recordFileOpen) {
        errorMessage("You must close the active recording before opening a playback file.");
        return;
    }

    stagedPlaybackFileForLoad = fileName;
    if (playbackFileOpen) {
        connect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::openPlaybackFileLater);
        if (!setSource_internal(SourceNone)) {
            disconnect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::openPlaybackFileLater);
        }
        return;
    } else if (channelsConfigured) {
        connect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::openPlaybackFileLater);
        closeWorkspace_internal();
        return;
    }
    openPlaybackFileLater();

}

void TrodesCoreUtil::openPlaybackFileLater()
{
    disconnect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::openPlaybackFileLater);


    QString fileName = stagedPlaybackFileForLoad;
    int filePos = findEndOfConfigSection(fileName);
    usingExternalWorkspace = false;

    QFileInfo fI(fileName);

    QString baseName = fI.completeBaseName();
    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";

    QFileInfo workspaceFile(workspaceCheckName);
    if (workspaceFile.exists()) {
        debugMessage(QString("[TrodesCoreUtil::openPlaybackFile] Using the following external workspace file: %1").arg(workspaceFile.fileName()));
        usingExternalWorkspace = true;

    }

    playbackEmbeddedWorkspace.clear();
    playbackExternalWorkspace.clear();

//        int loadReturn;
    playbackFileOpen = true; //we set this global flag before opening the config file, because that way we can use it to stop opening modules
    if (usingExternalWorkspace) {
        QString errString = playbackExternalWorkspace.readTrodesConfig(workspaceFile.absoluteFilePath());
        if (!errString.isEmpty()) {
            errorMessage(QString("There was an error parsing %1. %2").arg(workspaceCheckName).arg(errString));
            sourceControl->setSource(SourceNone,activeWorkspacePointers);
            playbackFileOpen = false;
            return;
        }


    }

    QString errString = playbackEmbeddedWorkspace.readTrodesConfig(fileName);
    if (!errString.isEmpty()) {
        errorMessage(QString("There was an error parsing %1. %2").arg(fileName).arg(errString));
        sourceControl->setSource(SourceNone,activeWorkspacePointers);
        playbackFileOpen = false;
        return;
    }



    //The global configuration contains session-specific information, so it must be loaded from the embedded workspace in the recording file
    activeWorkspacePointers.globalConf = &playbackEmbeddedWorkspace.globalConf;

    if (usingExternalWorkspace) {
        activeWorkspacePointers.streamConf = &playbackExternalWorkspace.streamConf;
        activeWorkspacePointers.headerConf = &playbackExternalWorkspace.headerConf;
        activeWorkspacePointers.hardwareConf = &playbackExternalWorkspace.hardwareConf;
        activeWorkspacePointers.moduleConf = &playbackExternalWorkspace.moduleConf;
        activeWorkspacePointers.spikeConf = &playbackExternalWorkspace.spikeConf;
        activeWorkspacePointers.networkConf = &playbackExternalWorkspace.networkConf;
        activeWorkspacePointers.benchConfig = &playbackExternalWorkspace.benchConfig;
        activeWorkspacePointers.preferenceConf = &playbackExternalWorkspace.preferenceConf;

    } else {
        activeWorkspacePointers.streamConf = &playbackEmbeddedWorkspace.streamConf;
        activeWorkspacePointers.headerConf = &playbackEmbeddedWorkspace.headerConf;
        activeWorkspacePointers.hardwareConf = &playbackEmbeddedWorkspace.hardwareConf;
        activeWorkspacePointers.moduleConf = &playbackEmbeddedWorkspace.moduleConf;
        activeWorkspacePointers.spikeConf = &playbackEmbeddedWorkspace.spikeConf;
        activeWorkspacePointers.networkConf = &playbackEmbeddedWorkspace.networkConf;
        activeWorkspacePointers.benchConfig = &playbackEmbeddedWorkspace.benchConfig;
        activeWorkspacePointers.preferenceConf = &playbackEmbeddedWorkspace.preferenceConf;
    }

    bool loadSuccess = initializeSelectedWorkspace();

    playbackFile = fileName;
    fileDataPos = playbackEmbeddedWorkspace.dataStartLoc;

    playbackOpenTimer = new QTimer(this);
    connect(playbackOpenTimer, &QTimer::timeout, this, &TrodesCoreUtil::finalizeOpenPlaybackFile);
    //We need to allow enough time for the workspace threads to fully start before we start the file source thread.
    playbackOpenTimer->start(1000);

}

void  TrodesCoreUtil::finalizeOpenPlaybackFile()
{
    playbackOpenTimer->stop();
    sourceControl->setSource(SourceFile,activeWorkspacePointers);
    playbackOpenTimer->deleteLater();
}

void TrodesCoreUtil::setPlaybackTimeRange(uint32_t s,uint32_t e)
{
    playbackStartTime = s;
    playbackEndTime = e;
}

bool TrodesCoreUtil::openWorkspaceFileForAcquisition_internal(const QString fileName)
{
    if (channelsConfigured) {
        errorMessage("There is already a workspace open. It must be closed first.");
        return false;
    }

    debugMessage("Loading workspace for acquisition: " + fileName);



    //Read the config .xml file; the second argument specifies that this is being called within trodes
    acquisitionWorkspace.clear();
    QString errString = acquisitionWorkspace.readTrodesConfig(fileName);

    if (!errString.isEmpty()) {
        errorMessage("There was an error parsing " + fileName + ". " + errString);
        return false;
    }

    if (acquisitionWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {

        errorMessage("This workspace uses qsocket networking, which is no longer supported. ZMQ network must be selected.");
        acquisitionWorkspace.clear();
        return false;
    }

    loadedConfigFile = fileName;

    activeWorkspacePointers.globalConf = &acquisitionWorkspace.globalConf;
    activeWorkspacePointers.streamConf = &acquisitionWorkspace.streamConf;
    activeWorkspacePointers.headerConf = &acquisitionWorkspace.headerConf;
    activeWorkspacePointers.hardwareConf = &acquisitionWorkspace.hardwareConf;
    activeWorkspacePointers.moduleConf = &acquisitionWorkspace.moduleConf;
    activeWorkspacePointers.spikeConf = &acquisitionWorkspace.spikeConf;
    activeWorkspacePointers.networkConf = &acquisitionWorkspace.networkConf;
    activeWorkspacePointers.benchConfig = &acquisitionWorkspace.benchConfig;
    activeWorkspacePointers.preferenceConf = &acquisitionWorkspace.preferenceConf;


    bool loadSuccess = initializeSelectedWorkspace();

    return loadSuccess;

}

void TrodesCoreUtil::closeWorkspace_internal()
{

    if (channelsConfigured && !closingWorkspace) {
        //We need to shutdown multiple running threads before we can continue with exit
        //connect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::closeWorkspace);

        initiateWorkspaceClose();
        return;
    }
    //disconnect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::closeWorkspace);
    //debugMessage("Workspace closed.");
}

void TrodesCoreUtil::closeRecordFile_internal()
{
    recordOut->closeFile();
    recordFileOpen = false;
    currentFilePart = 1;

    //totalTimeRecorded = 0;
    debugMessage("Closed recording file.");
    emit recordFileClosed();

}
bool TrodesCoreUtil::openRecordFile_internal(QString fileName)
{
    int fileOpenStatus = recordOut->openFile(fileName, controllerSettings, headstageSettings); //creates the file and writes the current config info
    if (fileOpenStatus == -1) {
        errorMessage("File already exists. Please rename existing file first.");
        return false;
    }
    if (fileOpenStatus == -2) {
        errorMessage("File could not be created.");
        return false;
    }

    recordFileOpen = true;
    recordFileName = fileName;
    lastRecordFileName = fileName;
    //Comment dialog

    QFileInfo fInfo(recordFileName);
    recordFileBaseName = fInfo.absolutePath() + "/" + fInfo.completeBaseName();
    emit recordFileOpened(fileName);

    return true;
}

void TrodesCoreUtil::startRecording_internal()
{

    if (recordFileOpen) {
        recordOut->startRecord();
        recording = true;

        //statusbar->showMessage(tr("Started recording at ") + calcTimeString(currentTimeStamp));
        debugMessage("Started recording.");

        emit recordingStarted();
    } else {
        errorMessage("Can't start recording. There is currently no file open.");
    }

}
void TrodesCoreUtil::pauseRecording_internal()
{
    if (recordFileOpen) {

        recordOut->pauseRecord();
        recording = false;
        debugMessage("Paused recording");
        emit recordingStopped();

    } else {
        errorMessage("Can't pause recording. There is currently no file open.");
    }
}

void TrodesCoreUtil::toggleHardwareSimulationMode(bool on)
{

}

void TrodesCoreUtil::pausePlayback_internal()
{
    if (playbackFileOpen) {

        sourceControl->pauseSource();
        emit signal_sendPlaybackCommand(PC_PAUSE, currentTimeStamp);
    }
}

void TrodesCoreUtil::settleAmps_internal()
{
    sourceControl->sendSettleCommand();
}

bool TrodesCoreUtil::setSource_internal(DataSource source)
{
    //changes the source of the data stream

    if (getCurrentSourceState() == SOURCE_STATE_RUNNING) {
        errorMessage("You must stop streaming from the current source first.");
        return false;
    }

    if (source != sourceControl->currentSource) {

        bool needToClosePlaybackConfig = false;
        if (playbackFileOpen && (source != SourceFile) && (source != SourceNone)) {
            errorMessage("You must close the current playback file before changing to a different source.");
            return false;
        }

        if ((source != SourceFile) && (source != SourceNone) && !channelsConfigured) {
            errorMessage("You must open a workspace before connecting to this source.");
            return false;
        }


        QString pFileName;

        switch (source) {
        case SourceNone:
            if(sourceControl->currentSource == SourceDockUSB){
                if(recordFileOpen){
                    closeRecordFile_internal();
                }
            }
            if (playbackFileOpen) {
                needToClosePlaybackConfig = true;
            }

            headstageSettings.valid = false;
            controllerSettings.valid = false;

            sourceControl->setSource(source,activeWorkspacePointers);
            break;

        case SourceFake:

            sourceControl->setSource(source,activeWorkspacePointers);

            break;

        case SourceFakeSpikes:

            sourceControl->setSource(source,activeWorkspacePointers);

            break;

        case SourceFile:


            //pFileName = QFileDialog::getOpenFileName(this, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
            pFileName = playbackFilename;
            //Load the config file

            if (!pFileName.isEmpty()) {

                //Load the config file
                openPlaybackFile_internal(pFileName);

            }

            break;

        case SourceEthernet:

            sourceControl->setSource(source,activeWorkspacePointers);

            break;

        case SourceUSBDAQ:

            sourceControl->setSource(source,activeWorkspacePointers);

            break;
        case SourceUSB3:

            sourceControl->setSource(source,activeWorkspacePointers);

            break;
        case SourceRhythm:
            sourceControl->setSource(source,activeWorkspacePointers);
            break;

        case SourceDockUSB:

            sourceControl->setSource(source,activeWorkspacePointers);


            break;
        }


        if (needToClosePlaybackConfig) {
            closeWorkspace_internal(); //if the source was a file, we close the associated configuration
        }
        return true;
    }

}

bool TrodesCoreUtil::connectToSource_internal()
{
    if (getCurrentSourceState() != SOURCE_STATE_INITIALIZED) {
        errorMessage("You must connect to a source before you can stream from it.\n");
        return false;
    }

    if (hardwareSimulationModeOn) {
        //Tell the hardware to generate fake data
        sourceControl->connectToSource_Simulation();
    } else {
        //Regular streaming
        sourceControl->connectToSource();
    }

    //TODO: Check if connection went ok
    return true;

}

bool TrodesCoreUtil::disconnectFromSource_internal()
{
    if (getCurrentSourceState() != SOURCE_STATE_RUNNING) {
        errorMessage("Source not streaming. There is nothing to stop.\n");
        return false;
    }

    if (recordFileOpen) {
        if (recordOut->getBytesWritten() > 0) {
            //There is a record file open with data in it
            //This needs to be closed
            errorMessage("Recording in progress. You must close the recording before disconnecting from source.\n");
            return false;
        }
    }

    if (sourceState == SOURCE_STATE_RUNNING) {
        debugMessage("Stopping source streaming.");
        sourceControl->disconnectFromSource();
    }
    return true;
}

void TrodesCoreUtil::sendSettleCommand()
{

}

SourceSettings TrodesCoreUtil::runSourceDiagnistic_internal(DataSource source)
{
    SourceSettings s = sourceControl->runSourceDiagnistic(source);
    return s;
}

bool TrodesCoreUtil::testImpedance_internal(int nt, int ch, int cycles)
{

    //nt and ch should be 0-based

    if (!channelsConfigured) {
        errorMessage("Impedance testing requires a workspace to be open.");
        return false;
    }
    if (sourceState != SOURCE_STATE_INITIALIZED) {
        errorMessage("Impendance testing requires the source to be connected and not currently streaming data.");
        return false;
    }
    if ( (getCurrentSource() != DataSource::SourceUSBDAQ) && (getCurrentSource() != DataSource::SourceEthernet) ) {
        errorMessage("Impedance testing is only supported with USB or Ethernet source.");
        return false;
    }
    if ((activeWorkspacePointers.spikeConf->ntrodes.length() <= nt) || (nt < 0) ) {
        errorMessage("The selected NTrode for impedacne testing does not exist in the current workspace.");
        return false;
    }
    if ((activeWorkspacePointers.spikeConf->ntrodes.at(nt)->hw_chan.length() <= ch) || (ch < 0) ) {
        errorMessage("The selected channel in the selected NTrode for impedacne testing does not exist in the current workspace.");
        return false;
    }

    //TODO: check if connected headstage supports impendance test

    int hwChan = activeWorkspacePointers.spikeConf->ntrodes.at(nt)->unconverted_hw_chan.at(ch);
    HardwareImpedanceMeasureCommand s;
    s.frequency = 1000;
    s.notchHz = 60;  //TODO: support for 50 Hz notch
    s.numberOfCycles = cycles;
    s.channel = hwChan;

    emit impedanceMeasureRequested(s);
    return true;
}

void TrodesCoreUtil::printVersionInfo()
{
    QString info;
    info = GlobalConfiguration::getVersionInfo(false); //print version info to debug log
    consoleOutputMessage(info);
}

//----------------------------------------
//Private Slots/Functions
//-----------------------------------------

bool TrodesCoreUtil::initializeSelectedWorkspace()
{
    //Stream controller setup---------------------------
    setupStreamManager();
    //printDebugCheckpoint("Stream manager done");
    //progress.setValue(10);
    //QCoreApplication::processEvents();


    if (!streamManager->avxsupported) {
        errorMessage("AVX-based CPU support required. Aborting load.");
        channelsConfigured = false;
        //TODO -- close stream manager
        return false;
    }

    //connect(streamManager->spikeProcessor, &SpikeProcessorThread::newProcessedSpikes,this,&MultiNtrodeDisplayWidget::receiveNewSpikes,Qt::DirectConnection);

    //Audio thread setup--------------------------
    if (activeWorkspacePointers.hardwareConf->NCHAN > 0 && activeWorkspacePointers.spikeConf->ntrodes.length() > 0 && isAudioAvailable) {
        soundOut->setConfiguration(activeWorkspacePointers);
        soundOut->setChannel(activeWorkspacePointers.spikeConf->ntrodes[0]->hw_chan[0]); //set the audio channel to listen to
        soundOut->updateAudio();
    }
    //---------------------------------------------

    //Record thread setup---------------------------
    if (!playbackFileOpen) {
        recordOut = new RecordThread(&acquisitionWorkspace, activeWorkspacePointers.globalConf->saveDisplayedChanOnly, activeWorkspacePointers.globalConf->MBPerFileChunk);
        QThread* workerThread = new QThread();
        recordOut->moveToThread(workerThread);
        connect(workerThread, SIGNAL(started()), recordOut, SLOT(setUp()));
        connect(recordOut, SIGNAL(finished()), workerThread, SLOT(quit()));
        connect(recordOut,SIGNAL(finished()), recordOut, SLOT(deleteLater()));
        connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
        workerThread->setObjectName("Recorder");
        workerThread->start(QThread::HighPriority); //if the priority is too low, then it is possible for the buffer to fill up before a file write.
        connect(this, SIGNAL(endAllThreads()), recordOut, SLOT(endRecordThread()));
        // update the list of channels to save once a configuration file has been loaded
        connect(this, SIGNAL(workspaceOpened()), recordOut, SLOT(setupSaveDisplayedChan()));
        connect(recordOut, &RecordThread::writeError, this, &TrodesCoreUtil::errorSaving);
        connect(recordOut, &RecordThread::noSpaceLeftError, this, &TrodesCoreUtil::noSpaceLeftWhileSaving);
        connect(recordOut, &RecordThread::lowDiskSpaceWarning, this, &TrodesCoreUtil::lowDiskSpaceWarning);
        connect(recordOut, &RecordThread::newDebugMessage,this,&TrodesCoreUtil::debugMessage);
        connect(recordOut, &RecordThread::newErrorMessage,this,&TrodesCoreUtil::errorMessage);
        connect(recordOut, &RecordThread::newConsoleOutputMessage,this,&TrodesCoreUtil::consoleOutputMessage);
        //connect(recordOut, &RecordThread::triggerNextFilePart, this, &TrodesCoreUtil::createNextFilePart);

    }
    //----------------------------------------------


    channelsConfigured = true;
    closingWorkspace = false;

    //startTrodesNetwork();


    // set up the network connections with the modules if modules are defined
    // set myID to -1 to indicate that this is trodes
    activeWorkspacePointers.moduleConf->myID = TRODES_ID;

    //connect(sourceControl, &SourceController::newTimestamp, this, &MainWindow::seekPlaybackSignal, Qt::UniqueConnection);

    // update the list of channels to save for the record thread

    //Network-related stuff
    activeWorkspacePointers.networkConf->trodesHost = QString::fromStdString(trodesModule->getAddress());
    activeWorkspacePointers.networkConf->trodesPort = trodesModule->getPort();
    trodesModule->setModuleTimeRate(activeWorkspacePointers.hardwareConf->sourceSamplingRate);
    connect(streamManager, &StreamProcessorManager::newContinuousData, trodesModule.get(), &trodes::TrodesModule::registerNewContData, Qt::QueuedConnection);
    streamManager->startLFPPubThread();
    streamManager->startSpikePub();
    loadConfigIntoNetworkObject();

    emit workspaceOpened();

    return true;
}


void TrodesCoreUtil::setupStreamManager()
{
    streamManager = new StreamProcessorManager(nullptr,activeWorkspacePointers, enableAVXFlag);
    connect(streamManager, SIGNAL(bufferOverrun()), this, SLOT(bufferOverrunHandler())); //In case data rates are too fast, use an emergency stop signal
    connect(streamManager, SIGNAL(sourceFail_Sig(bool)), sourceControl, SLOT(noDataComing(bool)));
    connect(streamManager,SIGNAL(sourceFail_Sig(bool)),this,SLOT(sourceNoDataError(bool)));
    //connect(streamManager, &StreamProcessorManager::newDebugMessage,this,&TrodesCoreUtil::newDebugMessage);
    connect(streamManager,&StreamProcessorManager::newDebugMessage,this,&TrodesCoreUtil::debugMessage);


    connect(streamManager,SIGNAL(headstageFail_Sig(bool)),this,SLOT(sourceNoHeadstageError(bool)));
    //connect(this, SIGNAL(newTraceLength(double)), streamManager, SLOT(updateDataLength(double)));

    //All connections between streamconf/spikeconf and streammanager is done in streammanager's constructor
    //connect(activeWorkspacePointers.spikeConf, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
    //connect(activeWorkspacePointers.spikeConf, SIGNAL(changeAllThresh(int)), this, SLOT(setAllThresh(int)));
    connect(sourceControl, SIGNAL(acquisitionStarted()), streamManager, SLOT(startAcquisition()));
    connect(sourceControl, SIGNAL(acquisitionStopped()), streamManager, SLOT(stopAcquisition()));
    connect(sourceControl,SIGNAL(signal_UnresponsiveHeadstage(bool)), this, SLOT(sourceUnresponsiveHeadstage(bool)));
    connect(sourceControl, &SourceController::newDebugMessage, this, &TrodesCoreUtil::debugMessage);
    connect(sourceControl, &SourceController::newErrorMessage, this, &TrodesCoreUtil::errorMessage);
    connect(sourceControl, &SourceController::newConsoleOutputMessage, this, &TrodesCoreUtil::consoleOutputMessage);
    connect(sourceControl,&SourceController::newGlobalStimSettings,streamManager,&StreamProcessorManager::updateGlobalStimSettings);
    connect(streamManager, SIGNAL(functionTriggerRequest(int)), sourceControl, SLOT(sendFunctionTriggerCommand(int)));

    streamManager->setUp();
    debugMessage("Stream manager set up.");
}

bool TrodesCoreUtil::startTrodesNetwork() {
    //mark: network
    try{

        QString qs_address = networkAddress;
        int port = networkPort;

        if(!qs_address.startsWith("tcp://")){
            qs_address.insert(0, "tcp://");
        }

        std::string address = qs_address.toStdString();
        // address is a std::string with the network address
        // port is an int with the network port

        //Check if another trodes server is already running on this address
        if (trodes::network::Connection::network_exists(address, port)) {
            //printf("Could not start Trodes network. Please make sure the address %s:%d exists and that another instance of Trodes is not running already on that address.\n", address.c_str(),port);
            return false;
        }

        trodesModule = std::move(std::unique_ptr<trodes::TrodesModule>(new trodes::TrodesModule(address, port)));

        trodesModule->setModuleTimePtr(&currentTimeStamp);

        // keep this regardless because it's signalling itself
        connect(this, &TrodesCoreUtil::signal_sendPlaybackCommand, this, &TrodesCoreUtil::sendAquisitionCommand);

        // may require unique_ptr's get to obtain raw pointer
        // all of these connects are cause and effect
        connect(this, &TrodesCoreUtil::recordFileOpened, trodesModule.get(), &trodes::TrodesModule::sendFileOpened, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::recordFileClosed, trodesModule.get(), &trodes::TrodesModule::sendFileClose, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::sourceConnected, trodesModule.get(), &trodes::TrodesModule::sendSourceConnect, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::recordingStarted, trodesModule.get(), &trodes::TrodesModule::sendRecordingStarted, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::recordingStopped, trodesModule.get(), &trodes::TrodesModule::sendRecordingStopped, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::newErrorMessage, trodesModule.get(), &trodes::TrodesModule::sendConsoleErrorMessage, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::newDebugMessage, trodesModule.get(), &trodes::TrodesModule::sendConsoleDebugMessage, Qt::QueuedConnection);
        connect(this, &TrodesCoreUtil::newConsoleOutputMessage, trodesModule.get(), &trodes::TrodesModule::sendConsoleOutputMessage, Qt::QueuedConnection);
        connect(sourceControl, &SourceController::acquisitionStarted, trodesModule.get(), &trodes::TrodesModule::sendAcquisitionStarted, Qt::QueuedConnection);
        connect(sourceControl, &SourceController::acquisitionStopped, trodesModule.get(), &trodes::TrodesModule::sendAcquisitionStopped, Qt::QueuedConnection);
        //connect(trodesModule.get(), &trodes::TrodesModule::moduleCrashed, this, &TrodesCoreUtil::restartCrashedModule, Qt::QueuedConnection);

        connect(trodesModule.get(), &trodes::TrodesModule::settleCommandTriggered, this, &TrodesCoreUtil::sendSettleCommand);
        connect(this, &TrodesCoreUtil::sig_sendPlaybackCommand, trodesModule.get(), &trodes::TrodesModule::sendPlaybackCommand);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_acquisitionReceived, this, &TrodesCoreUtil::processAcquisitionCommand);

        // these can be removed because they're related to the previous system

        connect(sourceControl, &SourceController::newContinuousData, trodesModule.get(), &trodes::TrodesModule::registerNewContData, Qt::QueuedConnection);



        //        connect(sourceControl, &SourceController::headstageSettingsReturned, mainServer, &TrodesCentralServer::newHeadstageSettings, Qt::QueuedConnection);
        connect(sourceControl, &SourceController::controllerSettingsReturned, trodesModule.get(), &trodes::TrodesModule::newControllerSettings, Qt::QueuedConnection);

        /*if (spikeDisp != nullptr) {
            connect(spikeDisp, &MultiNtrodeDisplayWidget::newContinuousData, trodesModule.get(), &trodes::TrodesModule::registerNewContData, Qt::QueuedConnection);
            connect(spikeDisp, &MultiNtrodeDisplayWidget::broadcastNewEventReq, trodesModule.get(), &trodes::TrodesModule::provideEventSlot, Qt::QueuedConnection);
            connect(spikeDisp, &MultiNtrodeDisplayWidget::broadcastEventRemoveReq, trodesModule.get(), &trodes::TrodesModule::removeEventSlot, Qt::QueuedConnection);
        }*/
        connect(sourceControl, &SourceController::deregisterHighFreqData, trodesModule.get(), &trodes::TrodesModule::deregisterContData, Qt::QueuedConnection);

        // keep these because they're necessary to generate the connects
        qRegisterMetaType<uint16_t>("uint16_t");
        qRegisterMetaType<StimulationCommand>("StimulationCommand");
        qRegisterMetaType<GlobalStimulationSettings>("GlobalStimulationSettings");
        qRegisterMetaType<GlobalStimulationCommand>("GlobalStimulationCommand");

        // all of these are related to receiving commands over the network and
        // making them happen on the hardware because the current code is
        // directly connected to the hardware
        connect(trodesModule.get(), &trodes::TrodesModule::sendSetStimulationParams, sourceControl, &SourceController::SetStimulationParams);
        connect(trodesModule.get(), &trodes::TrodesModule::sendClearStimulationParams, sourceControl, &SourceController::ClearStimulationParams);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStartSlot, sourceControl, &SourceController::SendStimulationStart);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStartGroup, sourceControl, &SourceController::SendStimulationStartGroup);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStopSlot, sourceControl, &SourceController::SendStimulationStop);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStopGroup, sourceControl, &SourceController::SendStimulationStopGroup);
        connect(trodesModule.get(), &trodes::TrodesModule::sendGlobalStimulationSettings, sourceControl, &SourceController::SendGlobalStimulationSettings);
        connect(trodesModule.get(), &trodes::TrodesModule::sendGlobalStimulationCommand, sourceControl, &SourceController::SendGlobalStimulationCommand);
        connect(trodesModule.get(), &trodes::TrodesModule::sendECUShortcutMessage, sourceControl, &SourceController::SendECUShortcutMessage);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_console_command, this, &TrodesCoreUtil::parseTextControl_no_ok);

        // open .rec file, start recording, pause recording, close file
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_open, this, &TrodesCoreUtil::openRecordFile_internal);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_start, this, &TrodesCoreUtil::startRecording_internal);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_pause, this, &TrodesCoreUtil::pauseRecording_internal);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_close, this, &TrodesCoreUtil::closeRecordFile_internal);

        trodesModule->provideEvent("NtrodeChanSelect");
        //connect(eegDisp, &StreamDisplayManager::nTrode_chan_selected, trodesModule.get(), &trodes::TrodesModule::ntrodeChanSelected);

        //networkPanel->setAddressAndPort(trodesModule->getAddress().c_str(), trodesModule->getPort());
        //networkPanel->updateClients(trodesModule->getClientList());
        //connect(trodesModule.get(), &trodes::TrodesModule::moduleClosed, networkPanel, &NetworkPanel::clientClosed);
        //connect(trodesModule.get(), &trodes::TrodesModule::moduleConnected, networkPanel, &NetworkPanel::clientConnected);
        //connect(trodesModule.get(), &trodes::TrodesModule::moduleCrashed, networkPanel, &NetworkPanel::clientCrashed);
        //connect(networkPanel, &NetworkPanel::sendQuit, trodesModule.get(), &trodes::TrodesModule::sendQuitToModule);

        //if(spikeDisp) spikeDisp->startDedicatedSpikePub();
        //consoleOutputMessage(QString("Trodes network open at address %1, port %2").arg(qs_address).arg(port));

        return true;

    } catch(std::string &e){
        errorMessage(QString("Could not start the network. %1").arg(e.c_str()));
        return false;
    }
}

void TrodesCoreUtil::endTrodesNetwork() {
    //shutdown the trodes network
    trodesModule.reset();
    debugMessage("ZMQ-based network shut down.");
}


void TrodesCoreUtil::loadConfigIntoNetworkObject(void)
{
    TrodesConfig config;

    //Add Devices
    for (int i = 0; i < activeWorkspacePointers.hardwareConf->devices.length(); i++) {
        DeviceInfo curDev = activeWorkspacePointers.hardwareConf->devices.at(i);
        NDevice newDev;
        newDev.setName(curDev.name.toStdString());
        newDev.setNumBytes(curDev.numBytes);
        newDev.setByteOffset(curDev.byteOffset);

        for (int j = 0; j < curDev.channels.length(); j++) {
            DeviceChannel curChan = curDev.channels.at(j);
            NDeviceChannel newChan;

            newChan.setId(curChan.idString.toStdString());
            newChan.setStartByte(curChan.startByte);
            newChan.setStartBit(curChan.digitalBit);
            newChan.setType((int)curChan.dataType);
            newChan.setInterleavedDataByte(curChan.interleavedDataIDByte);
            newChan.setInterleavedDataBit(curChan.interleavedDataIDBit);

            newDev.addChannel(newChan);
        }

        config.addDevice(newDev);
    }

    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        SingleSpikeTrodeConf *curNTrode = activeWorkspacePointers.spikeConf->ntrodes.at(i);
        NTrodeObj newNTrode;
        QString nTrodeID = QString("%1").arg(curNTrode->nTrodeId);
        newNTrode.setId(nTrodeID.toStdString());
        newNTrode.setIndex(curNTrode->nTrodeIndex);

        for (int j = 0; j < curNTrode->hw_chan.length(); j++) {
            newNTrode.addHWChan(curNTrode->hw_chan.at(j));
        }

        config.addNTrode(newNTrode);
    }

    trodesModule->setConfig(config);
    trodesModule->sendConfigToModules();
}

void TrodesCoreUtil::processAcquisitionCommand(QString cmd, qint32 time)
{
    //This is where network-based data acquisition commands are processed

    if (cmd == acq_PLAY) {
        //Start streaming command
        if (!connectToSource_internal()) {
            //Any additional error handling here

        }
    }
    else if (cmd == acq_PAUSE) {
        //Pause streaming command. Only valid during file playback.
        if (playbackFileOpen) {
            pausePlayback_internal();
        }
    }
    else if (cmd == acq_STOP) {
        //Stop streaming command
        if (!disconnectFromSource_internal()) {
            //Any adiitional error handling here

        }
    }
    else if (cmd == acq_SEEK) {
        //Seek command. Only valid during file playback.
        if (playbackFileOpen) {
            qreal pct = (qreal)(time-playbackStartTime)/(playbackEndTime - playbackStartTime);
            emit jumpFileTo(pct);
        }
    }
    else if (cmd == acq_RECORD) {
        //Start record command. Only valid during live source streaming.
        startRecording_internal();
    }
    else if (cmd == acq_STOPRECORD) {
        //Stop record command. Only valid during live source streaming.
        pauseRecording_internal();
    }
    else
        errorMessage("Network-based acquisition command not recognized: " + cmd);


}

void TrodesCoreUtil::sendAquisitionCommand(qint8 cmd, qint32 time)
{

}

void TrodesCoreUtil::playbackAcquisitionStopped()
{

}

void TrodesCoreUtil::broadcastEvent(TrodesEventMessage ev)
{

}

void TrodesCoreUtil::sourceStateChanged(int state)
{
    //When the state of the source changes, the state is emitted and this function is called
    //to set the menus
    if (sourceState == state) {
        //No change in state.
        emit sourceConnected(sourceState);
        return;
    }
    int prevState = sourceState;
    sourceState = state;
    if (state == SOURCE_STATE_NOT_CONNECTED) {

        //reset error flags
        sourcePacketSizeError(false);
        sourceNoDataError(false);
        sourceNoHeadstageError(false);
        sourceUnresponsiveHeadstage(false);

        dataStreaming = false;
        debugMessage("Source state: not connected to any source.");
        emit sourceConnected((TrodesSource)SourceNone);
    }
    else if (state == SOURCE_STATE_CONNECTERROR) {

        errorMessage("Connection to source failed.");
        setSource_internal(SourceNone);
    }
    else if (state == SOURCE_STATE_INITIALIZED) {
        if (channelsConfigured) {


        }

        if (playbackFileOpen) {

        }



        //reset error flags
        sourcePacketSizeError(false);
        sourceNoDataError(false);
        sourceNoHeadstageError(false);
        sourceUnresponsiveHeadstage(false);

        dataStreaming = false;

        if (prevState == SOURCE_STATE_RUNNING) {
            debugMessage("Source state: stopped streaming. Still connected to device.");
            emit sourceStopped();
        } else {
            debugMessage("Source state: connected to device. Currently not streaming.");
            emit sourceConnected((TrodesSource)sourceControl->currentSource);
        }


        if (quitting) {

        }
    }
    else if (state == SOURCE_STATE_RUNNING) {

        dataStreaming = true;
        if (sourceControl->currentSource != SourceFile) {
            //Create record file permitted

        }
        debugMessage("Source state: streaming from device.");
        emit sourceStarted();
    }
    else if (state == SOURCE_STATE_PAUSED) {
        dataStreaming = false;
        debugMessage("Source state: paused streaming.");
        emit sourcePaused();
    }
}

void TrodesCoreUtil::sourcePacketSizeError(bool on)
{

}

void TrodesCoreUtil::sourceUnresponsiveHeadstage(bool on)
{

}

void TrodesCoreUtil::sourceNoDataError(bool on)
{

}

void TrodesCoreUtil::sourceNoHeadstageError(bool on)
{

}

void TrodesCoreUtil::bufferOverrunHandler()
{

}

void TrodesCoreUtil::errorSaving()
{
    errorMessage("Serious error writing to disk.  A brief period of data was lost because the hard drive became unresponive. See debug log for more information. Please report to SpikeGadgets support team.");
    emit issueWithRecording(TrodesRecordingIssue::WriteFail);
}

void TrodesCoreUtil::noSpaceLeftWhileSaving()
{
    errorMessage("No space left on drive. Recording aborted.");
    emit issueWithRecording(TrodesRecordingIssue::NoDiskSpace);
}

void TrodesCoreUtil::lowDiskSpaceWarning()
{
    emit issueWithRecording(TrodesRecordingIssue::LowDiskSpace);
    errorMessage("You are running out is disk space! Please free up space now.");
}

void TrodesCoreUtil::errorMessage(QString msg)
{
    emit newErrorMessage(msg);
}

void  TrodesCoreUtil::consoleOutputMessage(QString msg) {
    emit newConsoleOutputMessage(msg);
}

void TrodesCoreUtil::debugMessage(QString msg)
{
    if (appendTimeToStatus) {
        QDateTime dateTime = QDateTime::currentDateTime();
        msg = dateTime.toString() + " " + msg;
    }

    emit newDebugMessage(msg);
}

void TrodesCoreUtil::startSourceController() {
    sourceControl = new SourceController(nullptr);
    connect(sourceControl, SIGNAL(stateChanged(int)), this, SLOT(sourceStateChanged(int)));
    connect(sourceControl,SIGNAL(setTimeStamps(uint32_t,uint32_t)), this, SLOT(setPlaybackTimeRange(uint32_t,uint32_t)));
    connect(sourceControl,SIGNAL(packetSizeError(bool)),this,SLOT(sourcePacketSizeError(bool)));
    connect(this, SIGNAL(jumpFileTo(qreal)), sourceControl, SIGNAL(jumpFileTo(qreal)));
    connect(this, &TrodesCoreUtil::impedanceMeasureRequested,sourceControl,&SourceController::measureImpedance);
    connect(sourceControl,&SourceController::impedanceValueReturned,this,&TrodesCoreUtil::processImpedanceValue);
    connect(sourceControl, SIGNAL(headstageSettingsReturned(HeadstageSettings)),this, SLOT(headstageSettingsChanged(HeadstageSettings)));
    connect(sourceControl, SIGNAL(controllerSettingsReturned(HardwareControllerSettings)),this, SLOT(controllerSettingsChanged(HardwareControllerSettings)));

}

void TrodesCoreUtil::startAudioThread()
{
    //Audio thread
    if (isAudioAvailable) {

        soundOut = new AudioController();
        if (!soundOut->setupOk()) {
            isAudioAvailable = false;
            errorMessage("Either no audio device was found, or the Qt Multimedia plugin was not found. Audio functionality turned off.");
        }
    }

    if (isAudioAvailable) {

        soundOut->setChannel(-1); //set the audio channel to listen to
        soundOut->setGain(0);

        connect(this, SIGNAL(setAudioChannel(int)), soundOut, SLOT(setChannel(int)));
        connect(this, SIGNAL(setAudioDevice(QString)), soundOut, SLOT(setDevice(QString)));
        connect(this, SIGNAL(updateAudio()), soundOut, SLOT(updateAudio()));
        connect(soundOut, SIGNAL(deviceChanged(QAudioSink*)),this,SLOT(audioDeviceChanged(QAudioSink*)));
        connect(this, SIGNAL(endAudioThread()),soundOut,SLOT(endAudio()));
        connect(sourceControl, SIGNAL(acquisitionStarted()), soundOut, SLOT(startAudio()));
        connect(sourceControl, SIGNAL(acquisitionStopped()), soundOut, SLOT(stopAudio()));
        connect(sourceControl, SIGNAL(acquisitionPaused()), soundOut, SLOT(pauseAudio()));
        connect(soundOut,&AudioController::newDebugMessage,this,&TrodesCoreUtil::debugMessage);
        connect(soundOut,&AudioController::newConsoleOutputMessage,this,&TrodesCoreUtil::consoleOutputMessage);
        connect(soundOut,&AudioController::newErrorMessage,this,&TrodesCoreUtil::errorMessage);




        //soundOut->setThresh(a_thresh);
        //soundOut->setGain(a_gain);
        //emit setAudioDevice(a_device);

        /*QStringList devs = soundOut->getAvailableDevices();
            for (int i=0; i<devs.length(); i++) {
                newSoundDialog->deviceCombo->addItem(devs.at(i));
                if (soundOut->getCurrentDevice().compare(devs.at(i)) == 0) {
                    newSoundDialog->deviceCombo->setCurrentIndex(i);
                }
            }*/


        QThread* audioThread = new QThread();
        audioThread->setObjectName("AudioThread");

        connect(audioThread, SIGNAL(started()), soundOut, SLOT(startAudio()));
        connect(soundOut, SIGNAL(finished()), audioThread, SLOT(quit()));
        connect(soundOut, SIGNAL(finished()), soundOut, SLOT(deleteLater()));
        connect(audioThread, SIGNAL(finished()), audioThread, SLOT(deleteLater()));
        soundOut->moveToThread(audioThread);
        audioThread->start();

    }
}

void TrodesCoreUtil::audioDeviceChanged(QAudioSink *oldDevice) {
    //The previous QAudioOutput device can only be deleted in the main UI thread, so we do it here.
    if (oldDevice != NULL) {
        oldDevice->deleteLater();
    }
}

void TrodesCoreUtil::commandFinished()
{
    commandTimoutTimer.stop();
    disconnect(&commandTimoutTimer,&QTimer::timeout,this, &TrodesCoreUtil::commandTimedOut);
    readyForCommand = true;
    emit controlCommandFinished();
}

void TrodesCoreUtil::commandTimedOut()
{
    commandTimoutTimer.stop();
    readyForCommand = true;
    emit controlCommandFailed();
}
void TrodesCoreUtil::startCommandTimeout(int timeout)
{
    readyForCommand = false;
    connect(&commandTimoutTimer,&QTimer::timeout,this, &TrodesCoreUtil::commandTimedOut);
    commandTimoutTimer.start(timeout);
}


void TrodesCoreUtil::headstageSettingsChanged(HeadstageSettings s)
{

}

void TrodesCoreUtil::controllerSettingsChanged(HardwareControllerSettings s)
{

}



void TrodesCoreUtil::initiateWorkspaceClose()
{

    if (channelsConfigured) {

        if (playbackFileOpen) {
            setSource_internal(SourceNone);
            return;

        } else {
            if (recordFileOpen) {
                closeRecordFile_internal();
            }
        }

        channelsConfigured = false;
        closingWorkspace = true;

        if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
            if(trodesModule) {
                trodesModule->sendQuit();
            }
        }

        if (sourceState == SOURCE_STATE_RUNNING) {
            disconnectFromSource_internal(); //TODO: ideally we do not fully disconnect, but rather pause processing of the input.
        }

        if (isAudioAvailable)
            soundOut->setChannel(-1); //set the audio channel to -1 (off)


        //We want to stay connected to the source
        //setSource(SourceNone);

        emit endAllThreads(); //this will end the recorder thread

        //Mark: to do - this is a direct call to end separate threads, causing race conditions
        streamManager->stopAcquisition();

        //this should always come last b/c of the zsys_shutdown()
        /*if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
            endTrodesNetwork();//Deletes zmq server stuff
        }*/

        workspaceShutdownCheckTimer = new QTimer(this);
        connect(workspaceShutdownCheckTimer, &QTimer::timeout, this, &TrodesCoreUtil::finishWorkspaceClose);
        workspaceShutdownCheckTimer->start(1000); //We need to allow enough time for the source controller and streamProcessor threads to end

    }

}

void TrodesCoreUtil::finishWorkspaceClose()
{
    workspaceShutdownCheckTimer->stop();

    //Mark: to do - this is a direct call to end separate threads, causing race conditions
    streamManager->removeAllProcessors();
    delete streamManager;

    //this should always come last b/c of the zsys_shutdown()
    //MOVE
    /*if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
        endTrodesNetwork();//Deletes zmq server stuff
    }*/


    playbackEmbeddedWorkspace.clear();
    playbackExternalWorkspace.clear();
    acquisitionWorkspace.clear();


    activeWorkspacePointers.networkConf=nullptr;
    activeWorkspacePointers.moduleConf=nullptr;
    activeWorkspacePointers.streamConf=nullptr;
    activeWorkspacePointers.spikeConf=nullptr;
    activeWorkspacePointers.headerConf=nullptr;
    activeWorkspacePointers.hardwareConf=nullptr;
    activeWorkspacePointers.globalConf=nullptr;
    activeWorkspacePointers.preferenceConf=nullptr;

    //sourceControl->clearBuffers();
    closingWorkspace = false;
    debugMessage("Workspace closed.");
    emit workspaceClosed();


}

void TrodesCoreUtil::shutDown() {

    if (!closeInitiated) {
        emit shutDownIntitiated();
    }
    closeInitiated = true;

    if (channelsConfigured) {
        //We need to shutdown multiple running threads before we can continue with exit
        connect(this, &TrodesCoreUtil::workspaceClosed, this, &TrodesCoreUtil::shutDown);
        initiateWorkspaceClose();
        return;
    }
    if (isAudioAvailable) {
        soundOut->endAudio();
        emit endAudioThread();
    }

    emit shutDownComplete();

}

int TrodesCoreUtil::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    Q_ASSERT(!configFileName.isEmpty());

    file.setFileName(configFileName);
    if (!file.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QFileInfo fi(configFileName);
    QString ext = fi.suffix();
    if (ext.compare("rec") == 0) {
        //this is a rec file with a configuration in the header

        QString configContent;
        QString configLine;
        bool foundEndOfConfig = false;

        while (file.pos() < 1000000) {
            configLine += file.readLine();
            configContent += configLine;
            if (configLine.contains("</Configuration>") ) {
                foundEndOfConfig = true;
                break;
            }
            configLine = "";
        }

        if (foundEndOfConfig) {
            filePos = file.pos();
        }
    }
    file.close();
    return filePos;
}

void TrodesCoreUtil::openQueuedRecordFile()
{
    openRecordFile_internal(queuedRecordFileName);
}

void TrodesCoreUtil::createNextFilePart()
{
//This is used to break the recording up into multiple parts of equal sizes.
//Signals are sent out to modules to create new files as well.


    if (recordFileOpen) {

        int fPart = currentFilePart+1;
        queuedRecordFileName = recordFileBaseName+QString(".part%1").arg(fPart)+".rec";
        closeRecordFile_internal(); //resets cuurentFilePart to 1
        currentFilePart = fPart;

        //We create the next file and start recording a bit later to give modules time to stop recording, etc.
        QTimer::singleShot(1000, this, &TrodesCoreUtil::openQueuedRecordFile);

        currentFilePart++;

    }

}

void TrodesCoreUtil::processImpedanceValue(int HWChan, int value)
{

    if (HWChan > -1) {
        consoleOutputMessage(QString("Impedance measured: %1 Ohm").arg(value));
        emit impedanceMeasureReturned(value);
    }
}

void TrodesCoreUtil::processRMSValues(QList<qreal> values)
{

    consoleOutputMessage("Noise measurement finished.");

    if (requestedNoiseHWChannel == -1) {
        //All channels requested
        QString output;
        output.append("NTrode | Channel | Noise measure (uV RMS)\n");
        for (int nt = 0; nt < activeWorkspacePointers.spikeConf->ntrodes.length();nt++) {
            for (int ch = 0; ch < activeWorkspacePointers.spikeConf->ntrodes.at(nt)->hw_chan.length(); ch++) {
                output.append(QString("%1         %2        %3\n").arg(activeWorkspacePointers.spikeConf->ntrodes.at(nt)->nTrodeId).arg(ch + 1).arg(values.at(activeWorkspacePointers.spikeConf->ntrodes.at(nt)->hw_chan.at(ch)),0,'f',2));
            }
        }
        consoleOutputMessage(output);
        emit noiseMeasureReturned(-1.0);
    } else if ((requestedNoiseHWChannel >= 0) && (requestedNoiseHWChannel < values.length())) {
        //One channel selected
        float val = values.at(requestedNoiseHWChannel);
           consoleOutputMessage(QString("Noise measure: %1 uV RMS").arg(val,0,'f',2));
           emit noiseMeasureReturned(val);
    }

    streamManager->enableRMSCalculations(false);

}



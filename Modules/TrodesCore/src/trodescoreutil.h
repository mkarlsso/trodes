#ifndef TRODESCOREUTIL_H
#define TRODESCOREUTIL_H

#include <sys/types.h>
//#include <QtGui>
#include <QObject>
#include <QtCore>
#include "streamProcessorThread.h"
#include <QAudioOutput>
#include <QAudioFormat>
#include "audioThread.h"
#include "triggerThread.h"
#include "recordThread.h"
#include "configuration.h"
#include "trodesSocket.h"
#include "networkMessage.h"
#include "globalObjects.h"
#include "sourceController.h"
#include "commandengine.h"
#include <TrodesCentralServer.h>
#include <AbstractModuleClient.h>
#include <TrodesModule.h>

enum TrodesRecordingIssue{
    LowDiskSpace,
    NoDiskSpace,
    WriteFail
};
Q_DECLARE_METATYPE(TrodesRecordingIssue)

class TrodesAudioInfo
{
public:
    TrodesAudioInfo():
        gain(0),
        threshold(0),
        on(false),
        availableDevices(),
        currentDevice() {}
    int gain;
    int threshold;
    bool on;
    QStringList availableDevices;
    QString currentDevice;
};

class TrodesCoreUtil : public QObject
{
    Q_OBJECT

public:
    TrodesCoreUtil();
    ~TrodesCoreUtil();
    bool                       init(QString nAddress = DEFAULT_SERVER_ADDRESS, int nPort = DEFAULT_SERVER_PORT);
    TrodesConfigurationPointers getCurrentWorkspace();
    DataSource                 getCurrentSource();
    int                        getCurrentSourceState();
    void                       setPlaybackFilename(QString f);
    bool                       isWorkspaceLoaded();
    TrodesRecordingStatus      getRecordingStatus();
    TrodesSourceStatus         getSourceStatus();
    TrodesAudioInfo            getAudioInfo();



private:

    TrodesConfiguration         playbackEmbeddedWorkspace; //workspace embedded in the playback file
    TrodesConfiguration         playbackExternalWorkspace; //replacement workspace for playback file
    TrodesConfiguration         acquisitionWorkspace; //workspace for acquiring data
    TrodesConfigurationPointers activeWorkspacePointers;
    bool                        usingExternalWorkspace;
    QString                     loadedConfigFile;
    QString                     playbackFilename;
    uint32_t                    playbackStartTime, playbackEndTime;
    QString                     recordFileName;
    QString                     lastRecordFileName;
    QString                     queuedRecordFileName;
    QString                     stagedPlaybackFileForLoad;
    int                         currentFilePart;
    QString                     recordFileBaseName;


    HeadstageSettings           headstageSettings;
    HardwareControllerSettings  controllerSettings;

    StreamProcessorManager      *streamManager;
    SourceController            *sourceControl;
    AudioController             *soundOut;
    RecordThread                *recordOut;

    bool                        enableAVXFlag;
    bool                        isAudioAvailable;
    bool                        isAudioOn;
    bool                        hardwareSimulationModeOn;
    int                         sourceState;
    bool                        unresponsiveHeadstageFlag;
    bool                        closeInitiated;
    bool                        quitting;
    bool                        recordFileOpen;
    bool                        dataStreaming;
    bool                        recording;
    bool                        channelsConfigured;
    bool                        closingWorkspace;
    bool                        readyForCommand;
    QString                     networkAddress;
    int                         networkPort;
    bool                        appendTimeToStatus;
    QTimer                      *workspaceShutdownCheckTimer;
    QTimer                      *playbackOpenTimer;
    QTimer                      commandTimoutTimer;
    std::unique_ptr<trodes::TrodesModule> trodesModule;
    int                         selectedNTrodeIndex;
    int                         selectedNTrodeChannelIndex;
    int                         requestedNoiseHWChannel;
    int                         lastAudioGain;



    int                         findEndOfConfigSection(QString configFileName);
    void                        printRecordingStatus();
    void                        printSourceStatus();
    void                        printWorkspaceStatus();
    void                        printAudioStatus();





public slots:

    void                        parseTextControl(QString inputString, bool* ok = nullptr);
    void                        parseTextControl_no_ok(QString inputString);
    void                        printTextControlMenu();
    bool                        openPlaybackFile(const QString fileName);
    bool                        calculateNoise(int nTrode, int channel);
    bool                        openWorkspaceFileForAcquisition(const QString fileName);
    bool                        closeWorkspace();
    bool                        closeRecordFile();
    bool                        openRecordFile(QString filename);
    bool                        startRecording();
    bool                        pauseRecording();
    bool                        pausePlayback();
    bool                        settleAmplifiers();
    bool                        setSource(DataSource source);
    bool                        connectToSource();
    bool                        disconnectFromSource();
    bool                        runSourceDiagnistic(DataSource source);
    bool                        testImpedance(int nt, int ch, int cycles = 200);
    bool                        selectChannel(int nTrode, int channel);
    bool                        setAudioOn(bool on);
    bool                        setAudioGain(int g);
    bool                        setAudioThreshold(int t);
    bool                        setAudioDeviceNumber(int n);

    void                        toggleHardwareSimulationMode(bool on);  
    void                        sendSettleCommand();
    void                        printVersionInfo();
    void                        shutDown();


private slots:
    void                        openPlaybackFile_internal(const QString fileName);
    bool                        openWorkspaceFileForAcquisition_internal(const QString fileName);
    void                        closeWorkspace_internal();
    void                        closeRecordFile_internal();
    bool                        openRecordFile_internal(QString filename);
    void                        startRecording_internal();
    void                        pauseRecording_internal();
    void                        pausePlayback_internal();
    void                        settleAmps_internal();
    bool                        setSource_internal(DataSource source);
    bool                        connectToSource_internal();
    bool                        disconnectFromSource_internal();
    SourceSettings                runSourceDiagnistic_internal(DataSource source);
    bool                        testImpedance_internal(int nt, int ch, int cycles = 200);
    void                        openPlaybackFileLater();
    void                        setPlaybackTimeRange(uint32_t,uint32_t);
    void                        finalizeOpenPlaybackFile();
    bool                        initializeSelectedWorkspace();
    void                        setupStreamManager();
    bool                        startTrodesNetwork(void);
    void                        endTrodesNetwork(void);
    void                        loadConfigIntoNetworkObject(void);
    void                        processAcquisitionCommand(QString cmd, qint32 time);
    void                        sendAquisitionCommand(qint8 cmd, qint32 time);
    void                        playbackAcquisitionStopped();
    void                        broadcastEvent(TrodesEventMessage ev);
    void                        sourceStateChanged(int);
    void                        sourcePacketSizeError(bool on);
    void                        headstageSettingsChanged(HeadstageSettings s);
    void                        controllerSettingsChanged(HardwareControllerSettings s);
    void                        sourceUnresponsiveHeadstage(bool on);
    void                        sourceNoDataError(bool on);
    void                        sourceNoHeadstageError(bool on);
    void                        bufferOverrunHandler();
    void                        errorSaving();
    void                        noSpaceLeftWhileSaving();
    void                        lowDiskSpaceWarning();
    void                        openQueuedRecordFile();
    void                        createNextFilePart();
    void                        initiateWorkspaceClose();
    void                        finishWorkspaceClose();
    void                        errorMessage(QString msg);
    void                        debugMessage(QString msg);
    void                        consoleOutputMessage(QString msg);
    void                        processImpedanceValue(int HWChan, int value);
    void                        processRMSValues(QList<qreal> values);
    void                        startAudioThread();
    void                        startSourceController();
    void                        commandFinished();
    void                        commandTimedOut();
    void                        startCommandTimeout(int timeout);
    void                        audioDeviceChanged(QAudioSink *);



signals:

    //Public signals

    void                        newDebugMessage(QString msg);
    void                        newConsoleOutputMessage(QString msg);
    void                        newErrorMessage(QString msg);
    void                        controlCommandFinished();
    void                        controlCommandFailed();
    void                        workspaceClosed();
    void                        shutDownComplete();
    void                        shutDownIntitiated();
    void                        workspaceOpened();
    void                        setAudioChannel(int hwchannel);
    void                        setAudioDevice(QString device);
    void                        recordFileOpened(QString filename);
    void                        sourceConnected(int);
    void                        sourceStarted();
    void                        sourceStopped();
    void                        sourcePaused();

    void                        recordFileClosed();
    void                        recordingStarted();
    void                        recordingStopped();
    void                        issueWithRecording(TrodesRecordingIssue issue);

    void                        impedanceMeasureReturned(int value);
    void                        noiseMeasureReturned(float value);


    //Private-ish signals
    void                        updateAudio();
    void                        endAllThreads();
    void                        endAudioThread();
    void                        messageForModules(TrodesMessage *tm);
    void                        clearDataAvailable(void); //This signal tells moduleNet to remove all non-trodes data available markers
    void                        sendEvent(uint32_t t, int evTime, QString eventName);
    void                        jumpFileTo(qreal value);
    void                        signal_sendPlaybackCommand(qint8 cmdFlg, qint32 ts);
    void                        sig_sendPlaybackCommand(QString cmd, uint32_t ts);
    void                        impedanceMeasureRequested(HardwareImpedanceMeasureCommand s);

};

#endif // TRODESCOREUTIL_H

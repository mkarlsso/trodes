include(../module_defaults.pri)

# QT       += opengl core gui network xml multimedia widgets openglwidgets

QT       += core xml gui network widgets multimedia opengl openglwidgets

CONFIG   += console
#CONFIG += USING_FTDI_USB3

TEMPLATE = app
DEFINES += TRODES_CODE


TARGET = trodescore

INCLUDEPATH  += ../TrodesCore
INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
INCLUDEPATH  += ../../Trodes/src-network
INCLUDEPATH  += ../../Trodes/src-display
INCLUDEPATH  += ../../Trodes/src-threads
INCLUDEPATH  += ./src
INCLUDEPATH += ../../3rdParty/fftreal
INCLUDEPATH += ../DataLoggerGUI/DockConsoleUtility

INCLUDEPATH  += $$REPO_LIBRARY_DIR
INCLUDEPATH  += $$REPO_LIBRARY_DIR/Utility

# trodesnetwork: includes, libs
include(../../trodesnetwork_defaults.pri)

#########################################
#Source files and libraries
#########################################

##-------------------------
USING_FTDI_USB3 {
    DEFINES += USE_D3XX
    HEADERS   += ../../Trodes/src-threads/usb3thread.h
    SOURCES   += ../../Trodes/src-threads/usb3thread.cpp
} else {
    HEADERS   += ../../Trodes/src-threads/usbdaqThread.h \
                 ../../Trodes/src-threads/dockusbthread.h
    SOURCES   += ../../Trodes/src-threads/usbdaqThread.cpp \
                 ../../Trodes/src-threads/dockusbthread.cpp
}

HEADERS   += ../../Trodes/src-config/configuration.h \
    ../../Trodes/src-main/sharedVariables.h \
    ../../Trodes/src-threads/avxfloattoshort.h \
    ../../Trodes/src-threads/avxutils.h \
    ../../Trodes/src-threads/filtercoefcalculator.h \
    ../../Trodes/src-threads/notchprocessor.h \
    ../../Trodes/src-threads/spikeprocessorthread.h \
    ../../Trodes/src-threads/streamprocesshandlers.h \
    ../../Trodes/src-threads/audioThread.h \
    ../../Trodes/src-main/iirFilter.h \
    ../../Trodes/src-main/globalObjects.h\
    ../../Trodes/src-threads/streamProcessorThread.h \
    ../../Trodes/src-threads/recordThread.h \
    ../../Trodes/src-threads/simulateDataThread.h \
    ../../Trodes/src-main/sourceController.h \
    ../../Trodes/src-threads/fileSourceThread.h \
    #../../Trodes/src-display/spikeDisplay.h \
    ../../Trodes/src-main/networkMessage.h \
    #../../Trodes/src-main/customApplication.h \
    ../../Trodes/src-main/trodesSocket.h \
    ../../Trodes/src-main/trodesSocketDefines.h \
    ../../Trodes/src-threads/ethernetSourceThread.h \
    ../../Trodes/src-main/abstractTrodesSource.h \
    ../../Trodes/src-threads/simulateSpikesThread.h \
    ../../Trodes/src-threads/spikeDetectorThread.h \
    ../../Trodes/src-main/trodesdatastructures.h \
    ../../Trodes/src-main/eventHandler.h \
    ../../Trodes/src-network/TrodesCentralServer.h \
    ../../Trodes/src-network/TrodesModule.h \
    ../../Trodes/src-network/trodesglobaltypes.h \
    ../../Libraries/qtmoduledebug.h \
    ../../Libraries/qtmoduleclient.h \
    ../../Libraries/Utility/concurrentqueue.h \
    ../../Trodes/src-config/hardwaresettings.h \
    ../../Trodes/src-threads/auxchanprocessor.h \
    ../../Trodes/src-threads/displayprocessor.h \
    ../../Trodes/src-threads/referenceprocessor.h \
    ../../Trodes/src-threads/lowpassprocessor.h \
    ../../Trodes/src-threads/bandpassprocessor.h \
    ../../Trodes/src-threads/thresholdprocessor.h \
    ../../Trodes/src-threads/sgringqueue.h \
    ../../Trodes/src-threads/sema.h \
    ../../Trodes/src-threads/benaphore.h \
    #../../Trodes/src-display/workspaceeditordialog.h \
    ../../Trodes/src-main/analyses.h \
    ../../Trodes/src-threads/vectorizedneuraldatahandler.h \
    src/trodescoreconsole.h \
    src/trodescoreutil.h \
    ../DataLoggerGUI/DockConsoleUtility/commandengine.h

SOURCES   += src/main.cpp\
    ../../Trodes/src-threads/avxfloattoshort.cpp \
    #../../Trodes/src-display/probelayoutpanel.cpp \
    #../../Trodes/src-display/rfwidget.cpp \
    ../../Trodes/src-threads/filtercoefcalculator.cpp \
    ../../Trodes/src-threads/notchprocessor.cpp \
    ../../Trodes/src-threads/spikeprocessorthread.cpp \
    ../../Trodes/src-threads/streamprocesshandlers.cpp \
    ../../Trodes/src-threads/audioThread.cpp \
    ../../Trodes/src-main/iirFilter.cpp \
    ../../Trodes/src-config/configuration.cpp\
    ../../Trodes/src-threads/streamProcessorThread.cpp \
    ../../Trodes/src-threads/recordThread.cpp \
    ../../Trodes/src-threads/simulateDataThread.cpp \
    ../../Trodes/src-main/sourceController.cpp \
    ../../Trodes/src-threads/fileSourceThread.cpp \
    #../../Trodes/src-display/spikeDisplay.cpp \
    #../../Trodes/src-main/customApplication.cpp \
    ../../Trodes/src-main/trodesSocket.cpp \
    ../../Trodes/src-threads/ethernetSourceThread.cpp \
    ../../Trodes/src-main/abstractTrodesSource.cpp \
    ../../Trodes/src-threads/simulateSpikesThread.cpp \
    ../../Trodes/src-threads/spikeDetectorThread.cpp \
    ../../Trodes/src-main/trodesdatastructures.cpp \
    ../../Trodes/src-main/eventHandler.cpp \
    ../../Trodes/src-network/TrodesCentralServer.cpp \
    ../../Trodes/src-network/TrodesModule.cpp \
    ../../Libraries/qtmoduledebug.cpp \
    ../../Libraries/qtmoduleclient.cpp \
    ../../Trodes/src-threads/auxchanprocessor.cpp \
    ../../Trodes/src-threads/displayprocessor.cpp \
    ../../Trodes/src-threads/referenceprocessor.cpp \
    ../../Trodes/src-threads/lowpassprocessor.cpp \
    ../../Trodes/src-threads/bandpassprocessor.cpp \
    ../../Trodes/src-threads/thresholdprocessor.cpp \
    ../../Trodes/src-main/analyses.cpp \
    ../../Trodes/src-threads/vectorizedneuraldatahandler.cpp \
    src/trodescoreconsole.cpp \
    src/trodescoreutil.cpp \
    ../DataLoggerGUI/DockConsoleUtility/commandengine.cpp


# FFTReal
HEADERS  += ../../3rdParty/fftreal/Array.h \
            ../../3rdParty/fftreal/Array.hpp \
            ../../3rdParty/fftreal/DynArray.h \
            ../../3rdParty/fftreal/DynArray.hpp \
            ../../3rdParty/fftreal/FFTRealFixLen.h \
            ../../3rdParty/fftreal/FFTRealFixLen.hpp \
            ../../3rdParty/fftreal/FFTRealFixLenParam.h \
            ../../3rdParty/fftreal/FFTRealPassDirect.h \
            ../../3rdParty/fftreal/FFTRealPassDirect.hpp \
            ../../3rdParty/fftreal/FFTRealPassInverse.h \
            ../../3rdParty/fftreal/FFTRealPassInverse.hpp \
            ../../3rdParty/fftreal/FFTRealSelect.h \
            ../../3rdParty/fftreal/FFTRealSelect.hpp \
            ../../3rdParty/fftreal/FFTRealUseTrigo.h \
            ../../3rdParty/fftreal/FFTRealUseTrigo.hpp \
            ../../3rdParty/fftreal/OscSinCos.h \
            ../../3rdParty/fftreal/OscSinCos.hpp \
            ../../3rdParty/fftreal/def.h

# Wrapper used to export the required instantiation of the FFTRealFixLen template
HEADERS  += ../../3rdParty/fftreal/fftreal_wrapper.h
SOURCES  += ../../3rdParty/fftreal/fftreal_wrapper.cpp

#Code for using fftreal for spectral analysis (from Qt)
HEADERS  += ../../3rdParty/fftreal/frequencyspectrum.h \
            ../../3rdParty/fftreal/spectrum.h \
            ../../3rdParty/fftreal/spectrumanalyser.h \
            ../../3rdParty/fftreal/utils.h

SOURCES  += ../../3rdParty/fftreal/frequencyspectrum.cpp \
            ../../3rdParty/fftreal/spectrumanalyser.cpp \
            ../../3rdParty/fftreal/utils.cpp




##OTHER_FILES += \
##    src-main/cocoaInitializer.mm

unix:!macx {
    INCLUDEPATH += ../../Trodes/Libraries/Linux ../../Libraries ../../Libraries/Linux


    USING_FTDI_USB3 {
        LIBS += -LLibraries/Linux -lftd3xx
        HEADERS    += Libraries/Linux/WinTypes.h Libraries/Linux/ftd3xx.h
    } else {
        LIBS += -LLibraries/Linux -lftd2xx
        HEADERS    += Libraries/Linux/WinTypes.h Libraries/Linux/ftd2xx.h
    }

    QMAKE_CXXFLAGS += -mavx2
}

win32{
#    INCLUDEPATH += Libraries/Windows ../Libraries ../Libraries/Windows ../Libraries/Utility
    INCLUDEPATH += ../../Libraries/Windows64/include
    LIBS += -L../../Libraries/Windows64/lib/ -lTrodesNetwork


    INCLUDEPATH += ../../Trodes/Libraries/Windows

    USING_FTDI_USB3 {
        HEADERS     += ../../Trodes/Libraries/Windows/FTD3XX.h
        LIBS        += ../../Trodes/Libraries/Windows/FTDI/amd64/FTD3XX.lib
    } else {
        HEADERS     += ../../Trodes/Libraries/Windows/ftd2xx.h
        LIBS        += ../../Trodes/Libraries/Windows/FTDI/amd64/ftd2xx64.lib
    }

    INCLUDEPATH += $$PWD/lib/Windows
    SOURCES += \
    lib/Windows/getopt.c
    HEADERS += \
    lib/Windows/getopt.h

    #RC_ICONS += trodesIcon.ico
    # LIBS += -lOpengl32
}

#stuff specific for mac
macx {

    #QMAKE_INFO_PLIST += src-main/Info.plist


    INCLUDEPATH += ../../Trodes/Libraries/Mac
    INCLUDEPATH += ../../Libraries/MacOS/include
    LIBS += -L../../Libraries/MacOS/lib -lTrodesNetwork

    USING_FTDI_USB3 {
        HEADERS    += ../../Trodes/Libraries/Mac/ftd3xx.h
        LIBS       += $$quote($$PWD/../../Trodes/Libraries/Mac/libftd3xx-static.a)
    } else {
        HEADERS    += ../../Trodes/Libraries/Mac/WinTypes.h \
                    ../../Trodes/Libraries/Mac/ftd2xx.h
        LIBS       += $$quote($$PWD/../../Trodes/Libraries/Mac/libftd2xx.a)
    }

    QMAKE_CXXFLAGS += -mavx -mavx2




}

#########################################
#INSTALLATION: LINUX
#########################################
unix:!macx{
    #FTDXX library
    #libraries.path is set in build_defaults.pri
    libraries.files += $$PWD/../../Trodes/Libraries/Linux/libftd2xx.so*
    libraries.files += $$PWD/../../Trodes/Libraries/Linux/libftd3xx.so*
    QtDeploy.commands += mkdir -p $$INSTALL_DIR/Libraries/;
}

#########################################
#INSTALLATION: WINDOWS
#########################################
win32:{
#libraries.path is set in build_defaults.pri
#    win32-g++ { # MinGW
#        libraries.files += $$PWD/Libraries/Windows/FTDI/i386/ftd2xx.dll
#    }
#    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # 32bit
            libraries.files += $$PWD/../../Trodes/Libraries/Windows/FTDI/i386/ftd2xx.dll
        } else { # 64bit
            libraries.files += $$PWD/../../Trodes/Libraries/Windows/FTDI/amd64/ftd2xx.dll
            libraries.files += $$PWD/../../Trodes/Libraries/Windows/FTDI/amd64/FTD3XX.dll
        }
#    }

    ftdxxsetup.path = $$INSTALL_DIR/Resources/SetupHelp/Windows/
    ftdxxsetup.files += $$TRODES_REPO_DIR/Resources/SetupHelp/Windows/CDM21228_Setup.exe
    INSTALLS += ftdxxsetup
}

#########################################
#INSTALLATION: ALL OPERATING SYSTEMS
#########################################

INSTALLS += libraries




#########################################
#Install
#########################################

macx {



    QMAKE_POST_LINK += "cp $$TRODES_REPO_DIR/Libraries/MacOS/lib/libTrodesNetwork.*.dylib $$DESTDIR/$${TARGET}.app/Contents/MacOS/"
}

unix:!macx{


    QtDeploy.commands += cp -a $$TRODES_REPO_DIR/Libraries/Linux/TrodesNetwork/lib/libTrodesNetwork.so* $$INSTALL_DIR/Libraries/

}



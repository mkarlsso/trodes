#include <QApplication>
#include "mainWindow.h"

int main(int argc, char *argv[])
{
    qSetMessagePattern("[WorkspaceGUI] %{message}");
    QApplication a(argc, argv);
    MainWindow w(a.arguments());

    w.show();

    return a.exec();
}

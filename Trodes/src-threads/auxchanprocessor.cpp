#include "auxchanprocessor.h"
#include <iostream>
#include <algorithm>
AuxChanProcessor::AuxChanProcessor(bool ECU) : ECUConnected(ECU), stream_inc(0)
{

}

void AuxChanProcessor::addInt16Channel(int startByte, std::string idstring)
{
    int16chan chan;
    chan.startByte = startByte;
    chan.pos = values.size();
    analogChans.push_back(chan);
    values.push_back(0);
    minValues.push_back(0);
    maxValues.push_back(0);
    stateChanges.push_back(0);
}

void AuxChanProcessor::addDigitalChannel(int startByte, int digitalBit, std::string idstring)
{
    digitalchan chan;
    chan.startByte = startByte;
    chan.digitalBit = digitalBit;
    chan.pos = values.size();
    digitalChans.push_back(chan);
    values.push_back(0);
    minValues.push_back(0);
    maxValues.push_back(0);
    stateChanges.push_back(0);
}

void AuxChanProcessor::addInterleavedInt16Channel(int IL_IDByte, int IL_IDBit, int startByte, std::string idstring)
{
    int16chan_IL chan;
    chan.startByte = startByte;
    chan.interleavedDataIDByte = IL_IDByte;
    chan.interleavedDataIDBit = IL_IDBit;
    chan.dataState = 0;
    chan.pos = values.size();
    interleavedAnalogChans.push_back(chan);
    values.push_back(0);
    minValues.push_back(0);
    maxValues.push_back(0);
    stateChanges.push_back(0);
}

void AuxChanProcessor::addInterleavedDigitalChannel(int IL_IDByte, int IL_IDBit, int startByte, int digitalBit, std::string idstring)
{
    digitalchan_IL chan;
    chan.startByte = startByte;
    chan.digitalBit = digitalBit;
    chan.interleavedDataIDByte = IL_IDByte;
    chan.interleavedDataIDBit = IL_IDBit;
    chan.dataState = 0;
    chan.pos = values.size();
    interleavedDigitalChans.push_back(chan);
    values.push_back(0);
    minValues.push_back(0);
    maxValues.push_back(0);
    stateChanges.push_back(0);
}

void AuxChanProcessor::setStreamBinSize(int size){
    stream_inc_max = size-1;
}

void AuxChanProcessor::newSamples(int16_t *data)
{
    uint8_t *dataptr = (uint8_t*)data;

//    //Set bits where MCU IO changes
//    uint8_t *mcuioptr = (uint8_t*)(dataptr+1);
//    MCUChanges = *mcuioptr  ^ prevMCU_IO;
//    prevMCU_IO = *mcuioptr;

//    if(ECUConnected){
//        //Set bits where ECU DIO changes
//        uint64_t *ecudioptr = (uint64_t*)(dataptr+2);
//        ECUChanges = *ecudioptr ^ prevECU_DIO;
//        prevECU_DIO = *ecudioptr;
//    }

    int16_t tmpDataPoint = 0;
    for(auto const &chan : analogChans){
        uint8_t *startBytePtr = (uint8_t*)data + chan.startByte;
        tmpDataPoint = *((int16_t*)(startBytePtr));
        //do something with tmpDataPoint...
        values[chan.pos] = tmpDataPoint;

    }

    for(auto const &chan : digitalChans){
         uint8_t *startBytePtr = (uint8_t*)data + chan.startByte;
         tmpDataPoint = (int16_t)((*startBytePtr & (1 << chan.digitalBit)) >> chan.digitalBit);

         //check for state changes before updating
         if(chan.pos == 18){
//             std::cout << tmpDataPoint << " " << values[chan.pos] << "\n";
         }
         if(tmpDataPoint != values[chan.pos]){
             //if 0, store as 1. if 1, store as 2.
             stateChanges[chan.pos] = (uint8_t)((*startBytePtr & (1 << chan.digitalBit)) >> chan.digitalBit) + 1;
         }
         else{
             stateChanges[chan.pos] = 0;
         }

         values[chan.pos] = tmpDataPoint;
    }

    for(auto &chan : interleavedAnalogChans){
        uint8_t *startBytePtr = (uint8_t*)data + chan.startByte;
        uint8_t *interleavedDataIDBytePtr = (uint8_t*)data + chan.interleavedDataIDByte;
        if (*interleavedDataIDBytePtr & (1 << chan.interleavedDataIDBit)) {
            //update the channel
            tmpDataPoint = *((int16_t*)(startBytePtr));
            chan.dataState = tmpDataPoint;
        } else {
            //Use the last data point
            tmpDataPoint = chan.dataState;
        }

        values[chan.pos] = tmpDataPoint;
    }

    for(auto &chan : interleavedDigitalChans){
        uint8_t *startBytePtr = (uint8_t*)data + chan.startByte;
        uint8_t *interleavedDataIDBytePtr = (uint8_t*)data + chan.interleavedDataIDByte;
        if (*interleavedDataIDBytePtr & (1 << chan.interleavedDataIDBit)) {
            tmpDataPoint = (int16_t)((*startBytePtr & (1 << chan.digitalBit)) >> chan.digitalBit);
            //check for state changes before updating
            if(tmpDataPoint != chan.dataState){
                //if 0, store as 1. if 1, store as 2.
                stateChanges[chan.pos] = (uint8_t)((*startBytePtr & (1 << chan.digitalBit)) >> chan.digitalBit) + 1;
            }
            else{
                stateChanges[chan.pos] = 0;
            }

            //update the channel
            chan.dataState = tmpDataPoint;
        } else {
            //Use the last data point
            tmpDataPoint = chan.dataState;
        }

        values[chan.pos] = tmpDataPoint;
    }

    if(stream_inc){
        for(int i = 0; i < values.size(); ++i){
            minValues[i] = std::min((float)values[i], minValues[i]);
            maxValues[i] = std::max((float)values[i], maxValues[i]);
        }
    }
    else{
        for(int i = 0; i < values.size(); ++i){
            minValues[i] = (float)values[i];
            maxValues[i] = (float)values[i];
        }
    }
//    std::cout << minValues[18] << " " << maxValues[18] << "\n";

    //advance bin increment by 1, looping to 0 if passes max
    if(++stream_inc >= stream_inc_max){
        stream_inc = 0;
    }
}

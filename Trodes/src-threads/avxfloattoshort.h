#ifndef AVXFLOATTOSHORT_H
#define AVXFLOATTOSHORT_H

#include <stdint.h>

class alignas(32) AVXFloatToShort
{
public:
//    AVXFloatToShort();

    static void convertToShort(const float *data, int16_t *output);


//    const int16_t* getConvertedOutput() const {return output;}

private:
//    int16_t output[16];
};

#endif // AVXFLOATTOSHORT_H

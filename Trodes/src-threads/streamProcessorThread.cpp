/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "streamProcessorThread.h"
#include "globalObjects.h"
#include "time.h"
#include "vectorizedneuraldatahandler.h"


#include "TrodesNetworkUtil.h"
#include <TrodesNetwork/Generated/TrodesLFPData.h>
#include <TrodesNetwork/Util.h> // for get_timestamp()
// #include "czmq.h" // for zclock_time()
//#define USEZMQLFPSYNC 1



StreamProcessorManager::StreamProcessorManager(QWidget *parent, TrodesConfigurationPointers c_ptrs, bool enableAVX) :
    QObject(parent),
    neuralDataMinMax(nullptr),
    auxDataMinMax(nullptr),
    displaySpikeTicks(false),
    conf_ptrs(c_ptrs)
{

    qRegisterMetaType<QVector<SpikeWaveform> >("QVector<SpikeWaveform>");
}

void StreamProcessorManager::setUp()
{

    neuralDataMinMax = new vertex2d[conf_ptrs.hardwareConf->NCHAN*EEG_TIME_POINTS*2];


    gotSourceFailSignal = false;
    gotHeadstageFailSignal = false;
    PSTHAuxTriggerChannel = -1;
    PSTHAuxTriggerState = true;

    int streamChannels = 0; // the number of channels in the current streamProcessor

    //TODO: have update* funtions provide an ntrode, and have updateChannels be update*, taking an ntrode
    //That should make updating things faster on 1024 channels

    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedRef, this, &StreamProcessorManager::updateChannelsRef);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedNotchFilter, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedFilter, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedFilterOn, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedFilter, this, &StreamProcessorManager::updateFilters);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedModuleDataFilter, this, &StreamProcessorManager::updateFilters);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedModuleData, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedLFPFilter, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedLFPMode, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedSpikeMode, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedAllModes, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedThresh, this, &StreamProcessorManager::updateChannels);
    connect(conf_ptrs.spikeConf, &SpikeConfiguration::updatedTriggerMode, this, &StreamProcessorManager::updateChannels);


    //enableAVX = true; //hard code AVX enabling
    bool enableAVX = conf_ptrs.hardwareConf->useIntrinsics; //hard code AVX enabling
    avxsupported = false;
    avxsupported = enableAVX && detectAVXCapabilities();
    if (avxsupported) {
        //qDebug() << "AVX2 support detected. Using AVX2 processing.";
        emit newDebugMessage("AVX2 support detected. Using AVX2 processing.");
        int numberOfThreadsNeeded = conf_ptrs.hardwareConf->NCHAN/NUMCHANNELSPERAVXPROCESSOR;
        if (numberOfThreadsNeeded > conf_ptrs.globalConf->processorCount) {
            //qDebug() << "******   WARNING!!! YOU DO NOT HAVE ENOUGH PROCESSORS ON THIS COMPUTER TO HANDLE THIS NUMBER OF CHANNELS.   ******";
            //qDebug() << numberOfThreadsNeeded << "processors needed.";
            emit newDebugMessage("******   WARNING!!! YOU DO NOT HAVE ENOUGH PROCESSORS ON THIS COMPUTER TO HANDLE THIS NUMBER OF CHANNELS.   ******");
            emit newDebugMessage(QString("%1 processors needed.").arg(numberOfThreadsNeeded));
        }

    } else {
        emit newDebugMessage("No AVX2 support detected or is turned off in the workspace. Using basic processing.");
    }

    emit newDebugMessage(QString("Number of processors detected: %1").arg(conf_ptrs.globalConf->processorCount)); //We may use this to decide how many channels to put per thread.
    emit newDebugMessage(QString("Expected incoming packet header size: %1").arg(conf_ptrs.hardwareConf->headerSize*2));
    emit newDebugMessage(QString("Expected incoming packet size: %1").arg((conf_ptrs.hardwareConf->headerSize*2)+(conf_ptrs.hardwareConf->NCHAN*2)+4));

    int NUMCHANSPERTHREAD = avxsupported ? NUMCHANNELSPERAVXPROCESSOR : NUMCHANNELSPERSTREAMPROCESSOR;
    nTrodeToProcessorThread.fill(-1, conf_ptrs.spikeConf->ntrodes.length());

    createSpikeProcessorThread(); //Creates the thread responsible for processing spikes

    for (int n = 0; n < conf_ptrs.spikeConf->ntrodes.length(); n++) {
        spikeDetectors.append(nullptr); // initialize with NULL        
    }

    QList<int> nTrodeList;
//    spikeTickBins.resize(spikeConf->ntrodes.length());
    spikeTickBins = new bool*[conf_ptrs.spikeConf->ntrodes.length()];
    for (int trodeCount = 0; trodeCount < conf_ptrs.spikeConf->ntrodes.length(); trodeCount++) {

        //Create lists to store spike times for all nTrodes
//        spikeTickBins[trodeCount].fill(false, EEG_TIME_POINTS);
        spikeTickBins[trodeCount] = new bool[EEG_TIME_POINTS];

        /* The goal of the code below is to create threads with NUMCHANSPERTHREAD channels or fewer each.
         * This is only problematic if a single nTrodes has more than NUMCHANSPERTHREAD channels, in which case it will
         * have it's own thread */

        if ((streamChannels + conf_ptrs.spikeConf->ntrodes[trodeCount]->maxDisp.length()) > NUMCHANSPERTHREAD) {
            if (conf_ptrs.spikeConf->ntrodes[trodeCount]->maxDisp.length() > NUMCHANSPERTHREAD){
                // issue a warning
                emit newDebugMessage(QString("[StreamProcessorManager] WARNING: nTrode index %1 has more than %2 channels. Creating dedicated thread").arg(trodeCount).arg(NUMCHANSPERTHREAD));
            }
            if (streamChannels != 0) {
                // We have a set of channels from a previous iteration of the loop, so we create a thread for them and then move on
                createNewProcessorThread(nTrodeList);
                emit newDebugMessage(QString("[StreamProcessorManager] Starting new streamProcessorThread for %1 nTrodes").arg(nTrodeList.length()));
                if (!avxsupported) createNewSpikeManagerThread(nTrodeList);
                nTrodeList.clear();
                streamChannels = 0;
            }
        }
        // add the current nTrode to the list
        nTrodeList.push_back(trodeCount);
        streamChannels += conf_ptrs.spikeConf->ntrodes[trodeCount]->maxDisp.length();
    }
    //Create a processor for the last group
    int numHeaderChannels = conf_ptrs.headerConf->headerChannels.length(); //total number of header channels (aux. analog and digital channels)

    if ((nTrodeList.length() > 0) && (numHeaderChannels == 0)) {
        createNewProcessorThread(nTrodeList);
        //qDebug() << "[StreamProcessorManager] Starting new streamProcessorThread for nTrodes" << nTrodeList;
        emit newDebugMessage(QString("[StreamProcessorManager] Starting new streamProcessorThread for %1 nTrodes").arg(nTrodeList.length()));
    } else if (numHeaderChannels > 0) {
        //qDebug() << "[StreamProcessorManager] Starting new streamProcessorThread for just auxilliary channels";
        emit newDebugMessage(QString("[StreamProcessorManager] Starting new streamProcessorThread for %1 nTrodes and aux channels.").arg(nTrodeList.length()));
        QList<int> headerChanList;
//        auxDataMinMax.resize(numHeaderChannels);
//        auxDataMinMax = new vertex2d*[numHeaderChannels]; // some might be dead
//        for (int i=0; i < numHeaderChannels; i++)
//            auxDataMinMax[i] = new vertex2d[EEG_TIME_POINTS * 2]; // one for min, one for max
        auxDataMinMax = new vertex2d[numHeaderChannels*EEG_TIME_POINTS*2];

        for (int headerChan = 0; headerChan < numHeaderChannels; headerChan++) {
            headerChanList.append(headerChan);
            DigitalStateChangeInfo dsci;
            dioStateChanges.append(dsci);
        }
        createNewProcessorThread(nTrodeList,headerChanList);
    }

    if ((!avxsupported)&&(nTrodeList.length() > 0)) {
        createNewSpikeManagerThread(nTrodeList);
    }


    if (conf_ptrs.networkConf->networkType == NetworkConfiguration::zmq_based) {
        //ZMQ-based network
        zmqContext = zmq::context_t(0); // 0 IO threads is ok since using this only for inproc transport

        //----------------------
        //lfp queue. It is used by the
        //zmq-based network to send out LFP data to other modules.
        sglfpqueue = new SGRingQueue<int16_t, uint32_t, 1024>(conf_ptrs.spikeConf->ntrodes.length(), streamProcessors.length());

        //Create tokens for each producer (streamprocessor).
        //Each stream processor accesses their token using their "groupnum", which is their index in streamProcessors
        for(int i = 0; i < streamProcessors.length(); ++i){
            sgproducertoks.push_back(sglfpqueue->registerProducer(streamProcessors[i]->nTrodeList.length()));
        }

        if ((conf_ptrs.spikeConf->ntrodes.length() > 0) && (conf_ptrs.spikeConf->ntrodes[0]->hw_chan.length() > 0)) {
            //Create lfpworker object, and pass it to a QThread. It is used by the
            //zmq-based network to send out LFP data to other modules.
            //Ideally, move this to start using std threads.
            LFPAggregator *lfpworker = new LFPAggregator(this,conf_ptrs);

            lfpthread = new QThread;
            lfpworker->moveToThread(lfpthread);
            connect(this, &StreamProcessorManager::startAllProcessorLoops, lfpworker, &LFPAggregator::runloop);
            connect(this, &StreamProcessorManager::stopLFPAggregator, lfpworker, &LFPAggregator::endloop);
            connect(lfpthread, &QThread::started, lfpworker, &LFPAggregator::initialize);
            connect(lfpworker, &LFPAggregator::provideDataType, this, &StreamProcessorManager::newContinuousData);
            connect(lfpthread, &QThread::finished, lfpworker, &LFPAggregator::deleteLater);
        }

        //----------------------------
    }

    sumSquares = new int64_t[conf_ptrs.hardwareConf->NCHAN];
    sumSquaresN = new int[conf_ptrs.hardwareConf->NCHAN];
    sumSquaresCopy = new int64_t[conf_ptrs.hardwareConf->NCHAN];
    sumSquaresNCopy = new int[conf_ptrs.hardwareConf->NCHAN];
    for(int i = 0; i < conf_ptrs.hardwareConf->NCHAN; ++i){
        sumSquares[i] = 0;
        sumSquaresN[i] = 0;
    }
    connect(&RMSTimer, &QTimer::timeout, this, &StreamProcessorManager::calculateRMS);

}

StreamProcessorManager::~StreamProcessorManager()
{
//    for (int i=0; i < hardwareConf->NCHAN; i++)
//        delete [] neuralDataMinMax[i];
//    delete [] neuralDataMinMax;
    delete [] neuralDataMinMax;

    if (conf_ptrs.headerConf->headerChannels.length() > 0) {
//        for (int i=0; i < headerConf->headerChannels.length(); i++)
//            delete [] auxDataMinMax[i];
//        delete [] auxDataMinMax;
        delete [] auxDataMinMax;
    }

    delete [] sumSquares;
    delete [] sumSquaresN;
    delete [] sumSquaresCopy;
    delete [] sumSquaresNCopy;

}

void StreamProcessorManager::startSpikePub()
{
    //emit spikePubRequested();
    spikeProcessor->startSpikePub();
}

void StreamProcessorManager::startLFPPubThread()
{
    if ((conf_ptrs.spikeConf->ntrodes.length() > 0) && (conf_ptrs.spikeConf->ntrodes[0]->hw_chan.length() > 0)) {
        lfpthread->start();
    }
}
void StreamProcessorManager::sourceFail(bool fail)
{
    if (fail && !gotSourceFailSignal) {
        gotSourceFailSignal = true;
        emit sourceFail_Sig(true);
    } else if (!fail) {
        gotSourceFailSignal = false;
        emit sourceFail_Sig(false);
    }
}

void StreamProcessorManager::headstageFail(bool fail)
{
    if (fail && !gotHeadstageFailSignal) {
        gotHeadstageFailSignal = true;
        emit headstageFail_Sig(true);
    } else if (!fail) {
        gotHeadstageFailSignal = false;
        emit headstageFail_Sig(false);
    }
}

void StreamProcessorManager::digitalStateChanged(int headerChannelInd, quint32 t, bool state)
{


    dioStateChanges[headerChannelInd].timeStamps.push_back(t);
    dioStateChanges[headerChannelInd].states.push_back(state);

    if (dioStateChanges[headerChannelInd].timeStamps.length() > NUMDIGSTATECHANGESTOKEEP) {
        dioStateChanges[headerChannelInd].timeStamps.pop_front();
        dioStateChanges[headerChannelInd].states.pop_front();
    }
}

void StreamProcessorManager::calculateRMS()
{
    QList<qreal> values;
    memcpy(sumSquaresCopy, sumSquares, sizeof(int64_t)*conf_ptrs.hardwareConf->NCHAN);
    memcpy(sumSquaresNCopy, sumSquaresN, sizeof(int)*conf_ptrs.hardwareConf->NCHAN);
    memset(sumSquares, 0, sizeof(int64_t)*conf_ptrs.hardwareConf->NCHAN);
    memset(sumSquaresN, 0, sizeof(int)*conf_ptrs.hardwareConf->NCHAN);

    for(int i = 0; i < conf_ptrs.hardwareConf->NCHAN; ++i){
        if(sumSquaresNCopy[i]){
            int nt = conf_ptrs.streamConf->trodeIndexLookupByHWChan.at(i);
            values.append( sqrt((qreal)sumSquaresCopy[i] / (qreal)sumSquaresNCopy[i]) * conf_ptrs.spikeConf->ntrodes[nt]->spike_scaling_to_uV);
            //values.append( sqrt((qreal)sumSquaresCopy[i] / (qreal)sumSquaresNCopy[i]) * (qreal)AD_CONVERSION_FACTOR/65535.0); //assumes Intan-based source. TODO: Needs to be generalized
        }
        else{
            values.append(0);
        }
    }
    emit sendRMSValues(values);
}

void StreamProcessorManager::clearAllDigitalStateChanges() {
    for (int i=0; i<dioStateChanges.length(); i++) {
        dioStateChanges[i].states.clear();
        dioStateChanges[i].timeStamps.clear();
    }
}

void StreamProcessorManager::processNewSpikes(QVector<SpikeWaveform> spikes) {
    //This function should only use the time and ntrode info of the spikes. Accessing the waveform data
    //itself will lead to a race condition, since that info is being deleted in another thread.

    //For each spike, we foreward time and ntrode index to display spike ticks above the neural traces
    for (int spikeIndex = 0; spikeIndex < spikes.length(); spikeIndex++) {
        //qDebug() << "Spike on ntrode" << spikes[spikeIndex].ntrodeIndex << "Time" << spikes[spikeIndex].peakTime;
        receiveSpikeEvent(spikes[spikeIndex].ntrodeIndex,spikes[spikeIndex].peakTime);
    }
}

void StreamProcessorManager::receiveSpikeEvent(int nTrode, uint32_t time) {

    if (displaySpikeTicks) {
        //A spike occured-- keep track of which ntrode and which time bin in the continuous display the spike occured.
        int binsInPast = (currentTimeStamp-time)/streamProcessors[0]->raw_increment[0];
        int targetBin = streamProcessors[0]->dataIdx - binsInPast;
        if (targetBin < 0) {
            targetBin = EEG_TIME_POINTS+targetBin;
        }

        //If a spike occured, we change the bin to true. This will get reset to false by the streamProcessor threads when the display buffer loops.
        if ((targetBin >= 0) && (targetBin < EEG_TIME_POINTS)) {
            spikeTickBins[nTrode][targetBin] = true;
        }
    }

}

void StreamProcessorManager::setDisplaySpikeTicks(bool on)
{

    displaySpikeTicks = on;
}

void StreamProcessorManager::updateGlobalStimSettings(GlobalStimulationSettings s)
{
    //New global stim settings were sent to hardware.
    emit newDebugMessage(QString("[StreamProcessorManager] New Global Stimulation Settings set."));
    emit relayNewGlobalStimulationSettings(s);
}

void StreamProcessorManager::createSpikeProcessorThread()
{
    //Start the thread dedicated to processing new spikes

    spikeProcThread = new QThread;
    spikeProcThread->setObjectName("SpikeProcessor");
    spikeProcessor = new SpikeProcessorThread(nullptr,conf_ptrs);
    spikeProcessor->moveToThread(spikeProcThread);
    connect(spikeProcessor, &SpikeProcessorThread::newProcessedSpikes, this, &StreamProcessorManager::processNewSpikes,Qt::DirectConnection);
    connect(this, SIGNAL(startAllProcessorLoops()), spikeProcessor, SLOT(runLoop()),Qt::QueuedConnection);
    connect(spikeProcThread, SIGNAL(started()), spikeProcessor, SLOT(setUp()));
    connect(this, &StreamProcessorManager::spikePubRequested,spikeProcessor,&SpikeProcessorThread::startSpikePub);
    spikeProcThread->start(QThread::TimeCriticalPriority);
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nTrodeList)
{
    //Creates a new thread and streamProcessor object
    int groupNumber = streamProcessors.length();
    emit newDebugMessage(QString("Thread %1: %2 nTrodes").arg(groupNumber).arg(nTrodeList.length()));
    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("StreamProcessor");
    //processorThreads.last()->setPriority(QThread::TimeCriticalPriority);
    streamProcessors.push_back(new StreamProcessor(nullptr, groupNumber, nTrodeList, this, conf_ptrs));
    connect(streamProcessors.last(), SIGNAL(sourceFail(bool)),this,SLOT(sourceFail(bool)));
    connect(streamProcessors.last(), SIGNAL(noHeadstageFail(bool)),this,SLOT(headstageFail(bool)));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(streamProcessors.last(), SIGNAL(digitalStateChanged(int,quint32,bool)), this, SLOT(digitalStateChanged(int,quint32,bool)));
    connect(streamProcessors.last(),SIGNAL(functionTriggerRequest(int)), this, SIGNAL(functionTriggerRequest(int)));
    connect(streamProcessors.last(), &StreamProcessor::newSpikes, this, &StreamProcessorManager::processNewSpikes);
    connect(streamProcessors.last(), &StreamProcessor::newDebugMessage, this, &StreamProcessorManager::newDebugMessage);
    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));
    connect(this,&StreamProcessorManager::relayNewGlobalStimulationSettings,streamProcessors.last(),&StreamProcessor::newGlobalStimulationSettings);
    //connect(streamProcessors.last(), SIGNAL(), processorThreads.last(), SLOT(quit()), Qt::DirectConnection);
    processorThreads.last()->start(QThread::TimeCriticalPriority);

    for(int nt : nTrodeList){
        nTrodeToProcessorThread[nt] = groupNumber;
    }
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nTrodeList, QList<int> auxChanList)
{
    //Creates a new thread and streamProcessor object
    int groupNumber = streamProcessors.length();
    //qDebug() << "Thread" << groupNumber << ":" << nTrodeList;
    emit newDebugMessage(QString("Thread %1: %2 nTrodes").arg(groupNumber).arg(nTrodeList.length()));
    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("StreamProcessor");
    //processorThreads.last()->setPriority(QThread::TimeCriticalPriority);
    streamProcessors.push_back(new StreamProcessor(nullptr, groupNumber, nTrodeList, this, conf_ptrs));
    streamProcessors.last()->addAuxChannels(auxChanList);
    /*if (networkConf->networkType == NetworkConfiguration::qsocket_based) {
        connect(streamProcessors.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    }*/
    connect(streamProcessors.last(), SIGNAL(sourceFail(bool)),this,SLOT(sourceFail(bool)));
    connect(streamProcessors.last(), SIGNAL(noHeadstageFail(bool)),this,SLOT(headstageFail(bool)));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(streamProcessors.last(), SIGNAL(digitalStateChanged(int,quint32,bool)), this, SLOT(digitalStateChanged(int,quint32,bool)));
    connect(streamProcessors.last(),SIGNAL(functionTriggerRequest(int)), this, SIGNAL(functionTriggerRequest(int)));
    connect(streamProcessors.last(), &StreamProcessor::newSpikes, this, &StreamProcessorManager::processNewSpikes);
    connect(streamProcessors.last(), &StreamProcessor::newDebugMessage, this, &StreamProcessorManager::newDebugMessage);
    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));
    //connect(this, SIGNAL(startAllProcessorLoops()), spikeProcessor, SLOT(runLoop()),Qt::QueuedConnection);
    connect(this,&StreamProcessorManager::relayNewGlobalStimulationSettings,streamProcessors.last(),&StreamProcessor::newGlobalStimulationSettings);
    //connect(streamProcessors.last(), SIGNAL(), processorThreads.last(), SLOT(quit()), Qt::DirectConnection);
    processorThreads.last()->start(QThread::HighPriority);

    for(int nt : nTrodeList){
        nTrodeToProcessorThread[nt] = groupNumber;
    }
}

void StreamProcessorManager::createNewSpikeManagerThread(QList<int> nTrodeList)
{
    spikeDetectorThreads.push_back(new QThread);
    spikeDetectorThreads.last()->setObjectName("SpikeDetector");
    spikeDetectorManagers.push_back(new SpikeDetectorManager(nullptr, nTrodeList, &spikeDetectors, conf_ptrs)); // spikeDetectors is going to be populated
    /*if (networkConf->networkType == NetworkConfiguration::qsocket_based) {
        connect(spikeDetectorManagers.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    }*/
    spikeDetectorManagers.last()->moveToThread(spikeDetectorThreads.last());
    connect(spikeDetectorThreads.last(), SIGNAL(started()), spikeDetectorManagers.last(), SLOT(setupAndRun()));
    connect(conf_ptrs.spikeConf, SIGNAL(newThreshold(int, int)), spikeDetectorManagers.last(), SLOT(updateSpikeThreshold(int, int)));

    //connect(spikeConf, SIGNAL(newThreshold(int, int, int)), spikeDetectorManagers.last(), SLOT(updateSpikeThreshold(int, int, int)));
    connect(conf_ptrs.spikeConf, SIGNAL(newTriggerMode(int, int, bool)), spikeDetectorManagers.last(), SLOT(updateSpikeMode(int, int, bool)));
    spikeDetectorThreads.last()->start();
}


void StreamProcessorManager::startAcquisition()
{
    //qDebug() << "Got stream start sig";
    gotSourceFailSignal = false;
    emit startAllProcessorLoops(); //sends signal to all processor threads to start their loops

}

void StreamProcessorManager::stopAcquisition()
{

    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->quitNow = 1;
    }
    spikeProcessor->quitNow = 1;
    emit stopLFPAggregator();
}

void StreamProcessorManager::removeAllProcessors()
{
    bool printDebug = false;  //set to true if you are debugging a crash or problem

    if (printDebug) qDebug() << "Remove all processors begin";
    stopAcquisition();
    for (int i = 0; i < streamProcessors.length(); i++) {
        if (printDebug) qDebug() << "Removing processor" << i << "out of" << streamProcessors.length();
        processorThreads[i]->quit();
        processorThreads[i]->deleteLater();
        streamProcessors[i]->deleteLater();
    }
    if (printDebug) qDebug() << "Remove all processors: streamProcessors deleted";
    for (int i = 0; i < spikeDetectorManagers.length(); i++) {
        spikeDetectorManagers[i]->stopRunning();
    }

    spikeProcessor->shutdown();
    spikeProcThread->quit();
    spikeProcThread->deleteLater();
    spikeProcessor->deleteLater();

    if (printDebug) qDebug() << "Remove all processors: spike Manager stopped";

    //This is a bit of a hack, and will cause a crash upon shutdown if any of the StreamProcessors have
    //not finished running.  Ideally, the StreamProcessor threads would send a signal when they are done that
    //is used to delete the spike detection threads, but because there are multiple StreamProcessors and multiple
    //spikeDetectorThreads, this is complicated to set up.  So, for now, we sleep for long enough to ensure that
    //the StreamProcessors have all shut down.
    QThread::msleep(100);
    for (int i = 0; i < spikeDetectorThreads.length(); i++) {
        spikeDetectorThreads[i]->quit();
        spikeDetectorThreads[i]->deleteLater();
        spikeDetectorManagers[i]->deleteLater();

    }
    if (printDebug) qDebug() << "Remove all processors: spike detection threads deleted";

    if (conf_ptrs.networkConf->networkType == NetworkConfiguration::zmq_based) {
        if ((conf_ptrs.spikeConf->ntrodes.length() > 0) && (conf_ptrs.spikeConf->ntrodes[0]->hw_chan.length() > 0)) {
            lfpthread->quit();
            lfpthread->deleteLater();
        }
        if (printDebug) qDebug() << "Remove all processors: lfp thread deleted";
    }



}

QVector<uint32_t> StreamProcessorManager::getDigitalEventTimes() {

    QVector<uint32_t> outputTimes;
    if (PSTHAuxTriggerChannel < dioStateChanges.length() && PSTHAuxTriggerChannel > -1) {
        for (int i=0; i < dioStateChanges[PSTHAuxTriggerChannel].timeStamps.length(); i++) {
            if (dioStateChanges[PSTHAuxTriggerChannel].states[i] == PSTHAuxTriggerState) {
                outputTimes.push_back(dioStateChanges[PSTHAuxTriggerChannel].timeStamps[i]);
            }
        }
    }
    return outputTimes;

}

QString StreamProcessorManager::getPSTHTriggerID()
{
    if (conf_ptrs.headerConf != nullptr && PSTHAuxTriggerChannel > -1) {
        return conf_ptrs.headerConf->headerChannels[PSTHAuxTriggerChannel].idString;

    } else {
        return "";
    }
}

bool StreamProcessorManager::getPSTHTriggerState() {
    return PSTHAuxTriggerState;
}

void StreamProcessorManager::enableRMSCalculations(bool on)
{
    RMSEnabled = on;
    if(on){
        RMSTimer.start(1000);
    }
    else{
        RMSTimer.stop();
    }
}

void StreamProcessorManager::setOneSecBin(){
    if(RMSTimer.interval()==1000){
        return;
    }
    memset(sumSquares, 0, sizeof(int64_t)*conf_ptrs.hardwareConf->NCHAN);
    memset(sumSquaresN, 0, sizeof(int)*conf_ptrs.hardwareConf->NCHAN);
    RMSTimer.setInterval(1000);
}
void StreamProcessorManager::setTenSecBin(){
    if(RMSTimer.interval()==10000){
        return;
    }
    memset(sumSquares, 0, sizeof(int64_t)*conf_ptrs.hardwareConf->NCHAN);
    memset(sumSquaresN, 0, sizeof(int)*conf_ptrs.hardwareConf->NCHAN);
    RMSTimer.setInterval(10000);
}

void StreamProcessorManager::updateDataLength(double tlength)
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->newDataLength = conf_ptrs.hardwareConf->sourceSamplingRate * tlength;
        //When this flag is set to true, the thread's loop will update the trace length and reset the flag to false
        streamProcessors[i]->updateDataLengthFlag = true;
    }
}

void StreamProcessorManager::updateFilters(QList<int> ntrodes){
    for(int i = 0; i < ntrodes.length(); ++i){
        int nt = ntrodes[i];
        streamProcessors[nTrodeToProcessorThread[nt]]->updateChannelsFlag = 3;
    }
}

void StreamProcessorManager::updateChannels()
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->updateChannelsFlag = 1;
    }
}

void StreamProcessorManager::updateChannelsRef(){

    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->updateChannelsFlag = 2;
    }
}

void StreamProcessorManager::createSpikeLogs(QString dataDir)
{
//  for (int i = 0; i < nTrodeTriggerProcessors.length(); i++) {
//    nTrodeTriggerProcessors[i]->createLogFile(dataDir);
//  }
}

void StreamProcessorManager::setPSTHTrigger(int headerChannel, bool state)
{
    emit newDebugMessage(QString("[StreamProcessorManager] PSTH trigger set"));
    PSTHAuxTriggerChannel = headerChannel;
    PSTHAuxTriggerState = state;
}

//This object is used to pull out raw data for a set of channels, filter the signals, and
//calculate what should be displayed on the stream display. It runs inside a seperate thread.

StreamProcessor::StreamProcessor(QObject*, int groupNumber, QList <int> nTList, StreamProcessorManager* managerPtr, TrodesConfigurationPointers c_ptrs) :
    conf_ptrs(c_ptrs),
    groupNum(groupNumber),
    nTrodeList(nTList),
    lfpDecimation(conf_ptrs.hardwareConf->lfpSubsamplingInterval),
    streamManager(managerPtr)
{


    dataLength = conf_ptrs.hardwareConf->sourceSamplingRate * conf_ptrs.streamConf->tLength;
    isLooping = false;
    nAuxChan = 0;
    hasDigIO = false;
    hasAuxAnalog = false;
    isSetup = false;

    nChan = 0;
    for (int trode = 0; trode < nTList.length(); trode++) {
        nChan = nChan + conf_ptrs.spikeConf->ntrodes[trode]->hw_chan.length();
        nTrodeIdList.push_back(conf_ptrs.spikeConf->ntrodes[nTrodeList[trode]]->nTrodeId);
    }
}

StreamProcessor::~StreamProcessor()
{

    delete neuralDataHandler;

}

void StreamProcessor::addAuxChannels(QList<int> auxList) {
    //Can only add aux channels before setUp() has been executed
    if (!isSetup) {
        for (int i=0; i<auxList.length(); i++) {
            auxChannelList.append(auxList[i]);
        }
        //auxChannelList = auxList;
        nAuxChan = auxList.length(); // For header group, this is just the length of header channels

        for (int i=0; i < nAuxChan; i++) {
            if (conf_ptrs.headerConf->headerChannels[i].dataType == DeviceChannel::DIGITALTYPE) {
                hasDigIO = true;
            } else if (conf_ptrs.headerConf->headerChannels[i].dataType == DeviceChannel::INT16TYPE) {
                hasAuxAnalog = true;
            } else if (conf_ptrs.headerConf->headerChannels[i].dataType == DeviceChannel::UINT32TYPE) {
                //Nothing here yet.
            }

        }
    }
}

int StreamProcessor::getLfpDecimation() const
{
    return lfpDecimation;
}

void StreamProcessor::setLfpDecimation(int value)
{
    lfpDecimation = value;
}

void StreamProcessor::setUp() {
    //dataFilters = new BesselFilter[nChan];
    if(streamManager->avxsupported){
        uint32_t extraSteps = 0;

        if ((conf_ptrs.spikeConf->deviceType == "neuropixels1")||(conf_ptrs.spikeConf->deviceType == "neuropixelsNRIC")) {
            extraSteps |= AbstractNeuralDataHandler::ExtraProcessorType::NeuroPixels1;
        }

        neuralDataHandler = new VectorizedNeuralDataHandler(conf_ptrs, nTrodeList,extraSteps);
        neuralDataHandler->setSpikeInvert(true);

        spikeSourceIndex = -1;
        while (spikeSourceIndex == -1) {
            spikeSourceIndex = streamManager->spikeProcessor->spikeBuffer.addSourceBuffer(); //dedicated buffer index for this thread. Returns -1 if another thread is currently getting it's assigned value.

        }
    }
    else {
        neuralDataHandler = new NeuralDataHandler(conf_ptrs,nTrodeList);
        neuralDataHandler->setSpikeInvert(true);
    }

    neuralDataHandler->setDataLength(dataLength, EEG_TIME_POINTS);

    //Calculate how many data points go into one display pixel on the stream plot
    raw_increment.resize(EEG_TIME_POINTS);
    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);
//        qDebug() << raw_increment[j] << "=" <<
    }

    //Create a new semaphore for this group of channels.
    //if this is not the first time a workspace has been loaded, then there will already be semaphores in place.
    //Only create more if they are needed.
    rawDataAvailableMutex.lock();
    while (groupNum >= rawDataAvailable.length()) {
        rawDataAvailable.append(new QSemaphore);
    }
    rawDataAvailableMutex.unlock();

    nTrodeToLocal.fill(-1, conf_ptrs.spikeConf->ntrodes.length());

    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        int nt = nTrodeList.at(trode);
        notchFiltersOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->notchFilterOn);
        spikeFiltersOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->filterOn);
        lfpFiltersOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpFilterOn);
        lfpRefOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpRefOn);
        refOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->refOn);
        rawRefOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->rawRefOn);
        moduleDataChan.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpDataChan);
        refChan.push_back(conf_ptrs.spikeConf->ntrodes.refOfIndex(nt)->hw_chan[conf_ptrs.spikeConf->ntrodes[nt]->refChan]);
        stimCapableRef.push_back(conf_ptrs.spikeConf->ntrodes.refOfIndex(nt)->stimCapable[conf_ptrs.spikeConf->ntrodes[nt]->refChan]);
        spikeModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode);
        lfpModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode);
        spikeModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode);
        lfpModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode);
        triggered.push_back(false);
        nTrodeToLocal[nt] = trode;
    }
    ntrodesUpdated.reserve(nTrodeList.length());

    //dataPoints = new int16_t*[nTrodeList.length()];
    rawDataAvailableIdx = groupNum;
    for (int nt = 0; nt < nTrodeList.length(); nt++) {
        //Get channel-specific info from the loaded configuration file


        /*if (conf_ptrs.networkConf->networkType == NetworkConfiguration::qsocket_based) {
            // if we haven't added it already, add this to the data available index list
            if (dataProvided.contNTrodeIndexList.indexOf(nTrodeList.at(nt)) == -1) {
                dataProvided.contNTrodeIndexList.push_back(nTrodeList.at(nt));
            }
        }*/

        //dataMinMax is what keeps the currently plotted data (for streaming page)
        //For each horizontal pixel, it keeps the maximum and minimum values that occured
        //in the data stream for that channel during that time window. Minimum and maximum
        //values alternate (which is why the array is twice as long as EEE_TIME_POINTS).

        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan.length(); c++) {
            int ch = conf_ptrs.spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan[c];
//            streamManager->neuralDataMinMax[ch].resize((int)EEG_TIME_POINTS * 2);
            for (int j = 0; j < EEG_TIME_POINTS; j++) {
                int ind = ch*EEG_TIME_POINTS*2 + 2*j;
                streamManager->neuralDataMinMax[ind].x = (double)j;
                streamManager->neuralDataMinMax[ind].y = 0;
                streamManager->neuralDataMinMax[ind+1].x = (double)j;
                streamManager->neuralDataMinMax[ind+1].y = 0;
            }
        }

        //dataPoints[nt] = new int16_t[conf_ptrs.spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan.length()];
    }

    for (int auxCh = 0; auxCh < auxChannelList.length(); auxCh++) {
        //This is not for neural channels (digital inputs, aux analog inputs,...)
        int hdrChan = auxChannelList.at(auxCh);
        if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::INT16TYPE) &&
                    (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
            interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
        } else if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::UINT32TYPE) &&
                   (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
           interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
       }
//        streamManager->auxDataMinMax[hdrChan].resize((int)EEG_TIME_POINTS * 2);
        for (int j = 0; j < EEG_TIME_POINTS; j++) {
            int ind = hdrChan*EEG_TIME_POINTS*2;
            streamManager->auxDataMinMax[ind + j*2].x = (double)j;
            streamManager->auxDataMinMax[ind + j*2].y = 0;
            streamManager->auxDataMinMax[ind + j*2+1].x = (double)j;
            streamManager->auxDataMinMax[ind + j*2+1].y = 0;
        }
    }

    rawIdx = 0;
    dataIdx = 0;

    quitNow = 0;
    updateChannelsFlag = 0;
    updateDataLengthFlag = false;

    // if this handles digital IO data, allocate space for a variable that will hold the current digital IO state
    if (hasDigIO || hasAuxAnalog) {

        digStates.resize(conf_ptrs.headerConf->headerChannels.length());
        // fill the array with false to initialize
        digStates.fill(false);

        /*
        digInState = new bool[headerConf->maxDigitalPort(true)];
        digOutState = new bool[headerConf->maxDigitalPort(false)];

        // fill the arrays with zeros to initialize
        memset(digInState, 0, headerConf->maxDigitalPort(true) * sizeof(bool));
        memset(digOutState, 0, headerConf->maxDigitalPort(false) * sizeof(bool));*/
    }

    //hw_chan list is not going to change during streaming, so during setup,
    //create a local copy of the list.
    for (int n = 0; n < nTrodeList.length(); n++) {
        int nt = nTrodeList[n];
        nTrodeHWChans.append(conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.toVector());
    }

    connect(conf_ptrs.spikeConf, SIGNAL(updatedModuleData()), this, SLOT(updateModuleDataChan()));

    isSetup = true;
}


void StreamProcessor::updateDataLength(void)
{
    // Note that this is safe only because we've chosen to have a constant number of points for
    // the time base of the display. We would need to be more careful if raw_increment was going
    // to change size, in order to not have thread clashes....

    dataLength = newDataLength;
    raw_increment.resize(dataLength);

    neuralDataHandler->setDataLength(dataLength, EEG_TIME_POINTS);

    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);

        //There is really no need to update the xaxis on the traces, so for now we skip this step
        //This means that the xlabels are fixed to whatever values they had in the constructor,
        //and as long as we do not update them in the stream display widget, everything should work fine.
    }
    dataIdx = 0;

    emit newDebugMessage(QString("[StreamProcessor] New dataLength set"));

}



void StreamProcessor::updateChannels(void)
{

    //Edit: Now keeping track of all relevant values in **local copies** instead of accessing globals.

//    qDebug() << "[StreamProcessor] Updating channels";
    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        const int nt = nTrodeList[trode];
        //notchFiltersOn[trode] = spikeConf->ntrodes[nt]->notchFilterOn;
        //spikeFiltersOn[trode] = spikeConf->ntrodes[nt]->filterOn;
        //lfpFiltersOn[trode] = spikeConf->ntrodes[nt]->lfpFilterOn;
//        spikeModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode;
//        lfpModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode;

    }

    neuralDataHandler->updateChannels();

}

void StreamProcessor::updateChannelsRef(){
    /*for (int trode = 0; trode < nTrodeList.length(); trode++) {
        const int nt = nTrodeList[trode];
        lfpRefOn[trode] = spikeConf->ntrodes[nt]->lfpRefOn;
        refOn[trode] = spikeConf->ntrodes[nt]->refOn;
        rawRefOn[trode] = spikeConf->ntrodes[nt]->rawRefOn;
        refChan[trode] = spikeConf->ntrodes.refOfIndex(nt)->hw_chan[spikeConf->ntrodes[nt]->refChan];
        groupRefOn[trode] = spikeConf->ntrodes[nt]->groupRefOn;
        refGroup[trode] = spikeConf->ntrodes[nt]->refGroup;
        //qDebug() << "Reference for nTrode" << trode << "is" << refChan[trode] << spikeConf->ntrodes[nt]->hw_chan[0] << lfpRefOn[trode] << refOn[trode] << rawRefOn[trode] << groupRefOn[trode];
    }*/
//    qDebug() << "[StreamProcessor] Updating channels ref";
    neuralDataHandler->updateChannelsRef();
}

void StreamProcessor::updateFilters(){
    /*for(int i = 0; i < nTrodeList.length(); ++i){
        const int nt = nTrodeList[i];
        for(int c = 0; c < spikeConf->ntrodes[nt]->hw_chan.length(); ++c){
            spikeFilters[i][c]->setFilterRange(spikeConf->ntrodes[nt]->lowFilter, spikeConf->ntrodes[nt]->highFilter);
            notchFilters[i][c]->setBandwidth(spikeConf->ntrodes[nt]->notchBW);
            notchFilters[i][c]->setNotchFreq(spikeConf->ntrodes[nt]->notchFreq);
        }
        lfpFilters[i]->setFilterRange(0, spikeConf->ntrodes[nt]->moduleDataHighFilter);
        moduleDataChan[i] = spikeConf->ntrodes[nt]->moduleDataChan;
    }*/
//    qDebug() << "[StreamProcessor] Updating filters";
    neuralDataHandler->updateFilters();
}

void StreamProcessor::setupdateFiltersFlag(){
    updateChannelsFlag = 3;
}


void StreamProcessor::updateModuleDataChan()
{

}


void StreamProcessor::newGlobalStimulationSettings(GlobalStimulationSettings s)
{
    neuralDataHandler->globalStimSettings = s;

    //qDebug() << "New current scaling:" << neuralDataHandler->globalStimSettings.currentScaling();
}



void StreamProcessor::runLoop()
{
//    long            ns; // Milliseconds
//    struct timespec spec;
    //This is where the raw data is filtered, and where the streaming display is calculated.
    //In each display bin, we draw a vertical line connecting the minumim and maximum collected values
    //in that time bin.
    //When the 'stream from source' menu is chosen, this function is executed via an emitted signal
    //from the source controller

    int samplesToCopy = 0;

    quitNow = 0;
    isLooping = true;
    bool exitLoop = false;

    int16_t tmpDataPoint = 0;
    int16_t tmpMaxPoint = 0;
    int16_t tmpMinPoint = 0;
    int rdInd;

    bool newDigIOState;
    int port;
    char* startBytePtr; //for processing header channels
    uint8_t* interleavedDataIDBytePtr; //for processing header channels that are interleaved

    rawIdx = 0;
    dataIdx = 0;
    streamDataRead = 0;

    numLoopsWithNoData = 0;
    numLoopsWithNoHeadstageData = 0;

    int headerSize = conf_ptrs.hardwareConf->headerSize;
//    if (hardwareConf->sysTimeIncluded) {
//        headerSize = hardwareConf->headerSize-4;  //Remove the 8 bytes (4 16-bit values) defined in the config for sys clock
//    } else {
//        headerSize = hardwareConf->headerSize;
//    }
    //Setting this to true enabled a simple closed-loop latency test.  If used, select one (and only one) of the digital
    //output channels for analysis.  Then, in stateScript, set the channel to high.  When the high state is dected, this processor will call function 1.
    //Function 1 should 1) Set the channel to low, and 2) set it back to 1 after about 100ms (the test interval). Streamprocessor will measure the amount of time it
    //takes from the high edge to the low edge for each test.

    bool closedLoopLatencyTest = false;


    uint32_t latencyReceiveEventTime;
    uint32_t latencyReturnEventTime;


    int hw_chan = 0;
    int stream_inc = 0;

    uint32_t timestamp;

    bool avxsupported = streamManager->avxsupported;


    QVector<int16_t> lfpDataBlock(nTrodeList.length());
    lfp_msg_t lfpMsg;
    lfpMsg.pnum = groupNum; // don't need to change ever again
    lfpMsg.ts = 0;

//    std::vector<int16_t> lfpPacketBlock(nTrodeList.length(), 0); //Used for bulk enqueueing of lfp queue
//    int pb = 0;
//    if(groupNum == 0){
//        //Incredibly horrible way of also storing the timestamps somewhere without creating an entirely new queue
//        //for just uint32_ts.
//        lfpPacketBlock.push_back(0);
//        lfpPacketBlock.push_back(0);
//        pb = 2;
//    }
    bool decimatedtimestamp = false;

    for (int auxCh = 0; auxCh < auxChannelList.length(); auxCh++) {
        //Reset any interleaved aux channels to 0
        int hdrChan = auxChannelList.at(auxCh);
        if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::INT16TYPE) &&
                    (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
            interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
        } else if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::UINT32TYPE) &&
                   (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
           interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
       }
    }
    bool nodatafail = false;  //flag for no data error
    bool noheadstagefail = false;

    QVector<int> displayBinLookup;
    for (int ch=0; ch<conf_ptrs.hardwareConf->NCHAN;ch++) {
        displayBinLookup.push_back(ch*EEG_TIME_POINTS*2);
    }
    int allowedLoopsWithNoData = 5;
    if (conf_ptrs.hardwareConf->NCHAN == 0 && conf_ptrs.hardwareConf->sourceSamplingRate < 10000) {
        allowedLoopsWithNoData = 30;
    }
    //int processEventsCounter = 0;
    emit newDebugMessage(QString("[StreamProcessor] Starting stream processor"));

#ifdef USEZMQLFPSYNC
    zmq::socket_t socket(streamManager->zmqContext, zmq::socket_type::req);
    if (conf_ptrs.networkConf->networkType == NetworkConfiguration::zmq_based) {
        socket.connect("inproc://lfp");
    }
#endif

    QThread::usleep(200); //This appears to solve a race condition that sometimes occurs.  TODO: a real solution


    while (!exitLoop) {

        //QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents|QEventLoop::ExcludeSocketNotifiers);
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
        //QCoreApplication::processEvents();
        // checking event loop after processing buffer
        /*if (processEventsCounter > 100) {
            QCoreApplication::processEvents();
            processEventsCounter = 0;
        }*/
        //processEventsCounter++;
        if (rawDataAvailable[rawDataAvailableIdx]->tryAcquire(1, 100)) {

            samplesToCopy = rawDataAvailable[rawDataAvailableIdx]->available();
            numLoopsWithNoData = 0;
            if (nodatafail) {
                nodatafail = false;
                emit sourceFail(false); //reset source failure signal
            }
            if (samplesToCopy > 15000) {
                emit bufferOverrun();
            }
            if (!rawDataAvailable[rawDataAvailableIdx]->tryAcquire(samplesToCopy))
                emit newDebugMessage(QString("[StreamProcessor] Error acquiring available samples, group %1").arg(groupNum));


            samplesToCopy += 1; // we acquired one at the beginning


            /*if (conf_ptrs.networkConf->networkType == NetworkConfiguration::qsocket_based) {
                //Create a list of all data handlers that are on
                dataHandlersOn.clear();
                for (int d = 0; d < contDataHandlers.length(); d++) {
                    if (contDataHandlers[d]->isModuleDataStreamingOn()) {
                        dataHandlersOn.push_back(d);
                    }
                }
            }*/


            for (int s = 0; s < samplesToCopy && !quitNow; s++) {
                // -----------------------------------------------------------------
                // LOAD NEW DATA STRUCTURE



                rdInd = rawIdx * conf_ptrs.hardwareConf->NCHAN;

                timestamp = rawData.timestamps[rawIdx];
                int sysTimestamp = -1;
                if (conf_ptrs.benchConfig->isRecordingSysTime()) {
                    sysTimestamp = rawData.sysTimestamps[rawIdx];
                }



                if ((conf_ptrs.spikeConf->deviceType == "neuropixels1")||(conf_ptrs.spikeConf->deviceType == "neuropixelsNRIC")) {
                    //NP1.0 LFP data rate is fixed
                    if (neuralDataHandler->LFPSampleAvailable()) {
                        decimatedtimestamp = true;
                    } else {
                        decimatedtimestamp = false;
                    }
                } else  {
                    //Intan etc
                    decimatedtimestamp = ((timestamp % lfpDecimation) == 0);
                }

                numLoopsWithNoHeadstageData++; //assume headstage data is bad/fake


//                neuralDataHandler2->processNextSamples(timestamp,rawData.data+rdInd,rawData.carvals+(rawIdx*conf_ptrs.spikeConf->carGroups.length()));
                //neuralDataHandler->processNextSamples(timestamp,rawData.data+rdInd,rawData.carvals+(rawIdx*conf_ptrs.spikeConf->carGroups.length()));
                neuralDataHandler->processNextSamples(timestamp,rawData.data+rdInd,rawData.carvals+(rawIdx*conf_ptrs.spikeConf->carGroups.length()),(char*)(rawData.digitalInfo + (rawIdx * headerSize)));

                neuralDataHandler->processDisplaySamples(stream_inc==0);



                if (!noheadstagefail && neuralDataHandler->numLoopsWithNoHeadstageData > 1000) {
                    noheadstagefail = true;
                    //qDebug() << "No data!";
                    emit noHeadstageFail(true);
                } else if (noheadstagefail && neuralDataHandler->numLoopsWithNoHeadstageData == 0) {
                    noheadstagefail = false;
                    emit noHeadstageFail(false);
                }


                int displayCycle = dataIdx*2;

                for (int n = 0; n < nTrodeList.length(); n++) {
                    // the nTrodeList may not be identical to all the nTrodes in the case were there are multple streamProcessorThreads
                    int nt = nTrodeList.at(n);
                    int modDataChan = moduleDataChan[n];
                    int refChanInd = refChan[n];
//                    int16_t tmpRefVal = rawData.data[rdInd + refChanInd];

//                    if (stimCapableRef[n] && (tmpRefVal & 0x0001)) {
//                        //If the reference is stimulating, make the ref 0
//                        tmpRefVal = 0;
//                    }
                    for (int c = 0; c < nTrodeHWChans[n].length(); c++) {
                        //Loop through all the channels in the ntrode
                        hw_chan = nTrodeHWChans[n][c];

                        //sum of squares calculation
                        //int noiseVal = (int)neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::RAW_UNREFERENCED)[c];
                        int noiseVal = (int)neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::SPIKE)[c];
                        streamManager->sumSquares[hw_chan] += noiseVal*noiseVal;
                        streamManager->sumSquaresN[hw_chan]++;


                        // Fetch the current max and min values in the current display bin
                        //int curIndex = hw_chan*EEG_TIME_POINTS*2 + dataIdx*2;

                        int curIndex = displayBinLookup[hw_chan]+displayCycle;
                        streamManager->neuralDataMinMax[curIndex].y = neuralDataHandler->getMaxDisplaySamples(n)[c];
                        streamManager->neuralDataMinMax[curIndex+1].y = neuralDataHandler->getMinDisplaySamples(n)[c];

                    }

                    //set lfp data for each ntrode
                    if(decimatedtimestamp){
                        lfpDataBlock[n] = neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::LFP)[modDataChan];
                    }

                    if(!avxsupported){
                        //Raw data sent to spike detectors if AVX processing is not supported. Moving data to these
                        //separate threads is not good and really messes with performance.
                        streamManager->spikeDetectors[nt]->newData(neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::SPIKE), timestamp, sysTimestamp);
                    }

                    if (stream_inc == 0) {
                        streamManager->spikeTickBins[nt][dataIdx] = false; //Reset the spike tick bin to 0
                    }
                }


                //Newer ZMQ-based network code is used to send out LFP data
                if (conf_ptrs.networkConf->networkType == NetworkConfiguration::zmq_based) {
                    if(decimatedtimestamp){

//                        qDebug() << "Processor" << groupNum << "sending timestamp" << timestamp;
                        lfpMsg.ts = timestamp;
                        if(groupNum == 0){
                            //                        *(uint32_t*)lfpPacketBlock.data() = timestamp;
                            streamManager->sglfpqueue->enqueue(
                                streamManager->sgproducertoks[groupNum],
                                timestamp, lfpDataBlock.data(), lfpDataBlock.size());
                        }
                        else {
                            streamManager->sglfpqueue->enqueue(
                                streamManager->sgproducertoks[groupNum],
                                lfpDataBlock.data(), lfpDataBlock.size());
                        }


#ifdef USEZMQLFPSYNC
                        //This code causes serious issues. Commented out. MK
                        std::string packedData = trodes::network::util::pack<lfp_msg_t>(lfpMsg);
                        socket.send(
                            zmq::message_t(
                                packedData.data(),
                                packedData.size()),
                            zmq::send_flags::none);

                        zmq::message_t rep;
                        auto rv = socket.recv(rep);
#endif

                    }
                }



                //Now we process the auxilliary channels (digial I/O, analog I/O), if any
                for (int h = 0; h < auxChannelList.length(); h++) {
                    int hch = auxChannelList[h];
                    startBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * headerSize))) + conf_ptrs.headerConf->headerChannels[h].startByte;

                    if (conf_ptrs.headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        if (conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                            interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * headerSize))) + conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte;

                            if (*interleavedDataIDBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.

                                tmpDataPoint = (int16_t)((*startBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].digitalBit)) >>
                                                 conf_ptrs.headerConf->headerChannels[hch].digitalBit);
                                interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString] = tmpDataPoint;
                            } else {
                                //Use the last data point received
                                tmpDataPoint = interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString];
                            }
                        } else {
                            tmpDataPoint = (int16_t)((*startBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].digitalBit)) >>
                                             conf_ptrs.headerConf->headerChannels[hch].digitalBit);
                        }

                    }
                    else if (conf_ptrs.headerConf->headerChannels[hch].dataType == DeviceChannel::INT16TYPE) {
                        // TO DO: add analogIO output
                        if (conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                            interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * headerSize))) + conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte;

                            if (*interleavedDataIDBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                                tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                                interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString] = tmpDataPoint;
                            } else {
                                //Use the last data point received
                                tmpDataPoint = interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString];
                            }
                        } else {

                            tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                        }
                    }


                    if (conf_ptrs.headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        newDigIOState = false;

                        port = conf_ptrs.headerConf->headerChannels[hch].port;

                        // check if this is an input, and if so, if the state of the port has changed
                        if ((bool)tmpDataPoint != digStates[hch]) {
//                            newDigIOState = true;
                            digStates[hch] = (bool)tmpDataPoint;

                            //Send the change event info to the StreamProcessorManager, which
                            //keeps a record of all the events.
                            //We should have a gate for this to allow users to exclude
                            //channels that are changing often

                            if (conf_ptrs.headerConf->headerChannels[hch].storeStateChanges) {

                                //For latency testing
                                if (closedLoopLatencyTest) {
                                    if (digStates[hch]){
                                        emit functionTriggerRequest(1);
                                        latencyReceiveEventTime = timestamp;
                                    } else {
                                        latencyReturnEventTime = timestamp;
                                        qDebug() << "Closed loop latency: " << latencyReturnEventTime-latencyReceiveEventTime;
                                    }
                                }

                                emit digitalStateChanged(hch,timestamp,digStates[hch]);
                            }
                        }
                    }

                    //If the signals are huge, clip them in the display.  Use the max display setting for this channel as the clip.
                    if (tmpDataPoint > conf_ptrs.headerConf->headerChannels[hch].maxDisp) {
                        tmpDataPoint = conf_ptrs.headerConf->headerChannels[hch].maxDisp;
                    } else if (tmpDataPoint < conf_ptrs.headerConf->headerChannels[hch].maxDisp*-1) {
                        tmpDataPoint = conf_ptrs.headerConf->headerChannels[hch].maxDisp*-1;
                    }


                    //tmpMaxPoint = qMin(tmpDataPoint, 2);
                    //tmpMinPoint = qMax(tmpDataPoint, -1);

                    //Calulate the current max and min values in the current display bin
                    int curIndex = hch*EEG_TIME_POINTS*2 + dataIdx * 2;

                    if (stream_inc > 0) {
                        //Old bin, so compare to existing value
                        streamManager->auxDataMinMax[curIndex].y =
                                qMax((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[curIndex].y);
                        streamManager->auxDataMinMax[curIndex + 1].y =
                                qMin((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[curIndex + 1].y);
                    }
                    else {
                        //New bin, so reset the value
                        streamManager->auxDataMinMax[curIndex].y = tmpDataPoint;
                        streamManager->auxDataMinMax[curIndex + 1].y = tmpDataPoint;
                    }
                }


                rawIdx = (rawIdx + 1) % EEG_BUFFER_SIZE;
                if (++stream_inc >= raw_increment[dataIdx]) {
                    dataIdx = (dataIdx + 1) % EEG_TIME_POINTS;
                    stream_inc = 0;
                }



            }

            //With the AVX2-based processor, we check if there are available spikes after we have processed all new data
            if(avxsupported) {
                QVector<SpikeWaveform> newSpks;
                neuralDataHandler->getAvailableWaveforms(newSpks);
                if (!newSpks.isEmpty()) {
                    streamManager->spikeProcessor->spikeBuffer.addSpikes(newSpks,spikeSourceIndex);
                    //emit newSpikes(newSpks);
                }
            }

        } else if (quitNow == 1) {
            exitLoop = true;
            isLooping = false;
        } else {

            numLoopsWithNoData++;

            if (!nodatafail && numLoopsWithNoData > allowedLoopsWithNoData) {
                numLoopsWithNoData = 0;
                nodatafail = true;
                //qDebug() << "No data!";
                emit sourceFail(true);
            }
        }

        if (updateChannelsFlag.testAndSetAcquire(1, 0)) {
            updateChannels();
        }
        if (updateChannelsFlag.testAndSetAcquire(2, 0)){
            updateChannelsRef();
        }
        if (updateChannelsFlag.testAndSetAcquire(3, 0)){
            updateFilters();
        }
        if (updateDataLengthFlag.testAndSetAcquire(true, false)) {
            updateDataLength();
        }

        /*if (conf_ptrs.networkConf->networkType == NetworkConfiguration::qsocket_based) {
            // check to see if data are available on any of the dataHandlers
            for (int i = 0; i < contDataHandlers.length(); i++) {
                if (((contDataHandlers[i]->getSocketType() == TRODESSOCKETTYPE_TCPIP) &&
                     (contDataHandlers[i]->tcpSocket->waitForReadyRead(0))) ||
                        ((contDataHandlers[i]->getSocketType() == TRODESSOCKETTYPE_UDP) &&
                         (contDataHandlers[i]->udpSocket->hasPendingDatagrams()))) {
                    //qDebug() << "reading message on dataHandler" << i;
                    contDataHandlers[i]->readMessage();
                }
            }


            if (digitalIOHandler != NULL) {
                if (((digitalIOHandler->getSocketType() == TRODESSOCKETTYPE_TCPIP) &&
                     (digitalIOHandler->tcpSocket->waitForReadyRead(0))) ||
                        ((digitalIOHandler->getSocketType() == TRODESSOCKETTYPE_UDP) &&
                         (digitalIOHandler->udpSocket->hasPendingDatagrams()))) {
                    //qDebug() << "reading message on digitalIOHandler";
                    digitalIOHandler->readMessage();
                }
            }
            // TODO: Analog data
        }*/
    }
    emit newDebugMessage(QString("[StreamProcessor] Stream processor loop ended."));
}


LFPAggregator::LFPAggregator(StreamProcessorManager* sm, TrodesConfigurationPointers c)
    : quit(false),
      streamManager(sm),
//      consumertok(streamManager->lfpqueue),
      expecting(streamManager->streamProcessors.size(), true),
      conf_ptrs(c)
{
//    ;
    for(int i = 0, pos = 0; i < streamManager->streamProcessors.size(); i++){
        producer_ntrodes.push_back(streamManager->streamProcessors[i]->nTrodeList.size());
//        if(i == 0)
//            producer_ntrodes.back() += 2;
        buffer_start_pos.push_back(pos);
        pos += producer_ntrodes.back();
    }
}

void LFPAggregator::initialize(){
    /*std::string address = trodes::get_network_address(conf_ptrs.networkConf->trodesHost);
    int port = trodes::get_network_port(conf_ptrs.networkConf->trodesPort);

    lfpTrodesPub = std::move(
                std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesLFPData>>(
                    new trodes::network::SourcePublisher<trodes::network::TrodesLFPData>(
                        address, port, "source.lfp")
                    )
                );
*/

   /* qDebug() << "[LFPAggregator::initialize] Set up publisher \"source.lfp\" to publish at address"
             << QString::fromStdString(address) << "port" << port;*/
}

LFPAggregator::~LFPAggregator(){
    quit = true;
}

void LFPAggregator::endloop(){
    quit = true;
}

void LFPAggregator::runloop(){

    quit = false;

    //qDebug() << "Starting LFP publisher...";
    std::string address = trodes::get_network_address(conf_ptrs.networkConf->trodesHost);
    int port = trodes::get_network_port(conf_ptrs.networkConf->trodesPort);

    lfpTrodesPub = std::move(
                std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesLFPData>>(
                    new trodes::network::SourcePublisher<trodes::network::TrodesLFPData>(
                        address, port, "source.lfp")
                    )
                );


#ifdef USEZMQLFPSYNC
    zmq::socket_t socket(streamManager->zmqContext, zmq::socket_type::rep);
    socket.bind("inproc://lfp");
    zmq::pollitem_t items[1] = {
        {static_cast<void*>(socket), 0, ZMQ_POLLIN, 0}};
#endif

    // ok because reconfiguration is not allowed while this loop is active.
    // therefore the member variables we're accessing will not change
    int numProcessors = streamManager->streamProcessors.length();
    int numLfpChannels = 0;
    for (int i=0; i<numProcessors; ++i) {
        numLfpChannels += streamManager->streamProcessors[i]->nTrodeList.length();
    }

    std::vector<uint32_t> lfpts(LFP_BUFFER_SIZE);
    std::vector<int> counts(LFP_BUFFER_SIZE);
    std::fill(counts.begin(), counts.end(), 0);
    std::fill(lfpts.begin(), lfpts.end(), -1);

    int currind = 0;
    int maxind = -1;
    uint32_t sendTimestamp = 0;
    std::vector<int16_t> sendData(conf_ptrs.spikeConf->ntrodes.length());

    lfp_msg_t lfpMsg;

    while(!quit){
        QCoreApplication::processEvents(); // do we really need this?
        bool ready = false;

 #ifdef USEZMQLFPSYNC
        zmq::poll(&items[0], 1, 1);

        if (items[0].revents & ZMQ_POLLIN) {

            zmq::message_t workerMessage;
            auto rv = socket.recv(workerMessage);

            int res = 1;
            zmq::message_t response(&res, sizeof(int));
            socket.send(response, zmq::send_flags::none);

            lfpMsg = trodes::network::util::unpack<lfp_msg_t>(
                    workerMessage.to_string());

            int pnum = lfpMsg.pnum; // really only used for debugging
            uint32_t ts = lfpMsg.ts;

            bool tsAlreadyReceived = false;
            if (maxind < currind) { // looped around

                bool foundInFirstSegment = false;

                for (int i=0; i<=maxind; ++i) {
                    if (lfpts[i] == ts) {
                        foundInFirstSegment = true;
                        tsAlreadyReceived = true;
                        ++counts[i];
//                        qDebug() << pnum << ":" << ts << " " << " already found in first segment\n";
                        break;
                    }
                }

                // not found at beginning of circular buffer, but might be at the end
                if (!foundInFirstSegment) {
                    for (size_t i=currind; i<lfpts.size(); ++i) {
                        if (lfpts[i] == ts) {
                            tsAlreadyReceived = true;
                            ++counts[i];
//                            qDebug() << pnum << ":" << ts << " " << " already found in second segment\n";
                            break;
                        }
                    }
                }

            } else {

                for (int i=currind; i<=maxind; ++i) {
                    if (lfpts[i] == ts) {
                        tsAlreadyReceived = true;
                        ++counts[i];
//                        qDebug() << pnum << ":" << ts << " " << " already found\n";
                        break;
                    }
                }
            }

            if (!tsAlreadyReceived) { // new timestamp
                maxind = (maxind + 1) % LFP_BUFFER_SIZE;
                lfpts[maxind] = ts;
                ++counts[maxind];
            }

            ready = (counts[currind] == numProcessors);
        }
#endif
        QThread::usleep(100); //Not clear how long this should be
        ready = true;
        while (ready) {
            bool popped = streamManager->sglfpqueue->dequeue(&sendTimestamp, &sendData[0]);

            if (popped) {
                int64_t msg_timestamp = trodes::network::util::get_timestamp();
                trodes::network::TrodesLFPData lfpData = {sendTimestamp, sendData, msg_timestamp};
                lfpTrodesPub->publish(lfpData);
                // finish publishing

                expecting.assign(expecting.size(), true);

                counts[currind] = 0;
                currind = (currind + 1) % LFP_BUFFER_SIZE;

                //                    qDebug() << "Published LFP timestamp" << sendTimestamp;
            } else {
                ready = false;
                //qDebug() << "[LFPAggregator::runloop] Some accounting error happened; please contact a developer!";
            }
        }



    } // end while

    //qDebug() << "Ending LFP publisher...";
    lfpTrodesPub.reset();


}

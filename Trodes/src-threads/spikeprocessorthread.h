#ifndef SPIKEPROCESSORTHREAD_H
#define SPIKEPROCESSORTHREAD_H
#include <QObject>
#include <QtCore>

#include "configuration.h"
//#include "trodesSocket.h"
#include "trodesglobaltypes.h"
#include "trodesSocket.h"
#include <memory>
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/TrodesSpikeWaveformData.h>
#include <TrodesNetwork/Generated/SpikePacket.h>
#include "streamprocesshandlers.h"


class SpikeProcessingBuffer
{
public:
    SpikeProcessingBuffer();
    ~SpikeProcessingBuffer();
    void addSpikes(QVector<SpikeWaveform> &sp, int sourceIndex);
    int addSourceBuffer();
    bool fetchSpikes(QVector<SpikeWaveform>&sp);
private:
    int numSources;
    QAtomicInt lock;

    QVector<QVector<QVector<SpikeWaveform> > > spikeBuffer;
    QVector<int> readHeads;
    QVector<int> writeHeads;
    //QVector<QAtomicInt> buffStatus;
    //int readHead;
    int bufferSize;
    //QVector<bool> locked;
    QVector<QSemaphore*> spikesAvailableInSource;
    QSemaphore* spikesAvailable;

};

class AbstractSpikeSortingThread : public QObject
{
    Q_OBJECT
public:
    AbstractSpikeSortingThread(QObject *parent, TrodesConfigurationPointers c_ptrs);
    void startThread();
    SpikeProcessingBuffer spikeBuffer;

private:
    TrodesConfigurationPointers conf_ptrs;


protected:
    virtual void processWaveforms(QVector<SpikeWaveform> &spikes); //reimplement to do custom waveform processing (i.e., sorting)
    bool quitNow;

private slots:

public slots:
    void receiveNewSpikes(QVector<SpikeWaveform> &spikes); //calls processWaveforms
    virtual void setUp(); //reimplement to customize any setup required after thread starts
    void shutDown();


signals:
    void newProcessedSpikes(QVector<SpikeWaveform> &spikes);
    void readyForThreadShutdown();

};


class SpikeProcessorThread : public QObject
{
    Q_OBJECT
public:
    SpikeProcessorThread(QObject *parent, TrodesConfigurationPointers c_ptrs);
    SpikeProcessingBuffer spikeBuffer;
    AbstractSpikeSortingThread *sorter;
    bool quitNow;


private:
    TrodesConfigurationPointers conf_ptrs;


    // need to start the publisher
    //std::unique_ptr<trodes::network::SourcePublisher<trodes::network::SpikePacket>> spikeTrodesPub;
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesSpikeWaveformData>> waveformTrodesPub;
    // allocated when the publisher is created
    unsigned char *spikeDataBlock;
    bool spikePubStarted;

private slots:

public slots:
    //void receiveNewSpikes(QVector<SpikeWaveform>);
    void runLoop();
    void setUp();
    void startSpikePub();
    void shutdown();

signals:
    void newProcessedSpikes(QVector<SpikeWaveform> &spikes); //emitted (direct connection should be preserved to final destination) from sorting thread (default behavior if the sorting happens in another object's method)
    void spikesReadyForProcessing(QVector<SpikeWaveform> &spikes); //to sorting thread

};



#endif // SPIKEPROCESSORTHREAD_H

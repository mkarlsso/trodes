#include "spikeprocessorthread.h"
#include <TrodesNetworkUtil.h>
#include <TrodesNetwork/Generated/SpikePacket.h>
#include <TrodesNetwork/Generated/TrodesSpikeWaveformData.h>
#include <TrodesNetwork/Util.h> // for get_timestamp


SpikeProcessingBuffer::SpikeProcessingBuffer()
{
    /*This buffer object stores spikes from multiple streamprocessor threads.
     * Therefore, this is a multiple writers, one reader situation.
     * Each writer is assigned a unique 'source index' to keep the writing locations separate.
     * This way, we can use a simple ring buffer for each writer.
     */


    numSources = 0;
    lock = 0;
    bufferSize = 1000; //This is the size of each ring buffer. Each item in the ring buffer honds all the spikes processed in one loop of the streamprocessor
    spikesAvailable = new QSemaphore(0); //User to signal that a new spike has come in from one of the sources

}

SpikeProcessingBuffer::~SpikeProcessingBuffer()
{


    //delete all of the semaphores
    while (spikesAvailable->tryAcquire(1)) {}
    delete spikesAvailable;
    while (spikesAvailableInSource.length() > 0) {
        while (spikesAvailableInSource[0]->tryAcquire(1)) {}
        delete spikesAvailableInSource.takeFirst();
    }

}

int SpikeProcessingBuffer::addSourceBuffer() {

    /*This is where we assign a unique source index to each streamprocessor thread.
     * The index is returned to the calling streamprocessor.
     * We use a lock to make sure that only one index is being assigned at once.
     */

    if (lock.testAndSetRelaxed(0,1)) {

        int retSource;
        numSources++;
        QVector<QVector<SpikeWaveform> > newBuf;
        for (int i=0;i<bufferSize;i++) {
            QVector<SpikeWaveform> s;
            newBuf.push_back(s);
        }
        spikeBuffer.push_back(newBuf);

        spikesAvailableInSource.push_back(new QSemaphore(0));
        readHeads.push_back(0);
        writeHeads.push_back(0);
        retSource = numSources-1;
        lock = 0;
        return retSource; //The dedicated index of that source
    } else {
        return -1;
    }
}

void SpikeProcessingBuffer::addSpikes(QVector<SpikeWaveform> &sp, int sourceIndex)
{
    /*Here the calling streamprocessor adds spikes to it's dedicated ring buffer
    */


    if (numSources > sourceIndex && spikesAvailableInSource[sourceIndex]->available() < bufferSize) {
        spikeBuffer[sourceIndex][writeHeads.at(sourceIndex)] = sp;
        writeHeads[sourceIndex] = (writeHeads[sourceIndex]+1)%bufferSize;
        spikesAvailableInSource[sourceIndex]->release(1); //We release a semaphore for this specific source
        spikesAvailable->release(1); //We also realease a separate sepaphore to indicate that a new spike came in
    } else if (spikesAvailableInSource[sourceIndex]->available() >= bufferSize) {
        qDebug() << "Error: Spike processor buffer overflow.";

        //Becuase these spikes are being dumped early, we need to clear the memory now to avoid a memory leak.
        for (int i=0; i<sp.length(); i++) {
            sp[i].clearMemory();
        }
    }

}
bool SpikeProcessingBuffer::fetchSpikes(QVector<SpikeWaveform> &sp)
{
        /*This is called from the spikeprocessor thread. If new spikes are available from any of the streamprocessor threads,
         * Then the spike is processed.
         */

        if (spikesAvailableInSource.isEmpty() || (lock==1)) {
            return false;
        }

        if (spikesAvailable->tryAcquire(1,100)) {

            for (int i=0; i<numSources; i++) {
                if (spikesAvailableInSource[i]->tryAcquire(1)) {                    
                    sp = spikeBuffer[i][readHeads.at(i)];
                    readHeads[i] = (readHeads[i]+1)%bufferSize;
                    return true;
                }
            }
            //qDebug() << "[SpikeProcessor] Error in spike processing queue";
            return false;
        } else {
            return false;
        }

}

SpikeProcessorThread::SpikeProcessorThread(QObject *parent, TrodesConfigurationPointers c_ptrs) :
    conf_ptrs(c_ptrs),
    spikePubStarted(false)
{

}

void SpikeProcessorThread::runLoop()
{
    /*This is the main loop of the spike processing thread, and it handles all spikes from all streamprocessor threads.
    */

    quitNow = false;

    QVector<SpikeWaveform> newSpikes;
    while (!quitNow) {
        if (spikeBuffer.fetchSpikes(newSpikes)) {            

            if (spikePubStarted) {
                for (int i=0;i<newSpikes.length();i++) {
                    trodes::network::TrodesSpikeWaveformData waveformData = {newSpikes[i].peakTime,(uint32_t)conf_ptrs.spikeConf->ntrodes.at(newSpikes[i].ntrodeIndex)->nTrodeId,*newSpikes[i].waveData, newSpikes[i].sysTime};
                    waveformTrodesPub->publish(waveformData);
                }
            }

            //QMetaObject::invokeMethod(sorter, "receiveNewSpikes", Qt::QueuedConnection, Q_ARG(QVector<SpikeWaveform>&, newSpikes));
            //emit spikesReadyForProcessing(newSpikes);
            sorter->spikeBuffer.addSpikes(newSpikes,0);

            //emit newProcessedSpikes(newSpikes); //For now, we simply send the spike to the spike display window, but more processing will be done here eventually.

        }
    }
    //sorter->shutDown();

}

void SpikeProcessorThread::shutdown() {
    quitNow = true;
    sorter->shutDown();
}

void SpikeProcessorThread::setUp()
{


    sorter = new AbstractSpikeSortingThread(nullptr,conf_ptrs);
    connect(sorter,&AbstractSpikeSortingThread::newProcessedSpikes,this,&SpikeProcessorThread::newProcessedSpikes,Qt::DirectConnection); //if the sorting happends elsewhere, this direct connection ensures that the sorting thread still does the work
    connect(this,&SpikeProcessorThread::spikesReadyForProcessing,sorter,&AbstractSpikeSortingThread::receiveNewSpikes,Qt::QueuedConnection);
    sorter->startThread();

}

void SpikeProcessorThread::startSpikePub()
{
    std::string address = trodes::get_network_address(conf_ptrs.networkConf->trodesHost);
    int port = trodes::get_network_port(conf_ptrs.networkConf->trodesPort);
    //qDebug() << conf_ptrs.networkConf->trodesHost << port;

    waveformTrodesPub = std::move(
        std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesSpikeWaveformData>>(
            new trodes::network::SourcePublisher<trodes::network::TrodesSpikeWaveformData>(address, port, "source.waveforms")
        )
    );

    spikePubStarted = true;
}

//---------------------------------------------------------------------

AbstractSpikeSortingThread::AbstractSpikeSortingThread(QObject *parent, TrodesConfigurationPointers c_ptrs) :
    conf_ptrs(c_ptrs)

{
    spikeBuffer.addSourceBuffer();
}

void AbstractSpikeSortingThread::startThread()
{
    QThread *spikeProcThread = new QThread;
    spikeProcThread->setObjectName("SpikeSorter");
    this->moveToThread(spikeProcThread);

    connect(spikeProcThread, SIGNAL(started()), this, SLOT(setUp()));
    connect(this, &AbstractSpikeSortingThread::readyForThreadShutdown,spikeProcThread,&QThread::quit);
    connect(spikeProcThread, &QThread::finished, spikeProcThread,&QThread::deleteLater);
    connect(spikeProcThread, &QThread::finished, this, &AbstractSpikeSortingThread::deleteLater);
    spikeProcThread->start();
}

void AbstractSpikeSortingThread::receiveNewSpikes(QVector<SpikeWaveform> &spikes)
{

    processWaveforms(spikes);
}

void AbstractSpikeSortingThread::shutDown()
{
    this->disconnect();
    quitNow = true;

}


void AbstractSpikeSortingThread::setUp()
{

    quitNow = false;


    QVector<SpikeWaveform> newSpikes;
    while (!quitNow) {
        if (spikeBuffer.fetchSpikes(newSpikes)) {
            processWaveforms(newSpikes);
        }
    }

    emit readyForThreadShutdown();
}

void AbstractSpikeSortingThread::processWaveforms(QVector<SpikeWaveform> &spikes)
{

    emit newProcessedSpikes(spikes); //Needs to be a direct connection to all receiving slots!! Otherwise, the memory clearing (below) could cause a race condition and crash.

    for (int i=0;i<spikes.length();i++) {
        spikes[i].clearMemory();
    }
}





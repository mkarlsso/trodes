/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "fileSourceThread.h"
#include "globalObjects.h"
#include "CZHelp.h"
#include <QFileDialog>
#include <limits>
#include <typeinfo>


FileSourceHelper::FileSourceHelper(TrodesConfigurationPointers c_ptrs, int packetSize) :
    conf_ptrs(c_ptrs),
    PACKET_SIZE(packetSize)
{
    quitNow = false;
    acquiring = false;
    runLoopActive = false;
    writeMarker = 0;
    readMarker = 0;
    numPacketsWritten = 0;
    numPacketsRead = 0;
    numPacketsInBuffer = 60000;

    jumpToBeginning = false;
    jumpToLocation = false;
    newPos = -1;
    sliderPos = 0;


}

FileSourceHelper::~FileSourceHelper() {

}

int FileSourceHelper::packetsAvailable()
{
    return numPacketsWritten-numPacketsRead;
}

char* FileSourceHelper::nextPacket(bool *ok)
{

    char* retVal = dataRingBuffer.data()+(PACKET_SIZE*readMarker);
    if (packetsAvailable() > 0) {
        *ok = true;
        readMarker = (readMarker+1)%numPacketsInBuffer;
        numPacketsRead++;
    } else {
        *ok = false;
    }
    return retVal;

}

void FileSourceHelper::Run()
{


    file = new QFile;
    file->setFileName(playbackFile);

    //open file
    if (!file->open(QIODevice::ReadOnly)) {
        emit newErrorMessage("Could not open file for playback.");
        delete file;
        emit finished();
        return;
    }

    if (!file->seek(fileDataPos)) { //skip past the config header
        emit newErrorMessage("Could not open file for playback.");
        file->close();
        delete file;
        emit finished();
        return;
    }


    dataRingBuffer.resize(PACKET_SIZE*numPacketsInBuffer);
    char *RxBuffer;


    //Some variables for implementing a 'brake' for the loop
    QElapsedTimer loopTimer;




    quitNow = false;

    //timeKeeper->start();
    //stopWatch.start();

    emit newDebugMessage("File playback helper loop running....");

    bool atBeginning = true;
    unsigned int counter = 19500;
    atEnd = false;
    runLoopActive = true;

    while (quitNow != true) {

        if(jumpToLocation){
            qint64 np = (qint64)(newPos*1000+0.5);
            qint64 newFilePos = (qint64)(((file->size() - (qint64)fileDataPos)/PACKET_SIZE)*np/1000)*PACKET_SIZE + (qint64)fileDataPos;
            file->seek(newFilePos);
            jumpToLocation = false;
            atEnd = false;

            qint64 currLoc = (qint64)file->pos() -(qint64)fileDataPos;
            qint64 datasize = (qint64)file->size() - (qint64)fileDataPos;
            qreal pct = (qreal)(currLoc)/(qreal)datasize;
            sliderPos = pct;


            //Process time stamp
            char *buff = new char[PACKET_SIZE];
            file->read(buff, PACKET_SIZE);
            char * tmp = buff;
            tmp += (2*conf_ptrs.hardwareConf->headerSize);
            uint32_t* dptr = (uint32_t *)(tmp);
            uint32_t ts = *dptr;
            emit newTimeStamp(ts);

        }

        if (!acquiring) {
            //stopWatch.restart();

            writeMarker = 0;
            readMarker = 0;
            numPacketsWritten = 0;
            numPacketsRead = 0;
            QThread::msleep(100);

            if (jumpToBeginning) { //reset back to the beginning of the file

                if (!atBeginning) {
                    file->seek(fileDataPos); //return to the begining of the file
                    atBeginning = true;
                    atEnd = false;
                    sliderPos = 0;
                    //emit updateSlider(0);
                    jumpToBeginning = false;
                }
            }

        }
        else {


            atBeginning = false;
            int buffAvailable = numPacketsInBuffer - (numPacketsWritten-numPacketsRead);

            int samplesDue;
            if (buffAvailable > numPacketsInBuffer/2) {
                samplesDue = numPacketsInBuffer/2;
            } else {
                samplesDue = 0;
                //QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
                QThread::msleep(100);
            }


            while (samplesDue > 0) {

                int packetsToRead;
                if ((samplesDue > 100) && ((numPacketsInBuffer - writeMarker)>100)) {
                    packetsToRead = 100;
                } else {
                    packetsToRead = 1;
                }

                if (!(file->read(dataRingBuffer.data()+(PACKET_SIZE*writeMarker),packetsToRead * PACKET_SIZE) == (packetsToRead * PACKET_SIZE))) {
                    //could not read a full packet.  Probably end of file.
                    atEnd = true;
                    break;
                }
                atEnd = false;
                writeMarker = (writeMarker+packetsToRead)%numPacketsInBuffer;
                numPacketsWritten = numPacketsWritten+packetsToRead;
                samplesDue = samplesDue-packetsToRead;
            }

            if(!(counter % 200)){
                qreal pct = (qint64)((file->pos() - (qint64)fileDataPos)*1000.0/(file->size() - (qint64)fileDataPos) + 0.5) / 1000.0;
                sliderPos = pct;
                counter = 0;
            }
            counter++;

        }

    }


    file->close();
    delete file;
    emit newDebugMessage("File read helper thread finished.");
    runLoopActive = false;

    emit finished();

}





fileSourceRuntime::fileSourceRuntime(bool saveDisplayedChan, TrodesConfigurationPointers c_ptrs, QObject *parent):
    AbstractSourceRuntime(c_ptrs),
    waitForThreadsFlag(false)
    , mSecElapsed(0)
    , currentSampleNum(0)
    , currentTimestamp(0)
    , saveDisplayedChan(saveDisplayedChan)
{
    loopFinished = true;
    jumpToBeginning = false;
    jumpToLocation = false;
    newPos = -1;

}

fileSourceRuntime::~fileSourceRuntime() {

}

void fileSourceRuntime::calculateHeaderSize() {
    if(conf_ptrs.hardwareConf->sysTimeIncluded){
        setHeaderSize(conf_ptrs.hardwareConf->headerSize+4);
    }
    else{
        setHeaderSize(conf_ptrs.hardwareConf->headerSize);
    }

    digitalInfoSize = conf_ptrs.hardwareConf->headerSize;
}

void fileSourceRuntime::Run() {



    bool USETRANSFERBUFFER = false; //Transfer buffer can't deal with file-source related issues yet (such as reduced channel-count save)


    int transferSize = PACKET_SIZE;

    TransferBlockRingBuffer transferBuffer(transferSize, 2000);
    transferBuffer.setOnePacketPerTransfer(true);
    if (USETRANSFERBUFFER) {
        runtimeHelper->setTransferBuffer(&transferBuffer);
    }


    emit startHelperThread();


    fileHelper = new FileSourceHelper(conf_ptrs, PACKET_SIZE);
    connect(fileHelper,SIGNAL(endOfFileReached()),this,SIGNAL(endOfFileReached()));
    connect(fileHelper, SIGNAL(updateSlider(qreal)), this, SIGNAL(updateSlider(qreal)));
    connect(fileHelper,SIGNAL(newTimeStamp(uint32_t)),this, SIGNAL(newTimeStamp(uint32_t)));
    QThread *workerThread = new QThread();
    fileHelper->moveToThread(workerThread);
    connect(workerThread, &QThread::started,fileHelper,&FileSourceHelper::Run);


    connect(fileHelper, SIGNAL(finished()), workerThread, SLOT(quit()));
    connect(fileHelper,SIGNAL(finished()), fileHelper, SLOT(deleteLater()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

    connect(fileHelper, &FileSourceHelper::newDebugMessage, this, &AbstractSourceRuntime::newDebugMessage);
    connect(fileHelper, &FileSourceHelper::newErrorMessage, this, &AbstractSourceRuntime::newErrorMessage);
    connect(fileHelper, &FileSourceHelper::newConsoleOutputMessage, this, &AbstractSourceRuntime::newConsoleOutputMessage);

    workerThread->setObjectName("File helper thread");
    workerThread->start(QThread::HighPriority);



    buffer.resize(PACKET_SIZE);
    char *RxBuffer;
    char *RxPacket;
    int remainingSamples = 0;
    int leftInBuffer;
    double dTimestamp = 0.0;

    //Some variables for implementing a 'brake' for the loop
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) conf_ptrs.hardwareConf->sourceSamplingRate*filePlaybackSpeed)*1000000000);
    qint64 loopTimeSurplus_nS;



    quitNow = false;
    stopWatch.start();

    emit newDebugMessage("File playback loop running....");
    qint64 samplesWritten = 0;
    float mSecPerSample;
    bool atBeginning = true;
    loopFinished = false;
    runLoopActive = true;
    unsigned int counter = 19500;



    while (quitNow != true) {

        if(jumpToLocation){

            fileHelper->newPos = newPos;
            fileHelper->jumpToLocation = true;
            jumpToLocation = false;
            packetSizeErrorThrown = true; //this prevents a dropped packets warning

        }

        if (!acquiring) {
            fileHelper->acquiring = false;
            stopWatch.restart();
            samplesWritten = 0;
            QThread::msleep(100);

            if (jumpToBeginning) { //reset back to the beginning of the file
                packetSizeErrorThrown = true; //this prevents a dropped packets warning
                stopWatch.restart();
                samplesWritten = 0;
                currentTimeStamp = 0;
                fileHelper->jumpToBeginning = true;
                jumpToBeginning = false;
                emit updateSlider(0);

            }

        }
        else {
            if (waitForThreadsFlag) {
                QThread::msleep(250);


                if (filePlaybackSpeed > 1) {
                    filePlaybackSpeed--;
                    qDebug() << "Reducing processing speed to " << filePlaybackSpeed;
                }
                //TODO: we need a lock here
                waitForThreadsFlag = false;
            }
            loopTimer.restart();
            atBeginning = false;
            fileHelper->acquiring = true;

            mSecPerSample = 1000.0/(conf_ptrs.hardwareConf->sourceSamplingRate*filePlaybackSpeed);
            mSecElapsed = stopWatch.elapsed();

            int samplesDue = floor(mSecElapsed/mSecPerSample)-samplesWritten;

            while (samplesDue > 0) {

                bool ok;
                RxBuffer = fileHelper->nextPacket(&ok);
                RxPacket = RxBuffer;
                if (!ok) {
                    if (fileHelper->atEnd) {

                        acquiring = false;
                        fileHelper->acquiring = false;
                        emit endOfFileReached();
                        break;
                    }
                    break;
                }

                if (USETRANSFERBUFFER) {
                    bool bufferReservable;
                    uchar* reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
                    if (!bufferReservable) {
                        //We have a buffer overrun situation. We need to wait until the consumer has caught up
                        //Maybe put a brief sleep here? Also a debug log warning?
                        //qDebug() << "WARNING: Source processing buffer full. Waiting for processing thread to catch up.";
                        while (!bufferReservable && acquiring) {
                            reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
                        }

                    }

                    memcpy(reservePtr, RxBuffer, PACKET_SIZE);

                    //memcpy(reservePtr, datagram.data(), PACKET_SIZE);
                    transferBuffer.setLastWriteTransferSize(PACKET_SIZE);
                    sourceDataAvailable->release(1);
                } else {


                    leftInBuffer = PACKET_SIZE;
                    checkHardwareStatus((const uchar*)RxBuffer+1);

                    //Read digital info
                    memcpy(&(rawData.digitalInfo[rawData.writeIdx*digitalInfoSize]), RxBuffer, digitalInfoSize*sizeof(uint16_t));


                    //Sysclock is next 8 bytes
                    //currently doing nothing with it

                    //skipping digital info and sysclock, to mcu timestamps
                    RxBuffer += (2*headerSize);
                    leftInBuffer -= (2*headerSize);


                    //Process time stamp
                    uint32_t* dataPtr = (uint32_t *)(RxBuffer);
                    currentTimeStamp = *dataPtr;

                    checkForCorrectTimeSequence();

                    rawData.timestamps[rawData.writeIdx] = *dataPtr;
                    rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();

                    RxBuffer += 4;
                    leftInBuffer -= 4;
                    dTimestamp += 1.0;
                    rawData.dTime[rawData.writeIdx] = dTimestamp;


                    int16_t *rawDataPtr = rawData.data + rawData.writeIdx*conf_ptrs.hardwareConf->NCHAN;
                    int16_t *RxBufferPtr;
                    // copy the data points to the appropriate elements of the rawData.data buffer
                    if (!saveDisplayedChan) {
                        //The entire harware channel list was saved
                        RxBufferPtr = (int16_t *) RxBuffer;
                        remainingSamples = conf_ptrs.hardwareConf->NCHAN;
                        memcpy(&(rawData.data[rawData.writeIdx*conf_ptrs.hardwareConf->NCHAN]), \
                               RxBuffer, remainingSamples*sizeof(uint16_t));
                    } else {
                        //Some channels were possibly excluded from the file
                        for (int i = 0; i < numChannelsInFile; i++) {
                            //RxBufferPtr = (int16_t *)(buffer.data()+channelPacketLocations_input[i]);
                            RxBufferPtr = (int16_t *)(RxPacket + channelPacketLocations_input[i]);
                            rawDataPtr[channelPacketLocations_output[i]] = *RxBufferPtr;

                        }
                    }

                    calculateReferences();

                    //Advance the write markers and release a semaphore
                    writeMarkerMutex.lock();
                    rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                    rawDataWritten++;
                    writeMarkerMutex.unlock();

                    for (int a = 0; a < rawDataAvailable.length(); a++) {
                        rawDataAvailable[a]->release(1);
                    }
                    sourceDataAvailable->release(1);
                }

                samplesWritten++;
                samplesDue--;

            }


            loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();
            if (loopTimeSurplus_nS > 1000) {
                QThread::usleep(5*loopTimeSurplus_nS/1000);
            }
        }
        if(!(counter % 200)){
            qreal pct = (qint64)((file->pos() - (qint64)fileDataPos)*1000.0/(file->size() - (qint64)fileDataPos) + 0.5) / 1000.0;
            emit updateSlider(fileHelper->sliderPos);

        }

        if(!(counter % 1000)){
            emit newTimeStamp(currentTimeStamp);
            counter = 0;
        }
        counter++;
    }




    /*while (quitNow != true) {

        if(jumpToLocation){
            qint64 np = (qint64)(newPos*1000+0.5);
            qint64 newFilePos = (qint64)(((file->size() - (qint64)fileDataPos)/PACKET_SIZE)*np/1000)*PACKET_SIZE + (qint64)fileDataPos;
            file->seek(newFilePos);
            jumpToLocation = false;

            qint64 currLoc = (qint64)file->pos() -(qint64)fileDataPos;
            qint64 datasize = (qint64)file->size() - (qint64)fileDataPos;
            qreal pct = (qreal)(currLoc)/(qreal)datasize;
            emit updateSlider(pct);
            //Process time stamp
            char *buff = new char[PACKET_SIZE];
            file->read(buff, PACKET_SIZE);
            char * tmp = buff;
            tmp += (2*headerSize);
            uint32_t* dptr = (uint32_t *)(tmp);
            currentTimeStamp = *dptr;
            emit newTimeStamp(currentTimeStamp);
            packetSizeErrorThrown = true; //this prevents a dropped packets warning
        }

        if (!acquiring) {
            stopWatch.restart();
            samplesWritten = 0;
            QThread::msleep(100);

            if (jumpToBeginning) { //reset back to the beginning of the file
                packetSizeErrorThrown = true; //this prevents a dropped packets warning
                stopWatch.restart();
                samplesWritten = 0;
                currentTimeStamp = 0;
                if (!atBeginning) {
                    file->seek(fileDataPos); //return to the begining of the file
                    atBeginning = true;
//                    playbackFileCurrentLocation = fileDataPos;
                    emit updateSlider(0);
                    jumpToBeginning = false;
                }
            }

        }
        else {
            if (waitForThreadsFlag) {
                QThread::msleep(250);


                if (filePlaybackSpeed > 1) {
                    filePlaybackSpeed--;
                    qDebug() << "Reducing processing speed to " << filePlaybackSpeed;
                }
                //TODO: we need a lock here
                waitForThreadsFlag = false;
            }
            loopTimer.restart();
            atBeginning = false;
            //samplesPerCycle = (hardwareConf->sourceSamplingRate/waveFrequency);
            //cycleStepsPerSample = waveRes/samplesPerCycle;

            //float nSecPerSample = 1000000000.0/hardwareConf->sourceSamplingRate;
            mSecPerSample = 1000.0/(conf_ptrs.hardwareConf->sourceSamplingRate*filePlaybackSpeed);

            //mSecPerSample = 1000.0/100000;


            //nSecElapsed = nSecElapsed + stopWatch.nsecsElapsed();
            //nSecElapsed = stopWatch.nsecsElapsed();
            mSecElapsed = stopWatch.elapsed();

            //stopWatch.restart();
            int samplesDue = floor(mSecElapsed/mSecPerSample)-samplesWritten;

            while (samplesDue > 0) {
                //int16_t currentValue = (int16_t) ((float) sourceData[currentCyclePosition] * (2*(float) waveAmplitude/12500.0));

                if (!(file->read(buffer.data(),PACKET_SIZE) == PACKET_SIZE)) {
                    //could not read a full packet.  Probably end of file.
                    qDebug() << "End of file reached";
                    //quitNow = true;

                    acquiring = false;
                    emit endOfFileReached();
                    break;
                }


//                playbackFileCurrentLocation = file->pos();

//                //------
//                qreal pct = (qint64)((file->pos() - (qint64)fileDataPos)*1000.0/(file->size() - (qint64)fileDataPos) + 0.5) / 1000.0;
//                emit updateSlider(pct);
//                //------

                leftInBuffer = PACKET_SIZE;
                RxBuffer = buffer.data();
                checkHardwareStatus((const uchar*)RxBuffer+1);

                //publishDataPacket((unsigned char*)RxBuffer, PACKET_SIZE, CZHelp::systemTimeMSecs());

                //Read digital info
                memcpy(&(rawData.digitalInfo[rawData.writeIdx*digitalInfoSize]), RxBuffer, digitalInfoSize*sizeof(uint16_t));


                //Sysclock is next 8 bytes
                //currently doing nothing with it

                //skipping digital info and sysclock, to mcu timestamps
                RxBuffer += (2*headerSize);
                leftInBuffer -= (2*headerSize);




                //Process time stamp
                uint32_t* dataPtr = (uint32_t *)(RxBuffer);
                currentTimeStamp = *dataPtr;

                checkForCorrectTimeSequence();


                //checkForCorrectTimeSequence();
                rawData.timestamps[rawData.writeIdx] = *dataPtr;
                //rawData.sysTimestamps[rawData.writeIdx] = CZHelp::systemTimeMSecs();

                rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();

                RxBuffer += 4;
                leftInBuffer -= 4;
                dTimestamp += 1.0;
                rawData.dTime[rawData.writeIdx] = dTimestamp;


                int16_t *rawDataPtr = rawData.data + rawData.writeIdx*conf_ptrs.hardwareConf->NCHAN;
                int16_t *RxBufferPtr;
                // copy the data points to the appropriate elements of the rawData.data buffer
                if (!saveDisplayedChan) {
                    //The entire harware channel list was saved
                    RxBufferPtr = (int16_t *) RxBuffer;
                    remainingSamples = conf_ptrs.hardwareConf->NCHAN;
                    memcpy(&(rawData.data[rawData.writeIdx*conf_ptrs.hardwareConf->NCHAN]), \
                           RxBuffer, remainingSamples*sizeof(uint16_t));
                } else {
                     //Some channels were possibly excluded from the file
                    for (int i = 0; i < numChannelsInFile; i++) {
                        RxBufferPtr = (int16_t *)(buffer.data()+channelPacketLocations_input[i]);
                        //RxBufferPtr = (int16_t *) RxBuffer + channelPacketLocations_input[i];
                        rawDataPtr[channelPacketLocations_output[i]] = *RxBufferPtr;

                    }
                }

                calculateReferences();

                //Advance the write markers and release a semaphore
                writeMarkerMutex.lock();
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                rawDataWritten++;
                writeMarkerMutex.unlock();

                for (int a = 0; a < rawDataAvailable.length(); a++) {
                    rawDataAvailable[a]->release(1);
                }
                sourceDataAvailable->release(1);
                //rawDataAvailableForSave.release(1);



                samplesWritten++;
                samplesDue--;


            }

            //QThread::usleep(1000);

            loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();
            if (loopTimeSurplus_nS > 1000) {
                QThread::usleep(5*loopTimeSurplus_nS/1000);
            }
        }

        if(!(counter % 200)){
            qreal pct = (qint64)((file->pos() - (qint64)fileDataPos)*1000.0/(file->size() - (qint64)fileDataPos) + 0.5) / 1000.0;
            emit updateSlider(pct);
        }
//        if(!(counter % 20000)){
//            emit newTimeStamp(currentTimeStamp);
//            counter = 0;
//        }
        if(!(counter % 1000)){
            emit newTimeStamp(currentTimeStamp);
            counter = 0;
        }
        counter++;
    }*/

    fileHelper->quitNow = true;
    runtimeHelper->quitNow = true;

    //file->close();
    //delete file;

    emit newDebugMessage("File read thread finished.");
    runLoopActive = false;

    loopFinished = true;
    emit finished();

}

void fileSourceRuntime::calculateChannelInfo() {

        // UNUSED
        // int packetHeaderSize = (2*hardwareConf->headerSize)+4; //Aux info plus 4-byte timestamp
        // int packetTimeLocation = 2*hardwareConf->headerSize;
       calculateHeaderSize();

        QList<int> sortedChannelList;
        //Gather all HW channels
        for (int n = 0; n < conf_ptrs.spikeConf->ntrodes.length(); n++) {
            for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[n]->hw_chan.length(); c++) {
                int tempHWRead = conf_ptrs.spikeConf->ntrodes[n]->hw_chan[c];
                sortedChannelList.push_back(tempHWRead);
            }
        }
        //Sort the channels
        std::sort(sortedChannelList.begin(),sortedChannelList.end());

        //Remember how many channels are actually saved in the file
        //(may be different from how many channels came from recording hardware)

        if (saveDisplayedChan) {
            //numChannelsInFile = sortedChannelList.length();
            numChannelsInFile = conf_ptrs.streamConf->nChanConfigured;
        } else {
            numChannelsInFile = conf_ptrs.hardwareConf->NCHAN;
        }

        //filePacketSize = packetHeaderSize+(2*numChannelsInFile);
        /*stimPacketSize = ceil(hardwareConf->numStimChan/8.0);
        if (stimPacketSize > 0) {
            stimPacketSize = stimPacketSize+2; //The first two bytes in the stim section is used to report commands recieved
        }*/

        //Then we find any unsaved channels, and from that determine the new packet locations
        //of each channel

        int lastChannel = -1;
        int unusedChannelsSoFar = 0;
        for (int i=0;i<sortedChannelList.length();i++) {
            if ((sortedChannelList[i]-lastChannel > 1) && (saveDisplayedChan)){
                unusedChannelsSoFar = unusedChannelsSoFar + (sortedChannelList[i]-lastChannel-1);

            }
            channelPacketLocations_input.push_back((2*headerSize)+4+(2*(sortedChannelList[i]-unusedChannelsSoFar)));
            channelPacketLocations_output.push_back(sortedChannelList[i]);
            //qDebug() << sortedChannelList[i] << channelPacketLocations_input.last() << channelPacketLocations_output.last();
            lastChannel = sortedChannelList[i];
        }
        //numChannelsInFile = numChannelsInFile-unusedChannelsSoFar;



        if (conf_ptrs.globalConf->saveDisplayedChanOnly) {
            PACKET_SIZE = 2*(numChannelsInFile) + 4 + (2*headerSize);
        } else {
            PACKET_SIZE = 2*(conf_ptrs.hardwareConf->NCHAN) + 4 + (2*headerSize);
        }


        //Finds start and end timestamp of file       
        char *tmpBuffer;
        buffer.resize(PACKET_SIZE);

        file->seek(fileDataPos);
        file->read(buffer.data(), PACKET_SIZE);
        tmpBuffer = buffer.data();
        tmpBuffer += (2*headerSize);
        uint32_t* timestamp = (uint32_t*)(tmpBuffer);
        uint32_t playbackStartTimeStamp = *timestamp;

        quint64 lastPacket = (quint64)((file->size() - fileDataPos)/PACKET_SIZE)*PACKET_SIZE + fileDataPos-PACKET_SIZE;
//        file->seek(file->size()-fileDataPos);
        file->seek(lastPacket);
        file->read(buffer.data(), PACKET_SIZE);
        tmpBuffer = buffer.data();
        tmpBuffer += (2*headerSize);
        timestamp = (uint32_t*)(tmpBuffer);
        uint32_t playbackEndTimeStamp = *timestamp;

        file->seek(fileDataPos);
        buffer.clear();

        emit setTimeStamps(playbackStartTimeStamp, playbackEndTimeStamp);

        //We don't need the file anymore in this thread;
        file->close();



}


/*
void fileSourceRuntime::endThread() {
    quit();
}*/


fileSourceInterface::fileSourceInterface(bool saveDisplayedChan, QObject*)
    : AbstractTrodesSource(),
      saveDisplayedChan(saveDisplayedChan)
{
  state = SOURCE_STATE_NOT_CONNECTED;
  acquisitionThread = NULL;
  filePaused = false;
//  fileJump = false;
}

fileSourceInterface::~fileSourceInterface() {

    //if (acquisitionThread != NULL) {
    //    delete(acquisitionThread);
    //}
}

void fileSourceInterface::InitInterface() {
  //open device here

  QFile* filePtr = new QFile;
  filePtr->setFileName(playbackFile);

  //open file
  if (!filePtr->open(QIODevice::ReadOnly)) {
      delete filePtr;
      return;
  }

  if (!filePtr->seek(fileDataPos)) { //skip past the config header
      delete filePtr;
      return;
  }

  //initialization went ok, so start the runtime thread
  acquisitionThread = new fileSourceRuntime(saveDisplayedChan, conf_ptrs, NULL);
  acquisitionThread->file = filePtr;
  //acquisitionThread->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  connect(acquisitionThread,SIGNAL(endOfFileReached()),this,SLOT(StopAcquisition()));
  connect(acquisitionThread,SIGNAL(setTimeStamps(uint32_t,uint32_t)), this, SIGNAL(setTimeStamps(uint32_t,uint32_t)));
  connect(acquisitionThread, SIGNAL(updateSlider(qreal)), this, SIGNAL(updateSlider(qreal)));
  connect(acquisitionThread,SIGNAL(newTimeStamp(uint32_t)),this, SIGNAL(newCurTimestamp(uint32_t)));
//  connect(this, SIGNAL(jumpFileTo(qreal)), acquisitionThread, SLOT());
//  connect(acquisitionThread,SIGNAL(setTimeStamps(uint32_t,uint32_t)), this, SLOT(dummySlot(uint32_t, uint32_t)));
  setUpThread(acquisitionThread);

  filePaused = false;
  playbackFileSize = filePtr->size();
//  playbackFileCurrentLocation = fileDataPos;
  filePlaybackSpeed = 1; //Normal speed

  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

//  HeadstageSettings headstageSettings;
//  headstageSettings.autoSettleOn = false;
//  headstageSettings.percentChannelsForSettle = 0;
//  headstageSettings.threshForSettle = 0;
//  headstageSettings.smartRefOn = false;
//  headstageSettings.accelSensorOn = false;
//  headstageSettings.gyroSensorOn = false;
//  headstageSettings.magSensorOn = false;
//  headstageSettings.rfChannel = 0;

//  headstageSettings.smartRefAvailable = false;
//  headstageSettings.autosettleAvailable = false;
//  headstageSettings.accelSensorAvailable = false;
//  headstageSettings.gyroSensorAvailable = false;
//  headstageSettings.magSensorAvailable = false;
//  headstageSettings.rfAvailable = false;

//  emit headstageSettingsReturned(headstageSettings);
  acquisitionThread->calculateChannelInfo();
}
//void fileSourceInterface::dummySlot(uint32_t a, uint32_t b){
//    qDebug() << "dummyslot: fileSourceInterface received " << a << " " << b << "\n";
//}

void fileSourceInterface::StartAcquisition(void) {

    //streamConf->listChanToSave();

    //if we were paused we don't need to start any new stream threads
    if (!filePaused) {

        rawData.writeIdx = 0; // location where we're currently writing
        emit acquisitionStarted(); //tell stream thread to start

        if (acquisitionThread->loopFinished) {
            acquisitionThread->acquiring = true;
            emit startRuntime();
            //qDebug() << "Told runtime to start";
        }
    }
    acquisitionThread->acquiring = true;
    filePaused = false;


    //emit acquisitionStarted();
    state = SOURCE_STATE_RUNNING;
    emit stateChanged(SOURCE_STATE_RUNNING);

}


void fileSourceInterface::PauseAcquisition() {

    filePaused = true;
    //state = SOURCE_STATE_INITIALIZED;
    //emit stateChanged(SOURCE_STATE_INITIALIZED);
    acquisitionThread->acquiring = false;
    emit acquisitionPaused();
}

void fileSourceInterface::jumpAcquisition(qreal value){
    fileJump = true;
    acquisitionThread->jumpToLocation = true;
   // acquisitionThread->aquiring = false;
//    qDebug() << "received value " << value;
    acquisitionThread->newPos = value;
}



void fileSourceInterface::StopAcquisition(void) {


  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  acquisitionThread->jumpToBeginning = true;
  acquisitionThread->acquiring = false;
  filePaused = false;

}

void fileSourceInterface::CloseInterface(void) {
  if (state == SOURCE_STATE_RUNNING) {
        StopAcquisition();
  }



  if (state == SOURCE_STATE_INITIALIZED) {

    //if the runtime thread is running, kill it

    /*
    if (acquisitionThread != NULL) {
        acquisitionThread->quitNow = true;
        acquisitionThread->endThread();
        acquisitionThread->wait(); //block until the thread has fully terminated
    }*/

    //close device

    if (acquisitionThread != NULL) {
        acquisitionThread->deleteDedicatedDataPub();
        acquisitionThread->quitNow = true;

    }

    while (!acquisitionThread->loopFinished) {
        QThread::msleep(10);
    }

    //acquisitionThread->file->close();
    //delete acquisitionThread->file;
    playbackFileOpen= false;



    //qDebug() << "Closed device.";

    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

  }
}

void fileSourceInterface::waitForThreads(void) {
    //TODO: we need a lock here
    acquisitionThread->waitForThreadsFlag = true;
}






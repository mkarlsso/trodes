#ifndef DISPLAYPROCESSOR_H
#define DISPLAYPROCESSOR_H

#include <stdint.h>
//#include <array>

//16 channel processor that calculates the displayed array
class DisplayProcessor
{
public:
    DisplayProcessor();
    ~DisplayProcessor();

    //must be 16 samples long AND aligned along 32-byte boundaries
    //should directly take the outputs of the corresponding neuralchan/aux processors
    //does the following:
    //  - select between spike/raw/lfp streams for each channel
    //  - calculate the min/max of each bin
    //  - keep track of the stream increment variable
    void neuralSamples(const float *rawdata, const float *spikedata, const float *lfpdata, const float *stimdata, float *minValuesOutput, float *maxValuesOutput, bool newbin);

    //set display for channel
    enum DisplaySetting{raw, lfp, spike, stim};
    int setDisplay(int ind, DisplaySetting disp);

    //
//    int setStreamSize(int datalength, int timepoints);

//    //get binned values  (min and max within each bin)
//    const float* getCurrMaxValues()      const {return maxValues;}
//    const float* getCurrMinValues()      const {return minValues;}

private:
//    typedef std::array<float, 8> avxps;
//    typedef std::array<int32_t, 8> avxi32;

    //display array and masks
//    avxi32 blendmask_lfp[2];
//    avxi32 blendmask_spk[2];
    int32_t *blendmask_lfp;
    int32_t *blendmask_spk;
    int32_t *blendmask_stim;
    uint8_t *full_mask_buffer;

    //min/max arrays and bin counters
//    float minValues[16];
//    float maxValues[16];
//    int stream_inc;
//    int stream_inc_max[2];
//    uint8_t bin;
};

#endif // DISPLAYPROCESSOR_H

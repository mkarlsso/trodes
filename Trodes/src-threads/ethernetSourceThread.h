/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ETHERNETSOURCETHREAD_H
#define ETHERNETSOURCETHREAD_H


#include <QThread>
#include <QVector>
#include <QtNetwork>
#include "abstractTrodesSource.h"
#include "trodesSocketDefines.h"


/*
* the port address for incoming ephys data is 8200
* the port address for ephys control (start, stop, etc) is 8100
* the port address for ECU stateScript data is 8110
*/

class EthernetSourceRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  EthernetSourceRuntime(TrodesConfigurationPointers c_ptrs, QObject *parent);
  ~EthernetSourceRuntime(void);
  QVector<unsigned char> buffer;

private:

  QUdpSocket *udpDataSocket;
  int packets;


public slots:
  void Run(void);

signals:
  void resetInterface();

};

class EthernetInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  EthernetInterface(QObject *parent);
  ~EthernetInterface(void) override;
  int state;
  quint64 getTotalDroppedPacketEvents() override;
  quint64 getTotalUnresponsiveHeadstagePackets() override;
  void SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) override;
  //HardwareControllerSettings lastControllerSettings;
  //HeadstageSettings          lastHeadstageSettings;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  EthernetSourceRuntime *UDPDataProcessor;
  //QThread       *workerThread;

  QUdpSocket *udpControlSocket;
  QUdpSocket *udpControlSocketReceiver;
  QUdpSocket *udpECUDirectPort;

  void sendMessageToHardware(QByteArray datagram);
  void sendMessageToHardware_wait(QByteArray datagram, int waitMsecs = 5);
  QElapsedTimer lastSendTime;

protected:
  bool getDataStreamFromHardware(QByteArray* data, int numBytes) override;
  bool sendCommandToHardware(QByteArray &command) override;



private slots:
  void controlSocketAcknowledgeReceived();
  void restartThread() override;

public slots:
  void InitInterface(void) override;
  bool RunDiagnostic(QByteArray *data,HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings);
  void ResetInterface(void);
  void StartAcquisition(void) override;
  void StartSimulation(void) override;
  void StopAcquisition(void) override;
  void CloseInterface(void) override;
  void SendSettleCommand(void) override;
  void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) override;
  void SendFunctionTrigger(int funcNum) override;
  void SendSDCardUnlock(void) override;
  void ConnectToSDCard(void) override;
  void ReconfigureSDCard(int numChannels) override;
  void SendHeadstageSettings(HeadstageSettings s) override;
  bool SendNeuroPixelsSettings(NeuroPixelsSettings s) override;
  void SendSaveHeadstageSettings() override;
  void SetStimulationParams(StimulationCommand s) override;
  bool SendGlobalStimulationSettings(GlobalStimulationSettings s) override;
  void SendGlobalStimulationAction(GlobalStimulationCommand s) override;
  void ClearStimulationParams(uint16_t slot) override;
  void SendStimulationStartSlot(uint16_t slot) override;
  void SendStimulationStartGroup(uint16_t group) override;
  void SendStimulationStopSlot(uint16_t slot) override;
  void SendStimulationStopGroup(uint16_t group) override;

  void EnableECUShortcutMessages() override;
  void SendECUShortcutMessage(uint16_t function) override;

  void SendControllerSettings(HardwareControllerSettings s) override;
  HeadstageSettings GetHeadstageSettings() override;
  HardwareControllerSettings GetControllerSettings() override;
  int MeasurePacketLength(HeadstageSettings settings) override;
  bool isSourceAvailable();
};


#endif // ETHERNETSOURCETHREAD_H

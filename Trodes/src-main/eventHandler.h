#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H
#include <QtCore>
#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QDialog>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QListWidget>
#include <QAbstractButton>
#include "trodesSocket.h"

#include "configuration.h"
#include <QKeyEvent>
#include "trodesdatastructures.h"
#include "qtmoduleclient.h"
#include "trodesmsg.h"

#define MAX_CONNECTION_MENU_SIZE 1000 //default max size of the event-connection window

/* ****** DOCUMENTATION ******
 * The EventHandler class provides a gui implementation that allows the user to dynamically
 * create connections between events and module methods.  A dialog-menu system is used to
 * allow the user to connect specific events with specific methods, and the implementation
 * of these menus is included in the EventHandler class.
 *
 * ****** USAGE ******
 * 1) Declare a new event handler via the call "new EventHandler(my_ActionList, this);".
 *    Note that you must provide the event handler with a QList<QString> my_ActionList of
 *    strings that list the names of all module provided methods (the methods you want to be
 *    able to connect events to).
 *
 * 2) Connect the event handler to your module's 'TrodesModuleNetwork' via te following
 *    call: "my_EventHandler->setUpConnections(my_TrodesModuleNetwork);".  Note that
 *    my_EventHandler is a ptr to your event handler and my_trodesModuleNetwork is a ptr to
 *    your TrodesModuleNetwork.
 *
 * 3) Create a slot method to call the methods that you provided to the event handler in the
 *    my_ActionList.  Make sure that your slot requires an integer corresponding to the
 *    method it will call as an argument (in this case, the integer should be the same as
 *    the integer location of the method in my_ActionList; for example, the method listed in
 *    my_ActionList[2] should be called by passing the integer '2' to your slot).  The slot
 *    should also include a TrodesEvent as an argument if you want detailed event information
 *    to be accessed by your module.
 *
 * 4) Connect your slot with the event handler, example:
 *       - "connect(my_EventHandler, SIGNAL(sig_executeAction(int,TrodesEvent)), this, SLOT(my_CallMethodSlot(int,TrodesEvent)));"
 */

//***********************************************************
//EventConnection struct

struct EventConnection {
    TrodesEvent ev;
    int methodInd;
};

//***********************************************************
//EventDialog class

class EventDialog : public QDialog {
    Q_OBJECT

public:
    EventDialog(QWidget *parent = 0);

public slots:
    void updateEvents(QVector<TrodesEvent> evList);
    void updateActions(QList<QString> actList);
    void sendSelectedEventAction(void);
    void bringToFront(int code = 0);

private:
    QHBoxLayout *headerLayout;
    QHBoxLayout *listLayout;
    QVBoxLayout *verticalLayout;
    QListWidget *eventList;
    QListWidget *actionList;
    QDialogButtonBox *buttonBox;

signals:
    void sig_selectedEvent(int eventInd);
    void sig_selectedAction(int actionInd);
    void sig_selectedEventAction(int eventInd, int actionInd);
};

//***********************************************************
//EventConnectionDialog class

class EventConnectionDialog : public QDialog {
    Q_OBJECT

public:
    EventConnectionDialog(QWidget *parent = 0);

    EventDialog *eventActionMenu;

public slots:
    void updateConnectionList(QVector<EventConnection> connections, QList<QString> actionList);
    void bringToFront(int code = 0);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    QVBoxLayout *mainLayout;
    QListWidget *eventConnectionList;

    QDialogButtonBox *buttonBox;

private slots:
    void setEventActionMenuVisable(void);
    void removeConnection(void);
    void buttonPushed(QAbstractButton *button);

signals:
    void sig_removeConnection(int connectionIndex);

};

//***********************************************************
//EventHandler class

class EventHandler : public QWidget {
    Q_OBJECT

public:
    EventHandler(QList<QString> actionList, QWidget *parent = 0);
    EventConnectionDialog *eventConnectionMenu;
    void setUpConnections(QtModuleClient* moduleclient);

public slots:
    void promptEventConnectionMenu(void);

    void updateEventList(QVector<TrodesEvent> evList);
    void updateActionList(QList<QString> actList);
    void addConnection(int eventInd, int actionInd);
    void removeConnection(int connectionIndex);
    void eventReceiver(uint32_t evTimeStamp, int evSysTimeStamp, TrodesEvent event);
private slots:
    void eventConverter(QString origin, QString event, TrodesMsg msg);
    void subscribeConverter(int ind);
    void eventlistConverter(QVector<EventDataType> list);
private:
    //void setUpConnections(TrodesModuleNetwork *moduleNet);
    QVector<EventConnection> connections;
    QVector<TrodesEvent> eventList;
    QList<QString> actionList;

    avgCalculator<int> eventLatency;
    int eventSysLatencyToken;

    void verifyConnections();


signals:
    void sig_eventSubscribeRequest(int eventInd);
    void sig_executeAction(int actionIndex, TrodesEvent event);
    void event_received(uint32_t timestamp, int systimestamp, TrodesEvent event);
    void subscribe_event(QString, QString);
    void unsubscribe_event(QString, QString);
    void request_event_list_update();
    void update_event_list(QVector<TrodesEvent>);
};



#endif // EVENTHANDLER_H

#ifndef TRODESDATASTRUCTURES_H
#define TRODESDATASTRUCTURES_H
#include <QVariant>
#include <QVector>
#include <QDebug>
#include <stdint.h>
#include <QTime>
#include "trodesSocketDefines.h"

/* ********** DOCUMENTATION
 * NOTE: if you want to send and receive these data types as signals and slots in your
 *       module, you must declare them in the main.cpp file (after initializing the
 *       QApplication) of that application via the following code:
 *       "qRegisterMetaType<dataStruct>("dataStruct")", where dataStruct is the name of
 *       the class/struct that you want to use.
*/

//TRODESDATATYPE_POSITION specifier flags
/**
enum PPacketType {PPT_NULL, PPT_Header, PPT_2DPos, PPT_Lin, PPT_LinTrack, PPT_Velocity};
enum DataType { DT_NULL, DT_int, DT_qreal, DT_uint8_t, DT_uint32_t, DT_int16_t };
**/


//***********************************************************
//dataSend Class

class dataSend {

public:

    dataSend();
    template <typename T>
    //dataSend(DataType ty, T cont) { init(ty,QVariant(cont),sizeof(T)); }
    dataSend(DataType ty, T cont) { init(ty,QVariant(cont),sizeOfType(ty)); }
    dataSend(const dataSend &obj); //copy constructor

    template <typename T>
    inline void insert(T obj) {container = QVariant(obj); bytes = sizeof(obj); };
    inline void setType(DataType ty) { type = ty; };

    inline QVariant getObj(void) const { return(container); };
    inline DataType getType(void) const { return(type); };
    inline int getSize(void) const { return(bytes); };
    int sizeOfType(DataType ty);

    void printAtr(void);

private:
    DataType type;
    QVariant container;
    int bytes;
    void init(DataType ty, QVariant cont, int by);
};
Q_DECLARE_METATYPE(dataSend);

//***********************************************************
//dataPacket Class

class dataPacket {
public:
    dataPacket();
    dataPacket(PPacketType ty);
    dataPacket(const dataPacket &obj); //copy contructor
    ~dataPacket();

    void insert(dataSend someData);
    inline PPacketType getType(void) const { return(type); };
    inline int dataLength(void) const { return(data.length()); };
    inline int getSize(void) const { return(totalBytes); };
    dataSend getDataAt(int index) const;
    void printAtr(void) const;

private:
    PPacketType type;
    QVector<dataSend> data;
    int totalBytes;
};
Q_DECLARE_METATYPE(dataPacket);  //enables this class in signals

//***********************************************************
//TrodesEvent Class

class TrodesEvent {
    /*
     * The TrodesEvent class holds all information necessary for the Trodes main server to easily store
     * particular event data.
     */
public:
    TrodesEvent();
    TrodesEvent(QString name, QString mod, qint8 modID);
    TrodesEvent(const TrodesEvent &obj); //copy constructor

    inline void setEventName(QString name) { eventName = name; };
    inline void setModuleName(QString mod) { parentModule = mod; };
    inline void setModuleID(qint8 modID) { parentModuleID = modID; };
    inline QString getEventName(void) const {return(eventName);};
    inline QString getParentModule(void) const {return(parentModule);};
    inline qint8 getParentModuleID(void) const {return(parentModuleID);};

    void printEventInfo(void) const;

private:
    QString eventName;
    QString parentModule;
    qint8 parentModuleID;


};
Q_DECLARE_METATYPE(TrodesEvent);

//***********************************************************
//TrodesEventMessage Class

class TrodesEventMessage {
    /*
     * The TrodesEventMessage class was created so that when events are broadcast, a system timer
     * value would also be recorded at the time of broadcast to help facilitate latency testing
     * and also sync events accurately to logged data.
     */
public:
    TrodesEventMessage();
    TrodesEventMessage(QString eventMess);

    inline void setEventMessage(QString eM) {eventMessage = eM;};
    inline void setEventTimeToCur(void) {eventTime = QTime::currentTime().msecsSinceStartOfDay();};

    inline QString getEventMessage(void) {return(eventMessage);};
    inline int getTime(void) {return(eventTime);};

private:
    QString eventMessage;
    int eventTime;
};
Q_DECLARE_METATYPE(TrodesEventMessage);

enum MultiplexedMessageCode : unsigned char
{
    //The first 16 codes are for return messages over radio
    system_hardware_state = 0, //Up to 48 bits indicating specific system states (0 or 1)
    system_specs = 1, //information about the hardware (number of channels, packet size, sampling rate)
    unit_name1 = 2, //user-given name 1
    unit_name2 = 3, //user given name 2
    status_text = 4, //a 6-letter status string
    unit_serial_number = 5, //serial number
    system_status = 6, //standby, writing, etc. Whatever mode the system is currently in. See HeadstageSystemState below.
    battery_status = 7, //percent charge left, voltage, estimated time to end
    battery_health = 8, //info about the battery's health--is it time to replace?
    sd_card_size = 9, //in megabytes
    sd_card_written = 10, //number of packets
    sd_radio_performance = 11, //number of SD packets dropped, percent of radio broadcast answered
    reserved1 = 12,
    reserved2 = 13,
    reserved3 = 14,
    reserved4 = 15

};

enum HeadstageSystemState : unsigned char
{

    ready_idle_notstarted = 0,
    active_logging = 1,
    ready_idle_paused = 2,
    radio_channel_monitoring = 3,
    battery_charging = 4,
    reserved_d = 5,
    reserved_e = 6,
    reserved_f = 7,
    reserved_g = 8,
    reserved_h = 9,
    reserved_i = 10,
    error_card_full = 11,
    error_battery_low = 12,
    error_SD_access_failure = 13,
    error_SD_not_enabled = 14,
    error_front_end_fault = 15,
    error_other_hardware_fault = 16,

};

struct RadioStatusMessage {
    uint32_t hardwareTimestamp;
    uint64_t computerClock;
    char unitID;
    MultiplexedMessageCode dataType;
    uchar data[6];
};

class HeadstageStatusByRadio
{
public:
    HeadstageStatusByRadio();
    void analyzeFullHistory(QList<RadioStatusMessage> log);
    void update(const RadioStatusMessage &message);
    void clear();


    char unitID;
    int16_t systemState;
    QString unitName1;
    QString unitName2;
    int32_t numChannels;
    int32_t packetSize;
    int32_t samplingRate;
    int32_t modelNumber;
    int32_t serialNumber;
    int16_t percentBatteryCharge;
    int32_t batteryVoltage;
    int32_t batteryTimeLeftMinutes;
    int64_t SDCardSizeMB;
    int64_t SDCardWrittenPackets;
    int64_t packetsDropped;
    int16_t percentReceivedRadioBroadcasts;
    QString statusText;


    char SDMounted;
    char SDEnabledForWrite;
    char frontEndOK;

    QList<uint64_t> lastComputerTimeByCode;
    QList<uint32_t> lastHardwareTimeByCode;

};

Q_DECLARE_METATYPE(HeadstageStatusByRadio);



//#include "trodesdatastructures.cpp"
#endif // TRODESDATASTRUCTURES_H

#ifndef ANALYSES_H
#define ANALYSES_H


#include <QBuffer>
#include <QThread>
#include <QQueue>
#include <QVector>
#include <QByteArray>
#include <QAudioFormat>
#include <QPointF>
#include "stdint.h"
#include <math.h>
#include "spectrum.h"
#include "spectrumanalyser.h"



class SpectralAnalysis : public QObject
{
    Q_OBJECT
public:

    SpectralAnalysis(QObject *parent = nullptr);
    ~SpectralAnalysis();

    qint64 totalReadHead;
    qint64 totalWriteHead;

    void addValue(int16_t sample);
    void setSamplingRate(int rate);


private:


    int16_t* dataBuffer;
    qint64 writeHead;
    qint64 readHead;
    qint64 bufferLength;
    int inputFreqHz;
    int analysisWindowSamples;

    void calculateSpectrum();
    int                 m_spectrumBufferLength;
    QByteArray          m_spectrumBuffer;
    SpectrumAnalyser    *m_spectrumAnalyser;
    qint64              m_spectrumPosition;


public slots:
    void resetBuffer();


private slots:
    void spectrumChanged(const FrequencySpectrum &spectrum);

signals:
    void newSpectrum(QList<QPointF> dataPoints);

};


#endif // ANALYSES_H

/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "sourceController.h"
#include "globalObjects.h"

//The SourceController object acts as a wrapper for all threads that aquire data from a data stream.
//The rest of the code can interact with this wrapper regardless of which source is currenly active.

uint32_t currentTimeStamp = 0;

//eegDataBuffer rawData;
QAtomicInteger<quint64> rawDataWritten = 0; //global variable in globalObjects.h
QList <QSemaphore *> rawDataAvailable; //For the stream processors
QSemaphore* sourceDataAvailable; //For the source helper thread
QSemaphore rawDataAvailableForSave;
QMutex rawDataAvailableMutex;
QMutex writeMarkerMutex;
QString playbackFile;
bool playbackFileOpen = false;
int fileDataPos;
int playbackFileSize;
int playbackFileCurrentLocation;
int filePlaybackSpeed;  // 1 is actual speed, 2 is 2X, etc.
uint32_t playbackStartTimeStamp;
uint32_t playbackEndTimeStamp;
extern bool unitTestMode;

//---------------------------------------
//SourceController

SourceController::SourceController(QObject *)
{
  state = 0;
  currentSource = SourceNone;


#ifdef USE_D3XX
  USB3Source = nullptr;
#else
  USBSource = nullptr;
#endif
  waveGeneratorSource = nullptr;
  spikesGeneratorSource = nullptr;
  fileSource = nullptr;
  ethernetSource = nullptr;
  currentSourceObj = nullptr;
}

void SourceController::clearBuffers()
{


    for (int i=0;i < EEG_BUFFER_SIZE*256; i++) {
        rawData.data[i] = 0;
    }

    for (int i=0;i < EEG_BUFFER_SIZE*8; i++) {
        rawData.digitalInfo[i] = 0;
    }
    for (int i=0;i < EEG_BUFFER_SIZE; i++) {
        rawData.timestamps[i] = 0;

        rawData.sysTimestamps[i] = 0;

        rawData.dTime[i] = 0;
    }
    for (int i=0; i < EEG_BUFFER_SIZE*MAXCARGROUPS; i++){
        rawData.carvals[i] = 0;
    }
    rawData.writeIdx = 0;

}

TrodesSourceStatus SourceController::getSourceStatus()
{
    TrodesSourceStatus stat;
    if (currentSourceObj != nullptr) {
        stat = currentSourceObj->getSourceStatus();
    } else {
        stat.streaming = false;
        stat.connected = false;
        stat.largestPacketDrop = 0;
        stat.numPacketsDropped = 0;
        stat.totalDroppedPacketEvents = 0;
        stat.totalUnresponsiveHeadstagePackets = 0;
        stat.unresponsiveHeadstage = false;
        stat.numPacketsProcessed = 0;
    }
    return stat;
}

SourceSettings SourceController::runSourceDiagnistic(DataSource source)
{
    SourceSettings ss;
    QByteArray data;
    HeadstageSettings hsSettings;
    HardwareControllerSettings cnSettings;
    bool dSuccess = false;
    QString devString;

    if (source == DataSource::SourceUSBDAQ) {
        USBDAQInterface *usbSource = new USBDAQInterface(nullptr); //USB2 input
        dSuccess = usbSource->RunDiagnostic(&data, hsSettings, cnSettings);
        delete usbSource;
        devString = "USB2";
    } else if (source == DataSource::SourceEthernet) {
        EthernetInterface *ethernetSource = new EthernetInterface(nullptr); //Ethernet input
        dSuccess = ethernetSource->RunDiagnostic(&data, hsSettings, cnSettings);
        delete ethernetSource;
        devString = "ethernet";
    }


    if (cnSettings.valid) {
        //The diagnostic ran successfully, so now we can look at the data
        ss.sourceFound = true;
        ss.report.append(QString("Device detected on %1.\n\n").arg(devString));
        parseHardwareInfo(ss,data,hsSettings,cnSettings);
    } else {
        ss.sourceFound = false;
        ss.report = QString("No device detected on %1.\n").arg(devString);
    }


    return ss;
}

void SourceController::parseHardwareInfo(SourceSettings &ss, QByteArray& data, HeadstageSettings& hsSettings, HardwareControllerSettings& cnSettings)
{

    uint16_t expectedPacketSize = 0;
    uint16_t actualPacketSize = 0;
    int controllerSampRate;
    int controllerRFChannel;
    bool controllerRFSessionIDMode = false;


    if (cnSettings.valid) {

        ss.report.append("Reported controller settings:\n");
        ss.report.append(QString("    Firmware version: %1.%2.%3\n").arg(cnSettings.majorVersion).arg(cnSettings.minorVersion).arg(cnSettings.patchVersion));
        ss.report.append(QString("    Serial number: %1 %2\n").arg(cnSettings.modelNumber).arg(cnSettings.serialNumber));

        if (cnSettings.ECUDetected) {
            ss.report.append("    ECU detected: yes\n");
        } else {
            ss.report.append("    ECU detected: no\n");
        }

        controllerRFChannel = cnSettings.rfChannel;
        controllerRFSessionIDMode = cnSettings.rfSessionIDMode;

        if (cnSettings.RFDetected) {
            ss.report.append("    Radio transponder detected: yes\n");
            ss.report.append(QString("    Radio transponder channel: %1\n").arg(controllerRFChannel));
            ss.report.append(QString("    Radio session ID mode: %1\n").arg(controllerRFSessionIDMode));
        } else {
            ss.report.append("    Radio transponder detected: no\n");
        }

        ss.report.append(QString("    Combined packet size: %1\n").arg(cnSettings.packetSize));
        expectedPacketSize = cnSettings.packetSize;

        controllerSampRate = (int)cnSettings.samplingRateKhz * 1000;
        ss.report.append(QString("    Controller sampling rate: %1\n").arg(controllerSampRate));


    } else {
        ss.report.append("No reported controller settings.\n");
    }
    ss.report.append("\n");

    int hsSampRate;
    int sensorVersion;
    int hsRFChannel;
    bool hsRFSessionIDMode = false;

    if (hsSettings.valid) {
        ss.report.append("Reported headstage settings:\n");
        ss.report.append(QString("    Firmware version: %1.%2.%3\n").arg(hsSettings.majorVersion).arg(hsSettings.minorVersion).arg(hsSettings.patchVersion));
        ss.report.append(QString("    Secondary firmware version: %1.%2.%3\n").arg(hsSettings.controller_majorVersion).arg(hsSettings.controller_minorVersion).arg(hsSettings.controller_patchVersion));

        ss.report.append(QString("    Serial number: %1 %2\n").arg(hsSettings.hsTypeCode).arg(hsSettings.hsSerialNumber));
        ss.report.append(QString("    Headstage packet size: %1\n").arg(hsSettings.packetSize));
        //expectedPacketSize = hsSettings.packetSize;
        ss.report.append(QString("    Number of channels: %1\n").arg(hsSettings.numberOfChannels));
        hsSampRate = hsSettings.samplingRate;
        ss.report.append(QString("    Headstage sampling rate: %1\n").arg(hsSettings.samplingRate));
        //diagnosticsConsole->append(QString("    Auxilliary bytes per packet: %1").arg(hsSettings.auxbytes));
        sensorVersion = hsSettings.sensorboard_version;
        ss.report.append(QString("    Headstage sensor version: %1\n").arg(sensorVersion));

        hsRFChannel = hsSettings.rfChannel;
        hsRFSessionIDMode = hsSettings.rfSessionIDModeOn;
        if (hsSettings.rfAvailable) {
            ss.report.append(QString("    Headstage radio channel: %1\n").arg(hsRFChannel));
            ss.report.append(QString("    Headstage radio session ID mode: %1\n").arg(hsRFSessionIDMode));

        }

        ss.report.append("\n");
        ss.report.append("Warnings:\n");
        bool warningsMade = false;

        if (cnSettings.RFDetected && hsSettings.rfAvailable) {
            //Check to make sure the radio channels match
            if (hsRFChannel != controllerRFChannel) {
                ss.report.append("    WARNING: The radio channel used by the headstage does not match the radio channel used by the controller. Radio-based communication will not work unless they match.\n");
                warningsMade = true;
            }
            if (hsRFSessionIDMode != controllerRFSessionIDMode) {
                ss.report.append("    WARNING: The radio mode (session ID on/off) used by the headstage does not match the mode used by the controller. Radio-based communication will not work unless they match.\n");
                warningsMade = true;
            }
        }

        if (hsSampRate != controllerSampRate) {
            //check to make sure the sampling rates match
            ss.report.append("    WARNING: The sampling rate set for the headstage does not match the sampling rate set for the controller. The headstage sampling rate will apply.\n");
            warningsMade = true;
        }

        if (!warningsMade) {
            ss.report.append("    None\n");
        }

    } else {
        ss.report.append("No detected headstage.\n");
    }


    if (hsSettings.valid) {
        ss.report.append("\n");
        ss.report.append("Analyzing data stream:\n");


        if (!data.isEmpty()) {

            bool analysisComplete = false;
            bool quadSyncsFound = false;


            if (data.data()[0] == 0x55) {
                // look for next packet start


                for (int i = 1; i < 4095; i++) {
                    if ((data.data()[i] == 0x55) && (data.data()[i*2] == 0x55) && (data.data()[i*3] == 0x55) && (data.data()[i*4] == 0x55)) {
                        // found good candidate for next sync
                        quadSyncsFound = true;

                        ss.report.append(QString("    Packet size: %1 bytes\n").arg(i));
                        actualPacketSize = i;

                        analysisComplete = true;
                    }
                    if (analysisComplete) {
                        break;
                    }
                }
                if (!quadSyncsFound) {
                    ss.report.append(QString("    Error: could not find four evenly spaced sync bytes in data stream. Aborting test.\n"));
                } else {
                    if (expectedPacketSize == actualPacketSize) {
                        ss.report.append("\n");
                        ss.report.append(QString("    Reported and analyzed data in agreement. Test successful.\n"));
                    }
                }

            } else {
                ss.report.append(QString("    Error: data stream did not start with a sync byte. Aborting test.\n"));
            }
        } else if (data.isEmpty()) {
            ss.report.append(QString("    Error: the detected headstage did not stream data. Aborting test.\n"));
        }
    }

}

quint64 SourceController::getTotalDroppedPacketEvents()
{
    quint64 rVal = 0;
    if (currentSourceObj != nullptr) {
        rVal = currentSourceObj->getTotalDroppedPacketEvents();
    }
    return rVal;
}

quint64 SourceController::getTotalUnresponsiveHeadstagePackets()
{
    quint64 rVal = 0;
    if (currentSourceObj != NULL) {
        rVal = currentSourceObj->getTotalUnresponsiveHeadstagePackets();
    }
    return rVal;
}

void SourceController::measureImpedance(HardwareImpedanceMeasureCommand s)
{

    if (currentSourceObj != NULL) {

        //currentSourceObj->SendImpendaceMeasureCommand(s);
        currentSourceObj->MeasureImpedance(s);
    }

}



void SourceController::disconnectFromSource()
{


    if (currentSourceObj != nullptr) {
        currentSourceObj->StopAcquisition();
    }


    writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound and save threads
    writeMarkerMutex.unlock();

    //There might be available semaphores, so we aquire them up

    for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }        
    }


    //Make sure the current time is reset
    currentTimeStamp = 0;
    clearBuffers();

}

void SourceController::connectToSource_Simulation()
{

    numConnectionTries = 0;
    if (currentSourceObj != nullptr) {
        currentSourceObj->StartSimulation();
    }

}

void SourceController::connectToSource() {
    numConnectionTries = 0;
    if (currentSourceObj != nullptr) {
        currentSourceObj->StartAcquisition();
    }

}

void SourceController::setSource(DataSource source, TrodesConfigurationPointers c_ptrs)
{

    conf_ptrs = c_ptrs;
    //first we kill the current source thread
    if (currentSourceObj != nullptr) {


        disconnect(this,SLOT(setSourceState(int)));
        currentSourceObj->CloseInterface();

        //QThread::msleep(200);
        delete currentSourceObj;
        //currentSourceObj->deleteLater();
    }
    writeMarkerMutex.lock();
    rawDataWritten = 0;
    writeMarkerMutex.unlock();
    currentSource = source;

    //MARK: nrRefactor you must modify the 'run' function of each of these source types to send out the current timestamp's data.
    //      The abstract trodes source object should create the publisher socket and handle the sending of data00
    //then we start the new one
    switch (source) {
    case SourceNone:
        //No Source

        currentSourceObj = nullptr;
        emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
        break;
    case SourceFake:
        //generate fake data
        waveGeneratorSource = new simulateDataInterface(nullptr);
        currentSourceObj = waveGeneratorSource;
        currentSourceObj->setWorkspace(conf_ptrs);
        break;
    case SourceFakeSpikes:
        //generate fake data
        spikesGeneratorSource = new simulateSpikesInterface(nullptr);
        currentSourceObj = spikesGeneratorSource;
        currentSourceObj->setWorkspace(conf_ptrs);
        break;
    case SourceFile:
        //stream from file
        fileSource = new fileSourceInterface(conf_ptrs.globalConf->saveDisplayedChanOnly, nullptr);
        currentSourceObj = fileSource;
        currentSourceObj->setWorkspace(conf_ptrs);
        connect(currentSourceObj, SIGNAL(updateSlider(qreal)), this, SIGNAL(updateSlider(qreal)));
        connect(fileSource,SIGNAL(newCurTimestamp(uint32_t)),this,SIGNAL(newTimestamp(uint32_t)));
        connect(this, SIGNAL(jumpFileTo(qreal)), currentSourceObj, SLOT(jumpAcquisition(qreal)));
        connect(currentSourceObj,SIGNAL(acquisitionPaused()), this, SIGNAL(acquisitionPaused()));
        break;
    case SourceEthernet:
        //streaming via ethernet
        ethernetSource = new EthernetInterface(nullptr);
        currentSourceObj = ethernetSource;
        currentSourceObj->setWorkspace(conf_ptrs);
        if (conf_ptrs.hardwareConf != nullptr) {
            currentSourceObj->setECUConnected(conf_ptrs.hardwareConf->ECUConnected);
            currentSourceObj->setAppendSysClock(conf_ptrs.hardwareConf->sysTimeIncluded);
        }
        break;

    case SourceRhythm:
#ifdef RHYTHM
        //streaming via USB with Rhythm API
        currentSourceObj = new RhythmInterface(NULL);
        currentSourceObj->setWorkspace(conf_ptrs);
        if (conf_ptrs.hardwareConf != NULL) {
            currentSourceObj->setECUConnected(conf_ptrs.hardwareConf->ECUConnected);
        }

#endif
        break;

#ifdef USE_D3XX
    case SourceUSB3:
        //streaming via USB3
        USB3Source = new USB3Interface(nullptr);

        currentSourceObj = USB3Source;
        currentSourceObj->setWorkspace(conf_ptrs);
        if (conf_ptrs.hardwareConf != nullptr) {
            currentSourceObj->setECUConnected(conf_ptrs.hardwareConf->ECUConnected);
            currentSourceObj->setAppendSysClock(conf_ptrs.hardwareConf->sysTimeIncluded);
        }
        break;


#else
    case SourceUSBDAQ:
        //streaming via USB
        USBSource = new USBDAQInterface(nullptr);
        USBSource->setSelectedDevice(USBDAQInterface::MCU);       
        currentSourceObj = USBSource;
        currentSourceObj->setWorkspace(conf_ptrs);
        if (conf_ptrs.hardwareConf != nullptr) {
            currentSourceObj->setECUConnected(conf_ptrs.hardwareConf->ECUConnected);
            currentSourceObj->setAppendSysClock(conf_ptrs.hardwareConf->sysTimeIncluded);
        }
        break;

    case SourceDockUSB:

        //dockUSBSource = new DockUSBInterface(nullptr);
        //currentSourceObj = dockUSBSource;

        USBSource = new USBDAQInterface(nullptr);
        USBSource->setSelectedDevice(USBDAQInterface::DockingStation);
        currentSourceObj = USBSource;
        currentSourceObj->setWorkspace(conf_ptrs);
        if (conf_ptrs.hardwareConf != nullptr){
            currentSourceObj->setECUConnected(conf_ptrs.hardwareConf->ECUConnected);
            currentSourceObj->setAppendSysClock(conf_ptrs.hardwareConf->sysTimeIncluded);
        }
        break;
#endif



    }

    if (source != SourceNone) {
        if(currentSourceObj){
            connect(currentSourceObj,SIGNAL(stateChanged(int)),this,SLOT(setSourceState(int)));
            connect(currentSourceObj,SIGNAL(setTimeStamps(uint32_t,uint32_t)), this, SIGNAL(setTimeStamps(uint32_t,uint32_t)));
            connect(currentSourceObj,SIGNAL(headstageSettingsReturned(HeadstageSettings)),this,SLOT(newHeadstageSettings(HeadstageSettings)));
            connect(currentSourceObj,SIGNAL(controllerSettingsReturned(HardwareControllerSettings)),this,SLOT(newControllerSettings(HardwareControllerSettings)));
            connect(currentSourceObj,SIGNAL(newContinuousData(HighFreqDataType)), this, SIGNAL(newContinuousData(HighFreqDataType)));
            connect(currentSourceObj,SIGNAL(timeStampError(bool)),this,SIGNAL(packetSizeError(bool)));
            connect(currentSourceObj,SIGNAL(signal_UnresponsiveHeadstage(bool)),this,SIGNAL(signal_UnresponsiveHeadstage(bool)));
            connect(currentSourceObj,SIGNAL(impedanceValueReturned(int,int)),this,SIGNAL(impedanceValueReturned(int,int)));
            connect(currentSourceObj, &AbstractTrodesSource::newDebugMessage, this, &SourceController::newDebugMessage);
            connect(currentSourceObj, &AbstractTrodesSource::newErrorMessage, this, &SourceController::newErrorMessage);
            connect(currentSourceObj, &AbstractTrodesSource::newConsoleOutputMessage, this, &SourceController::newConsoleOutputMessage);
            connect(currentSourceObj, &AbstractTrodesSource::loggerStatusUpdate, this, &SourceController::loggerStatusUpdate);
            connect(currentSourceObj, SIGNAL(acquisitionStarted()),this,SLOT(StartAcquisition()));
            connect(currentSourceObj,SIGNAL(acquisitionStopped()),this,SLOT(StopAcquisition()));
            connect(currentSourceObj,SIGNAL(SDCardStatus(bool,int,bool,bool)),this,SIGNAL(SDCardStatus(bool,int,bool,bool)));
            connect(currentSourceObj, SIGNAL(deregisterHighFreqData(HighFreqDataType)), this, SIGNAL(deregisterHighFreqData(HighFreqDataType)));
            currentSourceObj->InitInterface();
        }

    }

}

//void SourceController::dummySlot(uint32_t a, uint32_t b){
//    qDebug() << "dummyslot: sourceController received " << a << " " << b << "\n";
//}

void SourceController::pauseSource()
{

    if (currentSource == SourceFile) {
        fileSource->PauseAcquisition();
    }

    /*writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound and save threads
    writeMarkerMutex.unlock();*/

    setSourceState(SOURCE_STATE_PAUSED);
}


void SourceController::StartAcquisition(void)
{
    emit acquisitionStarted(); //tells display thread to start streaming
}

void SourceController::PauseAcquisition(void)
{
    writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound and save threads
    writeMarkerMutex.unlock();

    //There might be available semaphores, so we aquire them up
    /*for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }
    }*/

    emit acquisitionPaused();
}

void SourceController::StopAcquisition(void)
{

    writeMarkerMutex.lock();
    rawDataWritten = 0; //for sound
    writeMarkerMutex.unlock();

    //There might be available semaphores, so we aquire them up


    /*for (int a = 0; a < rawDataAvailable.length(); a++) {
        while (rawDataAvailable[a]->available() > 0) {
            rawDataAvailable[a]->tryAcquire();
        }
    }
    while (sourceDataAvailable->available() > 0) {
        sourceDataAvailable->tryAcquire();
    }*/

    //Make sure the current time is reset
    currentTimeStamp = 0;

    //Make sure the current time is reset (unless we have paused a playback file)
    /*
    if (currentSource == 2) {
        if (!fileSource->filePaused) {
            currentTimeStamp = 0;
        }
    } else {
        currentTimeStamp = 0;
    }*/

    //qDebug() << "SourceController::acquisitionstopped";
    emit acquisitionStopped(); //tells display thread to stop streaming
}

void SourceController::newHeadstageSettings(HeadstageSettings s)
{

    emit headstageSettingsReturned(s);
}

void SourceController::newControllerSettings(HardwareControllerSettings s)
{

    emit controllerSettingsReturned(s);
}

void SourceController::sendFunctionTriggerCommand(int funcNum)
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendFunctionTrigger(funcNum);
    }
}

void SourceController::sendSettleCommand()
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendSettleCommand();
    }
}

void SourceController::sendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState)
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendSettleChannel(byteInPacket, bit, delay, triggerState);
    }
}

void SourceController::setNeuroPixelsSettings(NeuroPixelsSettings s)
{
    if (currentSourceObj != nullptr) {

        //currentSourceObj->StopAcquisition();
        bool success = currentSourceObj->SendNeuroPixelsSettings(s);
        if (!success) {
            qDebug() << "Sending again.";
            success = currentSourceObj->SendNeuroPixelsSettings(s);
        }

        if (success) {
            saveHeadstageSettings();
            //QThread::msleep(1000);
        }
    }
}

void SourceController::saveHeadstageSettings()
{
    if (currentSourceObj != nullptr) {

        currentSourceObj->SendSaveHeadstageSettings();

    }
}


void SourceController::setHeadstageSettings(HeadstageSettings s)
{
    currentHSSettings = s;
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendHeadstageSettings(currentHSSettings);
    }
}

void SourceController::setControllerSettings(HardwareControllerSettings s)
{
    currentControllerSettings = s;
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendControllerSettings(currentControllerSettings);
    }
}

HeadstageSettings SourceController::getHeadstageSettings()
{
    HeadstageSettings s;
    if (currentSourceObj != nullptr) {
        s = currentSourceObj->GetHeadstageSettings();
    }
    return s;
}

HardwareControllerSettings SourceController::getControllerSettings()
{
    HardwareControllerSettings s;
    if (currentSourceObj != nullptr) {
        s = currentSourceObj->GetControllerSettings();
    }
    return s;
}

void SourceController::SetStimulationParams(StimulationCommand s)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling SetStimulationParams";
        qDebug() << "Slot:" << s.getSlot() <<
                    "cathode ntrode/channel:" << s.getCathodeNTrodeID() << s.getCathodeChannel() <<
                    "\nanode ntrode/channel:" << s.getAnodeNTrodeID() << s.getAnodeChannel();
        currentSourceObj->SetStimulationParams(s);
    }
}

void SourceController::SendGlobalStimulationSettings(GlobalStimulationSettings s)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling SendGlobalStimulationSettings";
        if (currentSourceObj->SendGlobalStimulationSettings(s)) {
            emit newGlobalStimSettings(s);
        }
    }
}

void SourceController::SendGlobalStimulationCommand(GlobalStimulationCommand s)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling sendGlobalStimulationCommand";
        qDebug() << "Stim enabled " << s.stimEnabled();
        currentSourceObj->SendGlobalStimulationAction(s);
    }
}

void SourceController::ClearStimulationParams(uint16_t slot)
{
    qDebug() << "Calling ClearStimulationParams for slot" << slot;
    if (currentSourceObj != nullptr) {
        currentSourceObj->ClearStimulationParams(slot);
    }
}

void SourceController::SendStimulationStart(uint16_t slot)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling sendStimulationStart" << slot;
        currentSourceObj->SendStimulationStartSlot(slot);
    }
}

void SourceController::SendStimulationStartGroup(uint16_t group)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling sendStimulationStartGroup" << group;
        currentSourceObj->SendStimulationStartGroup(group);
    }
}

void SourceController::SendStimulationStop(uint16_t slot)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling sendStimulationStop" << slot;
        currentSourceObj->SendStimulationStopSlot(slot);
    }
}

void SourceController::SendStimulationStopGroup(uint16_t group)
{
    if (currentSourceObj != nullptr) {
        qDebug() << "Calling sendStimulationStopGroup" << group;
        currentSourceObj->SendStimulationStopGroup(group);
    }
}

void SourceController::SendECUShortcutMessage(uint16_t function)
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendECUShortcutMessage(function);
    }
}

void SourceController::sendLoggerCommand(unsigned char loggerIndex, unsigned char commandCode, QByteArray data)
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendLoggerCommand(loggerIndex, commandCode, data);
    }
}

void SourceController::connectToSDCard()
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->ConnectToSDCard();
    }
}

void SourceController::enableSDCard()
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->SendSDCardUnlock();
    }
}

void SourceController::reconfigureSDCard(int numChannels)
{
    if (currentSourceObj != nullptr) {
        currentSourceObj->ReconfigureSDCard(numChannels);
    }
}

void SourceController::dataError()
{

    numConnectionTries++;

    if (numConnectionTries < 3) {
        StopAcquisition();
        qDebug() << "Data coming in wrong.... retrying connection.";
        QThread::msleep(100);
        StartAcquisition();
    } else {
        StopAcquisition();

    }

}

void SourceController::noDataComing(bool c)
{
    if (currentSource != SourceFile) {
        if (c) {
            qDebug() << "Error: no data coming from hardware. Timestamp:" << currentTimeStamp;
            /*
        if (!unitTestMode) {
            if (currentSource != SourceFile) {

                QMessageBox messageBox;
                messageBox.critical(0,"Error","No data coming from hardware. Please reset the system and try again.");
                messageBox.setFixedSize(500,200);
                //disconnectFromSource();
            }
        }*/
        } else {
            qDebug() << "Data stream recovered. Timestamp:" << currentTimeStamp;
        }
    }
}

void SourceController::setSourceState(int state)
{
    //pass the state along to the mainWindow
    emit stateChanged(state);
}

void SourceController::waitForThreads()
{
    if (currentSource == 2) {
        //The source is a file
        fileSource->waitForThreads();
    }
}

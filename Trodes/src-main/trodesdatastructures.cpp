#include "trodesdatastructures.h"

//dataSend() *******************************
dataSend::dataSend() {
    DataType t = DT_NULL;
    QVariant v = -1;
    int b = 0;
    init(t,v,b);
}

dataSend::dataSend(const dataSend &obj) {
    init(obj.getType(),obj.getObj(),obj.getSize());
}

/**
template <typename T>
dataSend::dataSend(DataType ty, T cont) {
    init(ty,QVariant(cont),sizeof(T));
}**/

int dataSend::sizeOfType(DataType ty) {
    int retval = 0;
    switch(ty) {
    case DT_int: {
        retval = sizeof(int);
        break;
    }
    case DT_qreal: {
        retval = sizeof(qreal);
        break;
    }
    case DT_uint8_t: {
        retval = sizeof(uint8_t);
        break;
    }
    case DT_uint32_t: {
        retval = sizeof(uint32_t);
        break;
    }
    case DT_int16_t: {
        retval = sizeof(int16_t);
        break;
    }
    default: {
        qDebug() << "WARNING: Bad type size requested. (dataSend::sizeOfType)";
        break;
    }
    }
    return(retval);
}

void dataSend::printAtr(void) {
    qDebug() << "dataSend Attributes:";
    qDebug() << "-Type: " << type;
    qDebug() << "-Container: " << container;
    qDebug() << "-Bytes: " << bytes;
}


void dataSend::init(DataType ty, QVariant cont, int by) {
    type = ty;
    container = cont;
    bytes = by;
}

//dataPacket() *******************************

dataPacket::dataPacket() {
    type = PPT_NULL;
    totalBytes = 0;
}

dataPacket::dataPacket(PPacketType ty) {
    type = ty;
    totalBytes = 0;
}

dataPacket::dataPacket(const dataPacket &obj)  {
    type = obj.type;
    totalBytes = obj.totalBytes;
    for (int i = 0; i < obj.dataLength(); i++) {
        data.append(obj.data.at(i));
    }
}

dataPacket::~dataPacket() {

}

void dataPacket::insert(dataSend someData) {
    data.append(someData);
    totalBytes += someData.getSize();
    //qDebug() << " size: " << totalBytes;
}

dataSend dataPacket::getDataAt(int index) const {
    if (index < data.length() && index >= 0) {
        return(data.at(index));
    }
    else {
        dataSend error = dataSend();
        return(error);
    }

}

void dataPacket::printAtr() const {
    qDebug() << "DataPacket attributes:";
    qDebug() << "-type: " << type;
    qDebug() << "-totalBytes: " << totalBytes;
    qDebug() << "-Contains:";
    for (int i = 0; i < data.length(); i++) {
        qDebug() << " -Item[" << i << "]:";
        //int ty = data.at(i).getType();

        qDebug() << "   -Type: " << data.at(i).getType();
        qDebug() << "   -Container: " << data.at(i).getObj();
        qDebug() << "   -Bytes: " << data.at(i).getSize();
    }

}

TrodesEvent::TrodesEvent() {
    TrodesEvent("","",-1);
}

TrodesEvent::TrodesEvent(QString name, QString mod, qint8 modID) {
    eventName = name;
    parentModule = mod;
    parentModuleID = modID;
}

TrodesEvent::TrodesEvent(const TrodesEvent &obj) {
    eventName = obj.getEventName();
    parentModule = obj.getParentModule();
    parentModuleID = obj.getParentModuleID();
}

void TrodesEvent::printEventInfo(void) const {
    QString event = QString("Event: [%1-(%2)] [%3]").arg(parentModule).arg(parentModuleID).arg(eventName);
    qDebug() << event;
}

TrodesEventMessage::TrodesEventMessage() {
    TrodesEventMessage("");
}

TrodesEventMessage::TrodesEventMessage(QString eventMess) {
    eventMessage = eventMess;
    setEventTimeToCur();
}

//-----------------------------------------
HeadstageStatusByRadio::HeadstageStatusByRadio():
    unitID(-1),
    systemState(-1),
    unitName1(""),
    unitName2(""),
    numChannels(-1),
    packetSize(-1),
    samplingRate(-1),
    modelNumber(-1),
    serialNumber(-1),
    percentBatteryCharge(-1),
    batteryVoltage(-1),
    batteryTimeLeftMinutes(-1),
    SDCardSizeMB(-1),
    SDCardWrittenPackets(-1),
    packetsDropped(-1),
    percentReceivedRadioBroadcasts(-1),
    statusText(""),
    SDMounted(-1),
    SDEnabledForWrite(-1),
    frontEndOK(-1)
{
    for (int i=0;i<16;i++) {
        lastComputerTimeByCode.push_back(0);
        lastHardwareTimeByCode.push_back(0);
    }
}

void HeadstageStatusByRadio::clear()
{
    unitID = -1;
    systemState = -1;
    unitName1 = "";
    unitName2 = "";
    numChannels = -1;
    packetSize = -1;
    samplingRate = -1;
    modelNumber = -1;
    serialNumber = -1;
    percentBatteryCharge = -1;
    batteryVoltage = -1;
    batteryTimeLeftMinutes = -1;
    SDCardSizeMB = -1;
    SDCardWrittenPackets = -1;
    packetsDropped = -1;
    percentReceivedRadioBroadcasts = -1;
    statusText = "";
    SDMounted = -1;
    SDEnabledForWrite = -1;
    frontEndOK = -1;

    lastComputerTimeByCode.clear();
    lastHardwareTimeByCode.clear();
    for (int i=0;i<16;i++) {
        lastComputerTimeByCode.push_back(0);
        lastHardwareTimeByCode.push_back(0);
    }
}

void HeadstageStatusByRadio::analyzeFullHistory(QList<RadioStatusMessage> log)
{
    //Clear the data and rebuild using the entire log. Each data type will end up containing the latest entry for that data type.
    clear();
    for (int i = 0; i < log.length(); i++) {
        update(log.at(i));
    }
}

void HeadstageStatusByRadio::update(const RadioStatusMessage &message)
{
    if (unitID == -1) {
        unitID = message.unitID;
    } else {
        if (unitID != message.unitID) {
            //The message was from a different headstage. Don't add to this log
            return;
        }
    }

    //Remember when each data type was last updated
    if ((unsigned char)message.dataType < 16) {
        lastHardwareTimeByCode[(unsigned char)message.dataType] = message.hardwareTimestamp;
        lastComputerTimeByCode[(unsigned char)message.dataType] = message.computerClock;
    }

    switch (message.dataType) {
    case MultiplexedMessageCode::system_hardware_state: //Up to 48 bits indicating specific system states (0 or 1)
        //First byte...
        SDMounted = bool(message.data[0] & (1 << 0));
        SDEnabledForWrite = bool(message.data[0] & (1 << 1));
        frontEndOK = bool(message.data[0] & (1 << 2));
        break;
    case MultiplexedMessageCode::system_specs: //information abou the hardware (number of channels, packet size, sampling rate)
        numChannels = *(uint16_t*)message.data; //First 2 bytes is the number of channels
        packetSize = *(uint16_t*)message.data + 2; //The next 2 bytes is the pack size in bytes
        samplingRate = *(uint16_t*)message.data + 4; //The next 2 bytes us is the sampling rate
        break;
    case MultiplexedMessageCode::unit_name1: //user-given name 1
        unitName1 = QString().fromLocal8Bit((const char*)message.data,6); //A 6-byte string
        break;
    case MultiplexedMessageCode::unit_name2: //user given name 2
        unitName2 = QString().fromLocal8Bit((const char*)message.data,6); //A 6-byte string
        break;
    case MultiplexedMessageCode::status_text: //a 6-letter status string
        statusText = QString().fromLocal8Bit((const char*)message.data,6); //A 6-byte string
        break;
    case MultiplexedMessageCode::unit_serial_number: //serial number
        modelNumber = *(uint16_t*)message.data; //First 2 bytes are model number
        serialNumber = *(uint16_t*)(message.data+2); //Next 2 bytes are serial number
        break;
    case MultiplexedMessageCode::system_status: //standby, writing, etc. Whatever mode the system is currently in.
        systemState = message.data[0]; //The first byte contains the system's state (256 states can be encoded). The remaining 5 bytes are unused.
        break;
    case MultiplexedMessageCode::battery_status: //percent charge left, voltage, estimated time to end
        percentBatteryCharge = message.data[0]; //first byte is an integer between 0 and 100
        batteryVoltage = *(uint16_t*)(message.data+1); //next two bytes is the voltage in mV
        batteryTimeLeftMinutes = *(uint16_t*)(message.data+3); //next two bytes is the estimated time left in minutes
        break;
    case MultiplexedMessageCode::battery_health: //info about the battery's health--is it time to replace?
        //Not yet implemented
        break;
    case MultiplexedMessageCode::sd_card_size: //in megabytes
        SDCardSizeMB = *(uint32_t*)(message.data); //4 bytes, size is in MB
        break;
    case MultiplexedMessageCode::sd_card_written: //in megabytes
        SDCardWrittenPackets = *(uint32_t*)(message.data); //4 bytes, number of packets written to the card so far
        break;
    case MultiplexedMessageCode::sd_radio_performance: //number of packets dropped, largest continuous drop, etc.
        packetsDropped = *(uint32_t*)(message.data); //4 bytes, total number of packets dropped during the session
        percentReceivedRadioBroadcasts = message.data[4]; //the fifth byte tells the percent of answered RF broadcasts (integer between 0 and 100)
        break;

    default:

        break;

    }


}

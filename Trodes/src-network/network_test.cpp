
//Good for windows memchecks
//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>
#include <iostream>
#include "networkincludes.h"
#include "CZHelp.h"
#include <string>
#include "trodesmsg.h"
#include "TrodesCentralServer.h"
#include "AbstractModuleClient.h"

//! \file Unit test for network wrapper
//!
//! This script should be built in debug mode and run without any problems.
//! It tests the main functionalities of the network code
//!
//!

class TestData : public NetworkDataType{
public:
    TestData(){
        std::string s("id488s1");
        setFormat(s.c_str());
    }
    TestData(int t, const char *u, uint32_t y, uint64_t z, int64_t a,double v, uint8_t o)
        : i(t), d(v), four(y), eight(z), s_eight(a), one(o){
        s = (char*)malloc(strlen(u)+1);
        strcpy(s, u);
        std::string str("id488s1");
        setFormat(str.c_str());
    }
    ~TestData(){
        free(s);
    }

    int i;
    char *s;
    double d;
    uint32_t four;
    uint64_t eight;
    int64_t s_eight;
    uint8_t one;
    char *encode(){
        return CZHelp::encodeData(getFormat(), i, d, four, eight, s_eight, s, one);
    }
    int readByteArr(const char *byteArr){
        return CZHelp::decodeData(byteArr, getFormat(), &i, &d,  &four, &eight, &s_eight, &s, &one);
    }
    void print(){
        std::cout << "int: " << i << ", uint32: " << four << ", uint64: " << eight <<
                     ", int64: " << s_eight << ", double: " << d << ", char*: " << s << ", uint8: " << (int)one << "\n";
    }
};

class TestMlm : public MlmWrap{
public:
    TestMlm(const char *id) : MlmWrap(id){}

    int receiveStream(const char *sender, const char *subject, const char *format, TrodesMsg &content){
        std::string contentt = content.popstr();
        printf("%s: Got stream:: sender: %s, subject: %s, content: %s\n", id.c_str(),
               sender, subject, contentt.c_str());
        return 0;
    }

    int receiveMailbox(const char *sender, const char *subject, const char *format, TrodesMsg &content){
        if(streq(subject, "testdata")){
            TestData d;
            content.transfercontents(format, d);
            d.print();
        }
        else if(streq(format, "sis")){
            std::string a, c;
            int b;
            try{
            content.transfercontents(format, a, b, c);
            } catch(std::string s) {
                std::cout << s << std::endl;
            }
            printf("%s: Got mailbox:: sender: %s, subject: %s, contents: %s %d %s\n",
                   id.c_str(), sender, subject, a.c_str(), b, c.c_str());
        }
        else{
            std::cout << id << ": Got mailbox:: sender: " << sender << ", subject: " << subject <<
                         ", contents: " << content.popstr() << std::endl;
        }
        return 0;
    }
    int receiveService(const char *sender, const char *subject, const char *format, TrodesMsg &content){
        std::string contentt = content.popstr();
        printf("%s: Got request:: sender: %s, subject: %s, content: %s\n", id.c_str(),
               sender, subject, contentt.c_str());
        if(streq(subject, "rand")){
            int r = rand();
            std::string str("   random integer: ");
            str.append(std::to_string(r));
            std::cout << str << std::endl;
        }
        return 0;
    }
};

void trodesmsg_test(){
    try{
        TrodesMsg msg1("sis", "str1", 100, "str2");
        std::string a, b;
        int c;
        TrodesMsg msg2(msg1.copy());
        msg2.transfercontents("sis", a, c, b);
        assert (streq(a.c_str(), "str1"));
        assert (c==100);
        assert (streq(b.c_str(), "str2"));
        assert (msg1.size());
        assert (streq(msg1.getformat().c_str(), "sis"));
        assert (msg2.size() == 0);
        assert (streq(msg2.getformat().c_str(), ""));

        //**********start broker and mlmwraps
        CentralBroker *broker = new CentralBroker(DEFAULT_ENDPOINT);
        TestMlm *mainclient = new TestMlm("main");
        mainclient->initialize();
        TestMlm *recvclient = new TestMlm("recv");
        recvclient->initialize();

        zclock_sleep(25);
        TrodesMsg msg3("sis", "str3", 200, "str4");
        std::cout << "====================sender: main, subject: TestMsg1, contents: str1 100 str2\n";
        mainclient->sendMessage("recv", "TestMsg1", msg1);
        std::cout << "====================sender: main, subject: TestMsg3, contents: str3 200 str4\n";
        mainclient->sendMessage("recv", "TestMsg3", msg3);

        zclock_sleep(25);
        delete recvclient;
        delete mainclient;
        delete broker;
    }catch(std::string e){
        std::cout << "Error in trodesmsg_test: " << e << std::endl;
        exit(1);
    }
}

void mlmwrap_test(){
    try{
        //**********start broker
        CentralBroker *broker = new CentralBroker(DEFAULT_ENDPOINT);

        //**********start malamute wrappers
        //client1: main
        TestMlm *mainclient = new TestMlm("main");
        mainclient->setProducer("TestStream");

        mainclient->initialize();

        TestMlm *secondclient = new TestMlm("second");
        secondclient->setProducer("Stream2");

        secondclient->initialize();
                //client2: recv
        TestMlm *recvclient = new TestMlm("recv");
//        recvclient->subscribeStream("TestStream", "topic1");
        recvclient->provideService("rand");


        recvclient->initialize();


        //**********Test messaging
        //Send message to mailbox of 'recv'
        mainclient->sendMessage("recv", "subjectgoeshere", "", 0, "s", "contentgoeshere", NULL);
        std::cout << "Expecting ==========sender: main, subject: subjectgoeshere, contents: contentgoeshere\n";
        zclock_sleep(25);

        //Send message to mailbox of 'main'
        recvclient->sendMessage("main", "secondsubject", "", 0, "s", "othercontent", NULL);
        std::cout << "Expecting ==========sender: recv, subject: secondsubject, contents: othercontent\n";
        zclock_sleep(25);

        //Service requests still in progress, more complex than i thought
        mainclient->sendRequest("recv", "rand", "track", 2500, "ss", "give me a random number", "asdf");
        std::cout << "Expecting ==========sender: main, subject: rand, content: give me a random number\n";

        zclock_sleep(25);


        //**********Test sending data types over the wire
        int a = 1;
        uint32_t b= 1000000;
        uint64_t c= 1000000000;
        int64_t d= -24;
        double e =3.14;
        uint8_t o = 10;
        TestData object1(a, "teststring", b, c, d, e, o);
        object1.print();
        mainclient->sendMessage("recv", "testdata", "", 0, "n", &object1);
        zclock_sleep(25);

        std::cout << "========Testing Stream=======\n";

        std::string sub = "topic1|topic2";//!--------------------------
        std::cout << "========recv subscribed to " << sub << " ===expecting 2 messages" << "\n";
        recvclient->subscribeStream("TestStream", sub.c_str());

        mainclient->sendStream("topic1", "s", "msg1", NULL);

        zclock_sleep(25);
        mainclient->sendStream("topic2", "s", "msg2", NULL);

        zclock_sleep(25);
        mainclient->sendStream("topic3", "s", "msg3", NULL);

        zclock_sleep(100);


        sub = "topic1|topic3";//!--------------------------
        std::cout << "========recv subscribed to " << sub << "\n";
        recvclient->removeSubscriptions("TestStream");

        recvclient->subscribeStream("TestStream", sub.c_str());

        std::cout << "========recv subscribed to stream2 ===== expecting 3 messages\n";
        recvclient->subscribeStream("Stream2", ".*");

        zclock_sleep(25);

        secondclient->sendStream("topicx", "s", "msg13", NULL);

        zclock_sleep(25);

        mainclient->sendStream("topic1", "s", "msg7", NULL);

        zclock_sleep(25);
        mainclient->sendStream("topic2", "s", "msg8", NULL);

        zclock_sleep(25);
        mainclient->sendStream("topic3", "s", "msg9", NULL);

        zclock_sleep(25);


        sub = "topic3";//!--------------------------
        std::cout << "========recv subscribed to " << sub << "=====Expecting 2 messages" << "\n";
        recvclient->removeSubscriptions("TestStream");

        recvclient->subscribeStream("TestStream", sub.c_str());

        zclock_sleep(25);
        mainclient->sendStream("topic1", "s", "msg4", NULL);

        zclock_sleep(25);
        mainclient->sendStream("topic2", "s", "msg5", NULL);

        zclock_sleep(25);
        mainclient->sendStream("topic3", "s", "msg6", NULL);

        zclock_sleep(25);
        secondclient->sendStream("topicx", "s", "msg13", NULL);
        zclock_sleep(25);

        delete recvclient;
        delete mainclient;
        delete secondclient;
        delete broker;
        std::cout << "deleted\n";
    } catch(std::string e){
        std::cout << e << std::endl;
        exit(1);
    }
}

void trodes_and_abstractmodule_test(){
    try{
        CentralBroker *broker = new CentralBroker(DEFAULT_ADDRESS);
        TrodesCentralServer *trodes = new TrodesCentralServer(TRODES_NETWORK_ID, DEFAULT_ADDRESS, DEFAULT_PORT);
        trodes->start();

        AbstractModuleClient *mod = new AbstractModuleClient("testmodule1", STD_BROKER_ADDR, STD_BROKER_PORT);
        mod->initialize();
        AbstractModuleClient *mod2 = new AbstractModuleClient("testmodule1", STD_BROKER_ADDR, STD_BROKER_PORT);
        mod2->initialize();
        mod->provideEvent("event1");
        mod->provideEvent("event2");
        mod2->subscribeToEvent(mod->getID(), "event1");
        std::cout << "=======Test: Event list: " <<
                     (2 == mod2->requestEventList(mod->getID().c_str()).size() ? "Pass" : "Fail")<< "\n";
        std::cout << "=======Test: Client list: " <<
                     (3 == mod->getClients().size() ? "Pass" : "Fail")<< "\n";
//        std::cout << "=======Test: Sending event1 ... Received: ";
//        mod->sendOutEvent("event1", "ss", "12", "34");

        std::cout << "\n";
        delete mod2;
        zclock_sleep(10);
        std::cout << "=======Test: Client list v2: " <<
                     (2 == mod->getClients().size() ? "Pass" : "Fail")<< "\n";
        delete mod;
        delete trodes;
        delete broker;
    } catch(std::string e){
        std::cout << e << std::endl;
        exit(1);
    }
}

int main(int argc, char **argv){
    std::cout << "starting trodesmsg_test\n";
    trodesmsg_test();
    std::cout << "\nstarting mlmwrap_test\n";
    mlmwrap_test();

    zsys_shutdown();
    //Used for windows memchecks
    //_CrtDumpMemoryLeaks();

    return 0;
}

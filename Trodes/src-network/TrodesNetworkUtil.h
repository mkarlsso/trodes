#pragma once

#include <QObject>

namespace trodes {

/** extract address from networkConf */
inline std::string get_network_address(QString trodesHost) {
    return trodesHost.toStdString();
}

/** extract port from networkConf */
inline int get_network_port(quint16 trodesPort) {
    return trodesPort;
}

}  // namespace trodes


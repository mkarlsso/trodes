#include "rmsplot.h"

RMSPlot::RMSPlot(const TrodesConfiguration &conf, QWidget *parent) : QWidget(parent)
{
    setGeometry(300,300,1000,300);
    setMinimumHeight(300);
    setMinimumWidth(1000);
    setAttribute(Qt::WA_DeleteOnClose);

    workspace = conf;

    /*chart = new QChart;
    chart->setTitle("RMS Noise");
    chart->legend()->hide();

    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    Xaxis = new QBarCategoryAxis;
    Yaxis = new QValueAxis;

    Xaxis->setTitleText("Hardware channel");
    Yaxis->setTitleText("RMS (μV)");

    chart->addAxis(Xaxis, Qt::AlignBottom);
    chart->addAxis(Yaxis, Qt::AlignLeft);*/


    histChart = new ChannelHistogramWidget(workspace);
    histChart->setYUnits("RMS (μV)");
    histChart->setTitle("Noise measures");

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    QGridLayout *controlLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    //plotLayout->addWidget(chartView,0,0);
    plotLayout->addWidget(histChart,0,0);
    //plotLayout->setRowStretch(0,1);
    mainLayout->addLayout(plotLayout,0,0);

    infoLabel = new TrodesClickableLabel();
    infoLabel->setText("i");
    QFont labelFont;
    labelFont.setPixelSize(30);
    labelFont.setItalic(true);
    labelFont.setBold(true);
    labelFont.setFamily("Times");
    infoLabel->setStyleSheet("QLabel { color : blue; }");
    infoLabel->setFont(labelFont);
    //infoLabel->setMaximumHeight(25);
    char myToolTip[] = "<html><head/><body><p>This tool plots the RMS noise values of all channels." \
                "</p></body></html>";

    infoLabel->setToolTip(myToolTip);
    connect(infoLabel,&TrodesClickableLabel::clicked,infoLabel,&TrodesClickableLabel::showToolTip);

    controlLayout->addWidget(infoLabel,0,0);


    stopAutoScaling = new QCheckBox("Use custom max y-axis (μV)");
    customMax = new QSpinBox;
    customMax->setSingleStep(5);
    customMax->setMaximum(5000);
    customMax->setMinimum(5);
    connect(stopAutoScaling, &QCheckBox::toggled, customMax, &QSpinBox::setEnabled);
    stopAutoScaling->setChecked(false);
    customMax->setEnabled(false);

    controlLayout->addWidget(stopAutoScaling, 0,2);
    controlLayout->addWidget(customMax, 0, 3);

    freezePlot = new QCheckBox("Freeze plot");
    controlLayout->addWidget(freezePlot, 0, 4);

    QRadioButton *oneSecBin = new QRadioButton("1s bin");
    QRadioButton *tenSecBin = new QRadioButton("10s bin");
    controlLayout->addWidget(oneSecBin, 1, 2, Qt::AlignRight);
    controlLayout->addWidget(tenSecBin, 1, 3, Qt::AlignRight);
    oneSecBin->setChecked(true);
    connect(oneSecBin, &QRadioButton::pressed, this, &RMSPlot::oneSecBin);
    connect(tenSecBin, &QRadioButton::pressed, this, &RMSPlot::tenSecBin);

    QPushButton *exportData = new QPushButton("Export values");
    connect(exportData, &QPushButton::pressed, this, &RMSPlot::exportDataCSV);
    controlLayout->addWidget(exportData, 1, 4);

    controlLayout->setColumnStretch(1,1);

    mainLayout->addLayout(controlLayout,1,0);

    setLayout(mainLayout);
    mainLayout->setRowStretch(0,1);

}

void RMSPlot::plot(QList<qreal> values){
    if(!freezePlot->isChecked()){

        if(stopAutoScaling->isChecked()){

            histChart->setYMax(true,customMax->value());
        }
        else{
            histChart->setYMax(false,customMax->value());
        }

        this->values.clear();
        for (int nt = 0; nt < workspace.spikeConf.ntrodes.length(); nt++) {
            for (int ch = 0; ch < workspace.spikeConf.ntrodes.at(nt)->hw_chan.length(); ch++) {

                this->values.push_back(values.at(workspace.spikeConf.ntrodes.at(nt)->hw_chan.at(ch)));
            }
        }

        histChart->plot(this->values);


        /*chart->removeAllSeries();
        chart->removeAxis(Xaxis);
        chart->removeAxis(Yaxis);
        data = new QBarSeries;
        QBarSet *set = new QBarSet("");
        set->append(values);
        data->append(set);
        data->setBarWidth(0.5);
        chart->addSeries(data);
        Xaxis = new QBarCategoryAxis;
        Yaxis = new QValueAxis;

        Xaxis->setTitleText("Hardware channel");
        Yaxis->setTitleText("RMS (μV)");

        chart->addAxis(Xaxis, Qt::AlignBottom);
        chart->addAxis(Yaxis, Qt::AlignLeft);
        data->attachAxis(Xaxis);
        data->attachAxis(Yaxis);
        Yaxis->setMin(0.0);*/
    }
    else{
        return;
    }
    if(stopAutoScaling->isChecked()){

        //Yaxis->setMax(customMax->value());
    }
    else{
        //Yaxis->setMax(1.1*Yaxis->max());
    }
}

void RMSPlot::exportDataCSV(){
    QTextEdit *dataCSV = new QTextEdit(this);
    dataCSV->setWindowFlags(Qt::Dialog);
    dataCSV->setAttribute(Qt::WA_DeleteOnClose);
    QString str;
    for(auto const &val : values){
        str += QString::number(val) + " ,";
    }
    dataCSV->setText(str);
    dataCSV->show();
}

void RMSPlot::closeEvent(QCloseEvent *event){
    emit windowClosed();
    QWidget::closeEvent(event);
}

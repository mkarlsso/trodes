#include "rfwidget.h"

RFWidgetTile::RFWidgetTile(QWidget *parent)
    : QWidget(parent)
{

    startButtonDown = false;
    stopButtonDown = true;
    startButtonBlinking = false;
    stopButtonBlinking = false;
    blink = false;
    headstageIndex = 0;

    loggerName = new QLabel(this);
    loggerName->setText(QString("<h3>Logger %1</h3>").arg(0));
    button1 = new TrodesButton(this);
    button1->setText("Record");
    //button1->setCheckable(true);
    button1->setDown(false);
    button2 = new TrodesButton(this);
    button2->setText("Pause");
    //button2->setCheckable(true);
    button2->setDown(true);

    button3 = new TrodesButton(this);
    button3->setText("Ping");

    connect(button1, &TrodesButton::clicked,this,&RFWidgetTile::startButtonPressed);
    connect(button2, &TrodesButton::clicked,this,&RFWidgetTile::stopButtonPressed);
    connect(button3, &TrodesButton::clicked,this,&RFWidgetTile::pingButtonPressed);
    loggerInfo = new QLabel(this);
    infoFrame = new QFrame(this);
    infoFrame->setObjectName("LoggerInfoFrame");
    infoFrame->setStyleSheet("QFrame#LoggerInfoFrame {border: 2px solid lightgray;border-radius: 5px}");
    flayout = new QGridLayout(this);
    flayout->addWidget(loggerName,0,0,1,2);
    flayout->addWidget(button1,1,0);
    flayout->addWidget(button2,1,1);
    flayout->addWidget(button3,2,0);
    flayout->addWidget(loggerInfo,3,0,1,2);
    infoFrame->setLayout(flayout);

    QGridLayout *ml = new QGridLayout(this);
    ml->addWidget(infoFrame);
    setLayout(ml);

    connect(&blinkTimer, SIGNAL(timeout()), this, SLOT(toggleBlink()));
    blinkColor = QColor("red");

}

void RFWidgetTile::setHeadstageIndex(int index)
{
    headstageIndex = index;
    loggerName->setText(QString("<h3>Logger %1</h3>").arg(index+1)); //1-based numbering
}

void RFWidgetTile::updateDeviceStatus(HeadstageStatusByRadio info)
{
    LoggerStatusDisplayContent dc = parseInfo(info);

    loggerInfo->setText(QString(
                                      "<hr/>"
                                      "<table>"
                                      "<tr><td>Device name: </td><td>%2</td></tr>"
                                      "<tr><td>Last wireless update: </td><td>%3</td></tr>"
                                      "<tr><td>Serial number: </td><td>%4</td></tr>"
                                      "<tr><td># Channels: </td><td>%5</td></tr>"
                                      "<tr><td>Sampling rate: </td><td>%6</td></tr>"
                                      "</table>"
                                      "<table>"
                                      "<tr><td>Device status: </td><td>%7</td></tr>"
                                      "<tr><td>SD written: </td><td>%8</td></tr>"
                                      "<tr><td>Packets dropped: </td><td>%9</td></tr>"
                                      "<tr><td>Battery charge: </td><td>%10</td></tr>"
                                      "<tr><td>Radio response rate: </td><td>%11</td></tr>"
                                      "</table>"
                                      "<hr/>"
                                      )
                                      .arg(dc.name)
                                      .arg(dc.lastUpdate)
                                      .arg(dc.model)
                                      .arg(dc.numchan)
                                      .arg(dc.samplingRate)
                                      .arg(dc.status)
                                      .arg(dc.SDCardSpaceInfo)
                                      .arg(dc.packetsDropped)
                                      .arg(dc.percentBatteryCharge)
                                      .arg(dc.percentReceivedRadioBroadcasts)

                                  );

}

LoggerStatusDisplayContent RFWidgetTile::parseInfo(HeadstageStatusByRadio info)
{
    LoggerStatusDisplayContent output;


    QString bl = "- - - - - - - - - - - - - - -";
    output.name = bl;
    output.model = bl;
    output.lastUpdate = bl;
    output.numchan = bl;
    output.samplingRate = bl;
    output.status = bl;
    output.percentBatteryCharge = bl;
    output.SDCardSpaceInfo = bl;
    output.batteryTimeLeftMinutes = bl;
    output.packetsDropped = bl;
    output.percentReceivedRadioBroadcasts = bl;
    output.statusText = bl;

    uint64_t lastUpdateTime = 0;
    for (int u = 0; u < info.lastComputerTimeByCode.length(); u++) {
        if (info.lastComputerTimeByCode.at(u) > lastUpdateTime) {
            lastUpdateTime = info.lastComputerTimeByCode.at(u);
        }
    }
    if (lastUpdateTime > 0) {
        uint64_t tSinceUpdate = QDateTime::currentMSecsSinceEpoch() - lastUpdateTime;
        uint64_t minSinceUpdate = tSinceUpdate/(1000*60);
        uint64_t secSinceUpdate = (tSinceUpdate/1000)-(minSinceUpdate*60);

        if (minSinceUpdate == 0) {
            output.lastUpdate = QString("%1 sec ago").arg(secSinceUpdate);
        } else if (minSinceUpdate >= 1) {
            output.lastUpdate = QString("%1 min %2 sec ago").arg(minSinceUpdate).arg(secSinceUpdate);
        }
    }

    //Concatenate the two names into one string
    if (!(info.unitName1).isEmpty()) {
        output.name = info.unitName1+info.unitName2;
    }

    //The model number is actually model and serial number
    if (info.modelNumber > -1) {
        output.model = QString("%1 %2").arg(info.modelNumber).arg(info.serialNumber);
    }

    if (info.numChannels > -1) {
        output.numchan = QString("%1").arg(info.numChannels);
    }

    if (info.samplingRate > -1) {
        output.samplingRate = QString("%1 Hz").arg(info.samplingRate);
    }

    //Update the button state depending on what comes in.
    if (info.systemState > -1) {
        switch ((unsigned char)info.systemState) {
        case HeadstageSystemState::ready_idle_notstarted:
            output.status = "Ready";
            setButtonState(0,0);
            setButtonState(1,1);
            setblink(false);
            break;
        case HeadstageSystemState::active_logging:
            output.status = "Logging";
            setButtonState(1,1);
            setButtonState(0,0);
            setblink(false);
            break;
        case HeadstageSystemState::ready_idle_paused:
            output.status = "Paused";
            setButtonState(0,0);
            setButtonState(1,1);
            setblink(false);
            break;
        case HeadstageSystemState::radio_channel_monitoring:
            output.status = "Monitoring";
            setButtonState(0,0);
            setButtonState(0,0);
            setblink(false);
            break;
        case HeadstageSystemState::battery_charging:
            output.status = "Charging";
            setButtonState(0,0);
            setButtonState(0,0);
            setblink(false);
            break;
        case HeadstageSystemState::error_card_full:
            output.status = "Error--SD card full";
            setButtonState(1,0);
            setButtonState(2,0);
            setblink(true);
            break;
        case HeadstageSystemState::error_battery_low:
            output.status = "Error--battery too low";
            setButtonState(1,0);
            setButtonState(2,0);
            setblink(true);
            break;
        case HeadstageSystemState::error_SD_access_failure:
            output.status = "Error--SD card access failure";
            setButtonState(1,0);
            setButtonState(2,0);
            setblink(true);
            break;
        case HeadstageSystemState::error_SD_not_enabled:
            output.status = "Error--SD card not enabled for writing";
            setButtonState(1,0);
            setButtonState(2,0);
            break;
        case HeadstageSystemState::error_front_end_fault:
            output.status = "Error--hardware malfunction with front end chip";
            setButtonState(1,0);
            setButtonState(2,0);
            setblink(true);
            break;
        case HeadstageSystemState::error_other_hardware_fault:
            output.status = "Error--hardware malfunction other";
            setButtonState(1,0);
            setButtonState(2,0);
            setblink(true);
            break;
        default:
            output.status = "Unknown";
            setButtonState(0,0);
            setButtonState(0,0);
            setblink(false);
            break;

        }
    } else {
        //setButtonState(0,0);
        //setButtonState(1,1);
    }

    if (info.percentBatteryCharge > -1) {
        output.percentBatteryCharge = QString("%1 percent").arg(info.percentBatteryCharge);
    }

    if ((info.SDCardSizeMB > -1) && (info.SDCardWrittenPackets > -1) && (info.packetSize > -1)) {
        double GBused = ((double)info.packetSize * (double)info.SDCardWrittenPackets)/1000000000.0;
        double GBtotal = ((double)info.SDCardSizeMB)/1000;
        output.SDCardSpaceInfo = QString("%1GB out of %2GB").arg(GBused).arg(GBtotal);
    }

    if (info.batteryTimeLeftMinutes > -1) {
        output.batteryTimeLeftMinutes = QString("%1 minutes").arg(info.batteryTimeLeftMinutes);
    }

    if (info.packetsDropped > -1) {
        output.packetsDropped = QString("%1").arg(info.packetsDropped);
    }

    if (info.percentReceivedRadioBroadcasts > -1) {
        output.percentReceivedRadioBroadcasts = QString("%1 percent").arg(info.percentReceivedRadioBroadcasts);
    }


    return output;
}

void RFWidgetTile::startButtonPressed()
{
    if (!startButtonDown && !startButtonBlinking) {

        //Set start button to blink
        setButtonState(0,2);

        //Set stop button to up
        setButtonState(1,0);

        emit startRequested(headstageIndex);
    } else if (startButtonDown) {

        //Make sure it stays down
        setButtonState(0,1);
    }
}

void RFWidgetTile::stopButtonPressed()
{

    if (!stopButtonDown && !stopButtonBlinking) {

        //Set stop button to blink
        setButtonState(1,2);

        //Set start button to up
        setButtonState(0,0);

        emit stopRequested(headstageIndex);
    } else if (stopButtonDown) {

        //Make sure it stays down
        setButtonState(1,1);
    }
}

void RFWidgetTile::pingButtonPressed()
{
    emit pingRequested(headstageIndex);
}


void RFWidgetTile::setButtonState(int button, int state)
{
    TrodesButton* buttonPtr;
    bool* blinkingPtr;
    bool* downPtr;
    if (button ==0) {
        //Start (record) button
        buttonPtr = button1;
        blinkingPtr = &startButtonBlinking;
        downPtr = &startButtonDown;
    } else if (button == 1) {
        //Stop button
        buttonPtr = button2;
        blinkingPtr = &stopButtonBlinking;
        downPtr = &stopButtonDown;
    } else {
        return;
    }

    if (state == 0) {
        //Not down or blinking
        buttonPtr->setDown(false);
        buttonPtr->setblink(false);
        *downPtr = false;
        *blinkingPtr = false;

    } else if (state == 1) {
        //Down
        buttonPtr->setDown(true);
        buttonPtr->setblink(false);
        *downPtr = true;
        *blinkingPtr = false;

    } else if (state == 2) {
        //Blinking to indicate that it is waiting for reponse
        buttonPtr->setDown(false);
        buttonPtr->setblink(true,500,QColor("black"));
        *downPtr = false;
        *blinkingPtr = true;

    } else if (state == 3) {
        //Blinking to indicate error
        buttonPtr->setDown(false);
        buttonPtr->setblink(true,500,QColor("red"));
        *downPtr = false;
        *blinkingPtr = true;
    }

}

void RFWidgetTile::setblink(bool blinkOn, int msec, QColor color){
    if(blinkOn){

        blinkColor = color;
        blinkTimer.start(msec);
    }
    else{
        blinkTimer.stop();
        blink = false;
        infoFrame->setStyleSheet("QFrame#LoggerInfoFrame {border: 2px solid lightgray;border-radius: 5px}");
    }
}

void RFWidgetTile::toggleBlink(){
    if(!blink){
        infoFrame->setStyleSheet("QFrame#LoggerInfoFrame {border: 2px solid " + blinkColor.name() + ";"
                                                                                                    "border-radius: 5px;}");

        blink = true;
    }
    else{
        infoFrame->setStyleSheet("QFrame#LoggerInfoFrame {border: 2px solid lightgray;border-radius: 5px}");
        blink = false;
    }
}

//---------------------------------------------------------------------

RFWidget::RFWidget(QWidget *parent)
    : QWidget(parent)
{

    setGeometry(300,300,500,300);
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Logger status");

    currentLoggerIndex = 0;
    infolayout = new QGridLayout;

    for (int i=0; i < 8; i++) {
        statList.push_back(HeadstageStatusByRadio());
        RFWidgetTile *t = new RFWidgetTile(this);
        t->setHeadstageIndex(i);
        tiles.push_back(t);
        infolayout->addWidget(t, i/4, i%4, Qt::AlignLeft);
        connect(t,&RFWidgetTile::startRequested,this,&RFWidget::startRequested);
        connect(t,&RFWidgetTile::stopRequested,this,&RFWidget::stopRequested);
        connect(t,&RFWidgetTile::pingRequested,this,&RFWidget::pingRequested);
    }

    setLayout(infolayout);
    updateDeviceInfo();

    connect(&updateTimer, &QTimer::timeout,this,&RFWidget::updateDeviceInfo);
    updateTimer.start(5000);
}

void RFWidget::closeEvent(QCloseEvent *event){
    emit windowClosed();
    QWidget::closeEvent(event);
}


void RFWidget::newLoggerStatusUpdate(QList<HeadstageStatusByRadio> s)
{
    //qDebug() << "Got logger update";
    statList = s;
    updateDeviceInfo();

}

void RFWidget::updateDeviceInfo()
{
    for (int i=0;i<8;i++) {
        tiles[i]->updateDeviceStatus(statList[i]);
    }
}


/*Outgoing command codes
-------------------------
0: reset recording to beginning
1: record
2: pause
3: ping

*/
void RFWidget::startRequested(int loggerIndex)
{
    emit loggerCommand((unsigned char)loggerIndex, 1, QByteArray());
}

void RFWidget::stopRequested(int loggerIndex)
{
    emit loggerCommand((unsigned char)loggerIndex, 2, QByteArray());
}

void RFWidget::pingRequested(int loggerIndex)
{
    emit loggerCommand((unsigned char)loggerIndex, 3, QByteArray());
}

#include "quicksetup.h"

HardwareSettingsDisplay::HardwareSettingsDisplay(HardwareControllerSettings mcu, HeadstageSettings hs, QWidget *parent)
    : QWidget(parent)
{
    //**************************************************
    //MCU Settings
    //**************************************************
    mcusettingsbox = new QGroupBox("MCU Settings");
    QGridLayout *settingslayout = new QGridLayout;
    mcusettingsbox->setLayout(settingslayout);


    QLabel *mcuvlabel = new QLabel("MCU version number:");
    QLabel *MCUVersion = new QLabel(QString::number(mcu.majorVersion)+"."+QString::number(mcu.minorVersion));
    settingslayout->addWidget(mcuvlabel, 1, 0);
    settingslayout->addWidget(MCUVersion, 1, 1, Qt::AlignRight);

    QLabel *mcumodellabel = new QLabel("Model number: ");
    QLabel *MCUModel= new QLabel(QString::number(mcu.modelNumber));
    settingslayout->addWidget(mcumodellabel, 2, 0);
    settingslayout->addWidget(MCUModel, 2, 1, Qt::AlignRight);

    QLabel *mcuseriallabel = new QLabel("Serial number: ");
    QLabel *MCUSerial = new QLabel(QString::number(mcu.serialNumber));
    settingslayout->addWidget(mcuseriallabel, 3, 0);
    settingslayout->addWidget(MCUSerial, 3, 1, Qt::AlignRight);

    QLabel *mcusamplinglabel = new QLabel("Sampling rate: ");
    QLabel *MCUSamplingRate = new QLabel(QString::number(mcu.samplingRateKhz) + " kHz");
    settingslayout->addWidget(mcusamplinglabel, 4, 0);
    settingslayout->addWidget(MCUSamplingRate, 4, 1, Qt::AlignRight);


    QLabel *mcurfchanlabel = new QLabel("RF Channel");
    QLabel *MCURfChannel = new QLabel(QString::number(mcu.rfChannel));
    settingslayout->addWidget(mcurfchanlabel, 5, 0);
    settingslayout->addWidget(MCURfChannel, 5, 1, Qt::AlignRight);

//    QLabel *mcupsizelabel = new QLabel("Packet size");
//    QLabel *MCUPacketSize = new QLabel(QString("%1 bytes").arg(mcu.packetSize));
//    settingslayout->addWidget(mcupsizelabel, 6, 0);
//    settingslayout->addWidget(MCUPacketSize, 6, 1, Qt::AlignRight);
    //**************************************************
    //HS Settings
    //**************************************************
    hssettingsbox = new QGroupBox("Headstage Settings");
    QGridLayout *settingslayouths = new QGridLayout;
    hssettingsbox->setLayout(settingslayouths);

    QLabel *hsvlabel = new QLabel("Headstage version number:");
    QLabel *HSVersion = new QLabel(QString::number(hs.majorVersion)+"."+QString::number(hs.minorVersion)+"."+QString::number(hs.patchVersion));
    settingslayouths->addWidget(hsvlabel, 1, 0);
    settingslayouths->addWidget(HSVersion, 1, 1, Qt::AlignRight);

    QLabel *hsmodel= new QLabel("Model number: ");
    QLabel *HSModel = new QLabel(QString::number(hs.hsTypeCode));
    settingslayouths->addWidget(hsmodel, 2, 0);
    settingslayouths->addWidget(HSModel, 2, 1, Qt::AlignRight);

    QLabel *hsseriallabel = new QLabel("Serial number: ");
    QLabel *HSSerial = new QLabel(QString::number(hs.hsSerialNumber));
    settingslayouths->addWidget(hsseriallabel, 3, 0);
    settingslayouths->addWidget(HSSerial, 3, 1, Qt::AlignRight);

    QLabel *hsnumchanlabel = new QLabel("Number of channels: ");
    QLabel *HSNumChan = new QLabel(QString::number(hs.numberOfChannels));
    settingslayouths->addWidget(hsnumchanlabel, 4, 0);
    settingslayouths->addWidget(HSNumChan, 4, 1, Qt::AlignRight);

    QLabel *hsrfchanlabel = new QLabel("Assigned RF Channel: ");
    QLabel *hsrfchan = new QLabel(QString::number(hs.rfChannel));
    settingslayouths->addWidget(hsrfchanlabel, 5, 0);
    settingslayouths->addWidget(hsrfchan, 5, 1, Qt::AlignRight);

    QLabel *hsaccelavailon = new QLabel("Accel sensor: ");
    QLabel *HSAccel = new QLabel(QString("%1")
                                 .arg((hs.accelSensorOn)?"on":"off"));
    settingslayouths->addWidget(hsaccelavailon, 6, 0);
    settingslayouths->addWidget(HSAccel, 6, 1, Qt::AlignRight);

    QLabel *hsgyroavailon = new QLabel("Gyro sensor: ");
    QLabel *HSGyro = new QLabel(QString("%1")
                                 .arg((hs.gyroSensorOn)?"on":"off"));
    settingslayouths->addWidget(hsgyroavailon, 7, 0);
    settingslayouths->addWidget(HSGyro, 7, 1, Qt::AlignRight);

    QLabel *hsmagavailon = new QLabel("Magnetometer: ");
    QLabel *HSMag = new QLabel(QString("%1")
                                 .arg((hs.magSensorOn)?"on":"off"));
    settingslayouths->addWidget(hsmagavailon, 8, 0);
    settingslayouths->addWidget(HSMag, 8, 1, Qt::AlignRight);

    QLabel *hssamplerate = new QLabel("Sample rate: ");
    QLabel *HSSampleRate = new QLabel(QString("%1 hz").arg(hs.samplingRate));
    settingslayouths->addWidget(hssamplerate, 9, 0);
    settingslayouths->addWidget(HSSampleRate, 9, 1, Qt::AlignRight);



    QLabel *hssamplesize = new QLabel("Sample size: ");
    //QLabel *HSSampleSize = new QLabel(QString("%1 bits").arg( (hs.sample12bitOn ? 12 : 16) ));
    QLabel *HSSampleSize = new QLabel(QString("%1 bits").arg( (hs.sampleSizeBits) ));
    settingslayouths->addWidget(hssamplesize, 10, 0);
    settingslayouths->addWidget(HSSampleSize, 10, 1, Qt::AlignRight);

    //**************************************************
    //Finishing up dialog display
    //**************************************************
    QHBoxLayout *settingsboxeslayout = new QHBoxLayout;
    settingsboxeslayout->addWidget(mcusettingsbox);
    settingsboxeslayout->addWidget(hssettingsbox);
    setLayout(settingsboxeslayout);

    //**************************************************
    //Modify dialog based on settings
    //**************************************************
    if(mcu.majorVersion==0 && mcu.minorVersion==0){
        MCUVersion->setText("-");
    }
    if(mcu.modelNumber==0){
        MCUModel->setText("-");
    }
    if(mcu.serialNumber==0 /*|| mcu.serialNumber==(uint16_t)(-1)*/){
        MCUSerial->setText("-");
    }
    if(mcu.samplingRateKhz == 0){
        MCUSamplingRate->setText("-");
    }
    if(hs.majorVersion==0 && hs.minorVersion==0){
        HSVersion->setText("-");
    }
    if(hs.hsSerialNumber==0/* || hs.hsSerialNumber==(uint16_t)(-1)*/){
        HSSerial->setText("-");
    }
    if(hs.numberOfChannels == 0){
        HSNumChan->setText("-");
    }
    if(!mcu.valid){
        //no mcu detected, can't do any setup
        mcusettingsbox->setEnabled(false);
        mcusettingsbox->setTitle("MCU Settings (None detected)");
        mcusettingsbox->setStyleSheet("QGroupBox  {color: red;}");
    }
    if(!hs.valid){
        //Perhaps no headstage means rf is connected
        hssettingsbox->setEnabled(false);
        hssettingsbox->setTitle("Headstage Settings (None detected)");
        hssettingsbox->setStyleSheet("QGroupBox  {color: red;}");
       // HSSampleSize->setText("0 bits");

        HSVersion->setText("-");
        HSModel->setText("-");
        HSSerial->setText("-");
        HSNumChan->setText("-");
        hsrfchan->setText("-");
        HSAccel->setText("-");
        HSGyro->setText("-");
        HSMag->setText("-");
        HSSampleRate->setText("-");
        HSSampleSize->setText("-");

    }


}

QuickSetup::QuickSetup(HardwareControllerSettings mcu, HeadstageSettings hs, int measuredPacketSize, QWidget *parent) : QDialog (parent)
{
    psize = measuredPacketSize;
    if(mcu.samplingRateKhz)
        samplingRate = mcu.samplingRateKhz*1000;
    else
        samplingRate = 30000;

    setWindowModality(Qt::WindowModal);
    setWindowTitle("QuickStart Setup");
    setAttribute(Qt::WA_DeleteOnClose);

    hardwaresettingsdisplay = new HardwareSettingsDisplay(mcu, hs);

    //**************************************************
    //Critical options for workspace creation
    //**************************************************
    QGridLayout *optionslayout = new QGridLayout;

    ECUcheckbox = new QCheckBox("ECU connected");

    RFcheckbox = new QCheckBox("RF connected");

    chansperntrode = new QComboBox();
    chansperntrode->addItems({"1", "2", "4", "8", "16"});
    chansperntrode->setCurrentIndex(2);
    chansperntrode->setFixedWidth(50);
    QLabel *cpnlabel = new QLabel("Channels per nTrode");

    optionslayout->addWidget(ECUcheckbox, 0,0);
    optionslayout->addWidget(RFcheckbox, 1,0);
    optionslayout->addWidget(cpnlabel, 2, 0);
    optionslayout->addWidget(chansperntrode, 2, 1,Qt::AlignLeft);
//    optionslayout->setColumnStretch(1, 1);

    //**************************************************
    //Ok/Cancel buttons
    //**************************************************
    QHBoxLayout *finishbuttonslayout = new QHBoxLayout;

    QPushButton *finishedbutton = new QPushButton("Launch");
    finishedbutton->setDefault(true);
    connect(finishedbutton, &QPushButton::released, this, &QuickSetup::DoneClicked);
    QPushButton *cancelbutton = new QPushButton("Cancel");
    connect(cancelbutton, &QPushButton::released, this, &QuickSetup::CancelPressed);
    connect(cancelbutton, &QPushButton::released, this, &QuickSetup::close);
    QPushButton *advancedbutton = new QPushButton("Advanced");
    connect(advancedbutton, &QPushButton::released, this, [this](){
        emit this->OpenWorkspaceEditor(ECUcheckbox->isChecked(), RFcheckbox->isChecked(), chansperntrode->currentText().toInt(), psize, samplingRate);
        close();
    });
    connect(this, &QuickSetup::rejected, this, &QuickSetup::CancelPressed);

    finishbuttonslayout->addWidget(cancelbutton, 0, Qt::AlignLeft);
    finishbuttonslayout->addWidget(new QWidget, 1);//dummy widget to fill in space
    finishbuttonslayout->addWidget(advancedbutton, 0, Qt::AlignRight);
    finishbuttonslayout->addWidget(finishedbutton, 0, Qt::AlignRight);

    //**************************************************
    //Finishing up dialog display
    //**************************************************



    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);

    layout->addWidget(hardwaresettingsdisplay);
    layout->addLayout(optionslayout);
    layout->addLayout(finishbuttonslayout);

    show();
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    //**************************************************
    //Modify dialog based on settings
    //**************************************************
    if(mcu.majorVersion == 3 && mcu.minorVersion<11 && !hs.valid){
        //Perhaps no headstage means rf is connected
        RFcheckbox->setChecked(true);
    }
    if(mcu.RFDetected){
        RFcheckbox->setChecked(true);
    }
    if(mcu.ECUDetected){
        ECUcheckbox->setChecked(true);
    }
}

void QuickSetup::DoneClicked(){
    emit CreateWorkspace(ECUcheckbox->isChecked(), RFcheckbox->isChecked(), chansperntrode->currentText().toInt(), psize, samplingRate);
    accept();
//    close();
}

void QuickSetup::hideMCUSettings(){
    hardwaresettingsdisplay->mcusettingsbox->setVisible(false);
}

void QuickSetup::hideHSSettings(){
    hardwaresettingsdisplay->hssettingsbox->setVisible(false);
}

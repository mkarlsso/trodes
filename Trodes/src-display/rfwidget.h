#ifndef RFWIDGET_H
#define RFWIDGET_H

#include <QtWidgets>
#include "trodesdatastructures.h"
#include "sharedtrodesstyles.h"

//#include "dockusbthread.h"

/*char unitID;
int16_t systemState;
QString unitName1;
QString unitName2;
int32_t numChannels;
int32_t packetSize;
int32_t samplingRate;
int32_t modelNumber;
int32_t serialNumber;
int16_t percentBatteryCharge;
int32_t batteryVoltage;
int32_t batteryTimeLeftMinutes;
int64_t SDCardSizeMB;
int64_t SDCardWrittenPackets;
int64_t packetsDropped;
int16_t percentReceivedRadioBroadcasts;
QString statusText;


char SDMounted;
char SDEnabledForWrite;
char frontEndOK;

QList<uint64_t> lastComputerTimeByCode;
QList<uint32_t> lastHardwareTimeByCode;*/

struct LoggerStatusDisplayContent {
    QString name;
    QString lastUpdate;
    QString model;
    QString numchan;
    QString samplingRate;

    QString status;
    QString percentBatteryCharge;
    QString SDCardSpaceInfo;

    QString batteryTimeLeftMinutes;

    QString packetsDropped;
    QString percentReceivedRadioBroadcasts;
    QString statusText;

};

class RFWidgetTile : public QWidget
{
    Q_OBJECT
public:
    explicit RFWidgetTile(QWidget *parent = nullptr);
    void setHeadstageIndex(int index);
    QLabel* loggerName;
    TrodesButton* button1;
    TrodesButton* button2;
    TrodesButton* button3;
    QLabel* loggerInfo;
    QFrame* infoFrame;
    QGridLayout* flayout;

public slots:

    void updateDeviceStatus(HeadstageStatusByRadio info);
    void startButtonPressed();
    void stopButtonPressed();
    void pingButtonPressed();

private slots:
    void toggleBlink();

private:

    void setButtonState(int button, int state);
    LoggerStatusDisplayContent parseInfo(HeadstageStatusByRadio info);
    void setblink(bool blink, int msec = 500, QColor color = QColor("red"));

    bool startButtonDown;
    bool stopButtonDown;
    bool startButtonBlinking;
    bool stopButtonBlinking;
    int headstageIndex;
    QTimer blinkTimer;
    QColor blinkColor;
    bool blink;

signals:

    void startRequested(int index);
    void stopRequested(int index);
    void pingRequested(int index);

};

class RFWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RFWidget(QWidget *parent = nullptr);

signals:
    void windowClosed();
    void loggerCommand(unsigned char loggerIndex, unsigned char commandCode, QByteArray data);

public slots:
    void newLoggerStatusUpdate(QList<HeadstageStatusByRadio> s);

private slots:
    void startRequested(int loggerIndex);
    void stopRequested(int loggerIndex);
    void pingRequested(int loggerIndex);
    void updateDeviceInfo();


private:
    void closeEvent(QCloseEvent* event) override;
    QTimer updateTimer;


    QList<HeadstageStatusByRadio> statList;
    QList<RFWidgetTile*> tiles;
    int currentLoggerIndex;
    QGridLayout *infolayout;



};

#endif // RFWIDGET_H

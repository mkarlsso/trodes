#include "sharedtrodesstyles.h"


TrodesClickableLabel::TrodesClickableLabel(QWidget *parent, Qt::WindowFlags f) {
    ntIndex = 0;
}

void TrodesClickableLabel::mousePressEvent(QMouseEvent *mouseEvent) {

    if (mouseEvent->button() == Qt::LeftButton) {
        //emit clicked(hwChan);
        emit clicked(ntIndex, mouseEvent->modifiers());
    } else if (mouseEvent->button() == Qt::RightButton) {
        emit clicked(ntIndex, mouseEvent->modifiers());
        emit rightClicked(ntIndex);
    }

}

void TrodesClickableLabel::showToolTip() {
    //QToolTip::showText(this->mapToGlobal(pos()), toolTip(),this);
    QToolTip::showText(QCursor::pos(), toolTip(),this);
}



TrodesFont::TrodesFont()
{

    setFamily("Arial");
    //setStyleHint(QFont::TypeWriter);

    setPixelSize(12);

}

TrodesButton::TrodesButton(QWidget *parent)
    :QPushButton(parent) {

    setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::hover {border: 2px solid lightgrey;"
                          "border-radius: 4px;"
                          "background-color:#e7e7e7}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::checked {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton:disabled { color: gray }"
    );


    TrodesFont buttonFont;

    setFont(buttonFont);
    connect(&blinkTimer, SIGNAL(timeout()), this, SLOT(toggleBlink()));
    blink = false;
    blinkColor = QColor("red");
}

void TrodesButton::setRedDown(bool yes) {
    if (yes) {
        setStyleSheet("QPushButton {border: 2px solid lightgrey;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    } else {
        setStyleSheet("QPushButton {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    }
}

void TrodesButton::setblink(bool blink, int msec, QColor color){
    if(blink){
        blinkColor = color;
        blinkTimer.start(msec);
    }
    else{
        blinkTimer.stop();
        blink = false;
        setStyleSheet("QPushButton {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::hover {border: 2px solid lightgrey;"
                              "border-radius: 4px;"
                              "background-color:#e7e7e7}"
                              "QPushButton::pressed {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
        );
    }
}

void TrodesButton::stopBlink(){
    blinkTimer.stop();
    setRedDown(false);
}

void TrodesButton::toggleBlink(){
    if(blink){
        setStyleSheet("QPushButton {border: 2px solid " + blinkColor.name() + ";"
                              "border-radius: 4px;"
                              "padding: 2px;}");
        blink = false;
    }
    else{
        setStyleSheet("QPushButton {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
        blink = true;
    }
}



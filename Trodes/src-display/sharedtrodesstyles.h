#ifndef SHAREDTRODESSTYLES_H
#define SHAREDTRODESSTYLES_H


#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>

class TrodesClickableLabel : public QLabel {
    Q_OBJECT

public:
    explicit TrodesClickableLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

    int ntIndex;

signals:

    void clicked(int ntIndex, Qt::KeyboardModifiers mods);
    void rightClicked(int ntIndex);

public slots:
    void showToolTip();


protected:
    void mousePressEvent(QMouseEvent* event);

};


class TrodesFont : public QFont {

public:
    TrodesFont();
};

class TrodesButton : public QPushButton {

Q_OBJECT

public:
    TrodesButton(QWidget *parent = 0);
    void setRedDown(bool yes);
    void setblink(bool blink, int msec = 500, QColor color = QColor("red"));
    void stopBlink();

private:
    QTimer blinkTimer;
    QColor blinkColor;
    bool blink;
private slots:
    void toggleBlink();
};

#endif // SHAREDTRODESSTYLES_H

# Trodes #


### An open source, cross-platform software suite for neuroscience data acquisition and experimental control. 
 
-----
### SpikeGadgets Website (with screenshots) ###
[http://www.spikegadgets.com/software/trodes.html](http://www.spikegadgets.com/software/trodes.html)

### Development Team ###
http://www.spikegadgets.com/software/dev_team.html

### Online documentation ###
(https://docs.spikegadgets.com/en/latest/)


### Required drivers ###
[FTDI D2XX Driver](http://www.ftdichip.com/Drivers/D2XX.htm)

-----
## Overview ###
Trodes is used to monitor incoming data from hardware used in neuroscience research, with a specific focus on electrophysiology. It is also being developed to allow complex control of behavioral apparatuses and neuro-perterbation devices.  Trodes is made up of "modules", each running as a separate process and serving a specialized purpose. The modules all communicate with a central graphical interface that performs the most crucial tasks associated with visualizing and saving data. With this architecture, the trodes suite is readily extendable without needing detailed understanding of existing code. 


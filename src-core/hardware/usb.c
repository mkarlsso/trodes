#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
    #include <windows.h>
#else
    #include "WinTypes.h"
#endif
#include "ftd2xx.h"

#define VENDOR 0x0403
#define DEVICE 0x6010
static const char desc_old[] = "Rat Backpack 2 A";
static const char desc[] = "Spikegadgets MCU A";
static const char desc2[] = " A";

#include "trodes_usb.h"
#include "common.h"
//#############################
//  Usb
//#############################

struct SpkgUsbConn{
    //Usb
    FT_HANDLE fthandle;
};

SpkgUsbConn* SpkgUsbConn_new(){
    SpkgUsbConn *h = (SpkgUsbConn*)malloc(sizeof(SpkgUsbConn));
    memset(h, 0, sizeof(*h));
    return h;
}

void SpkgUsbConn_delete(SpkgUsbConn* h){
    free(h);
}

int usb_initialize(SpkgUsbConn* h){
    FT_STATUS res;
#ifdef __linux__ 
    // need to manually set VID and PID on linux
    res = FT_SetVIDPID(VENDOR, DEVICE);
    if (res != FT_OK) {
    return res;
    }
#endif

    res = FT_OpenEx((void*)desc, FT_OPEN_BY_DESCRIPTION, &h->fthandle);
    if (res != FT_OK) {
        res = FT_OpenEx((void*)desc_old, FT_OPEN_BY_DESCRIPTION, &h->fthandle);
        if (res != FT_OK) {
            res = FT_OpenEx((void*)desc2, FT_OPEN_BY_DESCRIPTION, &h->fthandle);
            if (res != FT_OK) {
                return res;
            }
        }
    }
    res = FT_ResetDevice(h->fthandle);
    res |= FT_SetUSBParameters(h->fthandle, 65536, 65536);	//Set USB request transfer size
    res |= FT_SetFlowControl(h->fthandle,FT_FLOW_RTS_CTS,0,0);
    res |= FT_SetChars(h->fthandle, 0, 0, 0, 0);	 //Disable event and error characters
    res |= FT_SetBitMode(h->fthandle, 0xff, 0x40);
    res |= FT_Purge(h->fthandle, FT_PURGE_RX | FT_PURGE_TX);
    res |= FT_SetLatencyTimer(h->fthandle, 64);
    res |= FT_SetTimeouts(h->fthandle, 1000, 1000);
    if (res != FT_OK) {
        return res;
    }

    return 0;
}

void usb_close(SpkgUsbConn* h){
    FT_Close(h->fthandle);
}

int usb_startacquisition(SpkgUsbConn* h, int ecu_connected){
    FT_STATUS res;
    char buf[20] = {0};
    if(ecu_connected)
        buf[0] = command_startWithECU;
    else
        buf[0] = command_startNoECU;
    buf[1] = 0xff;
    DWORD byteswritten;
    res = FT_Write(h->fthandle, buf, 2, &byteswritten);
    if(res != FT_OK){
        return res;
    }
    return 0;
}

int usb_startsimulation(SpkgUsbConn *h, int auxbytes, int intancards){
    FT_STATUS res;
    char buf[20] = {0};
    buf[0] = command_startSimulation;
    buf[1] = auxbytes;
    buf[2] = intancards;
    buf[3] = 0xff;
    DWORD byteswritten;
    res = FT_Write(h->fthandle, buf, 4, &byteswritten);
    if(res != FT_OK){
        return res;
    }
    return 0;
}

int usb_stopacquisition(SpkgUsbConn* h){
    FT_STATUS res;
    char buf[20] = {0};
    buf[0] = command_stop;

    DWORD bytes;
    res = FT_Write(h->fthandle, buf, 1, &bytes);
    if(res != FT_OK){
        return res;
    }

    return 0;
}

// int usb_pendingpacket(SpkgUsbConn* h, unsigned int packetsize){
//     FT_STATUS res;
//     DWORD bytesinqueue;
//     res = FT_GetQueueStatus(h->fthandle, &bytesinqueue);
//     if(res != FT_OK){
//         return 0;
//     }
//     if(bytesinqueue >= packetsize){
//         return 1;
//     }
//     else{
//         return 0;
//     }
//     return 0;
// }

int usb_recvpacket(SpkgUsbConn* h, void* buffer, unsigned int expectedlen){
    FT_STATUS res;
    DWORD bytesReceived;
    res = FT_Read(h->fthandle, buffer, expectedlen, &bytesReceived);
    if(res != FT_OK){
        return -res;
    }
    if(bytesReceived != expectedlen){
        return bytesReceived;
    }
    else{
        return expectedlen;
    }
}

int usb_getMCUSettings(SpkgUsbConn* h, MCUSettings* settings){
    //TODO: check if acquiring
    
    FT_STATUS res;
    char buffer[MCUSETTINGSSIZE] = {0};
    buffer[0] = command_getControllerSettings;
    //First clear anything in the receive queue
    DWORD BytesReceived;
    DWORD RxBytesAvailable;
    DWORD TxBytesAvailable;
    DWORD Event;
    res = FT_GetStatus(h->fthandle, &RxBytesAvailable, &TxBytesAvailable, &Event);
    if (res == FT_OK && RxBytesAvailable > 0) {
        char *tempbuf = (char*)malloc(RxBytesAvailable);
        res = FT_Read(h->fthandle, tempbuf,RxBytesAvailable,&BytesReceived);
        free(tempbuf);
    }

    //Send command for MCU settings
    DWORD BytesWritten;
    res = FT_Write(h->fthandle, buffer, 1, &BytesWritten);
    if (res != FT_OK){
        return res;
    }

    //Read for MCU settings. FT_Read is blocking with timeout. 3 tries max. 
    DWORD totalBytes = 0, tries = 0;
    do{
        res = FT_Read(h->fthandle, buffer+totalBytes, MCUSETTINGSSIZE-totalBytes, &BytesReceived);
        totalBytes += BytesReceived;
        ++tries;
    }while(totalBytes < MCUSETTINGSSIZE && tries < 3);

    if(res != FT_OK){
        return -1;
    }

    //If not MCUSETTINGSSIZE bytes read in
    if(totalBytes != MCUSETTINGSSIZE){
        return -2;
    }

    if(settings == NULL){
        return 0;
    }

    parseMCUSettings(buffer, settings);

    return 0;
}

int usb_getHSSettings(SpkgUsbConn* h, HSSettings* settings){
    FT_STATUS res;
    char buffer[HSSETTINGSSIZE] = {0};
    buffer[0] = command_getHeadstageSettings;
    //First clear anything in the receive queue
    DWORD BytesReceived;
    DWORD RxBytesAvailable;
    DWORD TxBytesAvailable;
    DWORD Event;
    res = FT_GetStatus(h->fthandle, &RxBytesAvailable, &TxBytesAvailable, &Event);
    if (res == FT_OK && RxBytesAvailable > 0) {
        char *tempbuf = (char*)malloc(RxBytesAvailable);
        res = FT_Read(h->fthandle, tempbuf,RxBytesAvailable,&BytesReceived);
        free(tempbuf);
    }

    //Send command for HS settings
    DWORD BytesWritten;
    res = FT_Write(h->fthandle, buffer, 1, &BytesWritten);
    if (res != FT_OK){
        return res;
    }

    //Read for HS settings. FT_Read is blocking with timeout. 3 tries max. 
    DWORD totalBytes = 0, tries = 0;
    do{
        res = FT_Read(h->fthandle, buffer+totalBytes, HSSETTINGSSIZE-totalBytes, &BytesReceived);
        totalBytes += BytesReceived;
        ++tries;
    }while(totalBytes < HSSETTINGSSIZE && tries < 3);

    if(res != FT_OK){
        return -1;
    }

    //If not HSSETTINGSSIZE bytes read in
    if(totalBytes != HSSETTINGSSIZE){
        return -2;
    }

    if(settings == NULL){
        return 0;
    }

    parseHSSettings(buffer, settings);
    
    return 0;
}

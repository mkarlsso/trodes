module Trodes.Network.Types.HWStimulationCommand where

import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data HWStimulationCommand = HWStimulationCommand
    { _group :: Word8
    , slot :: Word8
    , cathodeChannel :: Word16
    , anodeChannel :: Word16
    , cathodeNtrodeID :: Word16
    , anodeNtrodeID :: Word16

    , leadingPulseWidth_Samples :: Word16
    , leadingPulseAmplitude :: Word8

    , secondPulseWidth_Samples :: Word16
    , secondPulseAmplitude :: Word8

    , interPhaseDwell_Samples :: Word16
    , pulsePeriod_Samples :: Word16
    , startDelay_Samples :: Word16
    , numPulsesInTrain :: Word16
    } deriving (Generic, Cpp, CppType)


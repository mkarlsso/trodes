module Trodes.Network.Types.HWTrigger where

import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data HWTrigger = HWTrigger
    { fn :: Word16
    } deriving (Generic, Cpp, CppType)

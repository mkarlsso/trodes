{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
module Trodes.Network.Cpp.Generic where

import Data.Kind (Type)
import Data.Int
import Data.Text as T
import Data.Word
import GHC.Generics as Generics
import Trodes.Network.Cpp.Ast

class Cpp (t :: Type) where
    toCppAst :: CppDefinition

    default toCppAst :: (Generic t, GenericCppDefinition (Rep t)) => CppDefinition
    toCppAst = genericToCppDefinition $ from (error "evaluated" :: t)

class CppType (t :: Type) where
    cppRef :: TypeRef

    default cppRef :: (Cpp t) => TypeRef
    cppRef = RefCustom $ TypeName $ cppName $ toCppAst @t

-- Primitive instances

instance CppType Bool where cppRef = RefPrim CppBool
instance CppType Word8 where cppRef = RefPrim CppUint8
instance CppType Word16 where cppRef = RefPrim CppUint16
instance CppType Word32 where cppRef = RefPrim CppUint32
instance CppType Word64 where cppRef = RefPrim CppUint64
instance CppType Int8 where cppRef = RefPrim CppInt8
instance CppType Int16 where cppRef = RefPrim CppInt16
instance CppType Int32 where cppRef = RefPrim CppInt32
instance CppType Int64 where cppRef = RefPrim CppInt64
instance CppType Float where cppRef = RefPrim CppFloat
instance CppType Double where cppRef = RefPrim CppDouble
instance CppType Text where cppRef = RefPrim CppString

instance CppType a => CppType [a] where
    cppRef = RefPrim $ CppVector (cppRef @a)

-- helper
class GenericCppDefinition (f :: k -> Type) where
    genericToCppDefinition :: f p -> CppDefinition

instance (Datatype d, GenericCppConstructors f) => GenericCppDefinition (D1 d f) where
    genericToCppDefinition datatype = CppDefinition name constructors
        where
            name = T.pack $ datatypeName datatype
            constructors = genericCppConstructors (unM1 datatype)


-- constructors (handle :+: and single)

class GenericCppConstructors (f :: k -> Type) where
    genericCppConstructors :: f p -> [CppConstructor]

-- | Sum types, create one of each and evaluate
instance (GenericCppConstructors f, GenericCppConstructors g) => GenericCppConstructors (f :+: g) where
    genericCppConstructors _ =
        genericCppConstructors (error "'f :+:' is evaluated" :: f p)
     <> genericCppConstructors (error "':+: g' is evaluated" :: g p)

-- | Single constructor
-- | Handles augmenting all fields
instance (Constructor c, GenericCppFields f) => GenericCppConstructors (C1 c f) where
    genericCppConstructors constructor = [CppConstructor name fields'']
        where
            name = T.pack $ conName constructor
            fields = genericCppFields (unM1 constructor)
            fields' = Prelude.zipWith (\f vn -> f { cppRecordFieldName = T.pack [vn] } ) fields ("abcdefghijklmnop" :: [Char])
            fields'' = if Prelude.any (\x -> cppRecordFieldName x == "") fields
                            then fields'
                            else fields

-- fields (handle :*: and single)

class GenericCppFields (f :: k -> Type) where
    genericCppFields :: f p -> [CppRecordField]

instance (GenericCppFields f, GenericCppFields g) => GenericCppFields (f :*: g) where
    genericCppFields _ =
        genericCppFields (error "'f :*:' is evaluated" :: f p)
     <> genericCppFields (error "':*: g' is evaluated" :: g p)

instance GenericCppFields U1 where
    genericCppFields _ = []

instance (Selector s, CppType a) => GenericCppFields (S1 s (Rec0 a)) where
    genericCppFields selector = [CppRecordField type' name]
        where
            name = T.pack $ selName selector :: Text
            type' = cppRef @a :: TypeRef
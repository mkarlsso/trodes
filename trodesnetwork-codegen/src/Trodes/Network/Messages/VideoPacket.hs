module Trodes.Network.Messages.VideoPacket where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data VideoPacket = VideoPacket
    { timestamp :: Word32
    , lineSegment :: Int32
    , posOnSegment :: Double
    , x :: Int16
    , y :: Int16
    , x2 :: Int16
    , y2 :: Int16
    } deriving (Generic, Cpp)

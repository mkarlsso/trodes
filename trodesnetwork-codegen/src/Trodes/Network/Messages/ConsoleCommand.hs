module Trodes.Network.Messages.ConsoleCommand where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data DataCommand = DataCommand
    { command :: Text
    } deriving (Generic, Cpp)

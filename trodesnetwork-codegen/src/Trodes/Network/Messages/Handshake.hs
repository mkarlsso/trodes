module Trodes.Network.Messages.Handshake where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data Handshake = Handshake
    { replyEndpoint :: Text
    , mailEndpoint :: Text
    , message :: Text
    } deriving (Generic, Cpp)

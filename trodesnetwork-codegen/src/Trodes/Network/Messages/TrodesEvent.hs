module Trodes.Network.Messages.TrodesEvent where

import Data.Text (Text)
import Data.Word
import Data.Int
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesEvent = TrodesEvent
    { name :: Text
    , localTimestamp :: Word32
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)

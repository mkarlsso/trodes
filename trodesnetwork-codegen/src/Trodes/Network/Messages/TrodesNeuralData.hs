module Trodes.Network.Messages.TrodesNeuralData where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesNeuralData = TrodesNeuralData
    { localTimestamp :: Word32
    , neuralData :: [Int16]
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)

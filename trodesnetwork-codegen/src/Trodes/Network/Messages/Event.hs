module Trodes.Network.Messages.Event where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data Event = Event
    { name :: Text
    } deriving (Generic, Cpp)

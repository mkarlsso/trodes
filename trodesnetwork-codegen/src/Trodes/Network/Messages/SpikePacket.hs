module Trodes.Network.Messages.SpikePacket where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data SpikePacket = SpikePacket
    { localTimestamp :: Word32
    , nTrodeId :: Int32
    , cluster :: Int32
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)

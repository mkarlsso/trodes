module Trodes.Network.Messages.AnnotationRequest where

import Data.Text (Text)
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data AnnotationRequest = AnnotationRequest
    { timestamp :: Word32
    , sender :: Text
    , event :: Text
    } deriving (Generic, Cpp)

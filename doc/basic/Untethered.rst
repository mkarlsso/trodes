Untethered Recordings Using Datalogging Headstages
===================================================

Overview
--------

Many SpikeGadgets headstages have the ability to record neural data in both 
tethered and untethered modes. This means a single system can be used for both 
standard recordings using a wire tether as well as completely wire-free recordings, enabling 
a wide range of experiments that could not be accomplished with a wire tether.     

During tethered recording, the headstage is connected to the MCU via HDMI tether. 
The tether powers and controls the headstage, streaming data back to the computer 
via a control unit. The neural and sensor data from the headstage is combined with 
any video timestamp data or signals recorded using the digital or analog 
inputs and outputs and saved to file. 

When recording in untethered mode, the headstage acts as a self-contained acquisition 
system operating under battery power; saving neural and sensor data to SD storage mounted 
to the headstage. This means that any data about the environment (lever presses, beam breaks, 
video, lights, sounds) must be recorded separately by the control unit. These two data streams 
must then be aligned and merged to create a single data file. SpikeGadgets systems use radio 
frequency (RF) synchronization signals to align data for later merging. Here we will cover how 
this works, and how the Trodes toolbox performs this critical data alignment operation. 


Synchronization
---------------

Synchronization between the environmental data recorded by Trodes and
the neural data recorded on the headstage relies on a radio frequency (RF)
synchronization signal that is sent to the headstage at regular intervals 
(most setups use a 10-second interval). 

This RF signal is sent from the control unit that acquires the environmental 
data and connect to your computer. This can be either the Main Control Unit or 
the Logger Dock. The RF signal is recorded by both the control unit and the headstage 
and is captured with high precision as the headstage and control unit are both set 
to record using the same sampling rate used to capture neural data (20KHz or 30KHz). 
This is all controlled using Trodes, which connects to the control unit and saves environmental 
data to the local computer.

**NOTE:** Environmental data can be viewed in Trodes in real-time while recording, but neural 
data recorded to the headstage cannot be viewed or accessed during recording. 

Once the recording session has ended, you will have 2 files: one containing 
neural data recorded to the headstage SD card, and one containing the 
environmental record recorded to your local computer. These files will be aligned 
using the RF synchronization signals saved to both data files.

Video data captured using the Camera Module in Trodes can also be aligned to the neural and environmental data. 
The Camera Module which records the video frame timestamps using the same 
sampling rate as the neural and environmental data. Camera Module setup for untethered 
recording is the same as it is when recording tethered. More information about Camera Module 
setup can be found `here <https://docs.spikegadgets.com/en/latest/basic/CameraModule.html>`_.


Recording
---------
During an untethered session, where neural data and environmental data are recorded 
by different devices, the workspace used for recording must contain only channels for 
environmental data. This is done by creating a Workspace in Trodes with channel count 
set to zero. 

The headstage and Control Unit must also be set to the same RF channel and sampling 
rate, and session ID mode should be turned on for both. This will ensure that the headstage can 
be properly controlled by the MCU/Logger Dock; receiving Start/Stop commands and RF sync 
signals throughout the recording session.

If you are also using an Environmental Control Unit (ECU) when recording, this must be added to 
your hardware devices when creating your zero-channel Workspace in the Workspace Editor. 


Untethered Recording Video Tutorials
------------------------------------
The following video tutorials cover untethered recording from setup to data extraction using the HH128 as an example.


.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/C3p8oHuBi04?si=XR0ckmrMGhKDYKjL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/q1JtLQjuaZE?si=VWFodlS8i497V93C" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/VG2AwsxS7Fk?si=U9MXc7PyZWmkwBhD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>



Setting up an Untethered Data Logging Experiment:
-------------------------------------------------
These steps provide an overview of basic setup for untethered recording. User manuals containing Headstage-specific setup instructions can be found on the `SpikeGadgets documentation page <https://spikegadgets.com/documentation/>`_.

1. Open Trodes and either create a new workspace “From Scratch” under the Create/Edit Workspace menu or open the workspace you have already created.

2. Connect your headstage to the MCU via HDMI, and link your MCU to Trodes using the "Connection" dropdown menu by selecting: Source > SpikeGadgets > USB OR Ethernet. 
  - Your SD card should not be inserted into the headstage yet.

3. Under the "Settings" dropdown menus, set the MCU and headstage RF channel and sampling rate to the same values and select the session ID mode checkbox for both:

  MCU: Settings > MCU Settings…

  Headstage: Settings > Headstage Settings…

4. Apply your settings above, then save your Workspace. This Workspace can be used later as the basis for the Workspace used to merge your neural and environmental data.


Initiating an Untethered Recording:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. Disconnect your headstage from the MCU, connect the headstage to battery power, insert your enabled SD card into the headstage and power the headstage on.

  - For more information about enabling your SD card, please see the `SD Card Enable <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html#sd-card-enable>`_ 
    subsection of the `DataLoggerGUI Guide <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html#>`_. 

2. Open Trodes and create a new Workspace with Amplifier/probe set to None. This workspace will be used to acquire the environmental record.

3. If you are using an Environmental Control Unit (ECU), make sure this is connected to your MCU via HDMI. Your ECU must also be added to your 
  Workspace by selecting "ECU" from the Hardware Devices dropdown menu, then clicking "+Add Device."

4. Connect your RF transceiver to the Aux 1 port on the front of the MCU.

5. Open your workspace, connect to your MCU, and set your RF channel and sampling rate to match the settings you previously applied to your headstage 
   settings, also making sure Session ID mode is checked.

6. When you are ready to begin recording, select "Stream from source" from the Connections menu. This will trigger the headstage to begin recording.

7. To initiate recording with the MCU, select "New Recording..." from the File dropdown menu and select your recording directory.

8. Hit the red "record" button to begin recording your environmental record with the MCU.

9. To end your recording, hit the pause button, and disconnect your stream.

**IMPORTANT NOTE:** The headstage will start recording when you begin streaming with the MCU, but the MCU will NOT start 
recording until you create a new recording file and hit the record button in Trodes. The same holds true for ending 
your recording; the MCU recording can be ended or paused using the interface buttons, but the headstage recording is 
ended by disconnecting your stream.

When using the Logger Dock to record, the Logger Dock RF channel and Session ID mode settings are configured by opening 
the DataLoggerGUI, selecting the Logger Dock entry that will appear under the "Detected Storage Devices" menu, then 
clicking the "Edit dock settings" button to the upper right. You will find the RF channel and session ID settings 
in this pop-up menu.


Transferring Data to Computer and Merging Files
-----------------------------------------------
Once your untethered recording session has ended, the data recorded by both systems will need to be merged 
before further processing and analysis will be possible. Three files are required for merging the data: 

1. The neural data file recorded to SD 
2. The environmental data file recorded to your computer
3. A Workspace file to be appended to the merged data. 

The Workspace file required should contain all the settings that would have been used if the recording 
had been taken in tethered mode. This Workspace will include accurate channel count and nTrodes mapping 
from your headstage AND any settings relevant for recording the environmental record, such as the ECU 
being added to your Workspace Hardware Device list. Once you have these files, the data can be merged 
using the Data Logger GUI application that can be found in the Trodes directory. More information about 
the merging process and Data Logger GUI can be found `here <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html>`_.
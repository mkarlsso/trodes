Exporting Data
==============

Export Utility Functions
~~~~~~~~~~~~~~~~~~~~~~~~

The trodesexport utility extracts specific data from the .rec files and outputs
them to separate ``.dat`` binary files. This function is detailed
:doc:`here </basic/ExportFunctions>`.

Once this export has been preformed, the resulting ``.dat`` files can then be imported into Matlab or Python to be used for further analysis:

Importing .rec Data Into Matlab or Octave
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the Resources > TrodesToMatlab folder, there is a Matlab function called 
"readTrodesExtractedDataFile.m" that will import any of the generated ``.dat`` files 
into a common data structure. You will need to pass the ``.dat`` filename complete with 
the path as a string to the function. 

In Matlab or Octave, the function *readTrodesExtractedDataFile* is used to read the 
``.dat`` files that result from using the trodesexport utility. The exact contents of 
the file depends on the type of data that was exported. In this example below, we are 
importing a file with spike snippets (spikes processor).

|

``imported_data = readTrodesExtractedDataFile('example_recording.spikes_nt2.dat');``


Create a structure containing the following fields:

    - description: 1x30 sq_string
    - byte_order: 1x13 sq_string
    - original_file: 1x21 sq_string
    - clockrate: 1x1 scalar
    - trodes_version: 1x5 sq_string
    - compile_date: 1x11 sq_string
    - compile_time: 1x8 sq_string
    - qt_version: 1x5 sq_string
    - commit_tag: 1x26 sq_string
    - controller_firmware: 1x1 scalar
    - headstage_firmware: 1x1 scalar
    - controller_serialnum: 1x1 scalar
    - headstage_serialnum: 1x1 scalar
    - autosettle: 1x1 scalar
    - smartref: 1x1 scalar
    - gyro: 1x1 scalar
    - accelerometer: 1x1 scalar
    - magnetometer: 1x1 scalar
    - time_offset: 1x1 scalar
    - system_time_at_creation: 1x1 scalar
    - timestamp_at_creation: 1x1 scalar
    - first_timestamp: 1x1 scalar
    - ntrode_id: 1x1 scalar
    - num_channels: 1x1 scalar
    - voltage_scaling: 1x1 scalar
    - threshold: 1x1 scalar
    - spike_invert: 1x3 sq_string
    - reference: 1x3 sq_string
    - filter: 1x2 sq_string
    - lowpassfilter: 1x1 scalar
    - highpassfilter: 1x1 scalar
    - spike_trigger_on_1: 1x1 scalar
    - fields: 1x2 struct

Most of these fields will exist for all of the exported data types, but some 
are type specific. In this case, 'threshold' is specific to spike trigger events, 
for example.

The data are always stored in 'fields':

|

``imported_data.fields``

ans =

  1x2 struct array containing the fields:

    name

    type

    columns

    bytesPerItem

    data

imported_data.fields(1).name is 'time' and imported_data.fields(2).name is 'waveformCh1'. 
If the nTrode has more than one channel, the array will have one extra entry per channel. 
Similarly, imported_data.fields(1).data contains each spike's time, and imported_data.fields(2).data 
contains each spikes' waveform data. 

**Note: the data type is given in the 'type' field. To save memory, this is usually not 
'double', which will create problems for some Matlab functions.** 

This can be resolved by converting the desired data to double: 
``myDoubleVar = double(originalVar);``

Similar structures will be created for all other data types.

Within the TrodesTo Matlab folder you will also find *readCameraModuleTimeStamps*, which can be used to extract Camera Module timestamps:

**readCameraModuleTimeStamps.m:**
- Reads in a single Camera Module timestamp file (``.videoTimeStamps``) exported from Trodes.
- Returns a numpy array containing the Camere Module video timestamps.


Importing .rec Data Into Python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once data has been exported from Trodes, it can be imported into Python for secondary 
processing and analysis using scripts provided in the Resources > TrodesToPython 
folder contained within the Trodes installation folder. The scripts contained within 
will enable you to import your recording binary data and camera module video timestamps into 
Python for further evaluation and/or analysis. 

**readTrodesExtractedDataFile3:**
- Reads in a single binary data (``.dat``) file exported from Trodes using the export utility. 
- Returns a Python dict containing the file metatdata as well as a Numpy array containing the binary data stored in the ‘data’ field.
- Some Camera Module outputs such as position data also create a binary file which can be parsed using this function. 

**NOTE: numpy installation is required**

For Python 2 support, use readTrodesExtractedDataFile2 contained within the same folder.

Information about about the data and data type(s) contained within 'data' can be found in the 'fields' dict key entry. 
::

   fields: <time uint32><LineSegment uint16><RelativeLinearPos double>

In this example, 'fields' indicates that each position in the 'data' array contains time, line segment and relative linear position data as seen below. 
::
   >importedData['data']
   [(160157682, 0,  0.23706176), (160159750, 0,  0.26997685),
   (160161938, 0,  0.34283835), (160165086, 0,  0.42896333),
   ...
   (160168038, 0,  0.492976), (160170927, 0,  0.55913753)]

Some camera module outputs such as position data also create a binary file. These files can also be parsed with this function.

**readCameraModuleTimeStamps:**
- Reads in a single Camera Module timestamp file (``.videoTimeStamps``) exported from Trodes.
- Returns a numpy array containing the Camere Module video timestamps.

Exporting Camera Module’s Data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Camera Module is able to stream and export multiple different types of data including
animal position data, an animal’s linear position within a defined track geometry, the track 
geometry itself, and user defined zone data. Refer to Camera Module’s 
:ref:`exporting section here <camera-module-exporting-data>` for more details.





Neuropixels Headstage
=====================

The SpikeGadgets Neuropixels headstage is designed to optimize the power and density of Imec’s Neuropixels 1.0 probes 
with the power and modularity of SpikeGadgets’ products. The headstage records neural signals from up to three 
Standard (rodent) Neuropixels 1.0 probes simultaneously, with 2880 selectable neural channels. 

The headstage can operate in 2 modes: 
1) tethered live streaming mode, where up to 1152 of the available 2880 
channels across three probes can be selected and livestreamed to your computer using the Main Control Unit (MCU), or 
2) untethered data-logging mode, where up to 400 channels across the three probes can be selected and the data is recorded 
locally to a microSD card on the headstage.

The Neuropixels headstage can be used with both the SpikeGadgets MCU as well as the Logger Dock, and requires 
the `Trodes application suite <https://spikegadgets.com/trodes/>`_, which can be downloaded `here <https://bitbucket.org/mkarlsso/trodes/downloads/>`_.  
See our Trodes wiki for more information about `Installing Trodes <https://docs.spikegadgets.com/en/latest/basic/Install.html>`_.

For detailed information about recording with the Neuropixels headstage, please see the product user manual, which 
can be downloaded from the SpikeGadgets Documentation page `here <https://spikegadgets.com/documentation/>`_.

Please also familiarize yourself with the Neuropixels user manual before recording with this headstage. 
The manual can be found `here <https://e2f49b05-3010-45e2-a7bf-b8f67811425a.filesusr.com/ugd/832f20_ce2feed52995400a9b77f79ca92b019b.pdf>`_. 

For additional information about Neuropixels Probes and recording with them, please see the UCL Neuropixels training 
course `here <https://www.ucl.ac.uk/neuropixels/training/2021-neuropixels-course>`_.


**Important Note:**
Trodes 2.5.0 or later must be used when analyzing Neuropixels LFP data where only a subset of the allowable 384 channels 
per probe have been selected. A bug exists in previous versions of the Trodes export utility that causes channel IDs to 
be scrambled for Neuropixels LFP data when fewer than 384 channels have been selected per probe. The latest version of 
Trodes can be downloaded `here <https://bitbucket.org/mkarlsso/trodes/downloads/>`_.  


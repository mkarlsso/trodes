Untethered Data Synchronization
===============================

Overview
--------

When taking an untethered recording using SpikeGadgets Data Logging Headstages, neural data is 
recorded directly to the headstage's onboard SD card, while the record of environmental events is 
recorded by the main Control Unit (MCU) and saved to your computer. Following the conclusion of a recording 
session these recordings must be merged in order for neural activity to be correlated to environmental data. 
Furthermore, this must be done with extremely high temporal accuracy, and consistency. Below is an overview of 
how SpikeGadgets systems maintain this precision during untethered recordings.

**NOTE:** Control unit (MCU and Logger Dock) communication with untethered headstages is unidirectional. The control unit transmits 
commands to the headstage, but the headstage does not send signals/feedback to the control unit. 


Radio-frequency (RF) Sync Pulses
--------------------------------
When starting or stopping an untethered recording the SpikeGadgets control unit uses a RF transceiver to transmit a Start/Stop
command to the headstage. When the headstage receives the command it starts or stops recording neural data to the onboard SD 
card at the sampling rate specified by the user. The RF signal operates with microsecond precision ensuring the control unit and headstage 
start and stop times are functionally simultaneous. The start and stop commands flag the current data sample in the recorded data 
on both the local computer and the headstage. 

During the recording, the control unit transmits RF sync pulses to ensure that data synchronization can be maintained throughout the 
entire session. For most setups these RF sync pulses occur at a 10-second interval. The RF sync pulses and Start/Stop commands are 
used to maintain data synchronization when dropped packets and/or drift occurs.


Dropped Packets
---------------
In most situations where large volumes of data are being transferred within a device, or streamed between devices, data is transferred in 
packets. When these packets are transferred, it is not uncommon for a small number of packets to be lost, or "dropped". This 
can be minimized or eliminated by using the appropriate hardware setup, but can still occur in most electronic systems. In this case, the 
packets being discussed are recording data and each packet contains header data and 1 sample from each channel being recorded. When 
synchronization between multiple electronic systems is important, dropped packets must be accounted for, as a missing packet in one record 
will cause a mismatch in packet/sample count between systems. When a mismatch does occur due, synchronization is achieved by adding or 
removing packets to realign the data. 


Importantly, SpikeGadgets systems give preference to neural data. This means that packets are never added or subtracted from the neural data, and 
all packet-level adjustments are made to the environmental record:


- If a packet is dropped from the neural recording, a packet will be removed from the same position in the environmental record. 

- If a packet is dropped from the environmental record, the missing packet will be replaced with a duplicate of the directly preceding packet from the environmental record.


SpikeGadgets systems detect and flag dropped packets. This ensures that packet adjustments occur in the correct position. This is a crucial 
aspect of maintaining accurate synchronization.


Drift
-----
Most electronic devices rely on an internal clock to operate, and these clocks are often used when multi-device synchronization is necessary in the 
absence of a wired connection. One problem that occurs when synchronizing devices using their internal clocks is drift. Drift is caused by small 
differences in clock timing, and can result in desynchronization that occurs slowly over time due to very small differences in sampling rate. One 
way to address this is to make small adjustments over time. This is done by adding or subtracting data packets systematically to realign the 
recordings being synchronized.

SpikeGadgets systems use the RF sync pulses to facilitate this. For most setups the sync period between RF sync pulses is 10 seconds. Because the RF sync 
pulses occur simultaneously on the headstage and control unit, the number of packets between RF sync pulses for each recording can be compared, and small 
adjustments made when necessary. If a discrepancy in sample count is identified, environmental record samples can either be added or subtracted at the end of a given 
sync period to realign the data. It is normal for a few dozen packets/samples to be added to or subtracted during a 10-second period, amounting to 1 or 2 milliseconds 
of drift correction. However, if drift on the order to thousands of of samples is observed, this indicated a larger problem that needs to be addressed.
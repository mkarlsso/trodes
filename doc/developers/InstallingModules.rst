Installing Modules
==================

All modules first require Trodes to be compiled, follow :doc:`Trodes
Install </basic/Install>` instructions.

Camera Module
-------------

Camera module processes video (USB/Vision GigE) to save and reconstruct
an animal's position.

Required packages (Ubuntu 14.04):

-  libavcodec-dev
-  libavformat-dev
-  libswscale-dev
-  libx264-dev
-  libbz2-dev

Required for Vision GigE (file is in trodes Git repository):

-  sudo cp
   Modules/cameraModule/libraries/linux/GigESDK/bin-pc/libPvAPI.so
   /usr/local/lib

-  

   -  Deprecated, only if you want to try Aravis (open source VisionGigE
      library):

-  intltool
-  libglib-dev
-  libxml2-dev
-  libgstreamer1.0-dev
-  libgstreamer-plugin-base1.0-dev
-  libgtk-3-dev
-  libnotify-dev]
